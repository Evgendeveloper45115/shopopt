.color_th{color:<?=$this->dataModel['color_site']?>!important;}
.color_th_hover:hover{color:<?=$this->dataModel['color_site']?>!important;}
.bg_color_th{background-color:<?=$this->dataModel['color_site']?>!important;}
.bg_color_th_hover:hover{background-color:<?=$this->dataModel['color_site']?>!important;}
.svg_fill_color{fill:<?=$this->dataModel['color_site']?>!important;}
.svg_stroke_color{stroke:<?=$this->dataModel['color_site']?>!important;}
.svg_stroke_color_hover:hover{stroke:<?=$this->dataModel['color_site']?>!important;}
.svg_fill_color_hover:hover{fill:<?=$this->dataModel['color_site']?>!important;}
.border_hover:hover{border-color:<?=$this->dataModel['color_site']?>!important;}
.border_color{border-color:<?=$this->dataModel['color_site']?>!important;}
.focus{border:1px solid <?=$this->dataModel['color_site']?>;box-shadow: 0px 1px 15px 0px <?=$this->dataModel['color_site']?>;}
.shadow_hover:hover{box-shadow: 0px 1px 15px 0px <?=$this->dataModel['color_site']?>;}