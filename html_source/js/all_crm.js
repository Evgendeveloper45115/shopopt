$(function(){

	$('#cd_order_status').click(function(e){
		e.preventDefault();
		if($(this).hasClass('active')){
			$(this).removeClass('active');
			$('#hid_status').animate({'opacity':0},600,function(){
				$(this).hide();
			});
		}else{
			$(this).addClass('active');
			$('#hid_status').css({'display':'block','opacity':0,'top':$(this).position().top+$(this).outerHeight(true),'left':(($(this).outerWidth(true)/2)+$(this).position().left)-$('#hid_status').width()/2}).animate({'opacity':1},600);
		}
	});

	$('.hid_cd_status p').click(function(){
		$(this).siblings('input').click();
		document.cd_status.submit();
	});

	function creat_content(flag_input,arr_input){
		if(flag_input){
			work_arr = arr_input;
		}else{
			work_arr = JSON.parse(content_item);
		}

		all_item = '<div class="msg_padding manager_catalog_all"><input id="add_item_finder" placeholder="Поиск"><br>';
		if(!work_arr.length>0){
			all_item +='<p class="oblom">Нет результатов поиска</p>';
		}
		for(var i=0; i<work_arr.length; i++) {
			count = 0;
			$('#tovari_tut').find('.goods_item').each(function(){
				if($(this).attr('data-id')==work_arr[i]['id']){
					count++;
				}
			});
			if(count>0){
				button_text = 'Уже добавлен';
				button_class = 'in_list';
			}else{
				button_text = 'Добавить';
				button_class = '';
			}

			tpl_item = '<div class="goods_item css_trans shadow_hover"><div class="img_fon" data-img="'+work_arr[i]['img']+'"" style="background-image:url('+work_arr[i]['img']+')"></div><p>'+work_arr[i]['title']+'</p><div class="price inline_style color_th"><span class="money">'+work_arr[i]['price']+'</span><span class="rub_lt">a</span></div><p class="'+button_class+' add_from_lt border_hover css_trans color_th_hover" data-id="'+work_arr[i]['id']+'">'+button_text+'</p></div>';
			all_item += tpl_item;
		}
		all_item +='</div>';
		creat_lb_sv(all_item,$('#align_zone').outerWidth(true));
	}

	$('.goods_add').click(function(e){
		e.preventDefault();
		creat_content(false);

	});

	function get_goods(value){
		str = 'type=get_goods&value='+value;
			$.ajax({
				type:'post',
				url:'/site/Rectovar',
				data:str,
				success:function(msg){
					arr_content = JSON.parse(msg);
					creat_content(true,arr_content);
				}
			});
	}

	$(document).on('blur','#add_item_finder',function(){
		if($(this).val()!=''){
			get_goods($(this).val());

		}
	});	
	$(document).on('keydown','#add_item_finder',function(e){
		if(e.which == 13){
			if($(this).val()!=''){
				get_goods($(this).val());
			}
		}
		
	});

	$(document).on('click','.add_from_lt',function(){
		if(!$(this).hasClass('in_list')){
			id = $(this).attr('data-id');
			img_ = $(this).siblings('.img_fon').attr('data-img');
			title = $(this).siblings('p').text();
			price = $(this).siblings('.price').children('.money').text();
			new_item = '<div class="goods_item css_trans shadow_hover" data-id="'+id+'"><a href="#"><div class="img_fon" style="background-image:url('+img_+')"></div><p>'+title+'</p></a><div class="price inline_style color_th"><span class="money">'+price+'</span><span class="rub_lt">a</span></div><button class="border_hover css_trans color_th_hover" data-id="'+id+'"><svg class="svg_stroke_color" preserveAspectRatio="xMidYMid meet" viewBox="0 0 194 194" xmlns="http://www.w3.org/2000/svg"><g display="inline"><line id="svg_7" y2="2.500002" x2="191.699994" y1="190.900002" x1="2.499994" stroke-linecap="null" stroke-linejoin="null"></line><line id="svg_6" y2="191.700002" x2="191.699994" y1="2.500002" x1="2.499994" stroke-linecap="null" stroke-linejoin="null"></line></g></svg> Удалить</button></div>';
			$('#tovari_tut').append(new_item);
			func_find_fixed_row();
			$(this).addClass('in_list').text('Уже добавлен');
		}
	});

	function creat_akz_content(flag_akz,input_val){
		if(flag_akz){
			work_arr = input_val;
		}else{
			work_arr = JSON.parse(contet_akz);
		}

		all_item = '<div class="msg_padding manager_catalog_all"><input id="add_item_finder2" placeholder="Поиск"><br>';
		if(!work_arr.length>0){
			all_item +='<p class="oblom">Нет результатов поиска</p>';
		}
		for(var i=0; i<work_arr.length; i++) {
			count = 0;
			$('#akz_full_wrp').find('.akz_special_item').each(function(){
				if($(this).attr('data-id')==work_arr[i]['id']){
					count++;
				}
			});
			if(count>0){
				button_text = 'Уже добавлена';
				button_class = 'in_list';
			}else{
				button_text = 'Добавить';
				button_class = '';
			}
			tpl_item = '<div class="akz_special_item" data-id="'+work_arr[i]['id']+'"><div class="img_fon"  data-img = "'+work_arr[i]['img']+'"style="background-image:url('+work_arr[i]['img']+')"></div><div class="demo_akz_txt_cont"><p class="demo_akz_title">'+work_arr[i]['title']+'</p><div class="descr"><p>'+work_arr[i]['descr']+'</p></div><div class="hid_content">'+work_arr[i]['content']+'</div><button class="border_hover css_trans color_th_hover add_akz_to_ '+button_class+'" data-id="'+work_arr[i]['id']+'">'+button_text+'</button></div></div>';
			all_item += tpl_item;
		}
		all_item +='</div>';
		creat_lb_sv(all_item,$('#align_zone').outerWidth(true));
	}

	$(document).on('click','.akz_add',function(e){
		e.preventDefault();
		creat_akz_content(false);

	});

	function get_akz(value){
		str = 'type=get_akz&value='+value;
			$.ajax({
				type:'post',
				url:'/site/RecStock',
				data:str,
				success:function(msg){
					arr_content = JSON.parse(msg);
					creat_akz_content(true,arr_content);
				}
			});
	}

	$(document).on('blur','#add_item_finder2',function(){
		if($(this).val()!=''){
			get_akz($(this).val());

		}
	});	
	$(document).on('keydown','#add_item_finder2',function(e){
		if(e.which == 13){
			if($(this).val()!=''){
				get_akz($(this).val());
			}
		}
		
	});

	$(document).on('click','.add_akz_to_',function(){

		if(!$(this).hasClass('in_list')){
			id = $(this).attr('data-id');
			img_ = $(this).parents('.akz_special_item').children('.img_fon').attr('data-img');
			title = $(this).siblings('.demo_akz_title').text();
			descr = $(this).siblings('.descr').html();
			content = $(this).siblings('.hid_content').html();
			new_akz = '<div class="akz_special_item" data-id="'+id+'"><div class="img_fon" style="background-image:url('+img_+')"></div><div class="demo_akz_txt_cont"><p class="demo_akz_title">'+title+'</p><div>'+descr+'</div><div class="hid_content">'+content+'</div><!--<button class="show_akz_lt border_hover css_trans color_th_hover">Подробнее <span class="color_th">→</span></button>--><button class="border_hover css_trans color_th_hover del_akz" data-id="'+id+'"><svg class="svg_stroke_color" preserveAspectRatio="xMidYMid meet" viewBox="0 0 194 194" xmlns="http://www.w3.org/2000/svg"><g display="inline"><line id="svg_7" y2="2.500002" x2="191.699994" y1="190.900002" x1="2.499994" stroke-linecap="null" stroke-linejoin="null"></line><line id="svg_6" y2="191.700002" x2="191.699994" y1="2.500002" x1="2.499994" stroke-linecap="null" stroke-linejoin="null"></line></g></svg>Удалить</button></div></div>';

			$('#akz_full_wrp').append(new_akz);
			func_find_fixed_row();
			$(this).addClass('in_list').text('Уже добавлена');
		}
	});

$('#save_client_page').click(function(e){
		e.preventDefault();
		arr_id = [];
		good_arr = [];
		akz_arr = [];

		$('#tovari_tut .goods_item').each(function(){
			good_arr.push($(this).find('button').attr('data-id'));
		});

		$('#akz_full_wrp .akz_special_item').each(function(){
			akz_arr.push($(this).find('.del_akz').attr('data-id'));
		});
		
		send_str = 'type=manager_client_page&data='+arr_id.join()+'&good_arr='+good_arr.join()+'&akz_arr='+akz_arr.join()+'&comp_id='+idCompany;

		$.ajax({
	      type:'post',
	      url:'/site/Save',
	      data:send_str,
	      success:function(msg){
			  creat_lb_sv('<div style="height:200px;"><div class="vert_style" style="width:80%">'+msg+'</div><div class="vert_style"></div></div>');
	      }
	    });
	});



	$('#crm_view_user_reviews select').on("change", function(){
		document.crm_view_user_reviews.submit();
	});

	$('#manager_documents_upload input').on("change", function(){
		document.manager_documents_upload.submit();
	});

	$(document).on('click','.goods_item button',function(e){
		e.preventDefault();
		/*str = 'type=remove_recomend&id='+$(this).attr('data-id');
		$.ajax({
			type:'post',
			url:'../ajax.php',
			data:str,
			success:function(msg){
				alert(msg);
			}
		});*/
		if(confirm('Вы точно хотите удалить ?')) {
			$(this).parent().remove();
		}
	});

	$(document).on('click','.akz_special_item .del_akz',function(e){
		e.preventDefault();
		/*str = 'type=remove_akz&id='+$(this).attr('data-id');
		$.ajax({
			type:'post',
			url:'../ajax.php',
			data:str,
			success:function(msg){
				alert(msg);
			}
		});*/
		if(confirm('Вы точно хотите удалить ?'))  {
			$(this).parents('.akz_special_item').remove();
		}


	});

	$('.crm_file_line').find('.row1').click(function(){
		creat_lb_sv('<img src='+$(this).children('img').attr('src')+' style="width:100%;display:block;">');
	});

	$(document).on('change','.publiser_file_input',function(e){

      e.stopPropagation();
   		reWin = /.*(.*)/;
	    str = $(this).val();
	    var fileTitle = str.replace(reWin, "$1");
	    $(this).siblings('p').text(str.replace( "C:\\fakepath\\", '' ));

	    var reader = new FileReader();
	    reader.img_obj = $(this);


        reader.onload = function (e) {
            $img_preview_wrap = reader.img_obj.parents('.img_file_wrap').siblings('.img_file_preview').children('img').attr('src',e.target.result).parent();
            if(!$img_preview_wrap.children('.del_this_pub_foto').length>0 && $('#catalog_menu').length>0){
               $img_preview_wrap.append('<div class="del_this_pub_foto"></div>');
            }else{
               $img_preview_wrap.children('.del_this_pub_foto').show();
            }
        }

        reader.readAsDataURL(this.files[0]);
    
   });


});