$(function () {
    $('#save_result').on('click', function () {
        var items = [];
        $('.cart_item_line').each(function () {
            if ($(this).hasClass('edit_item')) {
                items[$(this).data('id')] = $(this).find('.row4 a').text() + '/' + $(this).find('.row3 span:first-child').text();
            } else {
                items[$(this).data('id')] = 'no_edit/ ' + $(this).find('.row3 span:first-child').text();
            }
        });
        $.ajax({
            type: "POST",
            url: "/admin/orders/editOrder",
            data: items.length == 0 ? {id: $.urlParam()} : {items: items},
            success: function (msg) {
                var json = $.parseJSON(msg);
                if (msg) {
                    $('#save_result').text('данные сохранены!');
                    setTimeout(function () {
                        $('#save_result').text('Сохранить');
                        if (json.success == 'redirect') {
                            window.location.href = '/admin/Orders/admin';
                        }
                    }, 2000);
                }
            }
        })
    });
    $.urlParam = function () {
        return parseInt(window.location.href.replace(/\D+/g, ""));
    };

    $('#crm_file_promo').find('.row5').children('svg').click(function () {
        str_to_send = 'type=del_file_from_promo&name_file=' + $(this).parent().siblings('.row1').children('p').text();
        if (confirm('Вы точно хотите удалить?')) {
            $.ajax({
                type: 'post',
                url: '/site/Ajaxt',
                data: str_to_send,
                success: function (msg) {
                    alert(msg);
                }
            });
            $(this).parents('.table_tr').remove();
        }
    });
    $(document).on('click','.active_on, .active_off', function () {
       if($(this).attr('disabled') != 'disabled'){
           $.ajax({
               url: '/admin/magazineUsers/changeActive',
               type: 'POST',
               data: {
                   user_id: $(this).data('id'),
                   name: $(this).attr('name'),
                   manager_id: $('#select_new_manager option:selected').val()
               },
               success: function (msg) {
                   if (msg == 'Пользователь подтверждён') {
                       $('.active_on').text(msg);
                       $('.active_on').attr('disabled', true);
                   } else {
                       $('.active_off').text(msg);
                       setTimeout(function () {
                           window.location.href = '/admin/MagazineUsers/newUsers'
                       }, 3000);
                   }
               }
           });
       }
    });
    $(document).on('click', '.set_manager', function () {
        if ($(this).attr('disabled') != 'disabled') {
            $.ajax({
                url: '/admin/magazineUsers/changeManager',
                type: 'POST',
                data: {
                    user_id: $(this).data('id'),
                    manager_id: $('#select_new_manager option:selected').val()
                },
                success: function (msg) {
                    if (msg == 'ok') {
                        $('.set_manager').text('Подтвержденно');
                        $('.set_manager').attr('disabled', true);
                    }
                }
            });
        }
    });

});

