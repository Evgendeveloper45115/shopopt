
$(function (){
	dom_map = $('#map');

	

	var map,
	image = {
		url: '../img/map_logo.png',
		size: new google.maps.Size(42,67),
		origin: new google.maps.Point(0,0),
		anchor: new google.maps.Point(21,67)
	}

	function start(zoom) {
	    viewMap('map',dom_map.attr('data-x'),dom_map.attr('data-y'),zoom);
	    addMarker(dom_map.attr('data-x'),dom_map.attr('data-y'));	
	}

	function addMarker(b,c) {
	    var location = new google.maps.LatLng(b,c);
		  marker = new google.maps.Marker({
		    position: location,
		    map: map,
		    title: '',
		    //icon:image
		  });
	}

	function viewMap(a, b, c,zoom) {
		

	    var styles = [{
						//"stylers": [{"saturation": -100}]
					}];

	    var latlng = new google.maps.LatLng(b, c);
	    var mapOptions = {
	        zoom: zoom,
	        center: new google.maps.LatLng(b, c),
	        mapTypeControlOptions: {
	            mapTypeIds: [google.maps.MapTypeId.ROADMAP, 'opt']
	        },
	        panControl: false,
	        zoomControl: true,
	        mapTypeControl: false,
	        scaleControl: true,
	        streetViewControl: false,
	        overviewMapControl: false,
			scrollwheel: false
	    };
	   var styledMap = new google.maps.StyledMapType(styles,{name: "Styled Map"});
	    map = new google.maps.Map(document.getElementById(a), mapOptions);
	    map.mapTypes.set('opt', styledMap);
	    map.setMapTypeId('opt');
	}

	start(15);
});