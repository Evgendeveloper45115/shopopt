function update_ico_cart(price, count) {

    last_count = parseInt($('#cart_count').text());
    new_count = last_count + parseInt(count);
    $('#cart_count').text(new_count);

    last_price = parseInt(($('#align_cart_info').find('.money').text()).replace(/\s+/g, ''));
    new_price = (last_price + (count * parseInt(price))).toString();
    new_price = new_price.replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1 ");
    $('#align_cart_info').find('.money').text(new_price);
}

function getChar(event) {
    if (event.which == null) {
        if (event.keyCode < 32) return null;
        return String.fromCharCode(event.keyCode) // IE
    }

    if (event.which != 0 && event.charCode != 0) {
        if (event.which < 32) return null;
        return String.fromCharCode(event.which) // остальные
    }

    return null; // специальная клавиша
}

$(function () {

    if ($('#special_client_sl').length > 0 && $('#special_client_sl .goods_item').length > 5) {
        total = $('#special_client_sl').children('.goods_item').length;
        while (total > 0) {
            $('#special_client_sl').children('.goods_item:lt(5)').wrapAll('<div class="zone_slaider"></div>');
            total = $('#special_client_sl').children('.goods_item').length;
        }
        $('#special_client_sl').height($('.zone_slaider').eq(0).outerHeight(true));
        if ($('.zone_slaider').length > 1 && $('.zone_slaider').last().children('.goods_item').length < 5) {
            count_clone = 5 - $('.zone_slaider').last().children('.goods_item').length;
            $('.zone_slaider').eq(0).children('.goods_item:lt(' + count_clone + ')').clone().appendTo($('.zone_slaider').last());
        }
        $('#special_client_sl').slidesjs({
            pagination: {active: false},
            callback: {
                loaded: function () {
                    $('#special_client_sl').find('.slidesjs-navigation').addClass('border_hover css_trans').html('<svg  class="svg_stroke_color_hover css_trans" width="124" height="240" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 124 240"><!-- Created with SVG-edit - http://svg-edit.googlecode.com/ --><title>Layer 1</title><line id="svg_1" y2="121.499999" x2="2.5" y1="2.5" x1="121.500006" stroke-width="" stroke="" fill="none"/><line id="svg_2" y2="237.49999" x2="121.500018" y1="118.500003" x1="2.500007" stroke-linecap="null" stroke-linejoin="null" stroke-width="" stroke="" fill="none"/></svg>');
                }
            }
        });
    }


    $(document).on('click', '.goods_item button', function (e) {
        e.preventDefault();
        html_str = '<div id="add_to_cart_wrap" class="msg_padding"><p>Введите кол-во</p><input type="text" class="css_trans" value="' + $(this).attr('data-min') + '"><button class="bg_color_th" data-id="' + $(this).attr('data-id') + '" data-price="' + $(this).attr('data-price') + '" data-min="' + $(this).attr('data-min') + '">Добавить</button></div>';
        creat_lb_sv(html_str);
    });

    $(document).on('click', '#add_to_cart_wrap button', function (e) {
        /*e.preventDefault();
         ch_in = chek_form($('#add_to_cart_wrap input'),1);
         if(ch_in){
         update_ico_cart($(this).attr('data-price'),$(this).siblings('input').val());
         send_str = 'type=add_to_cart&id='+$(this).attr('data-id')+'&count='+$(this).siblings('input').val();
         $.ajax({
         type:'post',
         url:'/catalog/ajax',
         data:send_str,
         success:function(msg){
         // alert(msg);
         }
         });
         $('#cart_shadow').click();
         }*/
        e.preventDefault();
        ch_in = chek_form($('#add_to_cart_wrap input'), 1);
        if (parseInt($(this).siblings('input').val()) < parseInt($(this).attr('data-min'))) {
            $('.err_msg').remove();
            $('<p class="err_msg"><i>Минимальное кол-во для заказа: ' + $(this).attr('data-min') + '</i></p>').insertAfter('#add_to_cart_wrap p');
            return false;
        }
        if (ch_in) {
            update_ico_cart($(this).attr('data-price'), $(this).siblings('input').val());
            var dataID = $(this).attr('data-id');
            send_str = 'type=add_to_cart&id=' + $(this).attr('data-id') + '&count=' + $(this).siblings('input').val();
            $.ajax({
                type: 'post',
                url: '/html_source/ajax.php',
                data: send_str,
                success: function (msg) {
                    $('.goods_item button[data-id="' + dataID + '"]').text('В Корзине');

                }
            });
            $('#cart_shadow').click();
        }
    });

    $(document).on('keypress', '#add_to_cart_wrap input', function (e) {
        e = e || event;
        if (e.ctrlKey || e.altKey || e.metaKey) return;
        var chr = getChar(e);
        if (chr == null) return;
        if (chr < '0' || chr > '9') {
            return false;
        }
    });

    $('.manager_finder_box input').blur(function () {
        if ($(this).val() != '') {
            document.manager_finder.submit();
        }
    });

    $('.show_akz_lt').click(function (e) {
        e.preventDefault();
        title = $(this).siblings('.demo_akz_title').text();
        content = $(this).siblings('.hid_content').html();
        img_lt = $(this).parent().siblings('.img_fon').attr('style');
        akz_html_lt = '<div class="inline_style akz_lt_client2 img_fon" style="' + img_lt + '"></div><div class="inline_style akz_lt_client2"><p class="title">' + title + '</p><div>' + content + '</div></div>';
        creat_lb_sv_akz(akz_html_lt, 900);
    });


    if ($('.client_order_dashboard').length > 0) {
        function Calendar2(id, year, month) {
            var Dlast = new Date(year, month + 1, 0).getDate(),
                D = new Date(year, month, Dlast),
                DNlast = new Date(D.getFullYear(), D.getMonth(), Dlast).getDay(),
                DNfirst = new Date(D.getFullYear(), D.getMonth(), 1).getDay(),
                calendar = '<tr>',
                month = ["Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"];
            if (DNfirst != 0) {
                for (var i = 1; i < DNfirst; i++) calendar += '<td>';
            } else {
                for (var i = 0; i < 6; i++) calendar += '<td>';
            }


            for (var i = 1; i <= Dlast; i++) {
                if (i == new Date().getDate() && D.getFullYear() == new Date().getFullYear() && D.getMonth() == new Date().getMonth()) {
                    calendar += '<td class="today "><span class=" ">' + i + '</span>';
                } else {
                    if ((i < new Date().getDate() && D.getFullYear() <= new Date().getFullYear() && D.getMonth() <= new Date().getMonth()) || ( D.getFullYear() <= new Date().getFullYear() && D.getMonth() < new Date().getMonth()) || ( D.getFullYear() < new Date().getFullYear())) {
                        calendar += '<td class=><span  class="">' + i + '</span>';
                    } else {
                        /*if((i-1) == new Date().getDate() && D.getFullYear() == new Date().getFullYear() && D.getMonth() == new Date().getMonth()){
                         calendar += '<td class="next_d "><span  class="css_trans">' + i+'</span>';
                         }else{*/
                        calendar += '<td class=" "><span  class=" ">' + i + '</span>';
                        //}
                    }

                }
                if (new Date(D.getFullYear(), D.getMonth(), i).getDay() == 0) {
                    calendar += '<tr>';
                }
            }
            for (var i = DNlast; i < 7; i++) calendar += '<td>&nbsp;';
            document.querySelector('#' + id + ' tbody').innerHTML = calendar;
            document.querySelector('#' + id + ' thead td:nth-child(2)').innerHTML = month[D.getMonth()] + ' ' + D.getFullYear();
            document.querySelector('#' + id + ' thead td:nth-child(2)').dataset.month = D.getMonth();
            document.querySelector('#' + id + ' thead td:nth-child(2)').dataset.year = D.getFullYear();
            if (document.querySelectorAll('#' + id + ' tbody tr').length < 6) {  // чтобы при перелистывании месяцев не "подпрыгивала" вся страница, добавляется ряд пустых клеток. Итог: всегда 6 строк для цифр
                document.querySelector('#' + id + ' tbody').innerHTML += '<tr><td>&nbsp;<td>&nbsp;<td>&nbsp;<td>&nbsp;<td>&nbsp;<td>&nbsp;<td>&nbsp;';
            }
        }

        function chek_data() {
            cur_year = $('#calendar_data').attr('data-year');
            cur_month = parseInt($('#calendar_data').attr('data-month'));

            for (key in data_arr) {
                if (parseInt(cur_year) == parseInt(data_arr[key]['data_y'])) {
                    if (parseInt(cur_month) == (parseInt(data_arr[key]['data_m']) - 1)) {
                        $('#calendar2 tbody').find('span').each(function () {
                            cal_day = parseInt($(this).text());
                            if (cal_day == parseInt(data_arr[key]['data_d'])) {
                                $(this).addClass('bg_color_th').parent().addClass('active link_from_cal').attr('data-href', data_arr[key]['href']);
                            }
                        });

                    }
                }
            }
        }

        $(document).on('click', '.link_from_cal', function () {
            link_ = $(this).attr('data-href');
            document.location.href = link_;
        });


        Calendar2("calendar2", new Date().getFullYear(), new Date().getMonth());
        chek_data();
        // переключатель минус месяц
        document.querySelector('#calendar2 thead tr:nth-child(1) td:nth-child(1)').onclick = function () {
            Calendar2("calendar2", document.querySelector('#calendar2 thead td:nth-child(2)').dataset.year, parseFloat(document.querySelector('#calendar2 thead td:nth-child(2)').dataset.month) - 1);
            chek_data();

        }

        // переключатель плюс месяц
        document.querySelector('#calendar2 thead tr:nth-child(1) td:nth-child(3)').onclick = function () {
            Calendar2("calendar2", document.querySelector('#calendar2 thead td:nth-child(2)').dataset.year, parseFloat(document.querySelector('#calendar2 thead td:nth-child(2)').dataset.month) + 1);
            chek_data();
        }
    }


    $('.client_view_file_line').find('.row1').click(function () {
        creat_lb_sv('<img src=' + $(this).children('img').attr('src') + ' style="width:100%;display:blockblock;">');
    });

    $('.manager_file_view2').find('.d_inline').click(function () {
        creat_lb_sv('<img src=' + $(this).parent().children('img').attr('src') + ' style="width:100%;display:block;">');
    });


    $('.add_new_shop_form').click(function (e) {
        $('#magazine-clients-address-form').find('input').addClass('css_trans');
        e.preventDefault();
        if (chek_form($('#magazine-clients-address-form').find('input'), 6)) {
            document.add_new_shop_form.submit();
        }
    });
    $(document).on('change', '.publiser_file_input', function (e) {
        e.stopPropagation();
        reWin = /.*(.*)/;
        str = $(this).val();
        var fileTitle = str.replace(reWin, "$1");
        $(this).siblings('p').text(str.replace("C:\\fakepath\\", ''));

        var reader = new FileReader();
        reader.img_obj = $(this);

        reader.onload = function (e) {
            var example = str.split('.');
            $img_preview_wrap = reader.img_obj.parents('.img_file_wrap').siblings('.img_file_preview').children('img').attr('src', e.target.result).parent();
            if (!$img_preview_wrap.children('.del_this_pub_foto').length > 0 && $('#catalog_menu').length > 0) {
                $img_preview_wrap.append('<div class="del_this_pub_foto"></div>');
            } else {

                $img_preview_wrap.children('.del_this_pub_foto').show();
                var img = '<div class="img_file_box">' +
                    '<div class="img_file_wrap">' +
                    '<div class="img_file_wrap_i">' +
                    '<input type="file" data-type="image_product" class="publiser_file_input" data-id="" name="MagazineCompany[]" value="">' +
                    '</div>' +
                    '</div>' +
                    '<div class="img_file_preview">' +
                    '<img src="/html_source/img/save_ico.png">' +
                    '<div class="del_this_pub_foto"></div>' +
                    '</div>' +
                    '</div>';
                $('#publisher_foto_cont').append(img)
            }
            $('input[type="submit"]').removeAttr('disabled');
            var image = null;
            if (example[1] == 'docx') {
                image = '/html_source/img/docx-icon.png';
            } else if (example[1] == 'xlsx') {
                image = '/html_source/img/xlsx-icon.png';
            } else if (example[1] == 'pptx') {
                image = '/html_source/img/pptx-icon.png';
            } else if (example[1] == 'pdf') {
                image = '/html_source/img/pdf-icon.png';
            } else if (example[1] == 'jpg') {
                image = null
            } else if (example[1] == 'png') {
                image = null
            } else {
                image = '/html_source/img/txt-file-icon-1196.png';
            }
            if (image != null) {
                $img_preview_wrap.children('img').attr('src', image)
            }
        };
        reader.readAsDataURL(this.files[0]);
    });
    $(document).on('click', '.del_this_pub_foto', function () {
        var type = $(this).parent().parent().find('input[type="file"]').attr('data-type');
        var id = $(this).parent().parent().find('input[type="file"]').attr('data-id');
        var img = $(this).hide().siblings('img').attr('src');
        if ($('.img_file_box').length > 1 && $(this).siblings('img').attr('src') != '/html_source/img/save_ico.png') {
            $(this).closest('.img_file_box').remove();
        }
        $(this).hide().siblings('img').attr('src', '/html_source/img/save_ico.png');
        $(this).parent().siblings('.img_file_wrap').find('input').val('');
        /*$.ajax({
         type:'post',
         url:'/site/deletephoto',
         data:'photoname='+img+'&type='+type+'&id='+id,
         success:function(msg){
         //alert(msg);
         }
         });*/

    });

    var lastChild = $('.img_file_box:last-child .img_file_preview img');
    var src = lastChild.attr('src');
    $('.img_file_box .img_file_preview img').each(function (k, v) {
        var imgExt = $(v).attr('src').split('.');
        if (imgExt[1] == 'docx') {
            $(v).attr('src', '/html_source/img/docx-icon.png')
        } else if (imgExt[1] == 'xlsx') {
            $(v).attr('src', '/html_source/img/xlsx-icon.png')
        } else if (imgExt[1] == 'pptx') {
            $(v).attr('src', '/html_source/img/pptx-icon.png')
        } else if (imgExt[1] == 'pdf') {
            $(v).attr('src', '/html_source/img/pdf-icon.png')
        } else if (imgExt[1] == 'jpg') {
        } else if (imgExt[1] == 'png') {
        } else {
            $(v).attr('src', '/html_source/img/txt-file-icon-1196.png')
        }
    });

    if (src != '/html_source/img/save_ico.png') {
        var img = '<div class="img_file_box">' +
            '<div class="img_file_wrap">' +
            '<div class="img_file_wrap_i">' +
            '<input type="file" data-type="image_product" class="publiser_file_input" data-id="" name="MagazineCompany[]" value="">' +
            '</div>' +
            '</div>' +
            '<div class="img_file_preview">' +
            '<img src="/html_source/img/save_ico.png">' +
            '<div class="del_this_pub_foto"></div>' +
            '</div>' +
            '</div>';
        $('#publisher_foto_cont').append(img)
    }
    $('#manager_mail').on('click', function (e) {
        e.preventDefault();
        creat_lb_sv('<div style="margin-top: 15px"><div class="vert_style" style="width:80%;">' +
            '<textarea class="manager_text" name="Question[manager_text]" placeholder="Введите текст" style="padding-left: 10px;height: 160px; width: 386px; margin-bottom: 15px;font-size: 1.2vw;"></textarea>' +
            '<button id="manager_mail_text" class="bg_color_th" style="font-size: 1.1vw;text-transform: uppercase;color: white;padding: 0.5vw 2.9vw;border-radius: 50px;display: block;margin: 0 auto 15px auto;">Задать вопрос</button>' +
            '<div style="margin-bottom: 15px"><span style="color: #555555;">Или</span></div>' +
            '<div><input name="Question[manager_phone]" style="width: 45%; text-align: center;padding: 5px 0; margin-bottom: 15px" type="text" class="phone_mask"></div>' +
            '<button id="manager_mail_phone" class="bg_color_th" style="font-size: 1.1vw;text-transform: uppercase;color: white;padding: 0.5vw 2.9vw;border-radius: 50px;display: block;margin: 0 auto 15px auto;">Перезвоните мне</button>' +
            '</div><div class="vert_style" style="height: 70%"></div></div>');
        $('.phone_mask').attr('placeholder', 'Номер телефона');
        $('.phone_mask').focusout(function () {
            $('.phone_mask').removeClass('done');
        });
        $('.phone_mask').mask("+7 (999) 999-9999");
    });
    $(document).on('click', '#manager_mail_text', function () {
        if ($('textarea[name="Question[manager_text]"]').val() == '') {
            $('textarea[name="Question[manager_text]"]').css({
                'border': '1px solid red',
                'box-shadow': '0px 1px 15px 0px red'
            });
            return false;
        }
        $.ajax({
            type: 'post',
            url: '/client/default/feedback',
            data: {text: $('textarea[name="Question[manager_text]"]').val()},
            success: function (msg) {
                creat_lb_sv('<div style="height:200px;"><div class="vert_style" style="width:80%"><img style="display:inline_block;" src="/html_source/img/cart_ok.png" width="75px"><br/>Спасибо, ваше сообщение отправлено.<br/>Менеджер с вами свяжется по указанным контактным данным.</div><div class="vert_style"></div></div>');
                setTimeout(function () {
                    $('#cart_shadow').click();
                }, 3000);
            }
        });
    });
    $(document).on('keydown', 'textarea[name="Question[manager_text]"],input[name="Question[manager_phone]"]', function () {
        if ($(this).hasClass('focus')) {
            $(this).css({'border': '1px solid #83649b', 'box-shadow': '0px 1px 15px 0px #83649b'})
        }
    });
    $(document).on('click', '#manager_mail_phone', function () {
        if ($('input[name="Question[manager_phone]"]').val() == '') {
            $('input[name="Question[manager_phone]"]').css({
                'border': '1px solid red',
                'box-shadow': '0px 1px 15px 0px red'
            });
            return false;
        }

        $.ajax({
            type: 'post',
            url: '/client/default/feedback',
            data: {phone: $('input[name="Question[manager_phone]"]').val()},
            success: function (msg) {
                creat_lb_sv('<div style="height:200px;"><div class="vert_style" style="width:80%"><img style="display:inline_block;" src="/html_source/img/cart_ok.png" width="75px"><br/>Спасибо, ваше сообщение отправлено.<br/>Менеджер с вами свяжется по указанным контактным данным.</div><div class="vert_style"></div></div>');
                setTimeout(function () {
                    $('#cart_shadow').click();
                }, 3000);
            }
        });
    })
});