var tree = [];
$(function () {

    $('.img_file_box').each(function () {
        input = $(this).find('input');
        if (input.attr('value') != '') {
            input.siblings('p').text('Заменить файл');
            $(this).find('.img_file_preview').children('img').attr('src', input.attr('value'));
        } else {
            input.siblings('p').text('Прикрепить файл');
        }
    });

    $(document).on('click', 'a.del_element', function (e) {
        e.preventDefault();
        if (confirm('Вы точно хотите удалить ?')) {
            window.location.href = $(this).attr('href');
        }
    });

    $(document).on('change', '.publiser_file_input', function (e) {

        e.stopPropagation();
        reWin = /.*(.*)/;
        str = $(this).val();
        var fileTitle = str.replace(reWin, "$1");
        $(this).siblings('p').text(str.replace("C:\\fakepath\\", ''));

        var reader = new FileReader();
        reader.img_obj = $(this);


        reader.onload = function (e) {
            var example = str.split('.');
            $img_preview_wrap = reader.img_obj.parents('.img_file_wrap').siblings('.img_file_preview').children('img').attr('src', e.target.result).parent();
            if (!$img_preview_wrap.children('.del_this_pub_foto').length > 0 && $('#catalog_menu').length > 0) {
                $img_preview_wrap.append('<div class="del_this_pub_foto"></div>');
            } else {
                $img_preview_wrap.children('.del_this_pub_foto').show();
                var img = '<div class="img_file_box">' +
                    '<div class="img_file_wrap">' +
                    '<div class="img_file_wrap_i">' +
                    '<input type="file" data-type="image_product" class="publiser_file_input" data-id="" name="MagazineProduct[]" value="">' +
                    '</div>' +
                    '</div>' +
                    '<div class="img_file_preview">' +
                    '<img src="/html_source/img/save_ico.png">' +
                    '<div class="del_this_pub_foto"></div>' +
                    '</div>' +
                    '</div>';
                $('#publisher_foto_cont').append(img)

            }
            $('input[type="submit"]').removeAttr('disabled');
            var image = null;
            var format = example.pop();
            if (format == 'docx') {
                image = '/html_source/img/docx-icon.png';
            } else if (format == 'xlsx') {
                image = '/html_source/img/xlsx-icon.png';
            } else if (format == 'pptx') {
                image = '/html_source/img/pptx-icon.png';
            } else if (format == 'pdf') {
                image = '/html_source/img/pdf-icon.png';
            } else if (format == 'jpg') {
                image = null
            } else if (format == 'png') {
                image = null
            } else {
                image = '/html_source/img/txt-file-icon-1196.png';
            }
            if (image != null) {
                $img_preview_wrap.children('img').attr('src', image)
            }

        }

        reader.readAsDataURL(this.files[0]);


    });

    $(document).on('click', '.del_this_pub_foto', function () {
        var type = $(this).parent().parent().find('input[type="file"]').attr('data-type');
        var id = $(this).parent().parent().find('input[type="file"]').attr('data-id');
        var img = $(this).hide().siblings('img').attr('src');
        if ($('.img_file_box').length > 1 && $(this).siblings('img').attr('src') != '/html_source/img/save_ico.png') {
            $(this).closest('.img_file_box').remove();
        }
        $(this).hide().siblings('img').attr('src', '/html_source/img/save_ico.png');
        $(this).parent().siblings('.img_file_wrap').find('input').val('');


        /*$.ajax({
         type:'post',
         url:'/site/deletephoto',
         data:'photoname='+img+'&type='+type+'&id='+id,
         success:function(msg){
         //alert(msg);
         }
         });*/

    });
    $('.del_img').click(function () {
        var links = [];
        $('.img_file_box').each(function () {
            if ($(this).find('.img_file_wrap input[type="file"]').attr('value') != $(this).find('.img_file_preview img').attr('src')) {
                if ($(this).find('.img_file_wrap input[type="file"]').attr('value') != '') {
                    links.push(
                        {
                            link: $(this).find('.img_file_wrap input[type="file"]').attr('value'),
                            type: $(this).find('.img_file_wrap input[type="file"]').attr('data-type')
                        }
                    );
                }
            }
        });
        $.ajax({
            type: 'post',
            url: '/site/deletePhotoBySubmit',
            data: {
                obj: links
            },
            success: function (msg) {
                $('.form_del_submit').click();
            }
        });
    });


    $(document).on('click', '.add_f', function () {
        html_str = '<div id="slaider_wrap"><div class="img_file_box"><div class="img_file_wrap sl_file_box"><div class="img_file_wrap_i"><input type="file" class="publiser_file_input"  name="main_slaid[]" required data-value=""><p class="bg_color_th">Прикрепить файл</p></div><p class="add_f bg_color_th">Добавить слайд</p> <p class="remove_f bg_color_th">Удалить слайд</p></div><div class="img_file_preview"></div></div></div>';
        $(html_str).insertBefore(".form_submit");
    });

    $(document).on('click', '.remove_f', function () {
        $(this).parents('.img_file_box').remove();
    });


    //catalog tree
    function creat_tree() {
        tree = [];
        $('#catalog_menu a').each(function () {
            if ($(this).siblings('.open').length > 0) {
                tree.push($(this).attr('data-id'));
            }
        });
        send_tree = 'tree=' + JSON.stringify(tree);
        $.ajax({
            type: 'post',
            url: '/site/tree',
            data: send_tree,
            success: function (msg) {
                //alert(msg);
            }
        });
    }

    function open_tree() {
        tree_open = JSON.parse(tree_open);
        tree_open.forEach(function (item) {
            $('#node_' + item).addClass('open').siblings('ul').show();
        });
        var search = window.location.search.substr(1),
            keys = {};
        search.split('&').forEach(function (item) {
            item = item.split('=');
            keys[item[0]] = item[1];
        });
        var size = Object.keys(keys).length;
        if (size > 0) {
            $('#catalog_menu').scrollTop(keys['sct']);
        }
    }


    if ($('#catalog_menu').length > 0) {
        //find subfolder
        $('#catalog_menu a').each(function () {
            if ($(this).siblings('ul').length > 0) {
                id = $(this).attr('data-id');
                $(this).before('<span class="folder_ico" id="node_' + id + '"></span>');
            }
        });

        $('#catalog_menu a').on('click', function () {

            if ($(this).attr('href') == '') {
                e.preventDefault();

            } else {
                s_href = $(this).attr('href');
            }

            $(this).attr('href', s_href + '&sct=' + $('#catalog_menu').scrollTop());
            // document.location.href=$(this).attr('href')
        });

        $('#catalog_menu a').on('contextmenu', function (e) {
            e.preventDefault();
            $('.unpub_element').show();
            $('.pub_element').show();
            $('#contextmenu').height($('#contextmenu').siblings('ul').outerHeight(true));

            top_a = $('#catalog_menu').scrollTop() + $(this).position().top;
            left_a = $(this).position().left;
            el_id = $(this).attr('data-id');
            $('#contextmenu_wrap').attr('data-id', el_id);
            $('#contextmenu_wrap .title').text($(this).children('span').text());
            //option_menu_link
            var url = [location.protocol, '//', location.host, location.pathname].join('');
            scrl_box = $('#catalog_menu').scrollTop();
            $('.edit_element').attr('href', $(this).attr('href'));
            $('.child_add').attr('href', url + '?type=child_add&id=' + el_id);
            if ($(this).parent().hasClass('hiddenFolder')) {
                $('.unpub_element').hide();
                $('.pub_element').attr('href', url + '?type=pub_element&id=' + el_id);
            } else {
                $('.pub_element').hide();
                $('.unpub_element').attr('href', url + '?type=unpub_element&id=' + el_id);
            }


            $('.del_element').attr('href', url + '?type=del_element&id=' + el_id);
            $('.undel_element').attr('href', url + '?type=undel_element&id=' + el_id);
            $('.move_element').attr('data-id', el_id);
            $('.clone_element').attr('data-id', el_id);


            $('#contextmenu').show();
            $('#contextmenu_wrap').css('top', top_a);

            //edit link

        });

        $('#contextmenu_close').click(function () {
            $('#contextmenu').hide();
        });

        open_tree();


        $('.folder_ico').click(function () {
            if ($(this).hasClass('open')) {
                $(this).removeClass('open').siblings('ul').hide();
                creat_tree();
            } else {
                $(this).addClass('open').siblings('ul').show();
                creat_tree();
            }
        });


        $('.move_element').click(function (e) {
            e.preventDefault();
            move_txt = '<form action="" method="post"><input type="hidden" name="type_action" value="move_el"><input type="hidden" name="id_new_parent" id="id_new_parent"><input type="hidden" name="id_el" value="' + $(this).attr('data-id') + '"><p>Перемещаемый ресурс: <span class="curent_move">' + $(this).attr('data-id') + '</p><p>Выберите новый "родительский" ресурс в дереве сайта слева.</p><p>Новый родительский ресурс: <span class="new_move"></span></p><button class="save_new bg_color_th">Сохранить</button><button class="reset_new bg_color_th">Отмена</button></form>';
            $('#align_zone').html(move_txt).show();
            $('#contextmenu').hide();
            $('#catalog_menu ul').find('a').addClass('select_for_move');
        });

        $(document).on('click', '.reset_new', function (e) {
            e.preventDefault();
            document.location.reload();
        });

        $(document).on('click', '.save_new', function (e) {
            if (!$('#id_new_parent').val()) {
                e.preventDefault();
            }

        });

        $(document).on('click', '.select_for_move', function (e) {
            e.preventDefault();
            $('#id_new_parent').val($(this).attr('data-id'));
            $('.new_move').text($(this).attr('data-id'));
        });

        $('.clone_element').click(function (e) {
            e.preventDefault();
            clone_str = '<form action="" method="post" name="clone_form"><input type="hidden" name="type_action" value="clone"><input type="hidden" name="id_el" value="' + $(this).attr('data-id') + '"></form>';
            $('body').append(clone_str);
            document.clone_form.submit();
        });


        $('.img_file_preview').each(function () {
            if ($(this).children('img').attr('src') != '../img/save_ico.png') {
                $(this).append('<div class="del_this_pub_foto"></div>');
            }
        });

    }


    $('#select_type_ button').click(function (e) {
        e.preventDefault();
        if ($('.form_s:checked').val() == '1') {
            $('.folder').show();
        } else {
            $('.element').show();
        }
        $('#select_type_').hide();
    });


    $(document).on('click', '.del_type', function (e) {
        e.preventDefault();
        $(this).parent().remove();
    });


    $('.add_new_type').click(function (e) {
        e.preventDefault();
        last_item = $('.type_list_item').eq(-1);
        $('<div class="type_list_item"><input type="text" name="type[]" required=""><a class="del_type add_new_type" style="margin-left: 0.25vw">Удалить</a></div>').insertAfter(last_item);
    });

    $('.promo_click').click(function (e) {
        if (!$(this).hasClass('xren')) {
            e.preventDefault();
            img_src = $(this).attr('href');
            var img = img_src.split('.')[1];
            if (img == 'docx') {
                img = '<div class="icon-format" style="background-position: -4.4vw -0.2vw; background-size: 11vw; background-image: url(/html_source/img/pdf-jpg-word-icons2.jpg); width: 2.2vw; height: 2.6vw; display: inline-block;  vertical-align: middle;"></div>';
            } else if (img == 'xlsx') {
                img = '<div class="icon-format" style="background-position: 4.4vw -0.2vw; background-size: 11vw; background-image: url(/html_source/img/pdf-jpg-word-icons2.jpg); width: 2.2vw; height: 2.6vw; display: inline-block;  vertical-align: middle;"></div>';
            } else if (img == 'pptx') {
                img = '<div class="icon-format" style="background-position: 2.2vw -0.2vw; background-size: 11vw; background-image: url(/html_source/img/pdf-jpg-word-icons2.jpg); width: 2.2vw; height: 2.6vw; display: inline-block;  vertical-align: middle;"></div>';
            } else if (img == 'pdf') {
                img = '<div class="icon-format" style="background-position: -0.1vw -0.2vw; background-size: 11vw; background-image: url(/html_source/img/pdf-jpg-word-icons2.jpg); width: 2.2vw; height: 2.6vw; display: inline-block;  vertical-align: middle;"></div>';
            } else if (img == 'jpg') {
                img = '<img style="max-width:80%;" src="' + img_src + '">';
            } else if (img == 'png') {
                img = '<img style="max-width:80%;" src="' + img_src + '">';
            } else {
                img = '<div class="icon-format-none" style="background-position: -0.6vw 0; background-size: 3vw; background-image: url(/html_source/img/attachment-icon.jpg); width: 2.3vw; height: 2.6vw; display: inline-block;  vertical-align: middle;"></div>';
            }
            $('#align_zone').html('<a class="download_link_promo" target=_blank href="/site/dwload?file=' + $(this).attr('data-id') + '"> ' + img + ' <button class="download_promo bg_color_th">Скачать</button></div>');
        } else {
            e.preventDefault();
            $('#align_zone').html('<a class="download_link_promo" target=_blank href="/site/dwload?file=' + $(this).attr('data-id') + '"><p>просмотр данного файла не возможен</p><button class="download_promo bg_color_th">Скачать</button></a>');
        }
    });
    $('.promo_click span.color_th').click(function (e) {
        e.preventDefault();
        var data_id = $(this).parent().attr('data-id');
        if (confirm("Вы точно хотите удалить?")) {
            $.ajax({
                type: 'post',
                url: '/site/removePromo',
                data: 'type=del_promo&id=' + data_id
            });
            $('#align_zone').html();
            $(this).parent().remove();
        }
    });
    $('.akz_click span.color_th').click(function (e) {
        e.preventDefault();
        var data_id = $(this).parent().attr('data-id');
        if (confirm("Вы точно хотите удалить?")) {
            $.ajax({
                type: 'post',
                url: '/site/removeAction',
                data: 'type=del_akzo&id=' + data_id
            });
            $('#align_zone').html();
            $(this).parent().remove();
        }
    });


    $('.save_akz').click(function (e) {
        e.preventDefault();
        if (chek_form($('.akz_req_input'), 2)) {
            document.save_new_akz.submit();
        }
    });
    var lastChilds = $('#magazineCatalog').find('.clone_fields').last();
    var intChild = 0;
    if (lastChilds.length) {
        intChild = lastChilds.attr('data-clone');
    }
    $(document).on('click', '.add_field_publisher', function (e) {
        var duplicate = $('#magazineCatalog').find('[data-id="1"]');
        intChild = parseInt(intChild) + 1;
        e.preventDefault();
        var clone_input = '<div class="input_div clone_fields" data-id="1" data-clone="' + intChild + '"><p  style="margin-right: 0.2vw"><input placeholder="Название поля" type="text" name="duplicate_name_' + intChild + '"></p>' +
            '<input class="input_publisher" type="text" name="duplicate_value_' + intChild + '">  ' +
            '<input style="width: 0; height: 0; opacity: 0;" type="checkbox" checked class="is_active_checkbox" name="duplicate_value_' + intChild + '-check">' +
            '<div class="selector active bg_color_th" style="margin-left: 6.5% !important;"></div>' +
            '<div class="delete_publisher color_th_hover">Удалить</div>' +
            '</div>';
        $(clone_input).insertBefore('.form_submit.bg_color_th.delete-img.second');
    });
    $(document).on('click', '.selector', function () {
        if ($(this).hasClass('active')) {
            $(this).removeClass('active bg_color_th');
        } else {
            $(this).addClass('active bg_color_th');
        }
        if ($(this).siblings('.is_active_checkbox').prop('checked')) {
            $(this).siblings('.is_active_checkbox').prop('checked', false);
        } else {
            $(this).siblings('.is_active_checkbox').prop('checked', true);
        }
    });
    $(document).on('click', '.delete_publisher ', function () {
        $(this).closest('.input_div').remove()
    });
    var lastChild = $('.img_file_box:last-child .img_file_preview img');
    var src = lastChild.attr('src');
    $('.img_file_box .img_file_preview img').each(function (k, v) {
        var imgExt = $(v).attr('src').split('.');
        if (imgExt[1] == 'docx') {
            $(v).attr('src', '/html_source/img/docx-icon.png')
        } else if (imgExt[1] == 'xlsx') {
            $(v).attr('src', '/html_source/img/xlsx-icon.png')
        } else if (imgExt[1] == 'pptx') {
            $(v).attr('src', '/html_source/img/pptx-icon.png')
        } else if (imgExt[1] == 'pdf') {
            $(v).attr('src', '/html_source/img/pdf-icon.png')
        } else if (imgExt[1] == 'jpg') {
        } else if (imgExt[1] == 'png') {
        } else {
            $(v).attr('src', '/html_source/img/txt-file-icon-1196.png')
        }
    });

    if (src != '/html_source/img/save_ico.png') {
        var img = '<div class="img_file_box">' +
            '<div class="img_file_wrap">' +
            '<div class="img_file_wrap_i">' +
            '<input type="file" data-type="image_product" class="publiser_file_input" data-id="" name="MagazineProduct[]" value="">' +
            '</div>' +
            '</div>' +
            '<div class="img_file_preview">' +
            '<img src="/html_source/img/save_ico.png">' +
            '<div class="del_this_pub_foto"></div>' +
            '</div>' +
            '</div>';
        $('#publisher_foto_cont').append(img)
    }
        $('.cat_name').each(function (k,v) {
            $(this).on('click',function(){
                $(this).parent().find('li').each(function(){
                    if($(this).css('display') == 'none'){
                        $(this).css('display', 'block')
                    }else{
                        $(this).css('display', 'none')
                    }
                });
            });
        })
});
