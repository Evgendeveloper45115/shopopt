$(function(){

	var klient_selector = $('.client_item .selector');

	$('#select_all .selector').click(function(){
		if($(this).hasClass('active')){
			$(this).removeClass('active bg_color_th');
			klient_selector.removeClass('active bg_color_th');
		}else{
			$(this).addClass('active bg_color_th');
			klient_selector.addClass('active bg_color_th');
		}
	});


	klient_selector.click(function(){
		$('#select_all .selector').removeClass('active');
		if($(this).hasClass('active')){
			$(this).removeClass('active bg_color_th');
		}else{
			$(this).addClass('active bg_color_th');
		}
	});

	$('#save_add_client_file').click(function(e){
		e.preventDefault();
		$('body').addClass('cursor');
		arr_id = [];
		klient_selector.each(function(){
			if($(this).hasClass('active')){
				arr_id.push($(this).parent().attr('data-id'));
			}
		});
		
		send_str = 'file_id='+$('.client_item').attr('data-file-id')+'&type=add_client_file&data='+arr_id.join();

		$.ajax({
	      type:'post',
	      url:'/admin/promo/Save',
	      data:send_str,
	      success:function(msg){
	          creat_lb_sv(msg);
	        $('body').removeClass('cursor');
	      }
	    });
		//document.add_client_file.submit();
	});
});