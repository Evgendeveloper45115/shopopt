var win_ = new Object({
    'flag_use': false,
    'setup': function () {
        win_.x = win_.object.width();
        win_.y = win_.object.height();
        win_.flag_use = true;
    },
    'zapros': function (data) {
        function chek(data) {
            if (!win_.flag_use) {
                win_.setup();
            }
            return win_[data];
        }

        switch (data) {
            case 'x':
                out = chek(data);
                break;
            case 'y':
                out = chek(data);
                break;
            default:
                console.log('error - zapros data type, object win_');
                return false;
                break;
        }
        return out;
    },
    'x': 0,
    'y': 0,
    'object': $(window)
});

$(window).resize(function () {
    win_.flag_use = false;
});


function creat_lb_sv(str, width_) {

    if ($('#cart_msg').length == 0) {
        speed = 600;
    } else {
        speed = 0;
    }

    $('#cart_msg').remove();
    var url = window.location.href;
    var closest = "<div class=" + "close" + "></div>";
    tst = url.split(/:\/\/[^\/]*\//);
    if (tst[1] == '' && close == false) {
        closest = false;
    }
    $('body').append('<div id="cart_msg"><div id="cart_shadow"></div><div id="cart_msg_wrap" class="custom_style_modal">' + (closest != false ? closest : '') + '<div id="cart_msg_cont">' + str + '</div></div></div>');
    t = width_;
    if (window.t && win_.zapros('x') > 760) {
        $('#cart_msg_wrap').width(width_);
    } else {
        if (win_.zapros('x') > 760) {
            $('#cart_msg_wrap').width(500);
        }
    }


    $('#cart_msg_cont').html('<div class="scroll_cart_msg">' + str + '</div>');
    $('#cart_msg').css({'display': 'block', 'opacity': 0});
    x = $('.scroll_cart_msg').outerHeight(true);
    if ((win_.zapros('y') * 80 / 100) < x) {
        $('#cart_msg_wrap').height(win_.zapros('y') * 80 / 100);
    }
    top_x = (win_.zapros('y') - $('#cart_msg_wrap').outerHeight(true)) / 2;
    $('#cart_msg_wrap').css('top', top_x);

    $('#cart_msg').animate({'opacity': 1}, speed);
}
function creat_lb_sv_akz(str, width_) {

    $('#cart_msg').remove();
    $('body').append('<div id="cart_msg"><div id="cart_shadow"></div><div id="cart_msg_wrap" style="height:400px;"><div class="close"></div><div id="cart_msg_cont" style="height:400px;">' + str + '</div></div></div>');

    $('#cart_msg_wrap').width(width_);
    $('#cart_msg').css({'display': 'block', 'opacity': 0});
    top_x = (win_.zapros('y') - $('#cart_msg_wrap').outerHeight(true)) / 2;
    $('#cart_msg_wrap').css('top', top_x);

    $('#cart_msg').animate({'opacity': 1}, 600);
}

function chek_form(input_el, count_field) {
    var count = 0;
    input_el.each(function () {
        if ($(this).val() != '') {
            count++
        } else {
            $(this).addClass('failed');
        }
    });
    if (count == count_field) {

        return true;
    } else {
        return false
    }
}
$(function () {
    $(document).on('click', '#cart_shadow', function () {
        if (close == true) {
            $('#cart_msg').animate({'opacity': 0}, 600, function () {
                $('#cart_msg').remove();
            });
        } else if (isNative(String(close))) {
            $('#cart_msg').animate({'opacity': 0}, 600, function () {
                $('#cart_msg').remove();
            });
        }
    });
    $(document).on('focus', 'input,textarea', function () {
        $(this).removeClass('failed');
        $(this).addClass('focus');
    });

    $(document).on('blur', 'input,textarea', function () {
        //debugger;
        $(this).removeClass('focus');
        if ($(this).val().length > 0) {
            $(this).addClass('done');
        } else {
            $(this).removeClass('done');
        }
        if ($(this).hasClass('phone_mask') && $(this).val().indexOf('_') < 0) {
            $(this).addClass('done');
        } else {
            $(this).removeClass('done');
        }
    });
});
function isNative(fn) {
    return (/\{\s*\[native code\]\s*\}/).test('' + fn);
}