var func_find_fixed_row;


$(function () {
    var body_page = $('body'),
        left_bar = $('#left_bar'),
        content_zone = $('#conten_zone'),
        fix_row = '',
        oldDiscount = '',
        oldAllDiscount = '',
        scroll_row = '';

//money format
    $('.money').each(function () {
        str = $(this).text().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1 ");
        $(this).text(str);
    });


//min height setup
    function bar_setup() {
        body_page.children('.inline_style').css('min-height', win_.zapros('y'));
    }

    bar_setup();

    win_.object.resize(function () {
        bar_setup();
    });


//fixed row scroll
    //find fixed row
    function find_fixed_row() {

        if (left_bar.height() == content_zone.height()) {
            fix_row = false;
            scroll_row = false;
        } else {
            if (left_bar.height() > content_zone.height()) {
                fix_row = content_zone;
                scroll_row = left_bar;
            } else {
                fix_row = left_bar;
                scroll_row = content_zone;
            }
        }
    }

    func_find_fixed_row = function () {
        find_fixed_row();
    }
    //check scroll need
    function check_height(scrl) {
        //console.log(fix_row);
        if (fix_row) {
            if (fix_row.height() < scrl) {
                fix_row.addClass('fixed');
                scroll_row.addClass('scroll');
            } else {
                fix_row.removeClass('fixed scroll');
                scroll_row.removeClass('fixed scroll');
                return false;
            }
        } else {
            fix_row.removeClass('fixed scroll');
            scroll_row.removeClass('fixed scroll');
            return false;
        }
    }

    find_fixed_row();
    win_.object.resize(function () {
        find_fixed_row();
    })
    win_.object.load(function () {
        find_fixed_row();
    })

    win_.object.scroll(function () {
        check_height(win_.object.scrollTop() + win_.object.height());
    });

    $('body').append('<div id="subl_level_wrap"></div>');
    //$('.child_true a').click(function(e){
    //	e.preventDefault();
    /*$('.sub_level').remove();
     $('#subl_level_wrap').append('<div class="sub_level"></div>');
     $(this).siblings('ul').clone().appendTo('.sub_level');
     $('#left_bar_wrap').find('a').removeClass('hover_level1');
     $(this).addClass('hover_level1');*/
    //});
    $('#left_bar_wrap').find('a').on('mouseenter', function (e) {
        //e.preventDefault();
        if (!$(this).hasClass('back')) {
            $('.sub_level').remove();
            $('#left_bar_wrap').find('a').removeClass('hover_level1');
            if ($(this).siblings('ul').length > 0) {
                $('#subl_level_wrap').append('<div class="sub_level"></div>');
                $('.sub_level').last().append($(this).siblings('ul').clone());
                $(this).addClass('hover_level1');
            }
        }


    });
    $('.logo').css('cursor', 'pointer').on('click', function () {
        var url = window.location.href;
        if(url.indexOf('genty') + 1){
            window.location.href = 'http://genty.ru';
        }else{
            window.location.href = '/';
        }
    });

    $(document).on('mouseenter', '.sub_level ul li a', function (e) {
        e.preventDefault();
        if ($(this).parents('.sub_level').hasClass('show_sub')) {
            $('.sub_level').slice($(this).parents('.sub_level').index() + 1).remove();
            $(this).parents('.sub_level').removeClass('show_sub');
            $(this).parents('.sub_level').find('a').removeClass('this_link_open');

        }
        if ($(this).siblings('ul').length > 0) {
            $('#subl_level_wrap').append('<div class="sub_level"></div>');
            $('.sub_level').last().append($(this).siblings('ul').clone());
            $(this).parents('.sub_level').addClass('show_sub');
            $(this).addClass('this_link_open');
        }
    });

    $(document).on('mouseenter', '.sub_level:nth-child(2) ul li a', function (e) {
        e.preventDefault();
        $(this).each(function (key, value) {
            if ($(value).attr('href').indexOf('catalog') + 1 == 0) {
                var url = $(value).attr('href');
                $(value).attr('href', '/catalog/' + url);
                if (($(value).attr('href').split("/").length - 1) == 3) {
                    var string = $(value).attr('href'),
                        pattern = /[0-9]+/g,
                        matches = string.match(pattern),
                        parentLink = $('.sub_level:first-child ul li a.this_link_open').attr('href');
                    $(value).attr('href', parentLink + '/' + matches[0])
                }
            }
        });
    });

    $(document).on('mousemove', 'body', function (e) {
        if ($('.sub_level').length > 0) {
            //console.log(e.clientX + e.clientY);
            max_width = ($('.sub_level').length * $('.sub_level').eq(0).outerWidth(true)) + $('.sub_level').eq(0).outerWidth(true);
            if (max_width < e.clientX) {
                $('.sub_level').remove();
            }
        }

    });

    $('.click').on('click', function () {
        var arr = [];
        arr[0] = $('.begin_date').text();
        arr[1] = $('.end_date').text();
        var param = $.urlParam('status');
        if (strstr(window.location.href, 'comp_id') != false) {
            param = 'comp_id=' + param;
        }
        if (strstr(window.location.href, 'status') != false) {
            if (param == 12) {
                param = '1,2';
            }
            param = 'status=' + param;
        }
        $.ajax({
            type: "POST",
            url: "/manager/MagazineOrder/admin?" + param,
            data: {dates: arr}
        }).done(function (html) {
            if ($(html).find('.content-list .orders_list_item').length != 0) {
                $('.content-list').html($(html).find('.content-list').html());
            } else {
                $('.content-list').html('<span style="color: #5d5d5d; font-size: 1vw;"> Заказов за данный период не найдено=(</span>');

            }
        });
    });
    $('.click_admin').on('click', function () {
        var arr = [];
        arr[0] = $('.begin_date').text();
        arr[1] = $('.end_date').text();
        var param = $.urlParam('status');
        if (strstr(window.location.href, 'comp_id') != false) {
            param = 'comp_id=' + param;
        }
        if (strstr(window.location.href, 'status') != false) {
            if (param == 12) {
                param = '1,2';
            }
            param = 'status=' + param;
        }
        $.ajax({
            type: "POST",
            url: "/admin/orders/admin?" + param,
            data: {dates: arr}
        }).done(function (html) {
            if ($(html).find('#order_wrap .order_item').length != 0) {
                $('#order_wrap').html($(html).find('#order_wrap').html());
            } else {
                $('#order_wrap').html('<span style="color: #5d5d5d; margin-left: 1.6vw; font-size: 1vw;"> Заказов за данный период не найдено=(</span>');

            }
        });
    });

    $('.click_clients').on('click', function () {
        var arr = [];
        arr[0] = $('.begin_date').text();
        arr[1] = $('.end_date').text();
        var param = $.urlParam('price');
        if (strstr(window.location.href, 'price') != false) {
            param = '?price=' + param;
        } else {
            param = '';
        }
        $.ajax({
            type: "POST",
            url: "/admin/Clients/admin" + param,
            data: {dates: arr}
        }).done(function (html) {
            if ($(html).find('#klient_list .client_item').length != 0) {
                $('#klient_list').html($(html).find('#klient_list').html());
            } else {
                $('#klient_list').html('<span style="color: #5d5d5d; font-size: 1vw;"> Клиентов за данный период не найдено=(</span>');

            }
        });
    });

    $('.click_managers').on('click', function () {
        var arr = [];
        arr[0] = $('.begin_date').text();
        arr[1] = $('.end_date').text();
        $.ajax({
            type: "POST",
            url: "/admin/MagazineUsers/AdminManager",
            data: {dates: arr}
        }).done(function (html) {
            if ($(html).find('#manager_body .manager_item').length != 0) {
                $('#manager_body').html($(html).find('#manager_body').html());
                var allCost = String($(html).find('#manager_dop_info .money').html());
                allCost = allCost.replace(/(\s)+/g, "").replace(/(\d{1,3})(?=(?:\d{3})+$)/g, "$1 ");
                $('#manager_dop_info .money').html(allCost);
            } else {
                $('#manager_body').html('<span style="color: #5d5d5d; font-size: 1vw;"> Менеджеров за данный период не найдено=(</span>');

            }
        });
    });

    $('.click_abc').on('click', function () {
        var arr = [];
        arr[0] = $('.begin_date').text();
        arr[1] = $('.end_date').text();
        $.ajax({
            type: "POST",
            url: "/admin/abc/admin",
            data: {dates: arr}
        }).done(function (html) {
            if ($(html).find('#abc_body_table .item').length != 0) {
                $('#abc_body_table').html($(html).find('#abc_body_table').html());
                $('#abc_body_table .item').each(function () {
                    var allCost = String($(this).find('.row4 .money').html());
                    allCost = allCost.replace(/(\s)+/g, "").replace(/(\d{1,3})(?=(?:\d{3})+$)/g, "$1 ");
                    $(this).find('.row4 .money').text(allCost);
                });
            } else {
                $('#abc_body_table').html('<span style="color: #5d5d5d; font-size: 1vw;"> Товаров за данный период не найдено=(</span>');

            }
        });
    });

    $('.click_order_bonus').on('click', function () {
        var arr = [];
        arr[0] = $('.begin_date').text();
        arr[1] = $('.end_date').text();
        $.ajax({
            type: "POST",
            url: "/client/discount/admin",
            data: {dates: arr}
        }).done(function (html) {
            if ($(html).find('.set_bonus .order_info').length != 0) {
                $('.set_bonus').html($(html).find('.set_bonus').html());
            } else {
                $('.set_bonus').html('<span style="color: #5d5d5d; font-size: 1vw;"> Бонусов за данный период не найдено=(</span>');

            }
        });
    });
    $('.click_order_bonus_manager').on('click', function () {
        var arr = [];
        arr[0] = $('.begin_date').text();
        arr[1] = $('.end_date').text();
        var user_id = $('.user_id').val();
        $.ajax({
            type: "POST",
            url: "/admin/Clients/Discount/id/" + user_id,
            data: {dates: arr}
        }).done(function (html) {
            if ($(html).find('.set_bonus .order_info').length != 0) {
                $('.set_bonus').html($(html).find('.set_bonus').html());
            } else {
                $('.set_bonus').html('<span style="color: #5d5d5d; font-size: 1vw;"> Заказов за данный период не найдено=(</span>');

            }
        });
    });

    $('#abc_head_table .row2,#abc_head_table .row3,#abc_head_table .row4').on('click', function () {

        var arr = [];
        var sort = 'ASC';
        if ($(this).hasClass('sort-false')) {
            $(this).removeClass('sort-false');
            $(this).addClass('sort-true-ask');
        }
        if ($(this).hasClass('sort-true-ask')) {
            $(this).removeClass('sort-true-ask');
            $(this).addClass('sort-true-desc');
            sort = 'DESC'
        } else {
            $(this).removeClass('sort-true-desc');
            $(this).addClass('sort-true-ask');
            sort = 'ASC'
        }

        $.ajax({
            type: "POST",
            url: "/admin/abc/admin",
            data: {sort: $(this).data('name'), position: sort}
        }).done(function (html) {
            if ($(html).find('#abc_body_table .item').length != 0) {
                $('#abc_body_table').html($(html).find('#abc_body_table').html());
                $('#abc_body_table .item').each(function () {
                    var allCost = String($(this).find('.row4 .money').html());
                    allCost = allCost.replace(/(\s)+/g, "").replace(/(\d{1,3})(?=(?:\d{3})+$)/g, "$1 ");
                    $(this).find('.row4 .money').text(allCost);
                });
            } else {
                $('#abc_body_table').html('<span style="color: #5d5d5d; font-size: 1vw;"> Товаров за данный период не найдено=(</span>');

            }
        });

    });
    $('#manger_head .row2,#manger_head .row3,#manger_head .row4,#manger_head .row5').on('click', function () {

        var arr = [];
        var sort = 'ASC';
        if ($(this).hasClass('sort-false')) {
            $(this).removeClass('sort-false');
            $(this).addClass('sort-true-ask');
        }
        if ($(this).hasClass('sort-true-ask')) {
            $(this).removeClass('sort-true-ask');
            $(this).addClass('sort-true-desc');
            sort = 'ASC'
        } else {
            $(this).removeClass('sort-true-desc');
            $(this).addClass('sort-true-ask');
            sort = 'DESC'
        }

        $.ajax({
            type: "POST",
            url: "/admin/magazineUsers/adminManager",
            data: {sort: $(this).data('name'), position: sort}
        }).done(function (html) {
            if ($(html).find('#manager_body .manager_item').length != 0) {
                $('#manager_body').html($(html).find('#manager_body').html());
                $('#manager_body .manager_item').each(function () {
                    var allCost = String($(this).find('.row4 .money').html());
                    var allCost2 = String($(this).find('.row5 .money').html());
                    allCost = allCost.replace(/(\s)+/g, "").replace(/(\d{1,3})(?=(?:\d{3})+$)/g, "$1 ");
                    allCost2 = allCost2.replace(/(\s)+/g, "").replace(/(\d{1,3})(?=(?:\d{3})+$)/g, "$1 ");
                    $(this).find('.row4 .money').text(allCost);
                    $(this).find('.row5 .money').text(allCost2);
                });
            } else {
                $('#abc_body_table').html('<span style="color: #5d5d5d; font-size: 1vw;"> Товаров за данный период не найдено=(</span>');

            }
        });

    });
    function strstr(haystack, needle, bool) { // Find first occurrence of a string
        var pos = 0;
        pos = haystack.indexOf(needle);
        if (pos == -1) {
            return false;
        } else {
            if (bool) {
                return haystack.substr(0, pos);
            } else {
                return haystack.slice(pos);
            }
        }
    }

    $.urlParam = function (name) {
        var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
        if (results == null) {
            return null;
        }
        else {
            return results[1] || 0;
        }
    };
    $(document).on('click', '.selector_all', function () {
        if (oldDiscount == '') {
            oldDiscount = $('.summ_minus .money').text();
        }
        if (oldAllDiscount == '') {
            oldAllDiscount = $('.summ_plus .money').text();
        }
        var allSum = $('.summ .money').text();
        var t = allSum.replace(" ", "");
        var percent = 0;
        if ($('#pr_value').text() != '0') {
            percent += Number($('#pr_value').text());
        }
        if ($('.prepay').prop('checked') == false) {
            percent += Number($('.preDis').val());
        }
        if (percent > 70) {
            percent = 70
        }
        var discount = Number(t) * percent / 100;
        var AllOldDisc = $('.summ_plus .money').text();
        var NewDisc = Number(t) - Number(discount);
        if ($('.prepay').prop('checked') == false) {
            //$('.summ_minus .money').text(format_number(discount));
            $('.summ_plus .money').text(format_number(NewDisc));
            $('.prepay').prop('checked', true);
        } else {
            //$('.summ_minus .money').text(format_number(discount));
            $('.summ_plus .money').text(format_number(NewDisc));
            $('.prepay').prop('checked', false);
        }
        if ($(this).hasClass('active')) {
            $(this).removeClass('active bg_color_th');
        } else {
            $(this).addClass('active bg_color_th');
        }
        if ($(this).siblings('.is_active_checkbox').prop('checked')) {
            $(this).siblings('.is_active_checkbox').prop('checked', false);
        } else {
            $(this).siblings('.is_active_checkbox').prop('checked', true);
        }
    });
    $(document).on('click', '.bonus', function () {
        if ($(this).hasClass('active')) {
            $(this).removeClass('active bg_color_th');
        } else {
            $(this).addClass('active bg_color_th');
        }
        if ($(this).siblings('.is_active_checkbox').prop('checked')) {
            $(this).siblings('.is_active_checkbox').prop('checked', false);
        } else {
            $(this).siblings('.is_active_checkbox').prop('checked', true);
        }
    });
    $(document).on('click', '.bonus_check', function () {
        if ($(this).hasClass('active')) {
            $(this).removeClass('active bg_color_th');
        } else {
            $(this).addClass('active bg_color_th');
        }
        if ($(this).siblings('.is_active_checkbox').prop('checked')) {
            $(this).siblings('.is_active_checkbox').prop('checked', false);
        } else {
            $(this).siblings('.is_active_checkbox').prop('checked', true);
        }
    });

    function format_number(num) {
        var str = "" + num;
        var zeroPos = str.indexOf(".");
        if (zeroPos == -1) return str.replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1 ");
        str = num.toFixed(2).toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1 ");
        return str;
    }

    $('.bonus_check').on('click', function () {
        if ($('.bonus_input').prop('disabled') == true) {
            $('.bonus_input').prop('disabled', false)
        } else {
            $('.bonus_input').prop('disabled', true)
        }
    });
    $(document).on('input', '.bonus_input', function (e) {
        this.value = this.value.replace(/[^\d.]+|(\.\d{2})\d*$/g, '$1'); // Чистим и форматируем
    });

    $('.bonus_input').on('keydown', function (e) {
        if (this.value.includes('.') && e.keyCode === 190) {
            e.preventDefault();             // Нельзя вводить точку дважды
            return;
        }
        else if (!this.value.includes('.') && e.keyCode === 190) {
            if (this.selectionStart < this.value.length - 2) {
                e.preventDefault();           // Точку можно ввести только перед последней и предпоследней цифрой
                return;
            }
        }
    });

    $('.bonus_input').on('keyup', function (e) {
        if (Number($('.bonus_number').text()) < Number($(this).val())) {
            Number($(this).val($('.bonus_number').text()));
        }

        var maxBonus = $('.summ b .money');
        maxBonus = Number(maxBonus.text().replace(/\s+/g, ''));
        var auction_item = 0;
        $('.cart_item_line').filter(function () {
            return +($(this).data('disc')) != 0;
        }).each(function () {
            auction_item += Number($(this).find('.row5 .money').text().replace(/\s+/g, ''));
        });
        maxBonus -= auction_item;

        var discount = $('.summ_plus b .money');
        discount = Number(discount.text().replace(/\s+/g, ''));
        var minOrder = maxBonus * 0.3;
        var applyBonus = (discount - auction_item) - Number(minOrder);
        if ($(this).val() > applyBonus) {
            $(this).val(applyBonus);
            $('input[name="bonus_value"]').val(applyBonus);
            if ($('.maxDisc').css('display') == 'none') {
                $('.maxDisc').css('display', 'inline-block');
            }
        } else {
            if ($('.maxDisc').css('display') == 'inline-block') {
                $('.maxDisc').css('display', 'none');
            }
        }
        if ($('.summ_bonus').css('display') == 'none') {
            $('.summ_bonus').css('display', 'inline-block');
        }
        var allCost = String(discount - Number($(this).val()));
        allCost = allCost.replace(/(\s)+/g, "").replace(/(\d{1,3})(?=(?:\d{3})+$)/g, "$1 ");
        $('.summ_bonus .money').text(allCost);

        $('input[name="bonus_value"]').val($(this).val());
    });
});

