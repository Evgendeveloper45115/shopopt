function chek_pass() {
    if ($('.chek_p_1').val() != $('.chek_p_2').val()) {
        document.profile_settings.scnd_password.setCustomValidity('пароли не совпадают');
        return false;
    }
    return true;
}

var discount_tpl = '<div class="discont_form_line"><div class="row1"><input type="text" name="disc[{{index}}][summ]" required></div><div class="row2"><input type="text" name="disc[{{index}}][prozent]" required></div><div class="row3"><span class="del_line border_hover css_trans">Удалить</span></div></div>';


function getChar(event) {
    if (event.which == null) {
        if (event.keyCode < 32) return null;
        return String.fromCharCode(event.keyCode) // IE
    }

    if (event.which != 0 && event.charCode != 0) {
        if (event.which < 32) return null;
        return String.fromCharCode(event.which) // остальные
    }

    return null; // специальная клавиша
}

$(function () {
    function chek_data() {

        cur_year = $('#calendar_data').attr('data-year');
        cur_month = parseInt($('#calendar_data').attr('data-month'));

        for (key in data_arr) {
            if (parseInt(cur_year) == parseInt(data_arr[key]['data_y'])) {
                if (parseInt(cur_month) == (parseInt(data_arr[key]['data_m']) - 1)) {
                    $('#calendar2 tbody').find('span').each(function () {
                        cal_day = parseInt($(this).text());

                        if (cal_day == parseInt(data_arr[key]['data_d'])) {

                            $(this).addClass('bg_color_th').parent().addClass('active link_from_cal').attr('data-href', data_arr[key]['href']);
                        }
                    });

                }
            }
        }
    }

    function creat_content(flag_input, arr_input) {
        if (flag_input) {
            work_arr = arr_input;
        } else {
            work_arr = JSON.parse(content_item);
        }

        all_item = '<div class="msg_padding manager_catalog_all"><input id="add_item_finder" placeholder="Поиск"><br>';
        if (!work_arr.length > 0) {
            all_item += '<p class="oblom">Нет результатов поиска</p>';
        }
        for (var i = 0; i < work_arr.length; i++) {
            count = 0;
            $('.client_item').each(function () {
                if ($(this).children('.selector').hasClass('active')) {
                    arr_qwe = JSON.parse(document.getElementById('goods_' + $(this).attr('data-id')).innerHTML);
                    for (var j = 0; j < arr_qwe.length; j++) {
                        if (arr_qwe[j] == work_arr[i]['id']) {
                            count++;
                        }
                    }
                }
            });
            $('#tovari_tut').find('.goods_item').each(function () {
                if ($(this).attr('data-id') == work_arr[i]['id']) {
                    count++;
                }
            });


            if (count > 0) {
                button_text = 'Уже добавлен';
                button_class = 'in_list';
            } else {
                button_text = 'Добавить';
                button_class = '';
            }


            tpl_item = '<div class="goods_item css_trans shadow_hover"><div class="img_fon" data-img="' + work_arr[i]['img'] + '"" style="background-image:url(' + work_arr[i]['img'] + ')"></div><p>' + work_arr[i]['title'] + '</p><div class="price inline_style color_th"><span class="money">' + work_arr[i]['price'] + '</span><span class="rub_lt">a</span></div><p class="' + button_class + ' add_from_lt border_hover css_trans color_th_hover" data-id="' + work_arr[i]['id'] + '">' + button_text + '</p></div>';
            all_item += tpl_item;
        }
        all_item += '</div>';
        creat_lb_sv(all_item, $('#align_zone').outerWidth(true));
    }

    $('.goods_add').click(function (e) {
        e.preventDefault();
        creat_content(false);

    });

    function get_goods(value) {
        str = 'type=get_goods&value=' + value;
        $.ajax({
            type: 'post',
            url: '/site/Rectovar',
            data: str,
            success: function (msg) {
                arr_content = JSON.parse(msg);
                creat_content(true, arr_content);
            }
        });
    }

    $(document).on('blur', '#add_item_finder', function () {
        if ($(this).val() != '') {
            get_goods($(this).val());

        }
    });
    $(document).on('keydown', '#add_item_finder', function (e) {
        if (e.which == 13) {
            if ($(this).val() != '') {
                get_goods($(this).val());
            }
        }

    });

    $(document).on('click', '.add_from_lt', function () {
        if (!$(this).hasClass('in_list')) {
            id = $(this).attr('data-id');
            img_ = $(this).siblings('.img_fon').attr('data-img');
            title = $(this).siblings('p').text();
            price = $(this).siblings('.price').children('.money').text();
            new_item = '<div class="goods_item css_trans shadow_hover" data-id="' + id + '"><a href="#"><div class="img_fon" style="background-image:url(' + img_ + ')"></div><p>' + title + '</p></a><div class="price inline_style color_th"><span class="money">' + price + '</span><span class="rub_lt">a</span></div><button class="border_hover css_trans color_th_hover" data-id="' + id + '"><svg class="svg_stroke_color" preserveAspectRatio="xMidYMid meet" viewBox="0 0 194 194" xmlns="http://www.w3.org/2000/svg"><g display="inline"><line id="svg_7" y2="2.500002" x2="191.699994" y1="190.900002" x1="2.499994" stroke-linecap="null" stroke-linejoin="null"></line><line id="svg_6" y2="191.700002" x2="191.699994" y1="2.500002" x1="2.499994" stroke-linecap="null" stroke-linejoin="null"></line></g></svg>Удалить</button></div>';
            $('#tovari_tut').append(new_item);
            func_find_fixed_row();
            $(this).addClass('in_list').text('Уже добавлен');
        }

    });

    function creat_akz_content(flag_akz, input_val) {
        if (flag_akz) {
            work_arr = input_val;
        } else {
            work_arr = JSON.parse(contet_akz);
        }

        all_item = '<div class="msg_padding manager_catalog_all"><input id="add_item_finder2" placeholder="Поиск"><br>';
        if (!work_arr.length > 0) {
            all_item += '<p class="oblom">Нет результатов поиска</p>';
        }
        for (var i = 0; i < work_arr.length; i++) {
            count = 0;
            $('.client_item').each(function () {
                if ($(this).children('.selector').hasClass('active')) {
                    arr_qwe = JSON.parse(document.getElementById('akz_' + $(this).attr('data-id')).innerHTML);
                    for (var j = 0; j < arr_qwe.length; j++) {
                        if (arr_qwe[j] == work_arr[i]['id']) {
                            count++;
                        }
                    }
                }
            });

            $('#akz_full_wrp').find('.akz_special_item').each(function () {
                if ($(this).attr('data-id') == work_arr[i]['id']) {
                    count++;
                }
            });
            if (count > 0) {
                button_text = 'Уже добавлена';
                button_class = 'in_list';
            } else {
                button_text = 'Добавить';
                button_class = '';
            }
            tpl_item = '<div class="akz_special_item" data-id="' + work_arr[i]['id'] + '"><div class="img_fon"  data-img = "' + work_arr[i]['img'] + '"style="background-image:url(' + work_arr[i]['img'] + ')"></div><div class="demo_akz_txt_cont"><p class="demo_akz_title">' + work_arr[i]['title'] + '</p><div class="descr"><p>' + work_arr[i]['descr'] + '</p></div><div class="hid_content">' + work_arr[i]['content'] + '</div><button class="border_hover css_trans color_th_hover add_akz_to_ ' + button_class + '" data-id="' + work_arr[i]['id'] + '">' + button_text + '</button></div></div>';
            all_item += tpl_item;
        }
        all_item += '</div>';
        creat_lb_sv(all_item, $('#align_zone').outerWidth(true));
    }

    $(document).on('click', '.akz_add', function (e) {
        e.preventDefault();
        creat_akz_content(false);

    });

    function get_akz(value) {
        str = 'type="get_akz&value=' + value;
        $.ajax({
            type: 'post',
            url: '../ajax_manager2.php',
            data: str,
            success: function (msg) {
                arr_content = JSON.parse(msg);
                creat_akz_content(true, arr_content);
            }
        });
    }

    $(document).on('blur', '#add_item_finder2', function () {
        if ($(this).val() != '') {
            get_akz($(this).val());

        }
    });
    $(document).on('keydown', '#add_item_finder2', function (e) {
        if (e.which == 13) {
            if ($(this).val() != '') {
                get_akz($(this).val());
            }
        }

    });

    $(document).on('click', '.add_akz_to_', function () {
        if (!$(this).hasClass('in_list')) {
            id = $(this).attr('data-id');
            img_ = $(this).parents('.akz_special_item').children('.img_fon').attr('data-img');
            title = $(this).siblings('.demo_akz_title').text();
            descr = $(this).siblings('.descr').html();
            content = $(this).siblings('.hid_content').html();
            new_akz = '<div class="akz_special_item" data-id="' + id + '"><div class="img_fon" style="background-image:url(' + img_ + ')"></div><div class="demo_akz_txt_cont"><p class="demo_akz_title">' + title + '</p><div>' + descr + '</div><div class="hid_content">' + content + '</div><button class="show_akz_lt border_hover css_trans color_th_hover">Подробнее <span class="color_th">→</span></button><button class="border_hover css_trans color_th_hover del_akz" data-id="' + id + '"><svg class="svg_stroke_color" preserveAspectRatio="xMidYMid meet" viewBox="0 0 194 194" xmlns="http://www.w3.org/2000/svg"><g display="inline"><line id="svg_7" y2="2.500002" x2="191.699994" y1="190.900002" x1="2.499994" stroke-linecap="null" stroke-linejoin="null"></line><line id="svg_6" y2="191.700002" x2="191.699994" y1="2.500002" x1="2.499994" stroke-linecap="null" stroke-linejoin="null"></line></g></svg>Удалить</button></div></div>';

            $('#akz_full_wrp').append(new_akz);
            func_find_fixed_row();
            $(this).addClass('in_list').text('Уже добавлена');
        }
    });


//manager_special
    if ($('.wrap_content').length > 0) {
        $(document).on('click', '.goods_item button', function (e) {
            e.preventDefault();
            /*str = 'type=remove_recomend&id='+$(this).attr('data-id');
             $.ajax({
             type:'post',
             url:'../ajax.php',
             data:str,
             success:function(msg){
             alert(msg);
             }
             });*/
            $(this).parent().remove();
        });

        $(document).on('click', '.akz_special_item .del_akz', function (e) {
            e.preventDefault();
            /*str = 'type=remove_akz&id='+$(this).attr('data-id');
             $.ajax({
             type:'post',
             url:'../ajax.php',
             data:str,
             success:function(msg){
             alert(msg);
             }
             });*/
            $(this).parents('.akz_special_item').remove();

        });
    }


    $('#manager_special_klient').find('.selector').click(function () {
        if ($(this).hasClass('active')) {
            $(this).removeClass('active bg_color_th');
        } else {
            $(this).addClass('active bg_color_th');
        }
    });

    $('#save_client').click(function (e) {
        e.preventDefault();
        arr_id = [];
        good_arr = [];
        akz_arr = [];
        $('.selector').each(function () {
            if ($(this).hasClass('active')) {
                arr_id.push($(this).parent().attr('data-id'));
            }
        });
        $('#tovari_tut .goods_item').each(function () {
            good_arr.push($(this).find('button').attr('data-id'));
        });

        $('#akz_full_wrp .akz_special_item').each(function () {
            akz_arr.push($(this).find('.del_akz').attr('data-id'));
        });

        send_str = 'type=select_klient_manager&data=' + arr_id.join() + '&good_arr=' + good_arr.join() + '&akz_arr=' + akz_arr.join();

        $.ajax({
            type: 'post',
            url: '/site/Save2',
            data: send_str,
            success: function (msg) {

                creat_lb_sv('<div style="height:200px;"><div class="vert_style" style="width:80%"><img style="display:inline_block;" src="/html_source/img/cart_ok.png" width="75px"><br/>' + msg + '</div><div class="vert_style"></div></div>');
            }
        });
    });

    $('#manager_special_klient').find('.del_item').click(function (e) {
        e.preventDefault();
        str = 'tpye=del_klient_manager&data=' + $(this).parent().attr('data-id');
        $.ajax({
            type: 'post',
            url: '../ajax.php',
            data: str,
            success: function (msg) {
                alert(msg);
            }
        });
        $(this).parent().remove();
    });


    $(document).on('click', '.show_akz_lt', function (e) {
        e.preventDefault();
        title = $(this).siblings('.demo_akz_title').text();
        content = $(this).siblings('.hid_content').html();
        akz_html_lt = '<div class="msg_padding"><p class="color_th title_in_lt">' + title + '</p><div class="akz_content_lt">' + content + '</div></div>';
        creat_lb_sv(akz_html_lt, 900);
    });


    $('#save_client_page').click(function (e) {
        e.preventDefault();
        arr_id = [];
        good_arr = [];
        akz_arr = [];

        $('#tovari_tut .goods_item').each(function () {
            good_arr.push($(this).find('button').attr('data-id'));
        });

        $('#akz_full_wrp .akz_special_item').each(function () {
            akz_arr.push($(this).find('.del_akz').attr('data-id'));
        });

        send_str = 'type=manager_client_page&data=' + arr_id.join() + '&good_arr=' + good_arr.join() + '&akz_arr=' + akz_arr.join();

        $.ajax({
            type: 'post',
            url: '../ajax.php',
            data: send_str,
            success: function (msg) {
                creat_lb_sv('<p class="msg_padding">' + msg + '</p>');
            }
        });
    });


//manager_special end

//manager_settings start
    $('.chek_p_2').blur(function () {
        if ($('.chek_p_1').val() != $('.chek_p_2').val()) {
            $('.chek_p_2').addClass('failed').removeClass('done');
            $('.chek_p_1').addClass('failed').removeClass('done');

        }
    });

    $('#save_new_pass').click(function () {
        if ($('.chek_p_1').val() != $('.chek_p_2').val()) {
            $('.chek_p_2').get(0).setCustomValidity('пароли не совпадают');
        } else {
            $('.chek_p_2').get(0).setCustomValidity('');
        }
    });

//manager_promo

    $('#promo_file_box input').on("change", function () {
        document.promo_file_apload.submit();
    });

    $('#promo_table').find('.row5').children('svg').click(function () {
        str_to_send = 'type=del_file_from_promo&name_file=' + $(this).parent().siblings('.row1').children('p').text();
        if (confirm('Вы точно хотите удалить?')) {
            $.ajax({
                type: 'post',
                url: '/site/Ajaxt',
                data: str_to_send,
                success: function (msg) {
                    alert(msg);
                }
            });
            $(this).parents('.table_tr').remove();
        }
    });

    $('.p_box').click(function () {
        /*$('.p_box').removeClass('');
         $(this).addClass('active bg_color_th');
         if($(this).hasClass('all_select')){
         $('.selector').addClass('active');
         }else{
         $('.selector').removeClass('active');
         }*/
        if ($(this).hasClass('active')) {
            $(this).removeClass('active bg_color_th');
            $('.selector').removeClass('active bg_color_th');
        } else {
            $(this).addClass('active bg_color_th');
            $('.selector').addClass('active bg_color_th');
        }
    });

    $('#manager_promo_klient').find('.del_item').click(function (e) {
        e.preventDefault();
        str = 'tpye=del_manager_promo_klient&data=' + $(this).parent().attr('data-id');
        $.ajax({
            type: 'post',
            url: '../ajax.php',
            data: str,
            success: function (msg) {
                alert(msg);
            }
        });
        $(this).parent().remove();
    });

    $('#manager_promo_klient').find('.selector').click(function () {
        $('.p_box').removeClass('active bg_color_th');
        if ($(this).hasClass('active')) {
            $(this).removeClass('active bg_color_th');
        } else {
            $(this).addClass('active bg_color_th');
        }
    });

    $('#promo_save').click(function (e) {
        e.preventDefault();
        arr_id = [];
        $('.selector').each(function () {
            if ($(this).hasClass('active')) {
                arr_id.push($(this).parent().attr('data-id'));
            }
        });

        send_str = 'type=save_promo_klient&data=' + arr_id.join();

        $.ajax({
            type: 'post',
            url: '/site/Ajaxt2',
            data: send_str,
            success: function (msg) {
                creat_lb_sv('<div style="height:200px;"><div class="vert_style" style="width:80%"><img style="display:inline_block;" src="/html_source/img/cart_ok.png" width="75px"><br/>' + msg + '</div><div class="vert_style"></div></div>');
            }
        });
    });

    $('#promo_send_mail').click(function (e) {
        e.preventDefault();
        arr_id = [];
        $('.selector').each(function () {
            if ($(this).hasClass('active')) {
                arr_id.push($(this).parent().attr('data-id'));
            }
        });

        send_str = 'type=email_promo_klient&data=' + arr_id.join();

        $.ajax({
            type: 'post',
            url: '/site/Ajaxt3',
            data: send_str,
            success: function (msg) {
                creat_lb_sv('<div style="height:200px;"><div class="vert_style" style="width:80%"><img style="display:inline_block;" src="/html_source/img/cart_ok.png" width="75px"><br/>' + msg + '</div><div class="vert_style"></div></div>');
            }
        });
    });
    /*order*/

    $('.hid_cd_status p').click(function () {
        $(this).siblings('input').click();
        creat_lb_sv('<div style="height:200px;"><div class="vert_style" style="width:80%"><img style="display:inline_block;" src="/html_source/img/cart_ok.png" width="75px"><br/>Статус заказа изменен, текущий статус: ' + $(this).text() + '</div><div class="vert_style"></div></div>');
        setTimeout(function () {
            document.cd_status.submit();
        }, 1500);

    });
    $('#cd_order_status').before('<div class="click_to_hide"></div>');
    $('.click_to_hide').click(function () {
        $('#hid_status').animate({'opacity': 0}, 300, function () {
            $('#hid_status').hide();
        });
        $(this).hide();
        $('#cd_order_status').removeClass('active')
    });
    $('#cd_order_status').click(function (e) {
        e.preventDefault();
        if ($(this).hasClass('active')) {
            $(this).removeClass('active');
            $('#hid_status').animate({'opacity': 0}, 600, function () {
                $(this).hide();
            });
        } else {
            $(this).addClass('active');
            $('.click_to_hide').show();
            $('#hid_status').css({
                'display': 'block',
                'opacity': 0,
                'top': $(this).position().top + $(this).outerHeight(true),
                'left': (($(this).outerWidth(true) / 2) + $(this).position().left) - $('#hid_status').width() / 2
            }).animate({'opacity': 1}, 600);
        }
    });

    /*calendar cart*/
//calendar
    if ($('#marker_data').length > 0) {
        function Calendar2(id, year, month) {
            var Dlast = new Date(year, month + 1, 0).getDate(),
                D = new Date(year, month, Dlast),
                DNlast = new Date(D.getFullYear(), D.getMonth(), Dlast).getDay(),
                DNfirst = new Date(D.getFullYear(), D.getMonth(), 1).getDay(),
                calendar = '<tr>',
                month = ["Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"];
            if (DNfirst != 0) {
                for (var i = 1; i < DNfirst; i++) calendar += '<td>';
            } else {
                for (var i = 0; i < 6; i++) calendar += '<td>';
            }


            for (var i = 1; i <= Dlast; i++) {
                if (i == new Date().getDate() && D.getFullYear() == new Date().getFullYear() && D.getMonth() == new Date().getMonth()) {
                    calendar += '<td class="today "><span class="css_trans bg_color_th_hover">' + i + '</span>';
                } else {
                    if ((i < new Date().getDate() && D.getFullYear() <= new Date().getFullYear() && D.getMonth() <= new Date().getMonth()) || ( D.getFullYear() <= new Date().getFullYear() && D.getMonth() < new Date().getMonth()) || ( D.getFullYear() < new Date().getFullYear())) {
                        calendar += '<td class=block><span  class="css_trans">' + i + '</span>';
                    } else {
                        /*if((i-1) == new Date().getDate() && D.getFullYear() == new Date().getFullYear() && D.getMonth() == new Date().getMonth()){
                         calendar += '<td class="next_d "><span  class="css_trans">' + i+'</span>';
                         }else{*/
                        calendar += '<td class="work "><span  class="css_trans bg_color_th_hover">' + i + '</span>';
                        //}
                    }

                }
                if (new Date(D.getFullYear(), D.getMonth(), i).getDay() == 0) {
                    calendar += '<tr>';
                }
            }
            for (var i = DNlast; i < 7; i++) calendar += '<td>&nbsp;';
            document.querySelector('#' + id + ' tbody').innerHTML = calendar;
            document.querySelector('#' + id + ' thead td:nth-child(2)').innerHTML = month[D.getMonth()] + ' ' + D.getFullYear();
            document.querySelector('#' + id + ' thead td:nth-child(2)').dataset.month = D.getMonth();
            document.querySelector('#' + id + ' thead td:nth-child(2)').dataset.year = D.getFullYear();
            if (document.querySelectorAll('#' + id + ' tbody tr').length < 6) {  // чтобы при перелистывании месяцев не "подпрыгивала" вся страница, добавляется ряд пустых клеток. Итог: всегда 6 строк для цифр
                document.querySelector('#' + id + ' tbody').innerHTML += '<tr><td>&nbsp;<td>&nbsp;<td>&nbsp;<td>&nbsp;<td>&nbsp;<td>&nbsp;<td>&nbsp;';
            }
        }

        Calendar2("calendar2", new Date().getFullYear(), new Date().getMonth());
        chek_data();
        // переключатель минус месяц
        document.querySelector('#calendar2 thead tr:nth-child(1) td:nth-child(1)').onclick = function () {
            Calendar2("calendar2", document.querySelector('#calendar2 thead td:nth-child(2)').dataset.year, parseFloat(document.querySelector('#calendar2 thead td:nth-child(2)').dataset.month) - 1);
            chek_data();
        }

        // переключатель плюс месяц
        document.querySelector('#calendar2 thead tr:nth-child(1) td:nth-child(3)').onclick = function () {
            Calendar2("calendar2", document.querySelector('#calendar2 thead td:nth-child(2)').dataset.year, parseFloat(document.querySelector('#calendar2 thead td:nth-child(2)').dataset.month) + 1);
            chek_data();
        }

        $(document).on('click', '#calendar2 .work', function (e) {
            e.preventDefault();
            if (!$(this).hasClass('active')) {
                $('#calendar2 .work').removeClass('active');
                $('#calendar2 .work').each(function () {
                    $(this).children('span').removeClass('bg_color_th');
                });
                $(this).addClass('active');
                $(this).children('span').addClass('bg_color_th');
            }
        });

        $('#hid_marker_form').find('button').click(function (e) {
            e.preventDefault();
            if ($('#calendar2').find('.active').length > 0) {
                day = $('#calendar2').find('.active').text();
            } else {
                day = $('#calendar2').find('.today').text();
            }
            month = parseInt($('#calendar2 thead td:nth-child(2)').attr('data-month')) + 1;
            year = $('#calendar2 thead td:nth-child(2)').attr('data-year');
            if (parseInt(day) < 10) {
                day = '0' + day;
            }
            if (parseInt(month) < 10) {
                month = '0' + month;
            }
            document.manager_order_data.data_manager_order.value = year + '-' + month + '-' + day;
            document.manager_order_data.submit();
        });

        $('#marker_data').click(function (e) {
            e.preventDefault();
            if ($(this).hasClass('active')) {
                $(this).removeClass('active');
                $('#hid_marker_form').animate({'opacity': 0}, 600, function () {
                    $(this).hide();
                });
            } else {
                $(this).addClass('active');
                $('#hid_marker_form').css({
                    'display': 'block',
                    'opacity': 0,
                    'top': $(this).position().top + $(this).outerHeight(true),
                    'left': (($(this).outerWidth(true) / 2) + $(this).position().left) - $('#hid_marker_form').outerWidth(true) / 2
                }).animate({'opacity': 1}, 600);
            }
        });
    }
    ;
    /*calendar cart end*/


    $('#manager_documents_sort select').on("change", function () {
        document.manager_view_user_document.submit();
    });

    $('#manager_documents_upload input').on("change", function () {
        document.manager_documents_upload.submit();
    });


    $('.manager_finder_box input').blur(function () {
        if ($(this).val() != '') {
            document.manager_finder.submit();
        }
    });

    /*client_page*/
    $('.manager_all_file_in').hover(
        function () {
            $(this).find('a').addClass('border_color');
        },
        function () {
            $(this).find('a').removeClass('border_color');
        }
    );

    $('form[name=doc_in]').find('input').on('change', function () {
        document.doc_in.submit();
    });
    $('form[name=promo_in]').find('input').on('change', function () {
        document.promo_in.submit();
    });

    if ($('.w_c').length > 0) {
        $(document).on('click', '.goods_item button', function (e) {
            e.preventDefault();
            /*str = 'type=cl_page_remove_recomend&id='+$(this).attr('data-id');
             $.ajax({
             type:'post',
             url:'../ajax.php',
             data:str,
             success:function(msg){
             alert(msg);
             }
             });*/
            $(this).parent().remove();
        });

        $(document).on('click', '.akz_special_item .del_akz', function (e) {
            e.preventDefault();
            //str = 'type=cl_page_remove_akz&id='+$(this).attr('data-id');
            /*$.ajax({
             type:'post',
             url:'../ajax.php',
             data:str,
             success:function(msg){
             alert(msg);
             }
             });*/
            $(this).parents('.akz_special_item').remove();

        });
    }


    if ($('#discont_table').length > 0) {


        if ($('#form_disc').children('.discont_form_line').length == 0) {
            str = discount_tpl;
            str = str.split('{{index}}').join('0');
            $('#form_disc').prepend(str);
            $('#form_disc').find('.del_line').hide();
        }

        $('#add_disc_line').click(function (e) {
            e.preventDefault();
            $('#form_disc').find('.del_line').show();
            index = $('#form_disc').children('.discont_form_line').length;
            str = discount_tpl;
            str = str.split('{{index}}').join(index);
            last_item = $('#form_disc').children('.discont_form_line').eq(-1);
            $(str).insertAfter(last_item);
        });

        $(document).on('click', '.del_line', function () {
            $(this).parents('.discont_form_line').remove();
            if ($('.discont_form_line').length == 1) {
                $('#form_disc').find('.del_line').hide();
            }
            else {
                $('#form_disc').find('.del_line').show();
            }
        });


        $(document).on('keypress', '.discont_form_line input', function (e) {
            e = e || event;
            if (e.ctrlKey || e.altKey || e.metaKey) return;
            var chr = getChar(e);
            if (chr == null) return;
            if (chr < '0' || chr > '9') {
                return false;
            }
        });
    }

    $('.manager_view_file_line').find('.row7 svg').click(function () {
        if (confirm('Вы точно хотите удалить ?')) {

            str = 'type=delete_file&id=' + $(this).attr('data-id');
            $.ajax({
                type: 'post',
                url: '/site/delete',
                data: str
            });
            $(this).parents('.manager_view_file_line').remove();
        }

    });

    $('#promo_table').find('.table_body').find('.row1').click(function () {
        var str = $(this).children('img').attr('src');
        var example = str.split('.');
        if (example[1] == 'docx') {
            var frame = "<iframe src='https://view.officeapps.live.com/op/view.aspx?src=http://abc_test.opt-project.ru/upload/57de7b034f9e5.docx' width='1366px' height='623px' frameborder='0'>This is an embedded <a target='_blank' href='http://office.com'>Microsoft Office</a> document, powered by <a target='_blank' href='http://office.com/webapps'>Office Online</a>.</iframe>";
            creat_lb_sv(frame, '100%');

        } else {
            creat_lb_sv('<img src=' + $(this).children('img').attr('src') + ' style="width:100%;display:block;">');
        }
    });

    $('.avatar_line').find('input').on('change', function () {
        var reader = new FileReader();
        reader.img_obj = $(this);
        reader.onload = function (e) {
            reader.img_obj.siblings('img').attr('src', e.target.result);
        }
        reader.readAsDataURL(this.files[0]);
    });
    $(document).on('click', '.delete_item', function () {
        $(this).closest('.cart_item_line').remove();
        var items = {};
        var used_bonus = 0;
        $('.cart_item_line').each(function () {
            used_bonus += Number($(this).find('.row5 .set_bonus').text());
            $('#cart_item_table .bonus_val').val(used_bonus);
            if ($(this).hasClass('edit_item')) {
                items[$(this).data('id')] = $(this).data('id');
            } else {
                items[$(this).data('id')] = $(this).data('id');
            }
        });

        $.ajax({
            type: "POST",
            url: "/manager/MagazineOrder/editOrder",
            data: {
                is_del: 'true',
                items: items,
                bonus: $('.bonus_val').val()
            },
            success: function (msg) {
                $('#cart_item_table').html($(msg).find('#cart_item_table').html());
                $('.select_money').each(function (key, elm) {
                    elm.innerText = elm.innerText.replace(/(\s)+/g, "").replace(/(\d{1,3})(?=(?:\d{3})+)/, "$1 ");
                });
                $('#cart_item_table .bonus_val').val(used_bonus);
            }
        });

    });
    $(document).on('click', '#save_result', function () {
        var items = {};

        $('.cart_item_line').each(function () {
            if ($(this).hasClass('edit_item')) {
                items[$(this).data('id')] = [$(this).find('.row4 a').text(), $(this).find('.row3 span:first-child').text()];
            } else {
                items[$(this).data('id')] = ['no_edit', $(this).find('.row3 span:first-child').text()];
            }
        });
        $.ajax({
            type: "POST",
            url: "/manager/MagazineOrder/editOrder",
            data: $.isEmptyObject(items) ? {id: $.urlParam()} : {
                items: items,
                bonus: $('.bonus_val').val()
            },
            success: function (msg) {
                var json = $.parseJSON(msg);
                if (msg) {
                    $('#save_result').text('данные сохранены!');
                    setTimeout(function () {
                        $('#save_result').text('Сохранить');
                        if (json.success == 'redirect') {
                            window.location.href = '/manager/MagazineOrder/admin';
                        }
                    }, 2000);
                }
            }
        })

    });
    $.urlParam = function () {
        return parseInt(window.location.href.replace(/\D+/g, ""));
    };
});
function update_discount(summ) {
    /*
     discont_rules[][0] - discount_value
     discont_rules[][1] - discount_rule
     */
    var discount_value = 0,
        i = discont_rules.length,
        summa = 0;
    for (i = 0; i < discont_rules.length; i++) {
        $('.cart_item_line').each(function () {
            var bonusCost = $(this).find('.row5 .money').text();
            bonusCost = bonusCost.replace(" ", "");
            summa += Number(bonusCost);
        });
        if (summa >= discont_rules[i][1]) {
            discount_value = discont_rules[i][0] / 100;
            $('#pr_value').text(discont_rules[i][0]);
        }
    }
    if (discount_value == 0) {
        $('#pr_value').text(0);
        if (discont_rules.length > 0) {
            $('.money_disc').text('0')
        } else {
            $('.discont_line').eq(1).hide();
        }
        return false;
    } else {
        return Number(discount_value)
    }

}

