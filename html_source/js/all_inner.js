var save_sort_href;

function animate_to_cart($el_start, $el_finish) {
    scroll = $('#conten_zone').scrollTop();
    anim_start_x = $el_start.offset().top + scroll;
    anim_start_y = $el_start.offset().left + scroll;
    anim_width = $el_start.outerWidth();
    anim_height = $el_start.outerHeight(true);

    finish_x = $el_finish.offset().top;
    finish_y = $el_finish.offset().left;

    $('body').append('<div class="animation_cart bg_color_th"></div>');

    $('.animation_cart').css({'top': anim_start_x, 'left': anim_start_y, 'width': anim_width, 'height': anim_height})
        .animate({'top': finish_x, 'left': finish_y, 'width': 0, 'height': 0}, 500, function () {
            $(this).remove();
        });

}

function getChar(event) {
    if (event.which == null) {
        if (event.keyCode < 32) return null;
        return String.fromCharCode(event.keyCode) // IE
    }

    if (event.which != 0 && event.charCode != 0) {
        if (event.which < 32) return null;
        return String.fromCharCode(event.which) // остальные
    }

    return null; // специальная клавиша
}

function format_number(num) {
    var str = "" + num;
    var zeroPos = str.indexOf(".");
    if (zeroPos == -1) return str.replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1 ");
    str = num.toFixed(2).toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1 ");
    return str;
}
function update_ico_cart(price, count) {

    last_count = parseInt($('#cart_count').text());
    new_count = last_count + parseInt(count);
    $('#cart_count').text(new_count);

    last_price = parseInt(($('#align_cart_info').find('.money').text()).replace(/\s+/g, ''));
    new_price = (last_price + (count * parseInt(price))).toString();
    new_price = format_number(new_price);
    $('#align_cart_info').find('.money').text(new_price);
}
function set_ico_cart(price, count) {
    $('#cart_count').text(count);
    $('#align_cart_info').find('.money').text(format_number(price));
}

function ajax_update_cart(type, id, value) {
    str_to_send = 'type=' + type + '&id=' + id + '&value=' + value;
    $('body').addClass('cursor');
    $.ajax({
        type: 'post',
        url: '/catalog/ajax',
        data: str_to_send,
        success: function (msg) {
            //creat_lb_sv(msg);
            $('body').removeClass('cursor');
        }
    });
}

function order_ok() {
    str = '<div id="msg_ok_cart"><a href="/client/MagazineOrder/admin"><img src="/html_source/img/cart_ok.png"></a><p>Спасибо, ваш заказ подтвержден.</p></div>';
    creat_lb_sv(str, 600);
}

function order_fail(msg) {
    if (!msg) {
        msg = 'Чтобы оформить заказ, вам необходимо <br/><a href="#" class="log_in_link color_th">войти в личный кабинет</a> или<br/> <a href="#" class="sign_in_link color_th">зарегистрироваться</a>';
    }
    str = '<div id="msg_ok_cart"><img src="../img/convert_to_svg.png"><p>' + msg + '</p></div>';
    creat_lb_sv(str, 600);


}

function log_in(msg) {
    if (msg) {
        msg = '<p class="error_txt">' + msg + '</p>';
    } else {
        msg = '';
    }
    html_tpl = '<div id="log_form_wrap" class="msg_padding"><div class="error"></div><p>Вход на сайт</p>' + msg + '<form action="" method="POST" name="log_in"><input type="text" name="LoginForm[username]" class="input_log css_trans" placeholder="Логин / e-mail"><input type="password" name="LoginForm[password]" class="input_log css_trans" placeholder="Пароль"><button class="bg_color_th submit_log">Войти</button></form><a href="#" class="recover_pass color_th_hover css_trans">Забыли пароль?</a></div><div class="dop_autorization_box"><p>Еще нет аккаунта</p><a href="#" class="sig_in_link color_th_hover css_trans">Зарегистрируйтесь →</a></div>';
    $(window).resize(function () {
        creat_lb_sv(html_tpl, (win_.zapros('x') * 444 / 1920));
    });
    creat_lb_sv(html_tpl, (win_.zapros('x') * 444 / 1920));
    $('.bg_color_th.submit_log').on('click', function (e) {
        e.preventDefault();

        $.ajax({
            url: '/site/login',
            type: 'post',
            dataType: 'json',
            data: $('form[name="log_in"]').serialize(),
            success: function (msg) {
                $('#log_form_wrap div.error').html('');
                if (msg.error) {
                    if (msg.error == 'Пользователь не подтверждён!') {
                        $('#log_form_wrap div.error').html(msg.error);
                    } else {
                        $('#log_form_wrap div.error').html('неправильный логин или пароль');
                    }
                } else {
                    if (msg.redirect_url != '') {
                        window.location.href = msg.redirect_url;
                    }
                }
            }
        });
    });
}

function recover_pas(msg) {
    if (msg) {
        msg = '<p class="error_txt">' + msg + '</p>';
    } else {
        msg = '';
    }
    html_tpl = '<div id="log_form_wrap" class="msg_padding"><p>Забыли пароль?</p><div class="text"></div>' + msg + '<form action="" method="POST" name="recover_pas"><input type="text" name="mail" placeholder="Введите e-mail" class="pop_input css_trans"><button class="submit_recover bg_color_th">Востановить пароль</button></form></div>';
    creat_lb_sv(html_tpl, (win_.zapros('x') * 444 / 1920));
    $('.submit_recover.bg_color_th').on('click', function (e) {
        e.preventDefault();

        $.ajax({
            url: '/site/ResetPass',
            type: 'post',
            dataType: 'json',
            data: $('form[name="recover_pas"]').serialize(),
            success: function (msg) {
                $('#log_form_wrap div.text').text('');
                if (msg != 'ok') {
                    $('#log_form_wrap div.text').text(msg.msg);
                } else {
                    $('.submit_recover.bg_color_th').text('ОК');
                    $('#log_form_wrap div.text').text(msg.msg);
                }
            }
        });
    });
}

function sign_in(msg) {
    if (msg) {
        msg = '<p class="error_txt">' + msg + '</p>';
    } else {
        msg = '';
    }
    html_tpl = '<div id="log_form_wrap" class="msg_padding"><p>Регистрация</p><form action="" method="POST" name="new_user"><input type="text" name="MagazineUsers[user_name]" placeholder="Имя" class="pop_input css_trans new_user_margin"><input type="text" name="MagazineUsers[user_surname]" placeholder="Фамилия" class="pop_input css_trans new_user_margin"><input type="text" name="MagazineUsers[user_email]" placeholder="Ваш e-mail" class=" pop_input css_trans new_user_margin"><input type="text" name="MagazineUsers[user_phone]" placeholder="Номер телефона" class="phone_mask pop_input css_trans new_user_margin"><input type="text" name="MagazineUsers[user_password]" placeholder="Пароль" class="pop_input css_trans new_user_margin"><input type="text" name="MagazineCompany[name_company]" placeholder="Название компании" class="pop_input css_trans new_user_margin"><input type="text" name="MagazineCompany[inn]" placeholder="ИНН" class="pop_input css_trans new_user_margin"><div class="text"></div><button class="submit_new_user bg_color_th">Зарегистрироваться</button></form></div><div class="dop_autorization_box"><p>Уже есть аккаунт</p><a href="#" class="log_in_link color_th_hover css_trans">Авторизуйтесь →</a></div>';

    creat_lb_sv(html_tpl, (win_.zapros('x') * 444 / 1920));
    $('#log_form_wrap').find('.phone_mask').each(function () {
        if ($(this).attr('placeholder') == 'Номер телефона') {
            $(this).mask("+7 (999) 999-9999");
        }
    });
}


var discount_value = 0;
function update_discount(summ) {
    /*
     discont_rules[][0] - discount_value
     discont_rules[][1] - discount_rule
     */
    discount_value = 0;
    i = discont_rules.length;
    for (i = 0; i < discont_rules.length; i++) {
        if (summ >= discont_rules[i][1]) {
            discount_value = discont_rules[i][0] / 100;
            $('#pr_value').text(discont_rules[i][0]);
            if ((i + 1) < discont_rules.length) {
                $('.discont_line').eq(1).show();
                $('.discont_line').eq(1).find('.money').text(format_number(discont_rules[i + 1][1]));
                $('.discont_line').eq(1).find('#next_pr').children('span').text(discont_rules[i + 1][0]);
            } else {
                $('.discont_line').eq(1).hide();
            }
        }
    }
    if (discount_value == 0) {
        $('#pr_value').text(0);
        if (discont_rules.length > 0) {
            $('.discont_line').eq(1).show();
            $('.discont_line').eq(1).find('.money').text(format_number(discont_rules[0][1]));
            $('.discont_line').eq(1).find('#next_pr').children('span').text(discont_rules[0][0]);

        } else {
            $('.discont_line').eq(1).hide();
        }
    }

}


function update_cart() {
    if ($('.cart_item_line').length > 0) {
        total_summ = 0;
        count_all = 0;
        $('.cart_item_line').each(function () {
            if ($(this).data('disc') == 0) {
                def_price = parseFloat($(this).find('.row3').children('.money').text().replace(/\s+/g, '')).toFixed(2);
                count = parseInt($(this).find('.row4').children('input').val());
                count_all = count_all + count;
                line_summ = def_price * count;
                $(this).attr('data-summ', line_summ);
                total_summ = total_summ + line_summ;
            }
        });

        update_discount(total_summ);
        new_total = 0;
        summ_minus = 0;
        $('.cart_item_line').each(function () {
            if ($(this).data('disc') == 0) {
                clear_summ = parseFloat($(this).attr('data-summ')).toFixed(2);
                summ_minus += Math.round(clear_summ * discount_value);
                new_value = (clear_summ - (clear_summ * discount_value));
                new_total = Math.round(new_total + new_value);
                $(this).find('.row5').children('.money').text(format_number($(this).attr('data-summ')));
            } else {
                def_price = parseFloat($(this).find('.row3').children('.money').text().replace(/\s+/g, '')).toFixed(2);
                count = parseInt($(this).find('.row4').children('input').val());
                line_summ = def_price * count;
                total_summ += line_summ;
                new_total += line_summ;
            }
        });

        var percent = null;
        if ($('#pr_value').text() != '0') {
            percent += Number($('#pr_value').text());
        }
        if ($('.prepay').prop('checked') == true) {
            percent += Number($('.preDis').val());
            var discount = total_summ * percent / 100;
            new_total = total_summ - discount;
            summ_minus = discount;
        }
        $('.cart_itog').find('.summ').find('.money').text(format_number(total_summ));
        $('.cart_itog').find('.summ_minus').find('.money').text(format_number(summ_minus));
        $('.cart_itog').find('.summ_plus').find('.money').text(format_number(new_total));
        set_ico_cart(new_total, count_all);

    } else {
        $('.cart_itog').find('.money').text(0);
        set_ico_cart(0, 0);
    }
}


$(function () {
//main script
    if ($('#main_sl_wrap').length > 0) {

        if (win_.zapros('x') < 760) {
            $('#main_sl_wrap').height(win_.zapros('x') * 0.5);
        } else {
            $('#main_sl_wrap').height(win_.zapros('y'));
        }
        $('#main_sl_text_wrap button').click(function (e) {
            e.preventDefault();
            html_str = '<p class="msg_padding">' + $(this).siblings('.hid_content').html() + '</p>';
            creat_lb_sv(html_str, 800);
        });

        if ($('#main_sl .img_fon').length > 1) {
            $('#main_sl').slidesjs({
                navigation: {active: false},
                pagination: {active: true, effect: "slide"},
                callback: {
                    loaded: function (number) {
                        $('#main_sl').find('.slidesjs-pagination').wrap('<div id="main_sl_wrap_pag"></div>');
                    }
                }
            });
        }

        win_.object.resize(function () {
            $('#main_sl_wrap').height(win_.zapros('y'));
        });

    }

//end main script

    $(document).on('click', '.recover_pass', function (e) {
        e.preventDefault();
        recover_pas();
    });
    $(document).on('click', '.sig_in_link', function (e) {
        e.preventDefault();
        sign_in();
    });
    $(document).on('click', '.log_in_link', function (e) {
        e.preventDefault();
        log_in();
    });


    if ($('#inner_menu li').eq(2).children('a').attr('href') == '#') {
        $('#inner_menu li').eq(2).children('a').click(function (e) {
            e.preventDefault();
            log_in();
        });
    }

    if ($('#cart_info').children('a').attr('href') == '#') {
        $('#cart_info').children('a').click(function (e) {
            e.preventDefault();
            log_in();
        });
    }

    if ($('#about_wrap').length > 0) {
        $('#about_wrap').find('button').click(function (e) {
            e.preventDefault();
            $('html,body').animate({'scrollTop': $('#call_back').offset().top}, 600);
        });

        $('#about_form').find('button').click(function (e) {
            e.preventDefault();

            check1 = chek_form($('#about_form').find('input'), 2);
            check2 = chek_form($('#about_form').find('textarea'), 1);
            if (check1 && check2) {
                $('body').addClass('cursor');
                send_str = 'type=about_form_quest&' + $('#about_form form').serialize();
                $.ajax({
                    type: 'post',
                    url: '/site/about',
                    data: send_str,
                    success: function (msg) {
                        creat_lb_sv(msg);
                        $('#about_form').find('input').val('');
                        $('#about_form').find('textarea').val('');
                        $('body').removeClass('cursor');
                    }
                });
            }
        });
    }


    if ($('.goods_item').length > 0) {
        //$_goods_item_button = $('.goods_item').find('button:not(.is_guest)');

        $(document).on('click', '.goods_item button:not(.is_guest)', function (e) {
            e.preventDefault();
            html_str = '<div id="add_to_cart_wrap" class="msg_padding"><p>Введите кол-во</p><input type="text" class="css_trans" value="' + $(this).attr('data-min') + '"><button class="bg_color_th" data-id="' + $(this).attr('data-id') + '" data-stock="' + $(this).attr('data-stock') + '" data-price="' + $(this).attr('data-price') + '" data-min="' + $(this).attr('data-min') + '">Добавить</button></div>';
            creat_lb_sv(html_str);
        });

    }

    //$(document).on('click','#add_to_cart_wrap button, button.to_cart_from_det ',function(e){
    $(document).on('click', '#add_to_cart_wrap button ', function (e) {
        e.preventDefault();
        ch_in = chek_form($('#add_to_cart_wrap input'), 1);
        if (parseInt($(this).siblings('input').val()) < parseInt($(this).attr('data-min'))) {
            $('.err_msg').remove();
            $('<p class="err_msg"><i>Минимальное кол-во для заказа: ' + $(this).attr('data-min') + '</i></p>').insertAfter('#add_to_cart_wrap p');
            return false;
        }
        if (parseInt($(this).siblings('input').val()) > parseInt($(this).attr('data-stock'))) {
            $('.err_msg').remove();
            $('<p class="err_msg"><i>Максимальное кол-во для заказа: ' + $(this).attr('data-stock') + '</i></p>').insertAfter('#add_to_cart_wrap p');
            return false;
        }
        if (ch_in) {
            animate_to_cart($(this), $('#cart_info'));
            var data_id = $(this).attr('data-id');
            var data_price = $(this).attr('data-price');
            var data_count = $(this).siblings('input').val();
            send_str = 'type=add_to_cart&id=' + data_id + '&count=' + data_count;
            $.ajax({
                type: 'post',
                url: '/html_source/ajax.php',
                data: send_str,
                success: function (msg) {
                    $('.goods_item button[data-id="' + data_id + '"]').text('В Корзине');
                    update_ico_cart(data_price, data_count);
                }
            });
            $('#cart_shadow').click();
        }
    });

    $(document).on('keypress', '#add_to_cart_wrap input', function (e) {
        e = e || event;
        if (e.ctrlKey || e.altKey || e.metaKey) return;
        var chr = getChar(e);
        if (chr == null) return;
        if (chr < '0' || chr > '9') {
            return false;
        }
    });

//inner_cart
    if ($('#cart_item_table').length > 0) {
        $('#info_pr_text').click(function (e) {
            e.preventDefault();
            html_str = '<div class="msg_padding">' + $(this).siblings('.hidden').html() + '</div>';
            creat_lb_sv(html_str);
        });

        $('.next_step_cart').click(function (e) {
            e.preventDefault();
            $('html,body').animate({'scrollTop': $('#rules_delivery').offset().top}, 500);
        });

        $('#more_rules_delivery').click(function (e) {
            e.preventDefault();
            html_str = '<div class="msg_padding">' + $(this).siblings('.hidden').html() + '</div>';
            creat_lb_sv(html_str);
            $('.scroll_cart_msg').css('text-align', 'left');
        });
        //calendar
        function Calendar2(id, year, month) {
            var Dlast = new Date(year, month + 1, 0).getDate(),
                D = new Date(year, month, Dlast),
                DNlast = new Date(D.getFullYear(), D.getMonth(), Dlast).getDay(),
                DNfirst = new Date(D.getFullYear(), D.getMonth(), 1).getDay(),
                calendar = '<tr>',
                month = ["Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"];
            if (DNfirst != 0) {
                for (var i = 1; i < DNfirst; i++) calendar += '<td>';
            } else {
                for (var i = 0; i < 6; i++) calendar += '<td>';
            }


            for (var i = 1; i <= Dlast; i++) {
                if (i == new Date().getDate() && D.getFullYear() == new Date().getFullYear() && D.getMonth() == new Date().getMonth()) {
                    calendar += '<td class="today work"><span class="css_trans bg_color_th_hover">' + i + '</span>';
                } else {
                    if ((i < new Date().getDate() && D.getFullYear() <= new Date().getFullYear() && D.getMonth() <= new Date().getMonth()) || ( D.getFullYear() <= new Date().getFullYear() && D.getMonth() < new Date().getMonth()) || ( D.getFullYear() < new Date().getFullYear())) {
                        calendar += '<td class=block><span  class="css_trans">' + i + '</span>';
                    } else {
                        /*if((i-1) == new Date().getDate() && D.getFullYear() == new Date().getFullYear() && D.getMonth() == new Date().getMonth()){
                         calendar += '<td class="next_d "><span  class="css_trans">' + i+'</span>';
                         }else{*/
                        calendar += '<td class="work "><span  class="css_trans bg_color_th_hover">' + i + '</span>';
                        //}
                    }
                }
                if (new Date(D.getFullYear(), D.getMonth(), i).getDay() == 0) {
                    calendar += '<tr>';
                }
            }
            for (var i = DNlast; i < 7; i++) calendar += '<td>&nbsp;';
            document.querySelector('#' + id + ' tbody').innerHTML = calendar;
            document.querySelector('#' + id + ' thead td:nth-child(2)').innerHTML = month[D.getMonth()] + ' ' + D.getFullYear();
            document.querySelector('#' + id + ' thead td:nth-child(2)').dataset.month = D.getMonth();
            document.querySelector('#' + id + ' thead td:nth-child(2)').dataset.year = D.getFullYear();
            if (document.querySelectorAll('#' + id + ' tbody tr').length < 6) {  // чтобы при перелистывании месяцев не "подпрыгивала" вся страница, добавляется ряд пустых клеток. Итог: всегда 6 строк для цифр
                document.querySelector('#' + id + ' tbody').innerHTML += '<tr><td>&nbsp;<td>&nbsp;<td>&nbsp;<td>&nbsp;<td>&nbsp;<td>&nbsp;<td>&nbsp;';
            }
            if (str.indexOf('barbers') + 1) {
                $('#calendar2 tbody tr').each(function () {
                    $('td:last-child').removeClass('work ').addClass('block');
                    $('td:last-child').prev().removeClass('work ').addClass('block')
                })
            }

        }

        Calendar2("calendar2", new Date().getFullYear(), new Date().getMonth());
        // переключатель минус месяц
        document.querySelector('#calendar2 thead tr:nth-child(1) td:nth-child(1)').onclick = function () {
            Calendar2("calendar2", document.querySelector('#calendar2 thead td:nth-child(2)').dataset.year, parseFloat(document.querySelector('#calendar2 thead td:nth-child(2)').dataset.month) - 1);

        }

        // переключатель плюс месяц
        document.querySelector('#calendar2 thead tr:nth-child(1) td:nth-child(3)').onclick = function () {
            Calendar2("calendar2", document.querySelector('#calendar2 thead td:nth-child(2)').dataset.year, parseFloat(document.querySelector('#calendar2 thead td:nth-child(2)').dataset.month) + 1);

        }

        $(document).on('click', '#calendar2 .work', function (e) {
            e.preventDefault();
            $('#calendar_title').removeClass('color_failed');
            $('#calendar_cart').removeClass('failed');
            if (!$(this).hasClass('active')) {
                $('#calendar2 .work').removeClass('active');
                $('#calendar2 .work').each(function () {
                    $(this).children('span').removeClass('bg_color_th');
                });
                $(this).addClass('active');
                $(this).children('span').addClass('bg_color_th');
            }
        });
        //end calendar

        //shop_select
        $(document).on('click', '.select_shop p', function () {
            if ($(this).hasClass('active')) {
                $(this).removeClass('active');
                $(this).siblings('ul').fadeOut(300);
            } else {
                $(this).addClass('active');
                $(this).siblings('ul').fadeIn(300);
            }
        });
        $(document).on('click', '.select_shop ul li', function () {
            str = $(this).text();
            $(this).parent().siblings('p').text(str).removeClass('active').attr('data-id', $(this).attr('data-id'));
            $(this).parent().fadeOut(300);
            var $save_el = $(this).parents('.shop_setup');
            $.ajax({
                type: 'post',
                url: '/site/Magazine',
                data: 'magazine_id=' + $(this).attr('data-id'),
                success: function (msg) {
                    arr = JSON.parse(msg);
                    $save_el.find('.shop_name').val(arr[0]);
                    $save_el.find('.shop_lastname').val(arr[1]);
                    $save_el.find('.form-control').val(arr[2]);
                    $save_el.find('.shop_msg').val(arr[3]);
                }
            });
        });

        //add_shop

        $('.add_shop_form').click(function (e) {
            e.preventDefault();
            shop_html = $('.shop_setup').eq(0).clone();
            $(this).parents('.add_shop').before(shop_html);
        });

        //setup price

        if (!discont_rules) {
            $('.discont_line').hide();
            $('.summ_minus').hide();
            $('.summ_plus').hide();
            //$('.cart_itog_dop').hide();
        }
        update_cart();

        //inc dec action

        $('.inc').click(function (e) {
            e.preventDefault();
            if (!$('body').hasClass('cursor')) {

                curent_value = parseInt($(this).siblings('input').val());
                min_value = parseInt($(this).siblings('input').attr('data-min'));
                curent = curent_value + min_value;
                $(this).siblings('input').val(curent_value + min_value);
                update_cart();
                ajax_update_cart('update', $(this).parents('.cart_item_line').attr('data-id'), curent);
            }

        });

        $('.dec').click(function (e) {
            e.preventDefault();
            if (!$('body').hasClass('cursor')) {
                curent_value = parseInt($(this).siblings('input').val());
                min_value = parseInt($(this).siblings('input').attr('data-min'));
                curent = curent_value - min_value;
                console.log(curent);
                if (curent_value != min_value) {
                    $(this).siblings('input').val(curent_value - min_value);
                    update_cart();
                    ajax_update_cart('update', $(this).parents('.cart_item_line').attr('data-id'), curent);
                } else {
                    creat_lb_sv('<p class="msg_padding">Минимальное кол-во для покупки: ' + min_value + '</p>');
                    return false;
                }
            }

        });
        //input inc/dec
        $(document).on('keypress', '.row4 input', function (e) {
            e = e || event;
            if (e.ctrlKey || e.altKey || e.metaKey) return;
            var chr = getChar(e);
            if (chr == null) return;
            if (chr < '0' || chr > '9') {
                return false;
            }
        });

        $('.row4 input').blur(function () {
            if (parseInt($(this).val()) < parseInt($(this).attr('data-min'))) {
                $(this).val($(this).attr('data-min'))
                creat_lb_sv('<p class="msg_padding">Минимальное кол-во для покупки: ' + $(this).attr('data-min') + '</p>');
            }
            update_cart();
            ajax_update_cart('update', $(this).parents('.cart_item_line').attr('data-id'), $(this).val());
        });

        //del item
        $('.cart_item_line .row6').click(function (e) {
            e.preventDefault();
            if (!$('body').hasClass('cursor')) {
                ajax_update_cart('delete', $(this).parents('.cart_item_line').attr('data-id'));
                $(this).parents('.cart_item_line').remove();
                update_cart();
            }
            if (Number($('.bonus_number').text()) < Number($('.bonus_input').val())) {
                Number($('.bonus_input').val($('.bonus_number').text()));
            }
            var maxBonus = $('.summ b .money');
            maxBonus = Number(maxBonus.text().replace(/\s+/g, ''));
            var discount = $('.summ_plus b .money');
            discount = Number(discount.text().replace(/\s+/g, ''));
            var minOrder = maxBonus * 0.3;
            var applyBonus = discount - Number(minOrder);
            if ($('.bonus_input').val() > applyBonus) {
                $('.bonus_input').val(applyBonus);
                $('input[name="bonus_value"]').val(applyBonus);
                if ($('.maxDisc').css('display') == 'none') {
                    $('.maxDisc').css('display', 'inline-block');
                }
            } else {
                if ($('.maxDisc').css('display') == 'inline-block') {
                    $('.maxDisc').css('display', 'none');
                }
            }
            if ($('.summ_bonus').css('display') == 'none') {
                $('.summ_bonus').css('display', 'inline-block');
            }
            var allCost = String(discount - Number($('.bonus_input').val()));
            allCost = allCost.replace(/(\s)+/g, "").replace(/(\d{1,3})(?=(?:\d{3})+$)/g, "$1 ");
            $('.summ_bonus .money').text(allCost);

            $('input[name="bonus_value"]').val($('.bonus_input').val());


        });

        //update cart price

        //creat form for submit

        $('#finish_order').click(function (e) {
            e.preventDefault();
            time_flag = true;
            if ($('.cart_item_line').length > 0) {
                var summa = 0;
                if ($('.summ_bonus').css('display') != 'none') {
                    summa = String($('.summ_bonus .money').text());
                    summa = summa.replace(" ", "");
                } else {
                    summa = String($('.summ_plus .money').text());
                    summa = summa.replace(" ", "");
                }
                if (Number($('input[name="min_order"]').val()) > Number(summa)) {
                    creat_lb_sv('<div style="height:200px;">' +
                        '<div class="vert_style" style="width:100%;position: relative;top: 4vw;">Ваш заказ на сумму меньше ' + $('input[name="min_order"]').val() + 'руб. - минимального значения.<br> Решение о поставке будет принимать ваш менеджер</div>' +
                        '<div class="vert_style"></div>' +
                        '<div style="margin-top: 75px"><button id="min_order" class="bg_color_th">Продолжить</button><button id="min_order_cancel" class="bg_color_th">Добавить товары</button></div>' +
                        '</div>');
                    return false;
                }

                //goods_append
                goods_html = '';
                $('.cart_item_line').each(function () {
                    goods_html += '<input type="hidden" name="goods[' + $(this).index() + '][goods_id]" value="' + $(this).attr('data-id') + '"><input type="hidden" name="goods[' + $(this).index() + '][goods_count]" value="' + $(this).find('input').val() + '">';
                });
                $('#finish_order_form').append(goods_html);

                //time set
                if ($('#calendar2').find('.active').length > 0) {
                    day = $('#calendar2').find('.active').text();
                } else {
                    day = $('#calendar2').find('.today').text();
                    time_flag = false;
                }
                month = parseInt($('#calendar2 thead td:nth-child(2)').attr('data-month')) + 1;
                year = $('#calendar2 thead td:nth-child(2)').attr('data-year');
                if (parseInt(day) < 10) {
                    day = '0' + day;
                }
                if (parseInt(month) < 10) {
                    month = '0' + month;
                }
                document.cart_page.time_order.value = year + '-' + month + '-' + day;


                //shop_info
                shop_html = '';
                send_form = true;
                $('.shop_setup').each(function () {
                    $(this).find('input').each(function () {
                        if ($(this).val() == '') {
                            $(this).addClass('failed');
                            send_form = false;
                        }
                    });
                    shop_html += '<input type="hidden" name="shop_form[' + ($(this).index() - 1) + '][shop_id]" value="' + $(this).find('.select_shop').children('p').attr('data-id') + '" ><input type="hidden" name="shop_form[' + ($(this).index() - 1) + '][contact_name]" value="' + $(this).find('.shop_name').val() + '"><input type="hidden" name="shop_form[' + ($(this).index() - 1) + '][contact_lastname]" value="' + $(this).find('.shop_lastname').val() + '"><input type="hidden" name="shop_form[' + ($(this).index() - 1) + '][contact_phone]" value="' + $(this).find('.shop_phone').val() + '"><input type="hidden" name="shop_form[' + ($(this).index() - 1) + '][contact_msg]" value="' + $(this).find('.shop_msg').val() + '">';
                });

                if ($('.select_shop p').attr('data-id') == '') {
                    send_form = false;
                    $('.select_shop').addClass('failed');
                }

                if (!$('.select_wr select').val()) {
                    send_form = false;
                    $('.select_wr select').addClass('failed');
                }
                if (!time_flag) {
                    $('html,body').animate({scrollTop: $('#calendar_cart').offset().top}, 400);
                    $('#calendar_title').addClass('color_failed');
                    $('#calendar_cart').addClass('failed');

                    return false;
                }

                $('#finish_order_form').append(shop_html);

                if (send_form) {
                    document.cart_page.submit();
                } else {
                    $('html,body').animate({'scrollTop': $('#select_shop').offset().top}, 500);
                }

            } else {
                return false;
            }

        });
        $(document).on('click', '#min_order_cancel', function () {
            window.location.href = "/catalog";
        });

        $(document).on('click', '#min_order', function (e) {
            e.preventDefault();
            time_flag = true;
            if ($('.cart_item_line').length > 0) {
                //goods_append
                goods_html = '';
                $('.cart_item_line').each(function () {
                    goods_html += '<input type="hidden" name="goods[' + $(this).index() + '][goods_id]" value="' + $(this).attr('data-id') + '"><input type="hidden" name="goods[' + $(this).index() + '][goods_count]" value="' + $(this).find('input').val() + '">';
                });
                $('#finish_order_form').append(goods_html);

                //time set
                if ($('#calendar2').find('.active').length > 0) {
                    day = $('#calendar2').find('.active').text();
                } else {
                    day = $('#calendar2').find('.today').text();
                    time_flag = false;
                }
                month = parseInt($('#calendar2 thead td:nth-child(2)').attr('data-month')) + 1;
                year = $('#calendar2 thead td:nth-child(2)').attr('data-year');
                if (parseInt(day) < 10) {
                    day = '0' + day;
                }
                if (parseInt(month) < 10) {
                    month = '0' + month;
                }
                document.cart_page.time_order.value = year + '-' + month + '-' + day;


                //shop_info
                shop_html = '';
                send_form = true;
                $('.shop_setup').each(function () {
                    $(this).find('input').each(function () {
                        if ($(this).val() == '') {
                            $(this).addClass('failed');
                            send_form = false;
                        }
                    });
                    shop_html += '<input type="hidden" name="shop_form[' + ($(this).index() - 1) + '][shop_id]" value="' + $(this).find('.select_shop').children('p').attr('data-id') + '" ><input type="hidden" name="shop_form[' + ($(this).index() - 1) + '][contact_name]" value="' + $(this).find('.shop_name').val() + '"><input type="hidden" name="shop_form[' + ($(this).index() - 1) + '][contact_lastname]" value="' + $(this).find('.shop_lastname').val() + '"><input type="hidden" name="shop_form[' + ($(this).index() - 1) + '][contact_phone]" value="' + $(this).find('.shop_phone').val() + '"><input type="hidden" name="shop_form[' + ($(this).index() - 1) + '][contact_msg]" value="' + $(this).find('.shop_msg').val() + '">';
                });

                if ($('.select_shop p').attr('data-id') == '') {
                    send_form = false;
                    $('.select_shop').addClass('failed');
                }

                if (!$('.select_wr select').val()) {
                    send_form = false;
                    $('.select_wr select').addClass('failed');
                }
                if (!time_flag) {
                    $('html,body').animate({scrollTop: $('#calendar_cart').offset().top}, 400);
                    $('#calendar_title').addClass('color_failed');
                    $('#calendar_cart').addClass('failed');

                    return false;
                }

                $('#finish_order_form').append(shop_html);

                if (send_form) {
                    document.cart_page.submit();
                } else {
                    $('html,body').animate({'scrollTop': $('#select_shop').offset().top}, 500);
                }

            } else {
                return false;
            }
        });
        $('.select_shop').click(function () {
            $(this).removeClass('failed');
        });

    }

//end inner_cart

//detail pages
    if ($(".fancybox").length > 0) {
        $(".fancybox").fancybox({
            openEffect: 'none',
            closeEffect: 'none'
        });
    }

    if ($('#det_sl').length > 0 && $('#det_sl .img_fon').length > 1) {
        $('#det_sl').slidesjs({
            callback: {
                loaded: function () {
                    $('#det_sl').find('.slidesjs-navigation').html('<svg  class="svg_stroke_color" width="124" height="240" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 124 240"><!-- Created with SVG-edit - http://svg-edit.googlecode.com/ --><title>Layer 1</title><line id="svg_1" y2="121.499999" x2="2.5" y1="2.5" x1="121.500006" stroke-width="" stroke="" fill="none"/><line id="svg_2" y2="237.49999" x2="121.500018" y1="118.500003" x1="2.500007" stroke-linecap="null" stroke-linejoin="null" stroke-width="" stroke="" fill="none"/></svg>');

                    $('#det_sl').find('.img_fon').each(function () {
                        img = $(this).attr('style');
                        index = $(this).parent().index();
                        $('#det_sl').find('.slidesjs-pagination-item').eq(index).children('a').attr('style', img).html('<span class="border_color css_trans"></span>');
                    });
                }
            }
        });
    }

    if ($('#det_to_cart').length > 0) {
        var count = $('.det_to_cart_button input').attr('data-min');
        var maxCount = $('.det_to_cart_button input').attr('data-stock');
        count = Number(count);
        maxCount = Number(maxCount);
        $('.det_to_cart_button span').click(function () {
            if ($(this).hasClass('inc')) {
                var url = window.location.href;
                if (url.indexOf('barbers') + 1) {
                    if (Number($(this).siblings('input').val()) < maxCount) {
                        $(this).siblings('input').val(Number($(this).siblings('input').val()) + count);
                    } else {
                        return false;
                    }
                }else{
                    $(this).siblings('input').val(Number($(this).siblings('input').val()) + count);
                }
            } else {
                var count2 = parseInt($(this).siblings('input').val());
                if (count2 > count) {
                    $(this).siblings('input').val(count2 - count);
                } else {
                    return false;
                }
            }
        });

        $('.to_cart_from_det').click(function (e) {
            e.preventDefault();
            animate_to_cart($(this), $('#cart_info'));
            if ($('.det_to_cart_info del').length) {
                update_ico_cart(parseInt(($('.det_to_cart_info').find('.money:nth-child(2)').text()).replace(/\s+/g, '')), $(this).siblings('input').val());

            } else {
                update_ico_cart(parseInt(($('.det_to_cart_info').find('.money').text()).replace(/\s+/g, '')), $(this).siblings('input').val());
            }
            send_str = 'type=add_to_cart&id=' + $(this).attr('data-id') + '&count=' + $(this).siblings('input').val();
            $.ajax({
                type: 'post',
                url: '/html_source/ajax.php',
                data: send_str,
                success: function (msg) {
                    $('.to_cart_from_det').text('В Корзине');
                }
            });
        });

        $(document).on('keypress', '.det_to_cart_button input', function (e) {
            e = e || event;
            if (e.ctrlKey || e.altKey || e.metaKey) return;
            var chr = getChar(e);
            if (chr == null) return;
            if (chr < '0' || chr > '9') {
                return false;
            }
        });

        $(document).on('blur', '.det_to_cart_button input', function (e) {
            if (parseInt($(this).val()) < parseInt($(this).attr('data-min'))) {
                $(this).val($(this).attr('data-min'));
                creat_lb_sv('<p class="msg_padding">Минимальное кол-во для покупки ' + $(this).attr('data-min') + ' шт</p>');
            }
            if (parseInt($(this).val()) > parseInt($(this).attr('data-stock'))) {
                $(this).val($(this).attr('data-stock'));
                creat_lb_sv('<p class="msg_padding">Максимальное кол-во для покупки ' + $(this).attr('data-stock') + ' шт</p>');
            }
        });

    }

    if ($('#text_det_wrap').outerHeight(true) < $('#text_det_content').outerHeight(true)) {
        $('.show_more_content').hide();
    }


    if ($('.show_more_content').length > 0) {
        $('.show_more_content').click(function () {
            $('#text_det_content').addClass('full');
            $('.hide_more_content').show();
            $('.show_more_content').hide();
        });
        $('.hide_more_content').click(function () {
            $('#text_det_content').removeClass('full');
            $('.hide_more_content').hide();
            $('.show_more_content').show();
        });
    }

    $(document).on('click', '.submit_new_user', function (e) {
        e.preventDefault();
        var ph_pattern = /^((8|\+7)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{7,10}$/;
        ml_pattern = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        var count = 0,
            input_label = function (txt) {
                return '<div class="input_label">' + txt + '</div>';
            }
        if (!ph_pattern.test($('.phone_mask.pop_input').val())) {
            $('.phone_mask.pop_input').addClass('failed');
        }
        var inn = false;
        $(this).siblings('input').each(function () {
            if (($(this).attr('name') == 'MagazineCompany[inn]' && $(this).val().length < 10) || ($(this).attr('name') == 'MagazineCompany[inn]' && $(this).val().length > 10)) {
                $('body').append(input_label('количество знаков не меньше/больше 10'));
                top_ = $(this).offset().top + $(this).outerHeight(true);
                left_ = $(this).offset().left + ($(this).outerWidth(true) / 2);
                $('.input_label').eq(-1).css({'top': top_, 'left': left_}).animate({'opacity': 1}, 300);
                setTimeout(function () {
                    $('.input_label').animate({'opacity': 0}, 300, function () {
                        $(this).remove();
                    })
                }, 5000);
            } else if ($(this).attr('name') == 'MagazineCompany[inn]' && $(this).val().length <= 10) {
                inn = true
            }
            if ($(this).val() != '') {
                count++;

                if ($(this).attr('name') == 'MagazineUsers[user_phone]' && !ph_pattern.test($(this).val())) {
                    count--;
                    $(this).addClass('failed');
                    $('body').append(input_label('Введите данные в указанном формате:<br/>+7 (123) 123-11-22'));
                    top_ = $(this).offset().top + $(this).outerHeight(true);
                    left_ = $(this).offset().left + ($(this).outerWidth(true) / 2);
                    $('.input_label').eq(-1).css({'top': top_, 'left': left_}).animate({'opacity': 1}, 300);
                    setTimeout(function () {
                        $('.input_label').animate({'opacity': 0}, 300, function () {
                            $(this).remove();
                        })
                    }, 5000);
                }
                if ($(this).attr('name') == 'MagazineUsers[user_email]' && !ml_pattern.test($(this).val())) {
                    count--;
                    $(this).addClass('failed');
                    $('body').append(input_label('Введенные данные не являются почтовым адресом'));
                    top_ = $(this).offset().top + $(this).outerHeight(true);
                    left_ = $(this).offset().left + ($(this).outerWidth(true) / 2);
                    $('.input_label').eq(-1).css({'top': top_, 'left': left_}).animate({'opacity': 1}, 300);
                    setTimeout(function () {
                        $('.input_label').animate({'opacity': 0}, 300, function () {
                            $(this).remove();
                        })
                    }, 5000);
                }
            } else {
                $(this).addClass('failed');
                if ($(this).attr('name') == 'MagazineUsers[user_phone]') {
                    $('body').append(input_label('Введите данные в указанном формате:<br/>+7 (123) 123-11-22'));
                    top_ = $(this).offset().top + $(this).outerHeight(true);
                    left_ = $(this).offset().left + ($(this).outerWidth(true) / 2);
                    $('.input_label').eq(-1).css({'top': top_, 'left': left_}).animate({'opacity': 1}, 300);
                    setTimeout(function () {
                        $('.input_label').animate({'opacity': 0}, 300, function () {
                            $(this).remove();
                        })
                    }, 5000);
                }
                if ($(this).attr('name') == 'MagazineUsers[user_name]') {
                    $('body').append(input_label('Поле Имя является обязательным. Заполните'));
                    top_ = $(this).offset().top + $(this).outerHeight(true);
                    left_ = $(this).offset().left + ($(this).outerWidth(true) / 2);
                    $('.input_label').eq(-1).css({'top': top_, 'left': left_}).animate({'opacity': 1}, 300);
                    setTimeout(function () {
                        $('.input_label').animate({'opacity': 0}, 300, function () {
                            $(this).remove();
                        })
                    }, 5000);
                }
                if ($(this).attr('name') == 'MagazineCompany[name_company]') {
                    $('body').append(input_label('Поле Компания является обязательным. Заполните'));
                    top_ = $(this).offset().top + $(this).outerHeight(true);
                    left_ = $(this).offset().left + ($(this).outerWidth(true) / 2);
                    $('.input_label').eq(-1).css({'top': top_, 'left': left_}).animate({'opacity': 1}, 300);
                    setTimeout(function () {
                        $('.input_label').animate({'opacity': 0}, 300, function () {
                            $(this).remove();
                        })
                    }, 5000);
                }
                if ($(this).attr('name') == 'MagazineCompany[inn]') {
                    $('body').append(input_label('Поле ИНН является обязательным. Заполните'));
                    top_ = $(this).offset().top + $(this).outerHeight(true);
                    left_ = $(this).offset().left + ($(this).outerWidth(true) / 2);
                    $('.input_label').eq(-1).css({'top': top_, 'left': left_}).animate({'opacity': 1}, 300);
                    setTimeout(function () {
                        $('.input_label').animate({'opacity': 0}, 300, function () {
                            $(this).remove();
                        })
                    }, 5000);
                }

                if ($(this).attr('name') == 'MagazineUsers[user_surname]') {
                    $('body').append(input_label('Поле Фамилия является обязательным. Заполните'));
                    top_ = $(this).offset().top + $(this).outerHeight(true);
                    left_ = $(this).offset().left + ($(this).outerWidth(true) / 2);
                    $('.input_label').eq(-1).css({'top': top_, 'left': left_}).animate({'opacity': 1}, 300);
                    setTimeout(function () {
                        $('.input_label').animate({'opacity': 0}, 300, function () {
                            $(this).remove();
                        })
                    }, 5000);
                }

                if ($(this).attr('name') == 'MagazineUsers[user_password]') {
                    $('body').append(input_label('Поле Пароль является обязательным. Заполните'));
                    top_ = $(this).offset().top + $(this).outerHeight(true);
                    left_ = $(this).offset().left + ($(this).outerWidth(true) / 2);
                    $('.input_label').eq(-1).css({'top': top_, 'left': left_}).animate({'opacity': 1}, 300);
                    setTimeout(function () {
                        $('.input_label').animate({'opacity': 0}, 300, function () {
                            $(this).remove();
                        })
                    }, 5000);
                }

            }
        });

        if (count == 4) {
            $('.phone_mask.pop_input').removeClass('done').addClass('failed');
        }
        if (count == 7 && inn == true) {
            $.ajax({
                url: '/site/Reqister',
                type: 'post',
                dataType: 'json',
                data: $('form[name="new_user"]').serialize(),
                success: function (msg) {
                    if (msg.company) {
                        var input = $('input[name="MagazineCompany[inn]"]');
                        $('body').append(input_label('Ваша компания - <span class="company_name">' + msg.company + '</span>' + '<br>' + '<button style="font-size: 1.2vw;text-transform: uppercase;color: white;padding: 0.6vw 0;border-radius: 50px;width: 5.6vw;margin-top: 1.3vw;margin-right: 2vw;line-height: 1.2vw;" type="button" class="bg_color_th yes_reg">Да</button>' + '<button style="font-size: 1.2vw;text-transform: uppercase;color: white;padding: 0.6vw 0;border-radius: 50px;width: 5.6vw;margin-top: 1.3vw;margin-right: 2vw;line-height: 1.2vw;" type="button" class="bg_color_th no_reg">Нет</button>'));
                        top_ = $(input).offset().top + $(input).outerHeight(true);
                        left_ = $(input).offset().left + ($(input).outerWidth(true) / 2);
                        $('.input_label').eq(-1).css({'top': top_, 'left': left_}).animate({'opacity': 1}, 300);
                        setTimeout(function () {
                            $('.input_label').animate({'opacity': 0}, 300, function () {
                                $(this).remove();
                            })
                        }, 50000);

                    }
                    if (msg.msg == 'ok') {
                        creat_lb_sv('<div style="height:200px;"><div class="vert_style" style="width:80%"><img style="display:inline_block;" src="/html_source/img/cart_ok.png" width="75px"><br/>Спасибо, что зарегистрировались.<br/>Для входа в систему ожидайте подтверждения администратора.</div><div class="vert_style"></div></div>');
                        setTimeout(function () {
                            $('#cart_shadow').click();
                        }, 3000);

                    } else {
                        if (msg.msg = 'пользователь такой уже есть') {
                            var br = '<br>'
                            $('input[name="MagazineUsers[user_email]"]').focus();
                            $('#log_form_wrap div.text').html('Пользователь с email: ' + $('input[name="MagazineUsers[user_email]"]').val() + br + 'Уже занят');
                        } else {
                            $('#log_form_wrap div.text').text(msg.msg);
                        }

                    }
                }
            });
        }
        if (count != $(this).siblings('input').length) {
            return false;
        }
    });
    $(document).on('click', '.yes_reg', function () {
        $('input[name="MagazineCompany[name_company]"]').val($('.company_name').text());
        $('.input_label').animate({'opacity': 0}, 300, function () {
            $(this).remove();
        });
        $.ajax({
            url: '/site/RegisterByCompany',
            type: 'post',
            dataType: 'json',
            data: $('form[name="new_user"]').serialize(),
            success: function (msg) {
                if (msg.msg) {
                    creat_lb_sv('<div style="height:200px;"><div class="vert_style" style="width:80%"><img style="display:inline_block;" src="/html_source/img/cart_ok.png" width="75px"><br/>Спасибо, что зарегистрировались.<br/>Для входа в систему ожидайте подтверждения администратора.</div><div class="vert_style"></div></div>');
                }
            }
        });
    });
    $(document).on('click', '.no_reg', function () {
        input_label = function (txt) {
            return '<div class="input_label">' + txt + '</div>';
        };
        $('.input_label').animate({'opacity': 0}, 300, function () {
            $(this).remove();
        });
        var input = $('input[name="MagazineCompany[inn]"]');
        $('body').append(input_label('Введите другой ИНН'));
        $(input).val('');
        top_ = $(input).offset().top + $(input).outerHeight(true);
        left_ = $(input).offset().left + ($(input).outerWidth(true) / 2);
        $('.input_label').eq(-1).css({'top': top_, 'left': left_}).animate({'opacity': 1}, 300);
        setTimeout(function () {
            $('.input_label').animate({'opacity': 0}, 300, function () {
                $(this).remove();
            })
        }, 5000);
    });

    /*$(document).on('click','.input_label span',function(){
     $(this).parent().remove();
     });*/

    if ($('#sort_by').length > 0) {
        save_sort_href = $('#sort_by').attr('href');
        $('#category_title_wrap').append('<div id="finder_goods"><input type="text"><p class="bg_color_th">Найти</p></div>');
    }


    function creat_content(arr_content) {
        if (arr_content.length > 0) {
            tpl_item = '';
            for (var i = 0; i < arr_content.length; i++) {
                tpl_item += '<div class="goods_item css_trans shadow_hover"><a href="' + arr_content[i]['url'] + '"><div class="img_fon" style="background-image:url(' + arr_content[i]['img'] + ')"></div><p>' + arr_content[i]['title'] + '</p></a><div class="price inline_style color_th"><span class="money">' + arr_content[i]['price'] + '</span><span class="rub_lt">a</span></div><button class="bg_color_th ' + arr_content[i]['flag'] + '" data-id="' + arr_content[i]['id'] + '" data-price="' + arr_content[i]['price'] + '" data-min="' + arr_content[i]['min_count'] + '">Купить</button></div>';
            }

            $('#goods_item_wrap').html(tpl_item);
            $('.money').each(function () {
                str = $(this).text().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1 ");
                $(this).text(str);
            });

            if (!$('#finder_reset').length > 0) {
                $('#category_title_wrap').append('<a href="" class="css_trans color_th_hover" id="finder_reset">Очистить результат поиска</a>');
            }

            $('#sort_by').attr('href', save_sort_href + '&find_item=' + $('#finder_goods input').val());
        } else {
            creat_lb_sv('<p class="msg_padding">Нет результатов поиска</p>');
        }
    }

    $(document).on('change', '#finder_goods input', function () {
        str = 'type=get_goods&value=' + $(this).val();
        $.ajax({
            type: 'post',
            url: '/site/Rectovar',
            data: str,
            success: function (msg) {
                arr_content = JSON.parse(msg);
                creat_content(arr_content);
            }
        });
    });

});