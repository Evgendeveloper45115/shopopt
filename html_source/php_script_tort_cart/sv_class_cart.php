<?php
class sv_cart {

    static public function add_cart($id,$count,$nachinki)
    {
        global $modx;
        $time = time();
        $getResult = $modx->db->select('*','modx_site_content','id = "'.addslashes($id).'" ');
        $resultId = $modx->db->getRow($getResult);
        if($resultId['template']=='24'){
            $getPriceForId = $modx->db->select('*','modx_site_tmplvar_contentvalues','contentid = "'.addslashes($resultId['id']).'" AND tmplvarid = 25');
            $resultPrice = $modx->db->getRow($getPriceForId);
            $_SESSION['cart_price'][$time] = (int)$resultPrice['value'];
        }else{
           $price_arr = $modx->runSnippet('ddGetMultipleField',array('docField'=>'price_calc','docId'=>addslashes($id),'outputFormat'=>'array')); 
            foreach($price_arr as $key=>$value){
                if((float)$count >= $value[0]){
                    $_SESSION['cart_price'][$time]=$value[1];
                }
             }
        }
        
        
		$getImg = $modx->db->select('*','modx_site_tmplvar_contentvalues','contentid = "'.addslashes($resultId['id']).'" AND tmplvarid = 8');
		$resultImg = $modx->db->getRow($getImg);
        $idss = explode(',',$nachinki);
        $getImgNachinka = $modx->db->select('*','modx_site_tmplvar_contentvalues','contentid IN ("'.implode('","',$idss).'") AND tmplvarid = 12');
        $resultImgNachinka = $modx->db->makeArray($getImgNachinka);

        
        $_SESSION['cart_weight'][$time] = (float)$count;
        $_SESSION['cart_name'][$time]  = $resultId['pagetitle'];
 
       
       
        $_SESSION['cart_id'][$time] = $resultId['id'];
        $_SESSION['cart_img'][$time] = $resultImg['value'];
        $_SESSION['cart_nachinki'][$time]['cart_nachinki'] = $nachinki;

//var_dump($_SESSION['cart_id'][$time]);
        foreach($resultImgNachinka as $row) {
            $_SESSION['cart_nachinki'][$time]['img'][] = $row['value'];
        }

        /*unset($_SESSION['cart_weight']);
        unset($_SESSION['cart_img']);
        unset($_SESSION['cart_id']);
        unset($_SESSION['cart_price']);
        unset($_SESSION['cart_name']);
        unset($_SESSION['cart_nachinki']);*/
        var_dump($_SESSION);
        session_write_close();
    }

    static public function delete_cart($id)
    {
        $i= 0;
        foreach($_SESSION['cart_id'] as $key => $cart_id) {

            if($key == $id) {
                unset(
                    $_SESSION['cart_weight'][$key],
                    $_SESSION['cart_name'][$key],
                    $_SESSION['cart_price'][$key],
                    $_SESSION['cart_id'][$key],
					$_SESSION['cart_img'][$key],
					$_SESSION['cart_nachinki'][$key]
                );
            }
            $i++;
        }
        ksort($_SESSION['cart_weight']);
        ksort($_SESSION['cart_name']);
        ksort($_SESSION['cart_price']);
        ksort($_SESSION['cart_id']);
        ksort($_SESSION['cart_img']);
        ksort($_SESSION['cart_nachinki']);
        session_write_close();
    }

    static public function update_cart($id,$count,$nachinki)
    {
        global $modx;

        print_r($_SESSION['cart_id']);
        $i = 0;
        foreach($_SESSION['cart_id'] as $key => $cart_id) {

            if($key == $id) {
                if($count == 0) {
                    sv_cart::delete_cart($id);
                } else {
                    $price_arr = $modx->runSnippet('ddGetMultipleField',array('docField'=>'price_calc','docId'=>addslashes($cart_id),'outputFormat'=>'array'));
                    foreach($price_arr as $key_f=>$value){
                        if((int)$count >= $value[0]){
                            $_SESSION['cart_price'][$key]=$value[1];
                        }
                    }
                    $_SESSION['cart_weight'][$key] = $count;
                    $_SESSION['cart_nachinki'][$key]['cart_nachinki'] = $nachinki;
                    $idss = explode(',',$nachinki);
                    $getImgNachinka = $modx->db->select('*','modx_site_tmplvar_contentvalues','contentid IN ("'.implode('","',$idss).'") AND tmplvarid = 12');
                    $resultImgNachinka = $modx->db->makeArray($getImgNachinka);
                    unset($_SESSION['cart_nachinki'][$key]['img']);
                    foreach($resultImgNachinka as $row) {
                        $_SESSION['cart_nachinki'][$key]['img'][] = $row['value'];
                    }
                }

            }
            $i++;
        }

        session_write_close();
    }

    static public function getCart()
    {


        $arr = array();
        if(count($_SESSION['cart_name'])>0){
        foreach($_SESSION['cart_name'] as $key => $value ) {

            $arr[$key] = array(
                'name'=>$_SESSION['cart_name'][$key],
                'count'=>$_SESSION['cart_weight'][$key],
                'price'=>$_SESSION['cart_price'][$key],
                'id'=>$_SESSION['cart_id'][$key],
				'cart_img'=>$_SESSION['cart_img'][$key],

            );
            if(count($_SESSION['cart_nachinki'][$key]['img'])>0){
                foreach($_SESSION['cart_nachinki'][$key]['img'] as $valuess) {

                    $arr[$key]['nachinki'] = $_SESSION['cart_nachinki'][$key];
                }
            }
        }
        }


        return ($arr);

    }

    static public function saveOrder($Insert=true)
    {
        global $modx;
        $Data['dost'] = null;
//        if(count($_SESSION['cart_weight'])>0){
//
//        } else {
//            $url = $modx->makeUrl(1);
//            header('Location:'.$url);
//            exit;
//        }
		if($Insert===false){
//			if(!$_SESSION['data_cart_id']){
//				return false;
//			}
			$getResult = $modx->db->select('*','cartInfo','id = "'.addslashes($_SESSION['data_cart_id']).'" ');
			$Data = $modx->db->getRow($getResult);
		}else{

			$Data=$_REQUEST;
		}

        //print_r($_SESSION['cart_nachinki']);

        //сохраняем основые параметры заказа
        $getOrders = array(
            'name'=>addslashes($Data['name']),
            'surname'=>addslashes($Data['surname']),
            'tel'=>addslashes($Data['tel']),
            'email'=>addslashes($Data['email']),
            'address'=>addslashes($Data['address']),
            'comment'=>'',
            'price'=>(self::getAllPrice() == 0 ? $Data['price']:self::getAllPrice()),
            'date'=>date('Y-m-d'),
            'dost'=>$Data['dost'],
            'tort_name'=>$Data['tort_name']
        );


        if(strlen($Data['comment'])>0){
           $getOrders['comment']=addslashes($Data['comment']);
        }

        if(isset($Data['dost'])){
            switch ($Data['dost']) {
                case '1':
                    $getOrders['dost'] = 'Самовывоз';
                    $getOrders['address'] = $Data['rest_name'];
                    break;
                case '2':
                     $getOrders['dost'] = 'по Москве в пределах МКАД';
                     break;
                case '3':
                    $getOrders['dost'] = 'за пределами МКАД';
                    break;
                
                default:
                    break;
            }
        }

        $getOrders['dost_time'] = null;

        if(isset($Data['time_zone_select'])){
            $getOrders['dost'] .= '; Интервал времени доставки: '.$Data['time_zone_select'];
        }

        if(isset($Data['dost_time']) && $Insert === true){
            $getOrders['dost'] .= '; Доставка к конкретному времени '.$Data['hour'].':'.$Data['min'];
        }

        if(strlen($Data['calendar'])>0){
            $getOrders['dost'] .= '; Желаемая дата получения товара: '.$Data['calendar'];
        }
       if($Insert!==false){
		   $int = $modx->db->insert($getOrders,'cartInfo');
			$getOrders['number'] = $int;
			$_SESSION['data_cart_id']=$int;
	   }else{
			$_SESSION['data_cart_id']=null;
	   }
        $getOrders['data'] = date('Y-m-d H:i:s');
        $getOrders['cart_body'] = '';
        $getOrders['site_url'] = $_SERVER['SERVER_NAME'];

//товары в заказе
        if($Insert!==false) {
            if(count($_SESSION['cart_weight'])>0){
                foreach ($_SESSION['cart_weight'] as $key => $value) {
                    $arrDetial = array(
                        'cart_id' => $int,
                        'count_tovar' => $_SESSION['cart_weight'][$key],
                        'id_tovar' => $_SESSION['cart_id'][$key],
                        'article' => '',
                        'detail_info' => '',
                        'price' => ''
                    );

                    $arr_documet = $modx->getDocument($arrDetial['id_tovar']);
                    //проверяем тип товара с начинкой или без
                    $tv_document = $modx->getTemplateVars(array('articule', 'c_price'), '*', $arrDetial['id_tovar']);
                    foreach ($tv_document as $key_tv => $value_tv) {
                        $arr_tv_document[$value_tv['name']] = $value_tv['value'];
                    }
                    $testtt = '-';

                    if ($arr_documet['template'] != '24') {
                        $price_arr = $modx->runSnippet('ddGetMultipleField', array('docField' => 'price_calc', 'docId' => $arrDetial['id_tovar'], 'outputFormat' => 'JSON'));
                        $php_price_arr = json_decode($price_arr);
                        foreach ($php_price_arr as $key_pr => $value_pr) {
                            if ($arrDetial['count_tovar'] >= $value_pr[0]) {
                                $arr_tv_document['c_price'] = $value_pr[1];
                            }
                        }

                        //сохраняем начинки
                        $test = explode(',', $_SESSION['cart_nachinki'][$key]['cart_nachinki']);
                        $getNachinka = $modx->db->query('select pagetitle from modx_site_content where id in ("' . implode('","', $test) . '")');
                        $testtt = array();
                        $resultIdpriceNachinka = $modx->db->makeArray($getNachinka);

                        foreach ($resultIdpriceNachinka as $net) {
                            /*$arrDetialCart = array(
                                'pagetitle'=>$net['pagetitle'],
                                'cart_id'=>$int,
                            );*/
                            $testtt[] = $net['pagetitle'];
                            //$modx->db->insert($arrDetialCart,'cart_detaik_nac');

                        }
                        $testtt = implode(',<br/>', $testtt);
                    }


                    //сохраняем вес и id товара
                    $arrDetial['article'] = $arr_tv_document['articule'];
                    $arrDetial['detail_info'] = $testtt;
                    $arrDetial['price'] = $arr_tv_document['c_price'];
                    if ($Insert !== false) {
                        $result1 = $modx->db->insert($arrDetial, 'cart_detail');
                    }
                    //echo($result1);

                    //для письма админу
                    $getOrders['cart_body'] .= '<tr>
                    <td style="border:1px dotted gray;padding:10px;">' . $arr_documet['pagetitle'] . '</td>
                    <td style="border:1px dotted gray;padding:10px;">' . $arr_tv_document['articule'] . '</td>
                    <td style="border:1px dotted gray;padding:10px;">' . $arrDetial['count_tovar'] . '</td>
                    <td style="border:1px dotted gray;padding:10px;">' . $arr_tv_document['c_price'] . '</td>
                    <td style="border:1px dotted gray;padding:10px;">' . $testtt . '</td>
                    <td style="border:1px dotted gray;padding:10px;">' . $arrDetial['count_tovar'] * $arr_tv_document['c_price'] . '</td>
                    </tr>';


                    }
                }else{exit('Время сессии истекло, сделайте заказ заново');}
        } else {
            $getResult = $modx->db->select('*','cart_detail','cart_id = "'.$_GET['orderNumber'].'" ');

            $Data = $modx->db->makeArray($getResult);

            foreach($Data as $dat) {
                $arr_documet = $modx->getDocument($dat['id_tovar']);
                $tv_document = $modx->getTemplateVars(array('articule', 'c_price'), '*', $dat['id_tovar']);
                foreach ($tv_document as $key_tv => $value_tv) {
                    $arr_tv_document[$value_tv['name']] = $value_tv['value'];
                }

                $getOrders['cart_body'] .= '<tr>
                <td style="border:1px dotted gray;padding:10px;">' . $arr_documet['pagetitle'] . '</td>
                <td style="border:1px dotted gray;padding:10px;">' . $arr_tv_document['articule'] . '</td>
                <td style="border:1px dotted gray;padding:10px;">' . $dat['count_tovar'] . '</td>
                <td style="border:1px dotted gray;padding:10px;">' . $dat['price'] . '</td>
                <td style="border:1px dotted gray;padding:10px;">' . $dat['detail_info'] . '</td>
                <td style="border:1px dotted gray;padding:10px;">' . $dat['count_tovar'] * $dat['price'] . '</td>
                </tr>';
            }
        }

        return $Insert===false?$getOrders:$int;

    }

    static public function sendOrder()
    {
        global $modx;
		if(!$_GET['orderNumber']){
			return false;
		}
        $_SESSION['data_cart_id'] = $_GET['orderNumber'];

		$getOrders=sv_cart::saveOrder(false);
         if(strlen($getOrders['comment']) > 0){
            $getOrders['comment'] = '<p>Коментарий к заказу: '.$getOrders['comment'].'</p>';
        }
        if(strlen($getOrders['tort_name']) > 0){
             $getOrders['comment'] .='<p>Надпись на торт: '.$getOrders['tort_name'].'</p>';
        }
        $getOrders['id'] = (int)$_GET['orderNumber'];
        $html_admin = $modx->parseChunk('admin_mail_tpl',$getOrders,'[+','+]');
        $html_client = $modx->parseChunk('klient_mail_tpl',$getOrders,'[+','+]');

        //отправка клиенту
        $headers = "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

       
        mail($getOrders['email'],'Оформлен заказ',$html_client,$headers);

    //mail('poker717@gmail.com','Заказ N-'.$getOrders['id'],$html_admin,$headers);
    mail('anseroot@gmail.com','Заказ N-'.$getOrders['id'],$html_admin,$headers);
      //mail('d1.v.parshin@gmail.com','Заказ N-'.$getOrders['id'],$html_admin,$headers);
    mail('tortbar@yandex.ru','Заказ N-'.$getOrders['id'],$html_admin,$headers);
    mail('jenyatuchina@gmail.com','Заказ N-'.$getOrders['id'],$html_admin,$headers);
    mail('ar@net2pay.ru','Заказ N-'.$getOrders['id'],$html_admin,$headers);

//        return $_GET['orderNumber'];
    }

    static public function getAllPrice()
    {
        $price = 0;
        if(isset($_SESSION['cart_weight'])) {
            foreach($_SESSION['cart_weight'] as $key => $value) {
                $price+=$_SESSION['cart_weight'][$key]*$_SESSION['cart_price'][$key];
            }
        }

        if(1==1) {

        }
//unset($_SESSION['cart_weight']);unset($_SESSION['cart_img']);unset($_SESSION['cart_id']);unset($_SESSION['cart_price']);unset($_SESSION['cart_name']);

        return $price;
    }


    static public function getFormCart()
    {
        global $modx;

        $query = $modx->db->query('select id as id_rest,name_rest as name from restorans');
        $restorans = $modx->db->makeArray($query);

        $arr = array(
            'input'=>array(
                array(
                    'name'=>'name',
                    'placeholder'=>'Имя',
                    'required'=>true
                ),
                array(
                    'name'=>'surname',
                    'placeholder'=>'Фамилия',
                    'required'=>true
                ),
                array(
                    'name'=>'tel',
                    'placeholder'=>'Телефон',
                    'required'=>true
                ),
                array(
                    'name'=>'email',
                    'placeholder'=>'email',
                    'required'=>true
                ),
                array(
                    'name'=>'address',
                    'placeholder'=>'Улица',
                    'required'=>true
                ),
                array(
                    'name'=>'home',
                    'placeholder'=>'Дом',
                    'required'=>true
                ),
                array(
                    'name'=>'apartament',
                    'placeholder'=>'Квартира',
                    'required'=>true
                ),
                array(
                    'name'=>'comment',
                    'placeholder'=>'Текстовое описание',
                    'required'=>true
                ),
            ),
            'rest_id' => $restorans
        );
        return $arr;
    }
}