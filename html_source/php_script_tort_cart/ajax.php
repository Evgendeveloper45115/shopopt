<?php

require_once '../../../manager/includes/protect.inc.php';
include_once '../../../manager/includes/config.inc.php';
include_once MODX_BASE_PATH.'manager/includes/document.parser.class.inc.php';
include_once 'config_class.php';
include_once 'sv_class_cart.php';

$modx = new DocumentParser;
$modx->getSettings();
startCMSSession();
//var_dump($_POST['sv_send']);
$arrSV = json_decode($_POST['sv_send'],true);

if($arrSV['type']=='add_to_cart') {
    sv_cart::add_cart($arrSV['id'],$arrSV['weight'],$arrSV['nachinki']);
}

if($arrSV['type']=='delete_cart') {
    sv_cart::delete_cart($arrSV['id']);
}

if($arrSV['type']=='update_to_cart') {
    sv_cart::update_cart($arrSV['id'],$arrSV['weight'],$arrSV['nachinki']);
}

if($arrSV['type']=='save_order') {
    sv_cart::saveOrder($arrSV);
}


