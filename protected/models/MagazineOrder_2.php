<?php

/**
 * This is the model class for table "magazine_order_2".
 *
 * The followings are the available columns in table 'magazine_order_2':
 * @property integer $id
 * @property integer $count_item
 * @property string $cost_item
 * @property integer $item_sum
 * @property integer $with_bonus_sum
 * @property integer $bonus
 * @property integer $discount
 * @property integer $order_price
 * @property integer $order_id
 * @property integer $name
 * @property integer $status_item
 */
class MagazineOrder_2 extends AllModel
{


    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'magazine_order_2';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('count_item, cost_item, item_sum, with_bonus_sum, bonus, discount, order_price, order_id, status_item, name', 'safe'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array();
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'time_order' => 'Time Order',
            'show' => 'show',
            'status' => 'Status',
            'client_id' => 'Client',
            'manager_id' => 'Manager',
            'magazine_shop_id' => 'Вы должны указать магазин',
            'site_connect_id' => 'Site Connect',
            'client_company_id' => 'Вы должны указать компанию',
            'price' => 'price',
            'time_come' => 'time_come',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        $criteria = new CDbCriteria;
        $criteria->compare('id', $this->id);
        $criteria->compare('time_order', $this->time_order, true);
        $criteria->compare('status', $this->status);
        $criteria->compare('client_id', $this->client_id);
        $criteria->compare('manager_id', $this->manager_id);
        $criteria->compare('magazine_shop_id', $this->magazine_shop_id);
        $criteria->compare('site_connect_id', $this->site_connect_id);
        $criteria->compare('client_company_id', $this->client_company_id);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return MagazineOrder_2 the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
}
