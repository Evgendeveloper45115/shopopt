<?php

/**
 * This is the model class for table "magazine_clients_address".
 *
 * The followings are the available columns in table 'magazine_clients_address':
 * @property integer $id
 * @property string $address
 * @property string $phone
 * @property string $email
 * @property integer $user_id
 * @property integer $site_connect_id
 * @property string $name_contact
 * @property string $surname_contact
 * @property string $phone_contact
 * @property string $text_contact
 */
class MagazineClientsAddress extends AllModel
{
	/**
	 * @return string the associated database table name
	 */
	public $countNow=0;
	public $countHistory=0;

	public function tableName()
	{
		return 'magazine_clients_address';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('user_id, site_connect_id, email', 'required', 'message' => 'Обязательны для заполнения'),
			array('user_id, site_connect_id', 'numerical', 'integerOnly'=>true),
			array('address, email, name_contact, surname_contact', 'length', 'max'=>255 , 'message' => 'максимальная длина 25 символов'),
			array('phone, phone_contact', 'length',  'max'=>45 , 'message' => 'максимальная длина 45 символов'),
			array('text_contact', 'safe'),
			array('email', 'email', 'message' => 'Неправильный email'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, address, phone, email, user_id, site_connect_id, name_contact, surname_contact, phone_contact, text_contact', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'address' => 'Address',
			'phone' => 'Phone',
			'email' => 'Email',
			'user_id' => 'User',
			'site_connect_id' => 'Site Connect',
			'name_contact' => 'Name Contact',
			'surname_contact' => 'Surname Contact',
			'phone_contact' => 'Phone Contact',
			'text_contact' => 'Text Contact',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$connectID = SiteConnect::model()->findByAttributes(['url' => $_SERVER['SERVER_NAME']]);

		$criteria->compare('id',$this->id);
		$criteria->compare('address',$this->address,true);
		$criteria->compare('phone',$this->phone,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('user_id',Yii::app()->user->id);
		$criteria->compare('site_connect_id',$connectID->id);
		$criteria->compare('name_contact',$this->name_contact,true);
		$criteria->compare('surname_contact',$this->surname_contact,true);
		$criteria->compare('phone_contact',$this->phone_contact,true);
		$criteria->compare('text_contact',$this->text_contact,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return MagazineClientsAddress the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
