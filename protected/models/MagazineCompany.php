<?php

/**
 * This is the model class for table "magazine_company".
 *
 * The followings are the available columns in table 'magazine_company':
 * @property integer $id
 * @property string $name_company
 * @property integer $site_connect_id
 * @property integer $client_id
 * @property integer $inn
 * @property integer $kpp
 * @property integer $ogrn
 * @property string $ur_addr
 * @property string $chet
 * @property string $bank
 * @property integer $k_ch
 * @property string $gen_dir
 * @property string $image
 */
class MagazineCompany extends AllModel
{
	public $client_id;
	public $count;
	public $recTovar;
	public $recAkz;
	public $info;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'magazine_company';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name_company, site_connect_id, client_id', 'required'),
			array('site_connect_id, client_id, inn, kpp, ogrn, k_ch', 'numerical', 'integerOnly'=>true, 'message' => '{attribute} должен быть числом'),
			array('name_company, ur_addr, chet, bank, gen_dir', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, name_company, site_connect_id, client_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name_company' => 'Название компании',
			'site_connect_id' => 'Site Connect',
			'client_id' => 'Client',
			'inn' => 'ИНН',
			'kpp' => 'КПП',
			'ogrn' => 'ОГРН',
			'ur_addr' => 'Юр. Адрес',
			'chet' => 'Счет',
			'bank' => 'Банк',
			'k_ch' => 'К/сч',
			'gen_dir' => 'Генеральный директор',
			'image' => 'Фото',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name_company',$this->name_company,true);
		$criteria->compare('site_connect_id',$this->site_connect_id);
		$criteria->compare('client_id',$this->client_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return MagazineCompany the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
