<?php

/**
 * This is the model class for table "magazine_review".
 *
 * The followings are the available columns in table 'magazine_review':
 * @property integer $id
 * @property integer $client_id
 * @property integer $manager_id
 * @property integer $site_connect_id
 * @property string $text_review
 * @property string $text_answer
 * @property string $name
 * @property string $phone
 * @property string $email
 * @property string $position
 */
class MagazineReview extends AllModel
{
	public $info;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'magazine_review';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('client_id, manager_id, text_review', 'required'),
			array('client_id, manager_id, site_connect_id', 'numerical', 'integerOnly'=>true),
			array('name, phone, email, position', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, client_id, manager_id, text_review, name, phone, email, position', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'client_id' => 'Client',
			'manager_id' => 'Менеджер',
			'text_review' => 'Текст отзыва',
			'text_answer' => 'Текст отзыва',
			'name' => 'Ваше имя',
			'phone' => 'Телефон',
			'email' => 'Email',
			'position' => 'Должность',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('client_id',$this->client_id);
		$criteria->compare('manager_id',$this->manager_id);
		$criteria->compare('text_review',$this->text_review,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('phone',$this->phone,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('position',$this->position,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return MagazineReview the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
