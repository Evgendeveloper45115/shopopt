<?php

/**
 * This is the model class for table "site_connect".
 *
 * The followings are the available columns in table 'site_connect':
 * @property integer $id
 * @property string $url
 * @property string $email_admin
 * @property integer $status
 */
class SiteConnect extends CActiveRecord
{
    public $status = 0;
    const NGINX_DIR = '/usr/local/etc/nginx/';
    const SITE_DIR = '/var/www/magazineProject/';
    const CRM_DIR = '/Users/holyButters/PhpstormProjects/magazineCRM';

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'site_connect';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('status', 'numerical', 'integerOnly' => true),
            array('url, email_admin', 'length', 'max' => 255),
            array('url, email_admin, status', 'required'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, url, email_admin, status', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array();
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'url' => 'Url',
            'email_admin' => 'Email Admin',
            'status' => 'Status',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('url', $this->url, true);
        $criteria->compare('email_admin', $this->email_admin, true);
        $criteria->compare('status', $this->status);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return SiteConnect the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    protected function beforeSave()
    {
        if (parent::beforeSave()) {
            if (!isset($this->id) && is_file(self::NGINX_DIR . 'sites-available/' . $this->url) && is_file(self::NGINX_DIR . 'sites-enabled/' . $this->url)) {
                unlink(self::NGINX_DIR . 'sites-available/' . $this->url);
                unlink(self::NGINX_DIR . 'sites-enabled/' . $this->url);
            }
            $controller = new CController(null);
            $dataNginxConf = $controller->renderPartial('application.views.renderNginxConf.conf', ['model' => $this], true, true);
            file_put_contents(self::NGINX_DIR . 'sites-available/' . $this->url, $dataNginxConf);

            system('ln -sfv ' . self::NGINX_DIR . 'sites-available/' . $this->url . '  ' . self::NGINX_DIR . 'sites-enabled/' . $this->url);
            system('sudo launchctl unload /Library/LaunchDaemons/homebrew.mxcl.nginx.plist');
            system('sudo launchctl load /Library/LaunchDaemons/homebrew.mxcl.nginx.plist');
            return true;
        } else {
            return false;
        }
    }

    static function getSizeImage()
    {
        $url = Yii::app()->getBaseUrl(true);
        if (stristr($url, 'barbers')) {
            return 'height: 16vw; ';
        }
        return false;
    }
}
