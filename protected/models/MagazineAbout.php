<?php

/**
 * This is the model class for table "magazine_about".
 *
 * The followings are the available columns in table 'magazine_about':
 * @property integer $id
 * @property string $name_company
 * @property string $text_company
 * @property string $name_button
 * @property string $address_company
 * @property string $phone_company
 * @property string $email_company
 * @property integer $site_connect_id
 * @property string $point_x
 * @property string $img
 * @property string $point_y
 */
class MagazineAbout extends AllModel
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'magazine_about';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('site_connect_id', 'required'),
			array('site_connect_id', 'numerical', 'integerOnly'=>true),
			array('name_company, name_button, point_x, img, point_y', 'length', 'max'=>255),
			array('text_company, address_company, phone_company, email_company', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, name_company, text_company, name_button, address_company, phone_company, email_company, site_connect_id, point_x, img, point_y', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name_company' => 'Name Company',
			'text_company' => 'Text Company',
			'name_button' => 'Name Button',
			'address_company' => 'Address Company',
			'phone_company' => 'Phone Company',
			'email_company' => 'Email Company',
			'site_connect_id' => 'Site Connect',
			'point_x' => 'Point X',
			'img' => 'Img',
			'point_y' => 'Point Y',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name_company',$this->name_company,true);
		$criteria->compare('text_company',$this->text_company,true);
		$criteria->compare('name_button',$this->name_button,true);
		$criteria->compare('address_company',$this->address_company,true);
		$criteria->compare('phone_company',$this->phone_company,true);
		$criteria->compare('email_company',$this->email_company,true);
		$criteria->compare('site_connect_id',$this->site_connect_id);
		$criteria->compare('point_x',$this->point_x,true);
		$criteria->compare('img',$this->img,true);
		$criteria->compare('point_y',$this->point_y,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return MagazineAbout the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
