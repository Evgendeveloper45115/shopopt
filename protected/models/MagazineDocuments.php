<?php

/**
 * This is the model class for table "magazine_documents".
 *
 * The followings are the available columns in table 'magazine_documents':
 * @property integer $id
 * @property string $url
 * @property string $date_add
 * @property integer $order_id
 * @property integer $size_file
 * @property integer $status
 * @property string $name
 * @property string $extension
 */
class MagazineDocuments extends CActiveRecord
{
	public $file;
	public $comp;
	/**
	 * @return string the associated database table name
	 */
	static function HumanBytes($size) {
		$filesizename = array("Байты", " KB", " MB", " GB", " TB", " PB", " EB", " ZB", " YB");
		return $size ? round($size / pow(1024, ($i = floor(log($size, 1024)))), 2) . $filesizename[$i] : '0 Bytes';
	}

	public function tableName()
	{
		return 'magazine_documents';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('url, name, extension', 'required'),
			array('order_id, size_file, status', 'numerical', 'integerOnly'=>true),
			array('url, name, extension', 'length', 'max'=>255),
			array('date_add', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, url, date_add, order_id, size_file, status, name, extension', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'url' => 'Url',
			'date_add' => 'Date Add',
			'order_id' => 'Order',
			'size_file' => 'Size File',
			'status' => 'Status',
			'name' => 'Name',
			'extension' => 'Extension',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('url',$this->url,true);
		$criteria->compare('date_add',$this->date_add,true);
		$criteria->compare('order_id',$this->order_id);
		$criteria->compare('size_file',$this->size_file);
		$criteria->compare('status',$this->status);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('extension',$this->extension,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return MagazineDocuments the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
