<?php

/**
 * This is the model class for table "magazine_filters".
 *
 * The followings are the available columns in table 'magazine_filters':
 * @property integer $id
 * @property string $fields
 * @property integer $site_connect_id
 * @property SiteConnect $site
 * @property string $name
 */
class MagazineFilter extends AllModel
{
    public $item;
    public $subcat;

    public function behaviors()
    {
        return array(
            'nestedSetBehavior' => array(
                'class' => 'ext.nested-set-behavior-master.NestedSetBehavior',
                'leftAttribute' => 'lft',
                'rightAttribute' => 'rgt',
                'levelAttribute' => 'level',
                'hasManyRoots' => true,
            ),
            /*'TreeBehavior' => array(
                'class' => 'ext.nestedset.TreeBehavior',
                '_idCol' => 'id',
                '_lftCol' => 'lft',
                '_rgtCol' => 'rgt',
                '_lvlCol' => 'level',
            ),
            'TreeViewTreebehavior' => array(
                'class' => 'ext.nestedset.TreeViewTreebehavior',
            )*/
        );
    }

    public function treechild($id)
    {
        $curnode = self::model()->findByPk($id);
        if($curnode){
            $childrens = $curnode->children()->findAll();
            if(sizeOf($childrens)>0){
                $out = array();
                foreach($childrens as $children){
                    $currow=['id'=>$children->id,'text'=>$children->name,'children'=>$this->treechild($children->id)];
                    $out[]=$currow;
                }
                return $out;
            }
            else{
                return null;
            }
        }
        return null;
    }

    public function treedata()
    {
        $roots = self::model()->roots()->findAll();
        $out = array();
        foreach($roots as $root){
            $currow=['id'=>$root->id,'text'=>$root->name,'children'=>$this->treechild($root->id)];
            $out[]=$currow;
        }
        return $out;
    }
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'magazine_filters';
    }


    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('root, lft, rgt, level, name,	site_connect_id', 'safe'),
            array('item, subcat', 'required','message'=>'Заполните поле {attribute}.'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array('site' => array(self::BELONGS_TO, 'SiteConnect', 'site_connect_id'));
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'fields' => 'Фильтры и теги',
            'site_connect_id' => 'сайт',
            'item' => 'Категория',
            'subcat' => 'Подкатегория',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;
        $criteria->compare('id', $this->id);
        $criteria->compare('fields', $this->fields, true);
        $criteria->compare('site_connect_id', $this->site_connect_id, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return MagazineFilter the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
}
