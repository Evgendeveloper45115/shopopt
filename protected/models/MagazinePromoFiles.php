<?php

/**
 * This is the model class for table "magazine_promo_files".
 *
 * The followings are the available columns in table 'magazine_promo_files':
 * @property integer $id
 * @property string $name_file
 * @property integer $date_create
 * @property integer $file_size
 * @property integer $status
 * @property string $extension
 * @property integer $site_connect_id
 * @property string $url
 */
class MagazinePromoFiles extends AllModel
{
	public $file;
	public $site_connect_id;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'magazine_promo_files';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name_file, date_create, file_size, extension, url', 'required'),
			array('date_create, file_size, status, site_connect_id', 'numerical', 'integerOnly'=>true),
			array('name_file, extension, url', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, name_file, date_create, file_size, status, extension, site_connect_id, url', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name_file' => 'Name File',
			'date_create' => 'Date Create',
			'file_size' => 'File Size',
			'status' => 'Status',
			'extension' => 'Extension',
			'site_connect_id' => 'Site Connect',
			'url' => 'Url',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name_file',$this->name_file,true);
		$criteria->compare('date_create',$this->date_create);
		$criteria->compare('file_size',$this->file_size);
		$criteria->compare('status',$this->status);
		$criteria->compare('extension',$this->extension,true);
		$criteria->compare('site_connect_id',$this->site_connect_id);
		$criteria->compare('url',$this->url,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return MagazinePromoFiles the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
