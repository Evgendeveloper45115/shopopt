<?php

/**
 * This is the model class for table "magazine_product".
 *
 * The followings are the available columns in table 'magazine_product':
 * @property integer $id
 * @property integer $site_connect_id
 * @property integer $price
 * @property integer $min_count
 * @property integer $catalog_id
 * @property integer $publish
 * @property string $text_product
 * @property string $name_product
 * @property string $desc_product
 * @property string $image_product
 * @property integer $parent_id
 * @property string $type_product
 * @property string $articul
 * @property integer $discountPrice
 * @property string $countryOrigin
 * @property string $dop_fields
 * @property boolean $is_active
 * @property integer $balanceStock
 */
class MagazineProduct extends AllModel
{
    public $image;
    public $count;
    static $arrayTree = [];
    static $arrayTreeBread = [];

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'magazine_product';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('site_connect_id, catalog_id,  type_product', 'required'),
            array('site_connect_id, type, price, discountPrice, balanceStock, min_count, parent_id, catalog_id', 'numerical', 'integerOnly' => true),
            array('name_product, image_product_2, image_product_3', 'length', 'max' => 255),
            array('text_product, countryOrigin, articul, desc_product, image_product, title, title_en, is_active', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
//			array('id, site_connect_id, price, min_count, text_product, name_product, desc_product, image_product', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'infoOrder' => array(self::HAS_ONE, 'MagazineInfoOrder', 'goods_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'site_connect_id' => 'Site Connect',
            'price' => 'Цена',
            'discountPrice' => 'Цена со скидкой',
            'countryOrigin' => 'Страна производства',
            'is_active' => 'Отображать раздел',
            'dop_fields' => 'Доп. поля',
            'balanceStock' => 'Остаток на складе',
            'catalog_id' => 'Номер каталога',
            'min_count' => 'Минимальное количество',
            'text_product' => 'Описание продукта',
            'name_product' => 'Название продукта',
            'desc_product' => 'Дополнительное описание',
            'type_product' => 'Дополнительное описание',
            'image_product' => 'Картинка товара',
            'image_product_2' => 'Картинка товара',
            'image_product_3' => 'Картинка товара',
            'title' => 'title',
            'articul' => 'articul',
            'parent_id' => 'parent_id',
            'publish' => 'publish',
            'title_en' => 'title_en',
            'type' => 'type',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('site_connect_id', $this->site_connect_id);
        $criteria->compare('price', $this->price);
        $criteria->compare('discountPrice', $this->discountPrice);
        $criteria->compare('is_active', $this->is_active);
        $criteria->compare('min_count', $this->min_count);
        $criteria->compare('text_product', $this->text_product, true);
        $criteria->compare('name_product', $this->name_product, true);
        $criteria->compare('desc_product', $this->desc_product, true);
        $criteria->compare('image_product', $this->image_product, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return MagazineProduct the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @param $parentModel
     * @param bool|false $parent
     * @param bool|false $main
     * @return array
     */

    static function getTree($parentModel, $parent = false, $main = false)
    {
        /**
         * @var $parentModel self
         */

        if (!$parent) {
            self::$arrayTree[$parentModel->parent_id] = $parentModel->title;
        }
        if ($parentModel->parent_id) {
            $model = self::model()->findByPk($parentModel->parent_id);
            if ($model) {
                self::getTree($model, false);
            }
        }

        $arrayTree = array_reverse(self::$arrayTree);
        if ($main) {
            array_unshift($arrayTree, 'Каталог');
        }
        return $arrayTree;
    }


    static function getTreeBreadcrumbs($parentModel, $parent = false, $main = false)
    {
        /**
         * @var $parentModel self
         */
        if (!$parent) {
            if (in_array(trim($parentModel->title), $_GET) || array_key_exists(trim($parentModel->title), $_GET)) {
                self::$arrayTreeBread[] = trim($parentModel->title);
            }
        }

        if ($parentModel->parent_id) {
            $model = self::model()->findByPk($parentModel->parent_id);
            if ($model) {
                self::getTreeBreadcrumbs($model, false);
            }
        }

        $arrayTree = array_reverse(self::$arrayTreeBread);
        if ($main) {
            array_unshift($arrayTree, 'Каталог');
        }
        return $arrayTree;
    }
}
