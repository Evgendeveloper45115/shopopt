<?php

/**
 * This is the model class for table "magazine_bonus".
 *
 * The followings are the available columns in table 'magazine_bonus':
 * @property integer $id
 * @property integer $user_id
 * @property integer $active_bonus
 * @property datetime $date_create
 * @property integer $site_connect_id
 * @property integer $order_id
 */
class MagazineBonus extends CActiveRecord
{
    /**
     * @return string the associated database table name
     */

    public $children;

    public function tableName()
    {
        return 'magazine_bonus';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('user_id, active_bonus', 'required'),
            array('user_id, active_bonus', 'numerical', 'integerOnly' => true),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, user_id, active_bonus', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array();
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'client_id' => 'Client',
            'percent' => 'Persent',
            'sum' => 'Sum',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('client_id', $this->client_id);
        $criteria->compare('percent', $this->persent);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return MagazineBonus the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @param null $user_id
     */
    public static function getUserBonus($user_id = null)
    {

        $user_id = $user_id == null ? Yii::app()->user->id : $user_id;
        $bonuses = MagazineBonus::model()->findAllByAttributes(['user_id' => $user_id]);
        $used_bonus = 0;
        $activeBonus = 0;
        if (!empty($bonuses)) {
            foreach ($bonuses as $bonus) {
                $activeBonus += $bonus->active_bonus;
                $magBonOrders = MagazineBonusOrder::model()->findAllByAttributes(['bonus_id' => $bonus->id]);
                if (!empty($magBonOrders)) {
                    foreach ($magBonOrders as $magBonOrder) {
                        $used_bonus += $magBonOrder->used_bonus;
                    }
                }

            }
            return $activeBonus - $used_bonus;
        }
        return false;
    }
}
