<?php

/**
 * This is the model class for table "magazine_order".
 *
 * The followings are the available columns in table 'magazine_order':
 * @property integer $id
 * @property integer $time_order
 * @property string $time_come
 * @property integer $status
 * @property integer $price
 * @property integer $client_id
 * @property integer $manager_id
 * @property integer $discount
 * @property integer $magazine_shop_id
 * @property integer $site_connect_id
 * @property integer $show
 * @property integer $client_company_id
 */
class MagazineOrder extends AllModel
{

    public $company_name;
    public $date_m;
    public $date_d;
    public $date_y;
    public $countFiles;
    public $magazine_address;
    public $companyInfo;
    public $managerInfo;
    const STATUS_NEW = 0;
    const STATUS_COLLECTED = 1;
    const STATUS_SHIPPED = 2;
    const STATUS_CLOSE = 3;
    static $status_arr = ['Новый', 'Собирается', 'Отгружен', 'закрыт'];

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'magazine_order';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('time_order,price,client_id,discount, magazine_shop_id, site_connect_id', 'required', 'message' => 'Обязательно указать магазин и компанию'),
            array('status, client_id, manager_id, magazine_shop_id, site_connect_id', 'numerical', 'integerOnly' => true),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, time_order, status, client_id, manager_id, magazine_shop_id, site_connect_id', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'magazine_users' => array(self::BELONGS_TO, 'MagazineUsers', 'client_id', 'alias' => 'magazine_users'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'time_order' => 'Time Order',
            'show' => 'show',
            'status' => 'Status',
            'client_id' => 'Client',
            'manager_id' => 'Manager',
            'magazine_shop_id' => 'Вы должны указать магазин',
            'site_connect_id' => 'Site Connect',
            'client_company_id' => 'Вы должны указать компанию',
            'price' => 'price',
            'time_come' => 'time_come',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;
        if (Yii::app()->user->getRole() == WebUser::CLIENT) {
            $criteria->compare('client_id', Yii::app()->user->id);
        }

        if (Yii::app()->user->getRole() == WebUser::MANAGER) {
            $criteria->compare('manager_id', Yii::app()->user->id);
        }
        $criteria->compare('id', $this->id);
        $criteria->compare('time_order', $this->time_order, true);
        $criteria->compare('status', $this->status);
        $criteria->compare('client_id', $this->client_id);
        $criteria->compare('manager_id', $this->manager_id);
        $criteria->compare('magazine_shop_id', $this->magazine_shop_id);
        $criteria->compare('site_connect_id', $this->site_connect_id);
        $criteria->compare('client_company_id', $this->client_company_id);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return MagazineOrder the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
}
