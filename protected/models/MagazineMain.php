<?php

/**
 * This is the model class for table "magazine_main".
 *
 * The followings are the available columns in table 'magazine_main':
 * @property integer $id
 * @property string $text_popup
 * @property string $text_main
 * @property string $main_logo
 * @property integer $site_connect_id
 */
class MagazineMain extends AllModel
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'magazine_main';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('site_connect_id', 'required'),
			array('site_connect_id', 'numerical', 'integerOnly'=>true),
			array('main_logo', 'length', 'max'=>255),
			array('text_popup, text_main', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, text_popup, text_main, main_logo, site_connect_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'text_popup' => 'Text Popup',
			'text_main' => 'Text Main',
			'main_logo' => 'Main Logo',
			'site_connect_id' => 'Site Connect',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('text_popup',$this->text_popup,true);
		$criteria->compare('text_main',$this->text_main,true);
		$criteria->compare('main_logo',$this->main_logo,true);
		$criteria->compare('site_connect_id',$this->site_connect_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return MagazineMain the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
