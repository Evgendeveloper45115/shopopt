<?php

/**
 * This is the model class for table "magazine_users".
 *
 * The followings are the available columns in table 'magazine_users':
 * @property integer $id
 * @property string $user_email
 * @property string $user_password
 * @property string $user_phone
 * @property string $user_name
 * @property string $user_avatar
 * @property string $user_surname
 * @property integer $user_role
 * @property integer $site_connect_id
 */
class MagazineUsers extends AllModel
{
	public $file;
	public $clients;
	public $in_work;
	public $passw_old;
	public $passw_repeat;
	public $passw_repeat_2;
	public $price;
	public $price_work;
	public $in_review;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'magazine_users';
	}


	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('user_email, is_active, user_phone, user_name, user_role, site_connect_id', 'required'),
			array('user_role, site_connect_id', 'numerical', 'integerOnly'=>true),
			array('user_email', 'email', 'message' => 'неправильный email'),
			array('passw_repeat', 'required', 'on'=>'updatePass'),
			array('passw_repeat', 'compare', 'on'=>'updatePass', 'compareAttribute'=>'passw_repeat_2'),
			array('passw_old', 'currentPassword', 'on'=>'updatePass'),
			array('user_email', 'unique', 'message' => 'такой e-mail уже есть'),
			array('user_email, user_password, user_phone, user_name, user_surname', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, user_email, user_password, user_phone, user_name, user_surname, user_role, site_connect_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	public function currentPassword($attribute,$params)
	{
		if (!$this->validatePassword($attribute)){
			$this->addError($attribute, 'Неверный текущий пароль');
		}

	}
	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'user_email' => 'User Email',
			'user_password' => 'User Password',
			'user_phone' => 'User Phone',
			'user_name' => 'User Name',
			'user_surname' => 'User Surname',
			'user_role' => 'Роль пользователя',
			'user_avatar' => 'user_avatar',
			'site_connect_id' => 'Site Connect',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
		$criteria->compare('id',$this->id);
		$criteria->compare('user_email',$this->user_email,true);
		$criteria->compare('user_password',$this->user_password,true);
		$criteria->compare('user_phone',$this->user_phone,true);
		$criteria->compare('user_name',$this->user_name,true);
		$criteria->compare('user_surname',$this->user_surname,true);
		$criteria->compare('user_role',$this->user_role);
		$criteria->compare('site_connect_id',$this->site_connect_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return MagazineUsers the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function validatePassword($password)
	{
		if($password == 'passw_old' && $_POST['passw_old'] == $this->user_password) {
			return true;
		}else {
			return ($password == $this->user_password ? true : false);
		}
	}


	protected function beforeSave()
	{
		if (parent::beforeSave()) {
			if ($this->scenario === 'create') {
				$this->user_role = 1;
			}
			return true;
		} else {
			return false;
		}
	}
}
