<?php

/**
 * This is the model class for table "magazine_categories".
 *
 * The followings are the available columns in table 'magazine_categories':
 * @property integer $id
 * @property string $title
 * @property integer $parent_id
 * @property integer $site_connect_id
 * @property integer $global_parent_id
 * @property string $title_en
 */
class MagazineCategories extends AllModel
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'magazine_categories';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('site_connect_id, global_parent_id', 'required'),
			array('parent_id, site_connect_id, global_parent_id', 'numerical', 'integerOnly'=>true),
			array('title, title_en', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, title, parent_id, site_connect_id, global_parent_id, title_en', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'title' => 'Title',
			'parent_id' => 'Parent',
			'site_connect_id' => 'Site Connect',
			'global_parent_id' => 'Global Parent',
			'title_en' => 'Title En',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('parent_id',$this->parent_id);
		$criteria->compare('site_connect_id',$this->site_connect_id);
		$criteria->compare('global_parent_id',$this->global_parent_id);
		$criteria->compare('title_en',$this->title_en,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function getParentTypeById($id) {
		return @$this->model()->findByPk($id)->title;
	}

	public function getAllTypes() {
		$data = CHtml::listData($this->model()->findAll(), 'id', 'title');
		array_unshift($data, 'Корень') ;
		return $data;
	}
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return MagazineCategories the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
