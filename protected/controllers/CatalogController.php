<?php

class CatalogController extends Controller
{

    public $ids = null;

    public function actionIndex()
    {
        $this->render('index');
    }

    private function actionGetCatalog($lastID)
    {
        $ids = MagazineProduct::model()->findAll(['condition' => 'catalog_id = ' . $lastID]);
        if ($ids == null) {
            return;
        }
        foreach ($ids as $id) {
            $this->ids[] = $id->id;
            $this->actionGetCatalog($lastID);
        }
    }

    public function actionView()
    {

        $explode = explode('/', $_SERVER['REQUEST_URI']);
//		$_GET['find_item'] = '';
        if (!is_numeric($explode[(count($explode) - 1)])) {
            $explode[(count($explode) - 1)] = str_replace('?' . $_SERVER['QUERY_STRING'], '', $explode[(count($explode) - 1)]);
            //CVarDumper::dump(urldecode(trim($explode[(count($explode) - 1)])),11,1);
            $catId = MagazineProduct::model()->findAll('title = "' . urldecode(trim($explode[(count($explode) - 1)])) . '" ');
            if (count($catId) > 1) {
                foreach ($catId as $magazineProduct) {
                    if ($magazineProduct->parent_id && $magazineProduct->text_product == null) {
                        $model = MagazineProduct::model()->findByPk($magazineProduct->parent_id);
                        if (in_array(trim($model->title), $_GET) || array_key_exists(trim($model->title), $_GET)) {
                            $catId = $model->id;
                        }
                    }
                }
                $catId = MagazineProduct::model()->find('title = "' . urldecode(trim($explode[(count($explode) - 1)])) . '" and parent_id = "' . $catId . '" ');
            } else {
                $catId = MagazineProduct::model()->find('title = "' . urldecode(trim($explode[(count($explode) - 1)])) . '" ');
            }


            $this->ids[] = $catId->id;
            if ($catId->id == null) {
                $result2 = MagazineProduct::model()->findAll();
            } else {
                $result2 = MagazineProduct::model()->findAll('catalog_id = "' . $catId->id . '" ');
            }
            foreach ($result2 as $row2) {
                $this->ids[] = $row2->id;
            }
            if (in_array('', $this->ids)) {
                unset($this->ids[array_search('', $this->ids)]);
            }
            $result3 = MagazineProduct::model()->findAll('catalog_id in (' . implode(',', $this->ids) . ') ');
            foreach ($result3 as $row3) {
                $this->ids[] = $row3->id;
            }

            if (@$_GET['sort'] == 'asc' && isset($_GET['sort'])) {
                if (isset($_GET['find_item'])) {
                    $Items = MagazineProduct::model()->findAll(array('condition' => 'catalog_id in (' . implode(',', $this->ids) . ') and publish = 0 and type = 0 and name_product like "%' . $_GET['find_item'] . '%"', 'order' => 'price ASC'));
                } else {
                    $Items = MagazineProduct::model()->findAll(array('condition' => 'catalog_id in ( ' . implode(',', $this->ids) . ') and publish = 0 and type = 0 ', 'order' => 'price ASC'));
                }

            } else {
                if (isset($_GET['find_item'])) {
                    $Items = MagazineProduct::model()->findAll(array('condition' => 'catalog_id in ( ' . implode(',', $this->ids) . ') and publish = 0 and type = 0 and name_product like "%' . $_GET['find_item'] . '%"', 'order' => 'price DESC'));
                } else {
                    $Items = MagazineProduct::model()->findAll(array('condition' => 'catalog_id in ( ' . implode(',', $this->ids) . ') and publish = 0 and type = 0', 'order' => 'price DESC'));
                }
            }
            $this->render('category', [
                'Items' => $Items,
                'catId' => $catId
            ]);
        } else {
            $Item = MagazineProduct::model()->find('id = ' . $explode[(count($explode) - 1)]);
            $ids = [];

            $magazineRecommendPro = MagazineRecommendProduction::model()->findAll(['condition' => 'product_id = ' . $Item->id]);
            $types = MagazineTypeProduct::model()->findAll();
            foreach ($magazineRecommendPro as $id) {
                $ids[] = $id->recommend_id;
            }

            if (count($magazineRecommendPro) == 0) {
                $recMagaz = MagazineProduct::model()->findAll(['condition' => 'catalog_id = ' . $Item->catalog_id . ' and id not in (' . $Item->id . ')', 'limit' => '5']);

                foreach ($recMagaz as $rc) {
                    $ids[] = $rc->id;
                }
            }
            $ids = implode(',', $ids);
            $rec = [];
            if ($ids) {
                $rec = MagazineProduct::model()->findAll(['condition' => 'id in (' . $ids . ')']);
            }

            $_REQUEST['catalog'] = $explode[0];
            $this->render('item', [
                'Item' => $Item,
                'types' => $types,
                'rec' => $rec
            ]);
        }
    }

    public function actionCart()
    {
        $deliveryText = MagazinePublisherDelivery::model()->find();
        $CompanyUser = MagazineCompany::model()->findall(['condition' => 'client_id = "' . Yii::app()->user->id . '"']);
        $getUserPostavki = MagazineOrder::model()->findAll(['condition' => 'client_id = "' . Yii::app()->user->id . '" and time_order is not null', 'order' => 'time_order DESC']);
        $user = MagazineUsers::model()->findByPk(Yii::app()->user->id);
        $session = Yii::app()->session;
        $ids = [];

        if (isset($session['items'])) {
            foreach ($session['items'] as $key) {
                $ids[] = $key['id'];
            }
            $ids = implode(',', $ids);
        }
//		var_dump($ids);
        $order = null;
        $finish = false;
        $count = count($_POST['goods']);
        if (isset($_GET['finish'])) {
            $finish = true;
        }

        $mail = null;
//		$mail = "d1.v.parshin@gmail.com";
//
        if (isset($_POST['shop_form'])) {
            $order2 = [];
            $error = false;
            $oneBonus = [];
            foreach ($_POST['shop_form'] as $shop) {
                $price = 0;
                $order = new MagazineOrder();
                if (isset($_POST['time_order'])) {
                    $order->time_order = date('Y-m-d H:i:s', strtotime($_POST['time_order']));
                    $order->time_create = date('Y-m-d H:i:s');
                }

                $newPrice = null;
                $getManagerId = MagazineOrder::model()->find(['condition' => 'manager_id is not null and client_id = "' . Yii::app()->user->id . '" ']);
                if ($getManagerId) {
                    $manager = MagazineUsers::model()->find(['condition' => 'id = ' . $getManagerId->manager_id]);
                    if ($manager) {
                        $mail = $manager->user_email;
                    }
                    $order->manager_id = $getManagerId->manager_id;
                }
                $newPr = 0;
                foreach ($session['items'] as $val => $key) {
                    $product = MagazineProduct::model()->find(['condition' => 'id = ' . $key['id']]);
                    $order2[$key['id']]['count_item'] = $key['count'];
                    $order2[$key['id']]['cost_item'] = $product->discountPrice != '' ? $product->discountPrice : $product->price;
                    $order2[$key['id']]['name'] = $product->name_product;
                    $order2[$key['id']]['status'] = $product->discountPrice != '' ? 1 : 0;
                    $price += $product->discountPrice != '' ? ($product->discountPrice * $key['count']) : ($product->price * $key['count']);
                    $oneBonus[$key['id']] = $product->discountPrice != '' ? $product->discountPrice * $key['count'] : $product->price * $key['count'];
                    if ($product->discountPrice != '') {
                        $newPr += $product->discountPrice * $key['count'];
                    }
                }
                $newPr = $price - $newPr;
                $order->magazine_shop_id = $shop['shop_id'];
                $order->client_company_id = $_POST['cart_page_selector_company'];
                $order->client_id = Yii::app()->user->id;
                $order->site_connect_id = $this->connectID;
                $order->price = $price;
                $idscountClient = MagazineDiscount::model()->findAll(['condition' => 'client_id = "' . Yii::app()->user->id . '"']);
                $idddd = 0;
                foreach ($idscountClient as $idssss) {
                    if ($idssss->sum <= ($newPr != 0 ? $newPr : $price) && $idssss->persent > $idddd) {
                        $idddd = $idssss->persent;
                    }
                }

                if (isset($_POST['prepay']) && $_POST['prepay'] == 'on') {
                    $prepay = MagazinePrepay::model()->findByAttributes(['user_id' => Yii::app()->user->id]);
                    if ($prepay) {
                        $idddd = $idddd + $prepay->percent;
                        if ($idddd > 70) {
                            $idddd = 70;
                        }
                        $discount = (($newPr != 0 ? $newPr : $price) * $idddd) / 100;
                        $order->discount = $discount;
                    }
                } else {
                    if ($idddd > 70) {
                        $idddd = 70;
                    }
                    $order->discount = (($newPr != 0 ? $newPr : $price) / 100) * $idddd;
                }
                if (isset($_POST['bonus_value']) && $_POST['bonus_value'] != '') {
                    $order->price -= (int)$_POST['bonus_value'];
                    $order->discount += (int)$_POST['bonus_value'];
                }
                $criteria = new CDbCriteria();
                $criteria->order = "id DESC";
                $criteria->limit = 1;

                $last_ID_ORDER = MagazineOrder::model()->find($criteria);

                $order->id = ($last_ID_ORDER->id + 1);
                $order->price = $price - $order->discount;
                $freeBonus = 0;
                foreach ($oneBonus as $key => $cost) {
                    $magPr = MagazineProduct::model()->findByPk($key);
                    if ($magPr->discountPrice != '') {
                        $freeBonus += $cost;
                    }
                }
                foreach ($oneBonus as $key => $cost) {
                    $order2[$key]['discount'] = $order->discount - (int)$_POST['bonus_value'];
                    $order2[$key]['order_price'] = $order->price;
                    $bonus = 0;
                    $magPr = MagazineProduct::model()->findByPk($key);
                    if (isset($_POST['bonus_value'])) {
                        if ($magPr->discountPrice == '') {
                            $bonus = round(($cost / ($order->price + $order->discount - $freeBonus) * (int)$_POST['bonus_value']));
                            $order2[$key]['item_sum'] = $cost;
                        } else {
                            $order2[$key]['item_sum'] = $cost;
                        }
                    } else {
                        if ($magPr->discountPrice == '') {
                            $order2[$key]['item_sum'] = $cost / $order2['order_price'][0];
                        } else {
                            $order2[$key]['item_sum'] = $cost;
                        }


                    }
                    $order2[$key]['with_bonus_sum'] = $cost - $bonus;
                    $order2[$key]['bonus'] = $bonus;
                }
                if ($order->save()) {
                    $order2['order_id'] = [$order->id];
                    if (isset($_POST['bonus_value']) && $_POST['bonus_value'] != '') {
                        $bonus = MagazineBonus::model()->findByAttributes(['user_id' => Yii::app()->user->id]);
                        if ($bonus) {
                            $bonusOrder = new MagazineBonusOrder();
                            $bonusOrder->bonus_id = $bonus->id;
                            $bonusOrder->order_id = $order->id;
                            $bonusOrder->used_bonus = $_POST['bonus_value'];
                            $bonusOrder->site_connect_id = $this->connectID;
                            $bonusOrder->date_create = date('Y-m-d H:i:s');
                            $bonusOrder->save(false);
                        }
                    }
                    foreach ($_POST['goods'] as $key => $gds) {
                        $ord2 = new MagazineOrder_2();
                        $ord2->count_item = $order2[$gds['goods_id']]['count_item'];
                        $ord2->cost_item = $order2[$gds['goods_id']]['cost_item'];
                        $ord2->name = $order2[$gds['goods_id']]['name'];
                        $ord2->discount = $order2[$gds['goods_id']]['discount'];
                        $ord2->order_price = $order2[$gds['goods_id']]['order_price'];
                        $ord2->item_sum = $order2[$gds['goods_id']]['item_sum'];
                        $ord2->with_bonus_sum = $order2[$gds['goods_id']]['with_bonus_sum'];
                        $ord2->bonus = $order2[$gds['goods_id']]['bonus'];
                        $ord2->order_id = $order2['order_id'][0];
                        $ord2->status_item = $order2[$gds['goods_id']]['status'];
                        $ord2->image = MagazineProduct::model()->findByPk($gds['goods_id'])->image_product;
                        $ord2->site_connect_id = $this->connectID;
                        $ord2->goods_id = $gds['goods_id'];
                        $ord2->save(false);
                        $infoOrder = new MagazineInfoOrder();
                        $infoOrder->site_connect_id = $this->connectID;
                        $infoOrder->goods_id = $gds['goods_id'];
                        $infoOrder->goods_count = $gds['goods_count'];
                        $infoOrder->order_id = $order->id;
                        $infoOrder->save();
                    }
                } else {
                    $error = true;
                }
            }
            if ($error == false) {

                $name = '=?UTF-8?B?' . base64_encode('Новый заказ') . '?=';
                $subject = '=?UTF-8?B?' . base64_encode('Новый заказ') . '?=';
                $headers = "From: $name <{$_SERVER['SERVER_NAME']}>\r\n" .
                    "Reply-To: {$mail}\r\n" .
                    "MIME-Version: 1.0\r\n" .
                    "Content-Type: text/html; charset=UTF-8";


                $cart = MagazineProduct::model()->findAll(['condition' => 'id in (' . $ids . ')']);
                foreach ($cart as $cr) {
                    foreach ($session['items'] as $key => $count) {
                        if ($count['id'] == $cr->id) {
                            $cr->count = $count['count'];
                        }
                    }
                }
                $str = '';
                foreach ($cart as $key => $cr) {
                    $image = null;
                    if ($cr->image_product) {
                        $json = json_decode($cr->image_product);
                        if (!empty($json)) {
                            foreach ($json as $img) {
                                if ($img != '' || !stristr($img, '[')) {
                                    $image = $img;
                                    break;
                                }
                            }
                        }
                    }
                    $str .= '<tr>
<td style="border: 1px solid #c2c2c2;font-size: 16px;padding: 5px 2%;width: 20%;"><img src="http://' . $_SERVER['SERVER_NAME'] . $image . '" style="width:100%;height:auto;"/></td>
		<td style="border: 1px solid #c2c2c2;font-size: 16px;padding: 5px 2%;width: 39%;">' . $cr->name_product . '</td>
		<td style="border: 1px solid #c2c2c2;font-size: 16px;padding: 5px 2%;width: 15%;">' . $cr->price . '</td>
		<td style="border: 1px solid #c2c2c2;font-size: 16px;padding: 5px 2%;width: 15%;">' . $cr->count . '</td>
		<td style="border: 1px solid #c2c2c2;font-size: 16px;padding: 5px 2%;width: 15%">' . $cr->price * $cr->count . '</td>
	</tr>';
                }
                foreach ($_POST['shop_form'] as $shop) {
                    $order_id = MagazineOrder::model()->find(['condition' => 'magazine_shop_id = ' . $shop['shop_id'], 'order' => 'id DESC']);
                    $getShopName = MagazineClientsAddress::model()->findByPk($shop['shop_id']);
                    $getCompany = MagazineCompany::model()->findByPk($_POST['cart_page_selector_company']);
                    mail(
                        $mail,
                        $subject,
                        '
					<p>Заказ опт №' . $order_id->id . ' - "' . Yii::app()->getBaseUrl(true) . '"</p>
					<table style=" border-collapse: collapse;border: 1px solid #c2c2c2; width: 100%;">
						<tr>
						<td style="border: 1px solid #c2c2c2;font-size: 12px;text-transform: uppercase;color: #c2c2c2;padding: 5px 2%;width: 15%;">Вид товара</td>
							<td style="border: 1px solid #c2c2c2;font-size: 12px;text-transform: uppercase;color: #c2c2c2;padding: 5px 2%;width: 39%;">Наименование</td>
							<td style="border: 1px solid #c2c2c2;font-size: 12px;text-transform: uppercase;color: #c2c2c2;padding: 5px 2%;width: 15%;">Цена</td>
							<td style="border: 1px solid #c2c2c2;font-size: 12px;text-transform: uppercase;color: #c2c2c2;padding: 5px 2%;width: 15%;">кол-во</td>
							<td style="border: 1px solid #c2c2c2;font-size: 12px;text-transform: uppercase;color: #c2c2c2;padding: 5px 2%;width: 15%">Стоимость</td>
						</tr>
						' . $str . '
					</table>
					<div style="width: 100%;text-align: right;font-weight: bold;"><p style="padding-right: 50px;">Итого: ' . $order->price . '</p></div>
					<p>Заказ будет доставлен по адресу:' . $getShopName->address . '</p>
					<p>Принимающий: ' . $getShopName->surname_contact . '  ' . $getShopName->name_contact . ' тел. ' . $getShopName->phone_contact . '</p>
					<p>Компания: ' . $getCompany->name_company . '</p>
					<p>Спасибо за заказ!</p>
					',
                        $headers
                    );
                }
                $_SESSION['items'] = null;
                session_write_close();
                $this->redirect('/catalog/cart?finish');
            }
        }

        if (!empty($ids) && isset($session['items'])) {
            $cart = MagazineProduct::model()->findAll(['condition' => 'id in (' . $ids . ')']);

            foreach ($cart as $cr) {
                foreach ($session['items'] as $key => $count) {
                    if ($count['id'] == $cr->id) {
                        $cr->count = $count['count'];
                    }
                }
            }

            if (isset(Yii::app()->user->id)) {
                $magazineClientsAddress = MagazineClientsAddress::model()->findAll(['condition' => 'user_id = ' . Yii::app()->user->id]);
            }
            $idscountClient = MagazineDiscount::model()->findAll(['condition' => 'client_id = "' . Yii::app()->user->id . '"']);
            $discount = [];

            foreach ($idscountClient as $clientDiscount) {
                $discount [] = [$clientDiscount->persent, $clientDiscount->sum];
            }
            $discount = json_encode($discount);
            $prepay = MagazinePrepay::model()->findByAttributes(['user_id' => Yii::app()->user->id]);
            @$this->render('cart',
                [
                    'finish' => $finish,
                    'cart' => $cart,
                    'magazineClientsAddress' => $magazineClientsAddress,
                    'sessions' => $session['items'],
                    'deliveryText' => $deliveryText,
                    'CompanyUser' => $CompanyUser,
                    'order' => $order,
                    'discount' => $discount,
                    'prepay' => $prepay,
                    'user' => $user,
                    'getUserPostavki' => $getUserPostavki
                ]);
        } else {

            @$this->render('cart', ['emptyCart' => true, 'finish' => $finish]);
        }

    }

    public function actionAjax()
    {
        $session = Yii::app()->session;
        if (trim($_POST['type']) == 'add_to_cart') {
            if (!empty($_SESSION['items'])) {
                $add = false;
                foreach ($_SESSION['items'] as $key => &$item) {
                    if ($item['id'] == $_POST['id']) {
                        $add = true;
                        $_SESSION['items'][$key]['count'] += $_POST['count'];
                    }
                }
                if ($add == false) {
                    $item = ['id' => $_POST['id'], 'count' => $_POST['count']];
                    $_SESSION['items'][] = $item;
                }
            } else {

                $item = ['id' => $_POST['id'], 'count' => $_POST['count']];
                $_SESSION['items'][] = $item;
            }
            session_write_close();
            exit;
        }

        if ($_POST['type'] == 'update') {
            foreach ($_SESSION['items'] as $key => $item) {
                if ($item['id'] == $_POST['id']) {
                    $_SESSION['items'][$key]['count'] = $_POST['value'];
                    $item['count'] = $_POST['value'];
                }
            }
            session_write_close();
            exit;
        }

        if ($_POST['type'] == 'delete') {
            foreach ($_SESSION['items'] as $key => $item) {
                if ($item['id'] == $_POST['id']) {
                    unset($_SESSION['items'][$key]);
                }
            }
            session_write_close();
            exit;
        }
    }

    public function actionUnajax()
    {
        $session = Yii::app()->session;
        $_SESSION['items'] = null;
        $session->close();
    }

    // Uncomment the following methods and override them if needed
    /*
    public function filters()
    {
        // return the filter configuration for this controller, e.g.:
        return array(
            'inlineFilterName',
            array(
                'class'=>'path.to.FilterClass',
                'propertyName'=>'propertyValue',
            ),
        );
    }

    public function actions()
    {
        // return external action classes, e.g.:
        return array(
            'action1'=>'path.to.ActionClass',
            'action2'=>array(
                'class'=>'path.to.AnotherActionClass',
                'propertyName'=>'propertyValue',
            ),
        );
    }
    */
}