<?php

class MagazineReviewController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{

		$model=new MagazineReview;
		$getManagerUsers = MagazineOrder::model()->find(['condition'=>'client_id = ' . Yii::app()->user->id.' and manager_id is not null']);

		$userClient = MagazineUsers::model()->find(['condition' => 'id = ' . Yii::app()->user->id]);
		$model->name = $userClient->user_name;
		$model->phone = $userClient->user_phone;
		$model->email = $userClient->user_email;

		$managers = MagazineUsers::model()->findAll(['condition' => 't.id IN ("'.implode('","', $getManagerUsers->manager_id).'")']);

		$managers = CHtml::listData($managers,'id', 'user_name');
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['MagazineReview']))
		{

			$_POST['MagazineReview']['client_id'] = Yii::app()->user->id;
			$_POST['MagazineReview']['manager_id'] = $getManagerUsers->id;
			$_POST['MagazineReview']['site_connect_id'] = $this->connectID;
			$model->attributes=$_POST['MagazineReview'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
			'getManagerUsers'=>$managers
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);
		$getManagerUsers = MagazineOrder::model()->findAll(['condition'=>'client_id = ' . Yii::app()->user->id.' and manager_id is not null']);
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['MagazineReview']))
		{
			$_POST['MagazineReview']['client_id'] = Yii::app()->user->id;
			$model->attributes=$_POST['MagazineReview'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model' => $model,
			'getManagerUsers' => $getManagerUsers
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('MagazineReview');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new MagazineReview('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['MagazineReview']))
			$model->attributes=$_GET['MagazineReview'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return MagazineReview the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=MagazineReview::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param MagazineReview $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='magazine-review-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
