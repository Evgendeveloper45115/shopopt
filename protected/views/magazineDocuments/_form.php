<?php
/* @var $this MagazineDocumentsController */
/* @var $model MagazineDocuments */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'magazine-documents-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
	'htmlOptions'=>array('enctype'=>'multipart/form-data'),
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>
	<div class="row">
		<?php echo $form->labelEx($model,'url'); ?>
		<?php echo $form->fileField($model,'url'); ?>
		<?php echo $form->error($model,'url'); ?>
	</div>


	<div class="row">
		<?php echo $form->labelEx($model,'order_id'); ?>
		<?php echo CHtml::dropDownList('order_id', '',$ordersIds); ?>
		<?php echo $form->error($model,'order_id'); ?>
	</div>


	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->