<?php
/* @var $this MagazineDocumentsController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Magazine Documents',
);

$this->menu=array(
	array('label'=>'Create MagazineDocuments', 'url'=>array('create')),
	array('label'=>'Manage MagazineDocuments', 'url'=>array('admin')),
);
?>

<h1>Magazine Documents</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
