<?php
/* @var $this SiteConnectController */
/* @var $model SiteConnect */

?>
server {
    listen 80;

       server_name  <?=$model->url?>;


       set $rooting <?= $model::CRM_DIR?>;

       location / {
        root <?= $model::CRM_DIR?>;
        index  index.php index.html index.htm;
           try_files $uri $uri/ /index.php?$args;
           #rewrite .* /index.php?$args;
       }

        location ~* \.css|\.js|\.jpg|\.jpeg|\.png|\.gif|\.swf|\.svg|\.tiff|\.pdf$ {
        root <?= $model::CRM_DIR?>;
         }

        location ~ \.php$ {
            root <?= $model::CRM_DIR?>;
            try_files $uri = 404;
            fastcgi_pass    backend-demoAdmin;
            fastcgi_index  index.php;
            fastcgi_param  SCRIPT_FILENAME <?= $model::CRM_DIR?>$fastcgi_script_name;
            include        fastcgi_params;
        }
}


