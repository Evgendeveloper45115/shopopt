<div id="about_wrap" class="img_fon" style="background-image:url(<?=$model->img?>)">
    <div id="about_center">
        <p id="about_title"><?=$model->name_company?></p>
        <div id="about_intro"><?=$model->text_company?></div>
        <button class="css_trans border_hover color_th_hover"><?=$model->name_button?></button>
    </div>
</div>

<div id="call_back">
    <p id="call_back_title">Свяжитесь с нами</p>
    <div class="inline_style" id="map" data-x="<?=$model->point_x?>" data-y="<?=$model->point_y?>">

    </div><div class="inline_style" id="call_back_descr">
        <ul>
            <!-- !!!!!!!!!!ТУТ НУЖНЫ СВГ!!!!!!!!!!!-->
            <li>
                <!--to do change to svg-->
                <img src="/html_source/img/temp_ico1.png">
                <!--to do change to svg-->
                <div class="inline_style">
                    <?=$model->address_company?>
                </div>
            </li>
            <li>
                <!--to do change to svg-->
                <img src="/html_source/img/temp_ico2.png">
                <!--to do change to svg-->
                <div class="inline_style">
                    <?=$model->phone_company?>
                </div>
            </li>
            <li>
                <!--to do change to svg-->
                <img src="/html_source/img/temp_ico3.png">
                <!--to do change to svg-->
                <div class="inline_style">
                    <p>Электронный адрес:</p>
                    <a href="mailto:mail@mail.ru"><?=$model->email_company?></a>
                </div>
            </li>
        </ul>
        <div id="about_form">
            <p>Задать вопрос</p>
            <form name="about_form">
                <input type="text" name="name" placeholder="Имя" class="css_trans">
                <input type="text" name="mail" placeholder="E-mail"  class="css_trans">
                <textarea name="quest" placeholder="Ваш вопрос"  class="css_trans"></textarea>
                <button class="bg_color_th">Отправить</button>
            </form>
        </div>
    </div>
    <script type="text/javascript" src="http://maps.google.com/maps/api/js?libraries=geometry&amp;"></script>
    <script src="/html_source/js/map.js"></script>
</div>