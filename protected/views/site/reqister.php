<?php
/* @var $this MagazineUsersController */
/* @var $model MagazineUsers */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'magazine-users-reqister-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// See class documentation of CActiveForm for details on this,
	// you need to use the performAjaxValidation()-method described there.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'user_email'); ?>
		<?php echo $form->textField($model,'user_email'); ?>
		<?php echo $form->error($model,'user_email'); ?>
	</div>

	<div class="row">

		<?php echo $form->labelEx($model,'user_phone'); ?>
		<?php $this->widget("ext.maskedInput.MaskedInput", array(
			"model" => $model,
			"attribute" => "user_phone",
			"mask" => '+7(999) 999-9999'
		)); ?>
		<?php echo $form->error($model,'user_phone'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'user_name'); ?>
		<?php echo $form->textField($model,'user_name'); ?>
		<?php echo $form->error($model,'user_name'); ?>
	</div>

<!--	<div class="row">-->
<!--		--><?php //echo $form->labelEx($model,'user_role'); ?>
<!--		--><?php //echo $form->textField($model,'user_role'); ?>
<!--		--><?php //echo $form->error($model,'user_role'); ?>
<!--	</div>-->

<!--	<div class="row">-->
<!--		--><?php //echo $form->labelEx($model,'site_connect_id'); ?>
<!--		--><?php //echo $form->textField($model,'site_connect_id'); ?>
<!--		--><?php //echo $form->error($model,'site_connect_id'); ?>
<!--	</div>-->

	<div class="row">
		<?php echo $form->labelEx($model,'user_password'); ?>
		<?php echo $form->textField($model,'user_password'); ?>
		<?php echo $form->error($model,'user_password'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'user_surname'); ?>
		<?php echo $form->textField($model,'user_surname'); ?>
		<?php echo $form->error($model,'user_surname'); ?>
	</div>


	<div class="row buttons">
		<?php echo CHtml::submitButton('Submit'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->