<div id="align_zone">
<p class="reset_form_title">Сохраните новый пароль</p>
<div class="reset_align">
    <form action="" name="reset_pass" method="post">
        <p class="reset_error_msg"></p>
        <div class="reset_line">
            <p>Новый пароль:</p>
            <input type="password" class="css_trans rip" name="frst_pass">
<!--            <input type="hidden" name="email" value="--><?//=$email?><!--">-->
        </div>
        <div class="reset_line">
            <p>Повторите новый пароль:</p>
            <input type="password" class="css_trans rip" name="sec_pass">
        </div>
        <button type="submit" class="bg_color_th" value="">Сохранить</button>
    </form>
</div>
<script>
    $(function(){
        $('.reset_align button').click(function(e){
            if( $('input[name="frst_pass"]').val() !=   $('input[name="sec_pass"]').val()){
                e.preventDefault();
                $('.reset_error_msg').text('Пароли не совпадают.');
            }else{
                $('.reset_error_msg').text('');
            }
            if(!chek_form($('.rip'),2)){
                e.preventDefault();
            }


        });
    });
</script>
</div>