<?php
/* @var $this SiteController */
/* @var $app CWebApplication */

$this->pageTitle = Yii::app()->name;
$app = Yii::app();
?>

<div id="main_sl_wrap">
    <div id="main_sl">
        <?php foreach ($getSlider as $gSlider) {
            echo '<div class="img_fon" style="background-image:url(' . $gSlider->image . ')"></div>';
        }
        ?>
    </div>
    <div id="main_sl_text_wrap">
        <div class="vert_style">
            <img src="<?= $getMainInfo->main_logo ?>">

            <p style="padding:  3.3em;"><?= $getMainInfo->text_main ?></p>
            <button class="bg_color_th">Подробнее</button>
            <div class="hid_content">
                <?= $getMainInfo->text_popup ?>
            </div>
        </div>
        <div class="vert_style"></div>

    </div>
</div>

<div id="main_new_content">
    <p class="mnc_title">Новинки</p>

    <div>
        <?php
        $url = '';
        $parentUrl = '';
        $links = [];
        foreach ($getLastProducts as $gLastProduct) {
            $buttonText = 'Купить';
            if (isset($app->session['items'])) {
                foreach ($app->session['items'] as $items) {
                    if ($gLastProduct->id == $items['id']) {
                        $buttonText = 'В корзине';
                    }
                }
            }
            ?>
            <div class="goods_item css_trans shadow_hover">
            <?php
            $url = MagazineProduct::model()->find(['condition' => 'id = ' . $gLastProduct->catalog_id]);

            $links = MagazineProduct::getTree($url, false);
            $json = null;
            if ($gLastProduct->image_product) {
                $json = json_decode($gLastProduct->image_product);
            }
            if (!empty($json[0])) {
                if (!stristr($json[0], '[')) {
                    $json = $json[0];
                } else {
                    $json = $json[1];
                }
            }
            if (is_array($json)) {
                $json = $json[1];
            }
            $href = '/catalog/';
            foreach ($links as $link) {
                $href .= $link . '/';
            }
            $href .= $gLastProduct->id;
            ?>
            <a href="<?= $href ?>"
               data-id="<?= $gLastProduct->id ?>" class="href_fix">
                <div class="img_fon"
                     style="<?= SiteConnect::getSizeImage() ?>background-image:url(<?= $json != '' ? $json : null ?>)"></div>
                <p><?= $gLastProduct->name_product ?></p>
            </a>
            <div class="price inline_style color_th"><span class="money"><?= $gLastProduct->price ?></span><span
                    class="rub_lt">a</span></div>
            <?php
            if ($this->connectID == 36) {
                ?>
                <button class="bg_color_th" data-id="<?= $gLastProduct->id ?>" data-price="<?= $gLastProduct->price ?>"
                        data-stock="<?= $gLastProduct->balanceStock ? $gLastProduct->balanceStock : 0 ?>"
                        data-min="<?= $gLastProduct->min_count ?>"><?= $buttonText ?></button>
                <?php
            } else {
                ?>
                <button class="bg_color_th" data-id="<?= $gLastProduct->id ?>" data-price="<?= $gLastProduct->price ?>"
                        data-min="<?= $gLastProduct->min_count ?>"><?= $buttonText ?></button>

                <?php
            }
            ?>
            <?php
            if ($gLastProduct->balanceStock === '0' && $this->connectID == 36) {
                ?>
                    <div class="block_item">
                        <p style="color: white">Временно нет в наличии</p>
                    </div>
                <?php
            }
            ?>
            </div><?php } ?>
        <!--end singl-->
    </div>
</div>

<!--<script type="text/javascript">
	$(function(){
		$('.href_fix').on('click', function(e){
			e.preventDefault();
			var str_href = $(this).attr('data-id');
			console.log(str_href);
			$('#yw0 a').each(function(){
				if($(this).attr('href').indexOf(str_href) !== -1) {
					window.location.href = $(this).attr('href');
				}
			});
		});
	});
</script>
-->