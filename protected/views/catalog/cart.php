<?php
/**
 * Created by PhpStorm.
 * User: holyButters
 * Date: 27.12.15
 * Time: 19:39
 */
?>
<?php if ($emptyCart == true) {
    echo 'Ваша корзина пуста';
    if ($finish == true) { ?>

        <script type="text/javascript">
            $(function () {
                order_ok();
            });
        </script>
    <?php }
} else { ?>
    <script type="text/javascript">
        var discont_rules =  <?=$discount?>;
    </script>
    <div id="category_title_wrap">
        <p>Ваш заказ</p>
    </div>
    <?php if (@$order != null) {
        foreach ($order->getErrors() as $error => $post) {
            foreach ($post as $key) {
                echo $key;
            }
        }
    } ?>
    <div id="cart_item_table">
        <div id="cart_item_head">
            <div class="row1 inline_style">
                Фото
            </div>
            <div class="row2 inline_style">
                Наименование
            </div>
            <div class="row3 inline_style">
                Цена
            </div>
            <div class="row4 inline_style">
                Количество
            </div>
            <div class="row5 inline_style">
                Стоимость
            </div>
        </div>
        <div id="cart_item_body">
            <!-- cart_item_line-->
            <!-- single cart_item_line-->
            <?php
            $allPrice = 0;
            foreach ($cart as $key => $cr) {
                $image = null;
                if ($cr->image_product) {
                    $json = json_decode($cr->image_product);
                    if (is_array($json) && !empty($json)) {
                        foreach ($json as $img) {
                            if ($img != '' || !stristr($img, '[')) {
                                $image = $img;
                                break;
                            }
                        }
                    }

                }
                ?>
                <div class="cart_item_line" data-id="<?= $cr->id ?>"
                     data-disc="<?= $cr->discountPrice != '' ? $cr->discountPrice : 0 ?>">

                    <a href="/catalog/<?= $cr->id ?>">
                        <div class="row1 inline_style img_fon" style="background-image:url(<?= $image ?>)">

                        </div>
                    </a><a href="/catalog/<?= $cr->id ?>">
                        <div class="row2 inline_style">
                            <div style="display: inline-block"><?= $cr->name_product ?></div>
                            <?php
                            if ($cr->discountPrice != '') {
                                ?>
                                <div class="percent_cart" style="display: inline-block;vertical-align: middle;">

                                </div>
                                <?php
                            }
                            ?>
                        </div>
                    </a>

                    <div class="row3 inline_style">
                        <span
                            class="money"><?= $cr->discountPrice != '' ? $cr->discountPrice : $cr->price ?></span><span
                            class="rub_lt">a</span>
                    </div>
                    <div class="row4 inline_style">
                        <span class="dec color_th_hover css_trans">-</span><input disabled="disabled" type="text"
                                                                                  value="<?= $cr->count ?>"
                                                                                  data-min="<?= $cr->min_count ?>"><span
                            class="inc color_th_hover css_trans">+</span>
                    </div>
                    <div class="row5 inline_style">
                        <span
                            class="money"><? $allPrice += ($cr->discountPrice != '' ? $cr->discountPrice : $cr->price) * $cr->count; ?><?= ($cr->discountPrice != '' ? $cr->discountPrice : $cr->price) * $cr->count ?></span><span
                            class="rub_lt">a</span>
                    </div>
                    <div class="row6 inline_style color_th_hover css_trans" style="color: red;">Удалить
                    </div>
                </div>
            <?php } ?>
            <!--end single cart_item_line-->

            <!--end cart_item_line-->
        </div>
        <div id="cart_item_footer">
            <?php if (!empty($discount)) { ?>
                <div class="inline_style discont">
                    <div class="discont_line">Ваша скидка сейчас - <b><span id="pr_value">0</span>%</b>
                        <button id="info_pr_text" class="color_th_hover border_hover css_trans">
                            <span>О скидочной системе</span>
                            <span class="color_th">→</span></button>
                        <?php
                        /*                        if ($prepay != null) {
                                                    */ ?><!--
                            <div class="selector_all"></div>
                            Предоплата
                            --><?php
                        /*                        }
                                                */ ?>


                        <div class="hidden">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab placeat
                            accusantium praesentium, rem, earum sed molestias perferendis tempore voluptate asperiores
                            odit maiores repellat. Ut quaerat non magni repudiandae odio, recusandae.
                        </div>
                    </div>
                    <?php
                    if (MagazineBonus::getUserBonus()) {
                        ?>
                        <div class="bonus_sector">
                            <div class="bonus_check"></div>
                            <span>Использовать бонус</span>
                            <input type="text" class="bonus_input" disabled="disabled"><span class="maxDisc"
                                                                                             style="color: red;display: none"> <- Это ваша макс. допустимая скидка</span>

                            <div>
                                Доступно бонусов: <span class="bonus_number"><?= MagazineBonus::getUserBonus() ?></span>р.
                            </div>
                        </div>
                        <?php
                    }
                    ?>
                    <div class="discont_line">Доберите товара до <b><span class="money"><?= $allPrice ?></span> руб.</b>,
                        чтобы получить скидку <b id="next_pr"><span>0</span>%</b></div>
                </div>
            <?php } ?>
            <div class="inline_style cart_itog">
                <div class="summ">
                    <span>Сумма без скидки</span>:<b><span class="money"></span><span class="rub_lt">a</span></b>
                </div>
                <div class="summ_minus">
                    <span>Сумма скидки</span>:<b><span class="money"></span><span class="rub_lt">a</span></b>
                </div>
                <div class="summ_plus">
                    <span>Итого<span class="cart_itog_dop"> со скидкой</span>:</span><b><span
                            class="money"><?= $allPrice ?></span><span class="rub_lt">a</span></b>
                </div>
                <div class="summ_bonus" style="display: none">
                    <span>Итого<span class="cart_itog_dop"> со скидкой и бонусом</span>:</span><b><span
                            class="money"></span><span class="rub_lt">a</span></b>
                </div>
            </div>
            <button class="next_step_cart bg_color_th inline_style">ДАЛЕЕ ↓</button>
        </div>
    </div>
    <!--end cart_table-->
    <div id="rules_delivery" class="inline_style">
        <p id="rules_title">Условия поставки</p>

        <div id="rules_content">
            <?= $deliveryText->text ?>
        </div>
        <div class="hidden"><?= $deliveryText->text ?></div>
        <button id="more_rules_delivery" class="color_th">Читать полностью ↓</button>
    </div>

    <div id="calendar_cart" class="inline_style">
        <p id="calendar_title">Выберите дату поставки</p>
        <table id="calendar2">
            <thead class="color_th">
            <tr>
                <td>‹
                <td colspan="5">
                <td>›
            </tr>
            <tr>
                <td>Пн
                <td>Вт
                <td>Ср
                <td>Чт
                <td>Пт
                <td>Сб
                <td>Вс
            </tr>
            </thead>
            <tbody></tbody>
        </table>
        <div id="calendar_legend">
            <span class="legend_today"></span><span> - сегодня </span><span
                class="legend_active bg_color_th"></span><span> - выбранный день</span>
        </div>
    </div>

    <div id="graph_delivery">
        <p id="graph_delivery_title"><?= $this->connectID == 36 ? 'История заказов' : 'График поставок' ?></p>
        <ul>
            <?php
            if (!empty($getUserPostavki)) {
                foreach ($getUserPostavki as $gi) {
                    ?>
                    <li><span><?= (date('d-m-Y', strtotime($gi->time_order))) ?></span><a
                            href="/client/MagazineOrder/index/id/<?= $gi->id ?>"
                            class="color_th_hover css_trans border_hover"><span>Подробнее</span> <span class="color_th">→</span></a>
                    </li>
                <?php }
            } else { ?>
                <li>В этом окне
                    появится <?= $this->connectID == 36 ? 'ваша история заказов' : 'ваш график поставок' ?></li>
            <?php } ?>
        </ul>
        <?php if (count($getUserPostavki) > 0) { ?>
            <a href="/client/MagazineOrder/admin" class="show_all_delivery color_th_hover css_trans border_hover"><span>Посмотреть <?= $this->connectID == 36 ? 'всю историю заказов' : 'весь график поставок' ?> </span><span
                    class="color_th">→</span></a>
        <?php } ?>
    </div>


    <div id="select_shop">
        <p id="select_shop_title">Выберите магазины для поставки:</p>

        <div class="inline_style shop_setup">
            <div class="shop_head">
                <p><?= $this->connectID == 36 ? 'Адрес доставки:' : 'Адрес магазина:' ?></p>

                <div class="select_shop">
                    <?php if (empty($magazineClientsAddress)) { ?>
                        <p data-id="">&nbsp;</p>
                    <?php } else { ?>
                        <p data-id="<?= $magazineClientsAddress[0]->id ?>"><?= $magazineClientsAddress[0]->address ?></p>
                    <?php } ?>

                    <ul>
                        <?php foreach ($magazineClientsAddress as $mgz) {
                            echo '<li class="css_trans" data-id=' . $mgz->id . '">' . $mgz->address . '</li>';
                        }

                        ?>
                    </ul>
                </div>
            </div>
            <div class="shop_body">
                <p>Контактное лицо (приемщик):</p>

                <div class="shop_input_form">
                    <input type="text" placeholder="Имя" value="<?= $magazineClientsAddress[0]->name_contact ?>"
                           class="shop_name">
                    <input type="text" placeholder="Фамилия" value="<?= $magazineClientsAddress[0]->surname_contact ?>"
                           class="shop_lastname">
                    <?php
                    if (isset($magazineClientsAddress[0])) {
                        $this->widget("ext.maskedInput.MaskedInput", array(
                            "model" => $magazineClientsAddress[0],
                            "attribute" => "phone_contact",
                            "mask" => '+7 (999) 999-9999',
                            'htmlOptions' => array('size' => 60, 'maxlength' => 255, 'placeholder' => 'Номер телефона', 'class' => 'shop_phone')
                        ));
                    } else { ?>
                        <input type="text" placeholder="Номер телефона"
                               value="<?= $magazineClientsAddress[0]->phone_contact ?>" class="">
                    <?php }
                    ?>

                    <textarea placeholder="Дополнительная информация по заказу или уточнения по адресу"
                              name="contact_name"
                              class="shop_msg"><?= $magazineClientsAddress[0]->text_contact ?></textarea>
                </div>
            </div>
        </div>
        <div class="inline_style add_shop">
            <div class="add_block_text">
                <img src="/html_source/img/add_cart.png">

                <p>Поставить тот же перечень товаров в еще один магазин</p>

                <div class="add_shop_form">Добавить еще магазин</div>
            </div>
        </div>
        <form name="cart_page" method="POST" action="" id="finish_order_form">
            <!--js auto include goods input type hidden name(goods_id / goods_count)
            #demo tpl
                <input type="hidden" name="goods[i][goods_id]" >
                <input type="hidden" name="goods[i][goods_count]" >
            -->
            <!--js auto include shop form
            #demo tpl
                <input type="hidden" name="shop_form[i][shop_id]" >
                <input type="hidden" name="shop_form[i][contact_name]">
                <input type="hidden" name="shop_form[i][contact_lastname]" >
                <input type="hidden" name="shop_form[i][contact_phone]" >
                <input type="hidden" name="shop_form[i][contact_msg]" >
            -->
            <div class="cart_selec_align">
                <p>Выберите компанию</p>

                <div class="select_wr">
                    <select name="cart_page_selector_company" class="cart_comp_selector">
                        <?php foreach ($CompanyUser as $cUser) {
                            echo '<option value="' . $cUser->id . '">' . $cUser->name_company . '</option>';
                        } ?>
                    </select>
                </div>
            </div>
            <input class="prepay" style="opacity:0; position:absolute; left:0;" type="checkbox" name="prepay">
            <input type="hidden" name="min_order" value="<?= $user->min_order != null ? $user->min_order : 0 ?>">
            <input type="hidden" name="time_order">
            <input type="hidden" name="bonus_value">
            <input type="hidden" class="preDis" name="preDis" value="<?= $prepay != null ? $prepay->percent : 0 ?>">
            <button id="finish_order" class="bg_color_th">Подтвердить заказ</button>
        </form>
    </div>


<?php } ?>
