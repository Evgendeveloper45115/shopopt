<?php
/**
 * Created by PhpStorm.
 * User: holyButters
 * Date: 27.12.15
 * Time: 18:26
 */
/**
 * @var CWebApplication $app
 */
$explode = explode('/', $_SERVER['REQUEST_URI']);
$i = 0;
$app = Yii::app();
$q = [];
$brads = MagazineProduct::getTreeBreadcrumbs($Items[0], true, true);
?>
<div id="category_title_wrap">
    <p style="text-align: center;
    margin-left: 10%;
    margin-right: 30%;">
        <?php
        if ($brads) {
            $baseUrl = Yii::app()->getBaseUrl(true);
            $url = $baseUrl . '/catalog';
            $count = count($brads) - 1;
            $last = array_pop($brads);
            foreach ($brads as $key => $link) {
                if ($key == 0) {
                    echo '<a class="breadcrumbs_item_card"  href="' . $url . '">' . $link . '</a><span> / </span>';
                } else {
                    if (in_array($link, $_GET) || array_key_exists($link, $_GET)) {
                        $url .= '/' . $link;
                        echo '<a class="breadcrumbs_item_card"  href="' . $url . '">' . urldecode($link) . '</a>' . '<span> /  <span>';
                    }
                }
            }
            echo '<span class="breadcrumbs_item_card">' . ' ' . urldecode($last) . '</span>';

        }
        ?>
    </p>

    <?php if (isset($_GET['sort']) && $_GET['sort'] == 'asc') { ?>
        <a href="?sort=desc<?= (isset($_GET['find_item']) ? '&find_item=' . $_GET['find_item'] : '') ?>" id="sort_by"
           data-sort='desc' class="css_trans color_th_hover">Сортировать по цене ↓</a>
        <?php if (isset($_GET['find_item'])) { ?>
            <a href="<?= str_replace($_SERVER['QUERY_STRING'], '', $_SERVER['REQUEST_URI']) ?>"
               class="css_trans color_th_hover" id="finder_reset">Очистить результат поиска</a>
        <?php } ?>
    <?php } else { ?>
        <a href="?sort=asc<?= (isset($_GET['find_item']) ? '&find_item=' . $_GET['find_item'] : '') ?>" id="sort_by"
           data-sort='asc' class="css_trans color_th_hover">Сортировать по цене ↑</a>
        <?php if (isset($_GET['find_item'])) { ?>
            <a href="<?= str_replace($_SERVER['QUERY_STRING'], '', $_SERVER['REQUEST_URI']) ?>"
               class="css_trans color_th_hover" id="finder_reset">Очистить результат поиска</a>
        <?php } ?>
    <?php } ?>
    <div id="show_as">
        <a href="#"><img src="/html_source/img/show_type1.png"></a>
        <a href="#" class="active"><img src="/html_source/img/show_type2.png"></a>
    </div>
</div>

<div id="goods_item_wrap">
    <!--items-->
    <!--single-->
    <?php
    $links = [];
    foreach ($Items as $item) {
        $buttonText = 'Купить';
        if (isset($app->session['items'])) {
            foreach ($app->session['items'] as $items) {
                if ($item->id == $items['id']) {
                    $buttonText = 'В корзине';
                }
            }
        }
        $json = null;
        if ($item->image_product) {
            $json = json_decode($item->image_product);
        }
        if (is_array($json) && !empty($json)) {
            foreach ($json as $image) {
                if (!stristr($image, '[')) {
                    if ($image == '') {
                        continue;
                    } else {
                        $json = $image;
                    }
                }
                break;
            }
        }

        $links = MagazineProduct::getTree($item, true);
        $href = '/catalog/';
        foreach ($links as $link) {
            $href .= $link . '/';
        }
        $href .= $item->id;
        ?>
        <div class="goods_item css_trans shadow_hover">
            <a href="<?= $href ?>">
                <div class="img_fon"
                     style="<?= SiteConnect::getSizeImage() ?> background-image:url(<?= is_string($json) ? $json : null ?>)"></div>
                <p><?= $item->name_product ?></p>
                <?php
                if ($item->discountPrice != '') {
                    ?>
                    <div class="percent_sale">

                    </div>
                    <?php
                }
                ?>
            </a>

            <div class="price inline_style color_th"><span
                    class="money"><?= $item->discountPrice != '' ? $item->discountPrice : $item->price ?></span><span
                    class="rub_lt">a</span>
            </div>
            <?php
            if($this->connectID == 36){
                ?>
                <button class="bg_color_th" data-id="<?= $item->id ?>"
                        data-price="<?= $item->discountPrice != '' ? $item->discountPrice : $item->price ?>"
                        data-min="<?= $item->min_count ?>"
                        data-stock="<?= $item->balanceStock ? $item->balanceStock : 0 ?>"><?= $buttonText ?></button>
                <?php
            }else{
                ?>
                <button class="bg_color_th" data-id="<?= $item->id ?>"
                        data-price="<?= $item->discountPrice != '' ? $item->discountPrice : $item->price ?>"
                        data-min="<?= $item->min_count ?>"><?= $buttonText ?></button>

                <?php
            }
            ?>
            <?php
            if ($item->balanceStock === '0' && $this->connectID == 36) {
                ?>
                    <div class="block_item">
                        <p style="color: white">Временно нет в наличии</p>
                    </div>

                <?php
            }
            ?>

        </div>
    <?php } ?>


    <!--end items-->
</div>

