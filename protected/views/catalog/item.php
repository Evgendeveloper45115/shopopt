<?php
/**
 * Created by PhpStorm.
 * User: holyButters
 * Date: 27.12.15
 * Time: 18:26
 * @var $Item MagazineProduct
 */
$explode = explode('/', $_SERVER['REQUEST_URI']);
$i = 0;
$jsonObj = json_decode($Item->is_active);
$buttonText = 'В Корзину';
if (isset(Yii::app()->session['items'])) {
    foreach (Yii::app()->session['items'] as $items) {
        if ($Item->id == $items['id']) {
            $buttonText = 'В корзине';
        }
    }
}
$links = MagazineProduct::getTree($Item, true, true);
?>
<div id="category_title_wrap">
    <p style="text-align: center;
    margin-left: 10%;
    margin-right: 30%;">
        <?php
        if ($links) {
            $baseUrl = Yii::app()->getBaseUrl(true);
            $url = $baseUrl . '/catalog';
            foreach ($links as $key => $link) {
                if ($key == 0) {
                    echo '<a class="breadcrumbs_item_card"  href="' . $url . '">' . $link . '</a><span> / </span>';
                } else {
                    $url .= '/' . $link;
                    echo '<a class="breadcrumbs_item_card"  href="' . $url . '">' . urldecode($link) . '</a><span> / <span>';
                }
            }
            echo '<span class="breadcrumbs_item_card">' . ' ' . urldecode($Item->name_product) . '</span>';
        }
        ?>
    </p>
</div>
<?php
$style = null;
if ($this->connectID == 36) {
    $style = 'style="height: 39.5vw"';
    ?>
    <style>
        #det_sl .slidesjs-pagination li a {
            height: 5.2vw;
        !important;
        }
    </style>
    <?php
}
?>
<div id="detail_content">
    <div class="inline_style">
        <div id="det_sl" <?= $style ?>>
            <?php
            $json = null;
            if ($Item->image_product) {
                $json = json_decode($Item->image_product);
            }
            if (is_array($json) && !empty($json)) {
                foreach ($json as $image) {
                    if (!stristr($image, '[')) {
                        if ($image == '') {
                            continue;
                        } else {
                            $json = $image;
                            ?>
                            <div class="new_wrap_for_sl">
                                <div class="img_fon" style="background-image:url(<?= $json ?>)"
                                     data-url="<?= $json ?>">
                                    <a href="<?= $json ?>" class="fancybox" rel="gallery1">
                                        <?php
                                        if ($Item->discountPrice != '') {
                                            ?>
                                            <div class="percent_sale item_view">

                                            </div>
                                            <?php
                                        }
                                        ?>
                                    </a>
                                </div>
                            </div>
                            <?php
                        }
                    }
                }
            }
            ?>
        </div>
    </div>
    <div class="inline_style">
        <div id="det_des">
            <?php
            if ($jsonObj->{'articul'} != 'off') {
                ?>
                <p class="dt_title_des">Артикул: <span><?= $Item->articul ?></span></p>
                <?php
            }
            ?>
            <?php
            if ($jsonObj->{'name_product'} != 'off') {
                ?>
                <p class="dt_title_des">Наименование: <span><?= $Item->name_product ?></span></p>
                <?php
            }
            ?>
            <?php
            if ($jsonObj->{'text_product'} != 'off') {
                ?>
                <p class="dt_title_des">Описание:</p>

                <div id="text_det_content">
                    <div id="text_det_wrap">
                        <?= $Item->text_product ?>
                    </div>
                </div>
                <p class="color_th show_more_content">Читать полностью ↓</p>

                <p class="color_th hide_more_content">Скрыть описание ↑</p>

                <?php
            }
            ?>


            <?php
            if ($jsonObj->{'balanceStock'} != 'off') {
                ?>
                <p class="dt_title_des">Остаток на складе:
                    <span><?= $Item->balanceStock ? $Item->balanceStock : 0 ?></span></p>
                <?php
            }
            ?>

            <?php
            if ($Item->dop_fields) {
                $decode = json_decode($Item->dop_fields);
                foreach ($decode as $field => $value) {
                    // CVarDumper::dump($jsonObj,11,1);

                    if (stristr($field, 'duplicate_name')) {
                        $string = str_replace('duplicate_name', 'duplicate_value', $field);
                        if ($jsonObj->{$string} != 'off') {
                            ?>
                            <p class="dt_title_des"><?= $value ?> <?= !stristr($value, ':') ? ':' : null ?>
                            <?php
                        }
                    } else {
                        if ($jsonObj->{$field} != 'off') {
                            ?>
                            <span><?= $value ?></span></p>
                            <?php
                        }
                    }
                }
            }
            ?>

            <div id="det_to_cart">
                <div class="det_to_cart_info">
                    <p>
                        <?php
                        if ($jsonObj->{'price'} != 'off') {
                            ?>
                            <?= $Item->discountPrice ? '<del>' : '' ?><span class="money"><?= $Item->price ?></span>
                            <span
                                class="rub_lt">a</span><?= $Item->discountPrice ? '</del>' : '/' ?>
                            <?php
                        }
                        if ($jsonObj->{'discountPrice'} != 'off' && $Item->discountPrice) {
                            ?>
                            <span class="money"><?= $Item->discountPrice ?></span><span
                                class="rub_lt">a</span>/
                            <?php
                        }
                        ?>
                        <?php foreach ($types as $type) {
                            if ($type->id == $Item->type_product) {
                                if ($jsonObj->{'type_product'} != 'off') {
                                    echo $type->name;
                                }
                            }
                            ?>
                        <?php } ?>
                    </p>
                    <?php
                    if ($jsonObj->{'min_count'} != 'off') {
                        ?>
                        <p>Минимальный заказ - <span class="min_count_to_order"><?= $Item->min_count ?></span> <? ?></p>
                        <?php
                    }
                    ?>
                </div>
                <?php
                if ($this->connectID == 36) {
                    if ($Item->balanceStock || $Item->balanceStock > 0) {
                        ?>
                        <div class="det_to_cart_button">

                            <span class="dec color_th_hover css_trans">-</span><input type="text"
                                                                                      value="<?= $Item->min_count ?>"
                                                                                      data-min="<?= $Item->min_count ?>"
                                                                                      data-stock="<?= $Item->balanceStock ?>"
                                                                                      data-price="<?= $Item->discountPrice != '' ? $Item->discountPrice : $Item->price ?>"
                                                                                      data-id="<?= $Item->id ?>"><span
                                class="inc color_th_hover css_trans">+</span>
                            <button class="to_cart_from_det bg_color_th" data-min="<?= $Item->min_count ?>"
                                    data-price="<?= $Item->discountPrice != '' ? $Item->discountPrice : $Item->price ?>"
                                    data-id="<?= $Item->id ?>"
                                    data-stock="<?= $Item->balanceStock ?>"><?= $buttonText ?>
                            </button>
                        </div>
                        <?php
                    } else {
                        ?>
                        <div class="det_to_cart_button">

                        </div>
                        <?php
                    }
                } else {
                    ?>
                    <div class="det_to_cart_button">

                        <span class="dec color_th_hover css_trans">-</span><input type="text"
                                                                                  value="<?= $Item->min_count ?>"
                                                                                  data-min="<?= $Item->min_count ?>"
                                                                                  data-price="<?= $Item->discountPrice != '' ? $Item->discountPrice : $Item->price ?>"
                                                                                  data-id="<?= $Item->id ?>"><span
                            class="inc color_th_hover css_trans">+</span>
                        <button class="to_cart_from_det bg_color_th" data-min="<?= $Item->min_count ?>"
                                data-price="<?= $Item->discountPrice != '' ? $Item->discountPrice : $Item->price ?>"
                                data-id="<?= $Item->id ?>"><?= $buttonText ?>
                        </button>
                    </div>
                    <?php
                }
                ?>
            </div>
        </div>
    </div>
</div>
<?php if (!empty($rec)) { ?>
    <p id="recomend_title">Вас также может заинтересовать:</p>
<?php } ?>
<div id="goods_item_wrap">
    <!--start-->
    <?php
    foreach ($rec as $rc) {
        $buttonTextCat = 'Купить';
        if (isset(Yii::app()->session['items'])) {
            foreach (Yii::app()->session['items'] as $items) {
                if ($rc->id == $items['id']) {
                    $buttonTextCat = 'В корзине';
                }
            }
        }
        if (isset($links[0])) {
            unset($links[0]);
        }
        $json = null;
        if ($rc->image_product) {
            $json = json_decode($rc->image_product);
        }
        if (!empty($json[0])) {
            if (!stristr($json[0], '[')) {
                $json = $json[0];
            } else {
                $json = $json[1];
            }
        }
        if ($json == '') {
            $json = json_decode($rc->image_product);
            if ($json[0] != '') {
                $json = json_decode($json[0]);
                $json = $json[0];
            }
        }
        $href = '/catalog/';
        foreach ($links as $link) {
            $href .= $link . '/';
        }
        $href .= $rc->id;

        ?>
        <div class="goods_item css_trans shadow_hover">
        <a href="<?= $href ?>">
            <div class="img_fon"
                 style="<?= $this->connectID == 36 ? 'height: 16vw;' : null ?>background-image:url(<?= $json != '' ? $json : null ?>)"></div>
            <p><?= $rc->name_product ?></p>
        </a>

        <div class="price inline_style color_th"><span class="money"><?= $rc->price ?></span><span
                class="rub_lt">a</span></div>
        <button class="bg_color_th" data-id="<?= $rc->id ?>" data-price="<?= $rc->price ?>"
                data-min="<?= $rc->min_count ?>"><?= $buttonTextCat ?>
        </button>
        </div><?php } ?>
    <!--end-->
</div>



