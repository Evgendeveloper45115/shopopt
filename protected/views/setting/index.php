<?php
/* @var $this SettingController */
/* @var $dataModel array */
/* @var $modelSaveSettings array */

//echo  CHtml::errorSummary($modelSaveSettings);
echo  CHtml::beginForm('', 'post', ['enctype'=>'multipart/form-data']);
if(array_key_exists('color_site',$dataModel)) {
    echo 'Цвет сайта:'.CHtml::colorField('color_site', $dataModel['color_site']);
} else {
    echo 'Цвет сайта:'.CHtml::colorField('color_site');
}
?>
<br/>
<br/>
<br/>
<?
if(array_key_exists('logo_site',$dataModel)) {
    echo 'Логотип Сайта:'.CHtml::fileField('logo_site', $dataModel['logo_site']);
    echo CHtml::image('/upload/' . $dataModel['logo_site'], '', ['width'=> 200, 'height' => 200]);
} else {
    echo 'Логотип Сайта:'.CHtml::fileField('logo_site');
}
?>
<br/>
<br/>
<?=CHtml::submitButton('Сохранить', ['name' => 'save']);
echo  CHtml::endForm();


