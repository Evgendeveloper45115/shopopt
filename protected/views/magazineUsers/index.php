<?php
/* @var $this MagazineUsersController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Magazine Users',
);

$this->menu=array(
	array('label'=>'Create MagazineUsers', 'url'=>array('create')),
	array('label'=>'Manage MagazineUsers', 'url'=>array('admin')),
);
?>

<h1>Magazine Users</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
