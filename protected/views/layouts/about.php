<?php /* @var $this Controller */ ?>
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="language" content="ru">
    <meta content="width=device-width,initial-scale=1.0" name="viewport">
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">

    <!-- blueprint CSS framework -->
    <!--	<link rel="stylesheet" type="text/css" href="--><?php //echo Yii::app()->request->baseUrl; ?><!--/css/screen.css" media="screen, projection">-->
    <link rel="stylesheet" href="/html_source/css/all.css">
    <link rel="stylesheet" href="/html_source/css/all_inner.css">
<!--    <link rel="stylesheet" href="/html_source/css/all_client.css">-->
<!--    <link rel="stylesheet" href="/html_source/css/all_crm.css">-->

    <style>
        .color_th{color:<?=@$this->dataModel['color_site']?>!important;}
        .color_th_hover:hover{color:<?=@$this->dataModel['color_site']?>!important;}
        .bg_color_th{background-color:<?=@$this->dataModel['color_site']?>!important;}
        .bg_color_th_hover:hover{background-color:<?=@$this->dataModel['color_site']?>!important;}
        .svg_fill_color{fill:<?=@$this->dataModel['color_site']?>!important;}
        .svg_stroke_color{stroke:<?=@$this->dataModel['color_site']?>!important;}
        .svg_stroke_color_hover:hover{stroke:<?=@$this->dataModel['color_site']?>!important;}
        .svg_fill_color_hover:hover{fill:<?=@$this->dataModel['color_site']?>!important;}
        .border_hover:hover{border-color:<?=@$this->dataModel['color_site']?>!important;}
        .focus{border:1px solid <?=@$this->dataModel['color_site']?>;box-shadow: 0px 1px 15px 0px <?=@$this->dataModel['color_site']?>;}
        .shadow_hover:hover{box-shadow: 0px 1px 15px 0px <?=@$this->dataModel['color_site']?>;}
    </style>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
    <script src="/html_source/js/core.js"></script>
    <script src="/html_source/js/jquery.fancybox.pack.js"></script>
    <script src="/html_source/js/jquery.maskedinput.min.js"></script>
    <script src="/html_source/js/all.js"></script>
    <script type="text/javascript">
        <?php
        $close = 'true';
        if(Yii::app()->user->isGuest && $this->connectID == 36){
        ?>
                $(function(){
                    var url = window.location.href;
                    if(url.indexOf('barbers') ==  +1){
                        <?=$close = 'false' ?>
                    }

                });
                <?php
                }
            ?>

        var close = <?= $close ?>
    </script>
    <!-- page script-->

    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/print.css" media="print">
    <!--[if lt IE 8]>
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/ie.css" media="screen, projection">
    <![endif]-->

    <!--	<link rel="stylesheet" type="text/css" href="--><?php //echo Yii::app()->request->baseUrl; ?><!--/css/main.css">-->
    <!--	<link rel="stylesheet" type="text/css" href="--><?php //echo Yii::app()->request->baseUrl; ?><!--/css/form.css">-->
    <script src="/html_source/js/all_inner.js"></script>
    <script src="/html_source/js/jquery.slides.min.js"></script>
    <title><?php echo CHtml::encode($this->pageTitle); ?></title>
</head>
<body>
<div id="left_bar" class="inline_style">
    <div id="left_bar_wrap">

        <?php
        echo CHtml::image('/upload/' . @$this->dataModel['logo_site'], '', ['width'=> 50, 'height' => 50, 'class'=>'logo'. ($this->connectID == 36 ? ' barbers-logo' : null)]);
        ?>

        <?php $this->widget('zii.widgets.CMenu',array(
            'encodeLabel'=>false,
            'items'=>$this->getMenuItems(),
//                array('label'=>'Пользователи<span class="bg_color_th"></span>', 'url'=>array('/magazineUsers/admin'), 'visible'=>(Yii::app()->user->role == WebUser::ADMIN), 'tamplate' => '<span>1231</span>'),
//                array('label'=>'Управление категориями<span class="bg_color_th"></span>', 'url'=>array('/publisher/magazineCategories/admin'), 'visible'=>(Yii::app()->user->role == WebUser::PUBLISHER)),
//                array('label'=>'Управление товароом<span class="bg_color_th"></span>', 'url'=>array('/publisher/MagazineProduct/admin'), 'visible'=>(Yii::app()->user->role == WebUser::PUBLISHER)),
//                array('label'=>'Акции<span class="bg_color_th"></span>', 'url'=>array('/publisher/magazineStock/admin'), 'visible'=>(Yii::app()->user->role == WebUser::PUBLISHER)),
//                array('label'=>'Оставить отзыв<span class="bg_color_th"></span>', 'url'=>array('/magazineReview/create'), 'visible'=>(Yii::app()->user->role == WebUser::CLIENT)),
//                array('label'=>'Мои Заказы<span class="bg_color_th"></span>', 'url'=>array('/magazineReview/create'), 'visible'=>(Yii::app()->user->role == WebUser::CLIENT)),
//                array('label'=>'Магазины<span class="bg_color_th"></span>', 'url'=>array('/magazineclientsaddress/admin'), 'visible'=> (Yii::app()->user->role == WebUser::CLIENT)),

//                array('label'=>'Документооборот<span class="bg_color_th"></span>', 'url'=>array('/MagazineDocuments'), 'visible'=> (Yii::app()->user->role == WebUser::CLIENT)),
//                array('label'=>'Документооборот<span class="bg_color_th"></span>', 'url'=>array('/MagazineDocuments'), 'visible'=> (Yii::app()->user->role == WebUser::MANAGER)),
//                array('label'=>'Мои заказы<span class="bg_color_th"></span>', 'url'=>array('/MagazineDocuments'), 'visible'=> (Yii::app()->user->role == WebUser::MANAGER)),
//                array('label'=>'Logout ('.Yii::app()->user->name.')<span class="bg_color_th"></span>', 'url'=>array('/site/logout'), 'visible'=>!Yii::app()->user->isGuest)
            'submenuHtmlOptions' => ['class' => 'css_trans'],
            'itemCssClass' => 'child_true css_trans'
        )); ?>
    </div>

</div><div id="conten_zone" class="inline_style about_zone">
    <div id="inner_menu">
        <ul>
            <li><a href="/site/about" class="css_trans color_th_hover">О магазине</a></li>
            <li><a href="/site/delivery" class="css_trans color_th_hover">Доставка</a></li>
            <li><a href="<?=Yii::app()->user->id ? '/site/redirect': '#' ?>" class="css_trans color_th_hover">Личный кабинет</a></li>
            <?=$this->connectID == 36 ? '<li><a href="http://barbers.specialview.ru/" class="css_trans color_th_hover">Розничный сайт</a></li>' : null?>
        </ul>
        <div id="cart_info">
            <?= $this->renderPartial('/site/cartInfo');?>
        </div>
    </div>
    <!--content_area-->
        <?=$content?>
    <!--content_area end-->
    <!--footer start-->
    <div id="footer_margin"></div>
    <div id="footer">
        <p class="copyright">© 2015 opt prodject</p>
        <div class="inline_style">
            <a href="#" class="mail_rss color_th_hover css_trans border_hover"><span>Подписаться на рассылку</span> <span class="color_th">→</span></a>
        </div><div class="inline_style">
            <ul class="soc_li">
                <li><a href="#" target="blank"><img src="/html_source/img/soz1.png" alt=""></a></li>
                <li><a href="#" target="blank"><img src="/html_source/img/soz2.png" alt=""></a></li>
                <li><a href="#" target="blank"><img src="/html_source/img/soz3.png" alt=""></a></li>
            </ul>
        </div><div class="inline_style">
            <ul class="map_site">
                <li><a href="/site/about" class="color_th_hover css_trans">О магазине</a></li>
                <li><a href="/site/delivery" class="color_th_hover css_trans">Доставка</a></li>
                <li><a href="<?php if(Yii::app()->user->id){echo '/site/redirect';}else {echo '#';}?>" class="color_th_hover css_trans">Личный кабинет</a></li>
            </ul>
        </div>
    </div>
    <!--footer_end-->
</div>

</body>
<script>
    <?php
    if(Yii::app()->user->isGuest && $this->connectID == 36){
    ?>
    $(function(){
        $("body").css("overflow", "hidden");
        $('#inner_menu li').eq(2).children('a').click();
    });
    <?php
    }
?>
</script>
</html>
