<?php
/* @var $this MagazineProductController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Magazine Products',
);

$this->menu=array(
	array('label'=>'Create MagazineProduct', 'url'=>array('create')),
	array('label'=>'Manage MagazineProduct', 'url'=>array('admin')),
);
?>

<h1>Magazine Products</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
