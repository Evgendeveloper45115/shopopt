<?php
/* @var $this MagazineProductController */
/* @var $model MagazineProduct */

$this->breadcrumbs=array(
	'Magazine Products'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List MagazineProduct', 'url'=>array('index')),
	array('label'=>'Create MagazineProduct', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#magazine-product-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Magazine Products</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'magazine-product-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'site_connect_id',
		'price',
		'min_count',
		'text_product',
		'name_product',
		/*
		'desc_product',
		'image_product',
		*/
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
