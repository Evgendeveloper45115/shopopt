<?php
/* @var $this MagazineProductController */
/* @var $model MagazineProduct */

$this->breadcrumbs=array(
	'Magazine Products'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List MagazineProduct', 'url'=>array('index')),
	array('label'=>'Manage MagazineProduct', 'url'=>array('admin')),
);
?>

<h1>Create MagazineProduct</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>