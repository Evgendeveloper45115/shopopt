<?php
/* @var $this MagazineProductController */
/* @var $model MagazineProduct */

$this->breadcrumbs=array(
	'Magazine Products'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List MagazineProduct', 'url'=>array('index')),
	array('label'=>'Create MagazineProduct', 'url'=>array('create')),
	array('label'=>'Update MagazineProduct', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete MagazineProduct', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage MagazineProduct', 'url'=>array('admin')),
);
?>

<h1>View MagazineProduct #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'site_connect_id',
		'price',
		'min_count',
		'text_product',
		'name_product',
		'desc_product',
		'image_product',
	),
)); ?>
