<?php
/* @var $this MagazineProductController */
/* @var $model MagazineProduct */

$this->breadcrumbs=array(
	'Magazine Products'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List MagazineProduct', 'url'=>array('index')),
	array('label'=>'Create MagazineProduct', 'url'=>array('create')),
	array('label'=>'View MagazineProduct', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage MagazineProduct', 'url'=>array('admin')),
);
?>

<h1>Update MagazineProduct <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>