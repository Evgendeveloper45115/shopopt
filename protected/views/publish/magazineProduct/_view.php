<?php
/* @var $this MagazineProductController */
/* @var $data MagazineProduct */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('site_connect_id')); ?>:</b>
	<?php echo CHtml::encode($data->site_connect_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('price')); ?>:</b>
	<?php echo CHtml::encode($data->price); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('min_count')); ?>:</b>
	<?php echo CHtml::encode($data->min_count); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('text_product')); ?>:</b>
	<?php echo CHtml::encode($data->text_product); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('name_product')); ?>:</b>
	<?php echo CHtml::encode($data->name_product); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('desc_product')); ?>:</b>
	<?php echo CHtml::encode($data->desc_product); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('image_product')); ?>:</b>
	<?php echo CHtml::encode($data->image_product); ?>
	<br />

	*/ ?>

</div>