<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity
{
    private $_id;
    private $_role;
    private $_phone;
    private $_email;

    public function authenticate()
    {
        $username = strtolower($this->username);
        $user = MagazineUsers::model()->find(['condition' => 'user_email = "' . $username . '"']);
        if ($user === null)
            $this->errorCode = self::ERROR_USERNAME_INVALID;
        else if (!$user->validatePassword($this->password))
            $this->errorCode = self::ERROR_PASSWORD_INVALID;
        elseif ($user->is_active != 1) {
            $this->errorCode = 'Пользователь не подтверждён!';
        } else {
            $this->_id = $user->id;
            $this->_role = $user->user_role;
            $this->username = $user->user_name;
//			$this->username=$user->user_phone;
            $this->errorCode = self::ERROR_NONE;
        }
//		var_dump($user, $this->errorCode);
//		exit;
        return $this->errorCode == self::ERROR_NONE;
    }

    public function getEmail()
    {
        return $this->_email;
    }

    public function getPhone()
    {
        return $this->_phone;
    }


    public function getId()
    {
        return $this->_id;
    }

    public function getRole()
    {
        return $this->_role;
    }
}