<?php

/**
 * Created by PhpStorm.
 * User: holyButters
 * Date: 10.12.15
 * Time: 11:36
 */
class AllModel extends CActiveRecord
{
    public function defaultScope()
    {
        if (isset($_SERVER['SERVER_NAME'])) {
            $connectID = SiteConnect::model()->findByAttributes(['url' => $_SERVER['SERVER_NAME']]);
            if (isset($connectID->id) && $connectID->status == 1) {
                return array(
                    'alias' => $this->tableName(),
                    'condition' => $this->tableName().".site_connect_id='" . $connectID->id . "'",
                );
            } else {
                throw new HttpException('Сайт отключен от работы', 305);
            }
        } else {
            return [];
        }
    }
}