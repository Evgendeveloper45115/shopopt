<?php

/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
class Controller extends CController
{
    /**
     * @var string the default layout for the controller view. Defaults to '//layouts/column1',
     * meaning using a single column layout. See 'protected/views/layouts/column1.php'.
     */
    public $layout = '//layouts/column1';
    public $dataModel = '';
    public $connectID = '';
    public $cart_price = 0;
    public $cart_count = 0;
    /**
     * @var array context menu items. This property will be assigned to {@link CMenu::items}.
     */
    public $menu = array();
    /**
     * @var array the breadcrumbs of the current page. The value of this property will
     * be assigned to {@link CBreadcrumbs::links}. Please refer to {@link CBreadcrumbs::links}
     * for more details on how to specify this property.
     */
    public $breadcrumbs = array();

    public function init()
    {
        parent::init();
        $connectID = SiteConnect::model()->findByAttributes(['url' => $_SERVER['SERVER_NAME']]);
        $this->connectID = $connectID->id;
        $model = MagazineKeySettings::model()->findAll();
        $this->dataModel = CHtml::listData($model, 'key', 'value');
    }

    public function getMenuItems()
    {
        $session = Yii::app()->session;
        $ids = [];
        if (isset($session['items'])) {
            foreach ($session['items'] as $key) {
                if ($key['id'] != 'undefined') {
                    $ids[] = $key['id'];
                }
            }

            $this->cart_count = 0;
            $this->cart_price = 0;

            $ids = implode(',', $ids);

            if (!empty($ids)) {

                $cart = MagazineProduct::model()->findAll(['condition' => 'id in (' . $ids . ')']);
                foreach ($cart as $cr) {

                    foreach ($session['items'] as $item) {
                        if ($item['id'] == $cr->id) {
                            $this->cart_count += $item['count'];
                            $this->cart_price += $cr->discountPrice != '' ? $item['count'] * $cr->discountPrice : $item['count'] * $cr->price;
                        }
                    }
                }
            }
        }
        $dbQuery = MagazineProduct::model()->findAll(['condition' => 'publish = 0']);

        $menuItems = array();

        foreach ($dbQuery as $value) {
            $menuItems[$value['id']] = array(
                'parent_id' => $value['parent_id'],
                'label' => $value['title_en'],
                'template' => (preg_match('#' . preg_quote($value['title']) . '#', urldecode($_SERVER['REQUEST_URI'])) ? '<span class="active bg_color_th"></span>{menu}' : '{menu}'),
                'url' => ($value['parent_id'] == 0 ? '/catalog/' . $value['title'] : $value['title']),
                'linkOptions' => ['class' => 'css_trans color_th_hover'],
                'itemOptions' => ['class' => 'css_trans'],
                'type' => $value['type'],
                'id' => $value['id'],
                'nameCategory' => $value['title']
            );
        }

        $menuItems = $this->mapTree($menuItems);
        return $menuItems;
    }

    public function mapTree($dataset)
    {
        $tree = array();
        foreach ($dataset as $id => &$node) {
            if (!$node['parent_id']) {
                $tree[$id] = &$node;
            } else {
                if ($node['type'] == 0) {
                    $node['url'] = $dataset[$node['parent_id']]['url'] . '/' . $node['id'];
                } else {
                    $node['url'] = $dataset[$node['parent_id']]['url'] . '/' . $node['url'];
                }

                $node['nameCategory'] = $dataset[$node['parent_id']]['nameCategory'] . '/' . $node['nameCategory'];
                $dataset[$node['parent_id']]['items'][$id] = &$node;
            }
        }
        return $tree;
    }


}