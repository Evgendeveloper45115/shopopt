<?php
class WebUser extends CWebUser {

    private $_model = null;

    const ADMIN = 1;
    const MANAGER = 2;
    const PUBLISHER = 3;
    const MANAGER_PUBLISHER = 4;
    const CLIENT = 5;

    public function getRole() {
        if ($user = $this->getModel()) {
            return $user->user_role;
        }
    }

    public function getModel() {
        if (!$this->isGuest && $this->_model === null) {
            $this->_model = MagazineUsers::model()->findByPk($this->id);
        }
        return $this->_model;
    }

}