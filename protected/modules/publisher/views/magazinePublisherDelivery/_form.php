<?php
/* @var $this MagazinePublisherDeliveryController */
/* @var $model MagazinePublisherDelivery */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'magazine-publisher-delivery-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'site_connect_id'); ?>
		<?php echo $form->textField($model,'site_connect_id'); ?>
		<?php echo $form->error($model,'site_connect_id'); ?>
	</div>
	<?php $this->widget('ImperaviRedactorWidget', array(
	// Можно использовать пару имя модели - имя свойства
		'model' => $model,
		'attribute' => 'text',
		// либо можно задать свойство name
		'name' => 'text',

		//или заменить какой-нибудь элемент виджетом
//		'selector' => '.my_redactor',

		//также через selector можно заменить несколько элементов виджетом
//		'selector' => '#my_redactor1, #my_redactor2',
		//опции, о которых поговорим ниже
		'options' => array(
			'lang' => 'ru',
			'imageUpload'=>'/html_source/upload_test.php',
		),

		//плагины, о них также упомяну ниже
		'plugins' => array(
		'fullscreen' => array(
		'js' => array('fullscreen.js',),
		),

	))); ?>
<!--	<div class="row">-->
<!--		--><?php //echo $form->labelEx($model,'text'); ?>
<!--		--><?php //echo $form->textArea($model,'text',array('rows'=>6, 'cols'=>50)); ?>
<!--		--><?php //echo $form->error($model,'text'); ?>
<!--	</div>-->

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->