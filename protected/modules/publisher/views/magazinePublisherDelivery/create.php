<?php
/* @var $this MagazinePublisherDeliveryController */
/* @var $model MagazinePublisherDelivery */

$this->breadcrumbs=array(
	'Magazine Publisher Deliveries'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List MagazinePublisherDelivery', 'url'=>array('index')),
	array('label'=>'Manage MagazinePublisherDelivery', 'url'=>array('admin')),
);
?>

<h1>Create MagazinePublisherDelivery</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>