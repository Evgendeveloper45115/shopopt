<?php
/* @var $this MagazinePublisherDeliveryController */
/* @var $model MagazinePublisherDelivery */

$this->breadcrumbs=array(
	'Magazine Publisher Deliveries'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List MagazinePublisherDelivery', 'url'=>array('index')),
	array('label'=>'Create MagazinePublisherDelivery', 'url'=>array('create')),
	array('label'=>'View MagazinePublisherDelivery', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage MagazinePublisherDelivery', 'url'=>array('admin')),
);
?>

<h1>Update MagazinePublisherDelivery <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>