<?php
/* @var $this MagazinePublisherDeliveryController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Magazine Publisher Deliveries',
);

$this->menu=array(
	array('label'=>'Create MagazinePublisherDelivery', 'url'=>array('create')),
	array('label'=>'Manage MagazinePublisherDelivery', 'url'=>array('admin')),
);
?>

<h1>Magazine Publisher Deliveries</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
