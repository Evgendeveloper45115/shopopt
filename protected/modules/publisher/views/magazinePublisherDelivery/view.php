<?php
/* @var $this MagazinePublisherDeliveryController */
/* @var $model MagazinePublisherDelivery */

$this->breadcrumbs=array(
	'Magazine Publisher Deliveries'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List MagazinePublisherDelivery', 'url'=>array('index')),
	array('label'=>'Create MagazinePublisherDelivery', 'url'=>array('create')),
	array('label'=>'Update MagazinePublisherDelivery', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete MagazinePublisherDelivery', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage MagazinePublisherDelivery', 'url'=>array('admin')),
);
?>

<h1>View MagazinePublisherDelivery #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'site_connect_id',
		'text',
	),
)); ?>
