<?php
/**
 * @var $model MagazineFilter
 * @var CController $this
 */

?>
<div id="align_zone">
    <p class="publisher_title">Фильтры и тэги</p>
    <div class="white_fon form_style filters" style="text-align: center;">
            <a href="/publisher/magazineFilters/createFilter" class="add_new_type_filter">Добавить новый фильтр</a>
            <a href="/publisher/magazineFilters/createTag" class="add_new_type_filter">Добавить новый тэг</a>

        <?php
        if($roots){
            $this->widget('CTreeView', array(
                'id'=>'tree',
                'data'=>$roots, // передаем массив
                'animated'=>'medium', // скорость анимации свертывания/развертывания
                'collapsed'=>true, // если тру, то при генерации дерева, все его узлы будут свернуты
                'persist'=>'location', // метод запоминания открытого узла
                'unique'=>true, // если тру, то при открытии одного узла, будут закрываться остальные
                'cssFile'=>'/html_source/css/jquery.treeview.css', // меняем расположение css файла (он немного подправлен мной)
            ));
        }
        ?>
    </div>
</div>


