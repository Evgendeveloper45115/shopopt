<?php
/* @var $this MagazineCategoriesController */
/* @var $model MagazineCategories */
/* @var $form CActiveForm */
$roots = MagazineFilter::model()->roots()->findAll();
if(!empty($roots)){
	$roots = CHtml::listData(MagazineFilter::model()->roots()->findAll(),'id','name');
}

?>
<div id="conten_zone">
	<div id="align_zone">
		<div class="white_fon form_style">
			<div class="form">
				<?php $form=$this->beginWidget('CActiveForm', array(
					'id'=>'magazine-filters-form',
					'enableClientValidation'=>true,
					'clientOptions' => array(
						'validateOnSubmit' => true,
						'validateOnChange' => true,
						'afterValidate' => 'js:function(form,data,hasError){
   			if(hasError){
   				$("html, body").animate({
                    scrollTop: $(".error").offset().top
                }, 1000);
   			}
   			else
   			    return true;
   		}'
					),
				)); ?>
				<p class="note">Поля с <span class="required">*</span> Обязательные.</p>
				<?php echo $form->errorSummary($model,'Пожалуйсто исправьте ошибки',null,['class' =>'errorSummary']); ?>
				<div class="row">
					<?php echo $form->labelEx($model,'item'); ?>
					<?php echo $form->dropDownList($model,'item',$roots,['class' => 'cats_list']); ?>
					<?php echo $form->error($model,'item'); ?>
				</div>

				<div class="row">
					<?php echo $form->labelEx($model,'subcat'); ?>
					<?php echo $form->textField($model,'subcat',array('size'=>60,'maxlength'=>255)); ?>
					<?php echo $form->error($model,'subcat'); ?>
				</div>

				<div class="row buttons">
					<?php echo CHtml::submitButton($model->isNewRecord ? 'Сохранить' : 'Редактировать'); ?>
				</div>

				<?php $this->endWidget(); ?>

			</div><!-- form -->
		</div>
	</div>
</div>
