<?php
/* @var $this MagazineStockController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Magazine Stocks',
);

$this->menu=array(
	array('label'=>'Create MagazineStock', 'url'=>array('create')),
	array('label'=>'Manage MagazineStock', 'url'=>array('admin')),
);
?>
<div id="publisher_left_menu">
	<a href="/publisher/MagazineStock/create" class="add_akz color_th">Добавить акцию</a>
	<ul>
		<?php
		if($allModels) {
			foreach($allModels as $md){?>
				<li >
					<a class="color_th_hover css_trans promo_click <?=(in_array($md->extension,['docx'])?' xren':'')?>" href="<?=$md->url?>" data-id="<?=$md->id?>"><span><?=$md->name_file?></span><span class="color_th">x</span></a>
				</li>
			<?php }
		}
		?>
	</ul>
</div><div id="align_zone">
	<p class="publisher_title">Изменить акцию</p>

	<div class="white_fon form_style_l">
		<?php $form=$this->beginWidget('CActiveForm', array(
			'id'=>'magazine-stock-form',
			'enableAjaxValidation'=>false,
			'htmlOptions'=>array('enctype'=>'multipart/form-data'),
		)); ?>
		<input type="hidden" name="id" value="1">

		<p>Изображение:</p>
		<div class="img_file_box">
			<div class="img_file_wrap">
				<div class="img_file_wrap_i">
					<?php echo $form->fileField($model,'image'); ?>
					<p class="bg_color_th"></p>
				</div>
			</div><div class="img_file_preview" style="display:block;">
				<img src="<?=$model->image?>">
			</div>
		</div>
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить',  ['class' => 'form_submit bg_color_th']); ?>
		<?php $this->endWidget(); ?>
	</div><!-- form -->
</div>
