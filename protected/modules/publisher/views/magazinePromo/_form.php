<?php
/* @var $this MagazineStockController */
/* @var $model MagazineStock */
/* @var $catsPromo MagazinePromoCat */
/* @var $form CActiveForm */
?>
<div id="publisher_left_menu">
    <a href="/publisher/MagazinePromo/create" class="add_akz color_th">Добавить промо</a>
    <ul>
        <?php
        $ids = [];
        foreach ($catsPromo as $catPromo) {
            $hasCats = MagazinePromoHasCat::model()->findAllByAttributes(['cat_id' => $catPromo->id])
            ?>
            <ul class="category">
                <span class="cat_name"><?= $catPromo->name ?></span>
                <?php
                if (!empty($hasCats)) {
                    foreach ($hasCats as $hasCat) {
                        $promo = MagazinePromoFiles::model()->findByPk($hasCat->files_id);
                        if ($promo != null) {
                            $ids[] = $promo->id;
                            ?>
                            <li style="text-align: center; display: none">
                                <a class="color_th_hover css_trans promo_click <?= (in_array($promo->extension, ['docx']) ? ' xren' : '') ?>"
                                   href="<?= $promo->url ?>"
                                   data-id="<?= $promo->id ?>"><span><?= $promo->name_file ?></span><span
                                        class="color_th">x</span></a>
                            </li>
                            <?php
                        } else {
                            ?>
                            <li style="text-align: center; display: none">Промо нет</li>
                            <?php
                        }

                    }
                } else {
                    ?>
                    <li style="text-align: center; display: none">Промо нет</li>
                    <?php
                }
                ?>
            </ul>
            <?php
        }
        if(!empty($ids)){
            $notCats = MagazinePromoFiles::model()->findAll(['condition' => 'id not in (' . implode(',', $ids) . ')']);
        }else{
            $notCats = MagazinePromoFiles::model()->findAll();
        }
        foreach($notCats as $promo){
            ?>
            <li>
                <a class="color_th_hover css_trans promo_click <?= (in_array($promo->extension, ['docx']) ? ' xren' : '') ?>"
                   href="<?= $promo->url ?>"
                   data-id="<?= $promo->id ?>"><span><?= $promo->name_file ?></span><span
                        class="color_th">x</span></a>
            </li>
            <?php
        }

        ?>
    </ul>
</div>
<div id="align_zone">
    <p class="publisher_title">Добавить промо</p>

    <div class="white_fon form_style_l">
        <?php $form = $this->beginWidget('CActiveForm', array(
            'id' => 'magazine-stock-form',
            'enableAjaxValidation' => false,
            'htmlOptions' => array('enctype' => 'multipart/form-data'),
        )); ?>

        <div id="akz_file_input_img">
            <p class="inline_style pub_form_label_p">Изображение
            </p>

            <div id="wrap_eto">
                <div class="img_file_wrap">
                    <div class="img_file_wrap_i">
                        <?php if ($model->name_file != '') { ?>
                            <input type="file" class="publiser_file_input" name="MagazinePromoFiles[url]" required
                                   value="" disabled="disabled">
                        <?php } else { ?>
                            <input type="file" class="publiser_file_input" name="MagazinePromoFiles[url]"
                                   value="<?= $model->name_file ?>">
                            <p class="bg_color_th"></p>
                        <?php } ?>
                    </div>
                </div>
                <div class="img_file_preview">
                    <img src="/html_source/img/pub_preview2.png">
                </div>
            </div>
            <p>Выбрать категорию:</p>
            <?php echo CHtml::dropDownList('name_select', '', ['empty' => 'Выбрать категорию'] + CHtml::listData(MagazinePromoCat::model()->findAll(), 'id', 'name'), array('class' => 'css_trans')); ?>
            <p style="margin-top: 0.5vw">Создать категорию:</p>
            <?php echo $form->textField(MagazinePromoCat::model(), 'name', array('size' => 60, 'maxlength' => 255, 'class' => 'css_trans')); ?>

        </div>
        <?php echo CHtml::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить', ['class' => 'form_submit bg_color_th', 'disabled' => 'disabled']); ?>
        <?php $this->endWidget(); ?>
    </div>
    <!-- form -->
</div>