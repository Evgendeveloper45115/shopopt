<?php
/* @var $this MagazineStockController */
/* @var $model MagazineStock */

$this->breadcrumbs=array(
	'Magazine Stocks'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List MagazineStock', 'url'=>array('index')),
	array('label'=>'Manage MagazineStock', 'url'=>array('admin')),
);
?>


<?php $this->renderPartial('_form', array('model'=>$model, 'ordersIds' => $ordersIds, 'allModels'=>$allModels,'catsPromo' => $catsPromo)); ?>