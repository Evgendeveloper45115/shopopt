<?php
/* @var $this MagazineStockController */
/* @var $model MagazineStock */

$this->breadcrumbs=array(
	'Magazine Stocks'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List MagazineStock', 'url'=>array('index')),
	array('label'=>'Create MagazineStock', 'url'=>array('create')),
	array('label'=>'View MagazineStock', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage MagazineStock', 'url'=>array('admin')),
);
?>

<h1>Update MagazineStock <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>