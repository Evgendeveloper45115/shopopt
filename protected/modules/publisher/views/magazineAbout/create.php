<?php
/* @var $this MagazineAboutController */
/* @var $model MagazineAbout */

$this->breadcrumbs=array(
	'Magazine Abouts'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List MagazineAbout', 'url'=>array('index')),
	array('label'=>'Manage MagazineAbout', 'url'=>array('admin')),
);
?>

<h1>Create MagazineAbout</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>