<?php
/* @var $this MagazineAboutController */
/* @var $data MagazineAbout */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('name_company')); ?>:</b>
	<?php echo CHtml::encode($data->name_company); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('text_company')); ?>:</b>
	<?php echo CHtml::encode($data->text_company); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('name_button')); ?>:</b>
	<?php echo CHtml::encode($data->name_button); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('address_company')); ?>:</b>
	<?php echo CHtml::encode($data->address_company); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('phone_company')); ?>:</b>
	<?php echo CHtml::encode($data->phone_company); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('email_company')); ?>:</b>
	<?php echo CHtml::encode($data->email_company); ?>
	<br />


</div>