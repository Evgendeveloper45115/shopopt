<?php
/* @var $this MagazineAboutController */
/* @var $model MagazineAbout */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'magazine-about-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'name_company'); ?>
		<?php echo $form->textField($model,'name_company',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'name_company'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'text_company'); ?>
		<?php echo $form->textArea($model,'text_company',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'text_company'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'name_button'); ?>
		<?php echo $form->textField($model,'name_button'); ?>
		<?php echo $form->error($model,'name_button'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'address_company'); ?>
		<?php echo $form->textArea($model,'address_company',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'address_company'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'phone_company'); ?>
		<?php echo $form->textArea($model,'phone_company',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'phone_company'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'email_company'); ?>
		<?php echo $form->textArea($model,'email_company',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'email_company'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->