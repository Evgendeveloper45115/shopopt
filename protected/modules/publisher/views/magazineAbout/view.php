<?php
/* @var $this MagazineAboutController */
/* @var $model MagazineAbout */

$this->breadcrumbs=array(
	'Magazine Abouts'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List MagazineAbout', 'url'=>array('index')),
	array('label'=>'Create MagazineAbout', 'url'=>array('create')),
	array('label'=>'Update MagazineAbout', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete MagazineAbout', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage MagazineAbout', 'url'=>array('admin')),
);
?>

<h1>View MagazineAbout #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'name_company',
		'text_company',
		'name_button',
		'address_company',
		'phone_company',
		'email_company',
	),
)); ?>
