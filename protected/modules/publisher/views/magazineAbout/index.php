<?php
/* @var $this MagazineAboutController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Magazine Abouts',
);

$this->menu=array(
	array('label'=>'Create MagazineAbout', 'url'=>array('create')),
	array('label'=>'Manage MagazineAbout', 'url'=>array('admin')),
);
?>

<h1>Magazine Abouts</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
