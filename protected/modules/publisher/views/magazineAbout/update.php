<?php
/* @var $this MagazineAboutController */
/* @var $model MagazineAbout */

$this->breadcrumbs=array(
	'Magazine Abouts'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List MagazineAbout', 'url'=>array('index')),
	array('label'=>'Create MagazineAbout', 'url'=>array('create')),
	array('label'=>'View MagazineAbout', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage MagazineAbout', 'url'=>array('admin')),
);
?>

<h1>Update MagazineAbout <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>