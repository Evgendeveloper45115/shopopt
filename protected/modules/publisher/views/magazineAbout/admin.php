<div id="align_zone">
<p class="publisher_title">Редактировать информацию о компании</p>
<div class="white_fon form_style">
	<form name="comp_des" action="" method="post" enctype="multipart/form-data">
		<p>Название компании:</p>
		<textarea name="MagazineAbout[name_company]" class="css_trans" required><?=$model->name_company?></textarea>
		<p>Описание компании:</p>
		<textarea name="MagazineAbout[text_company]" class="css_trans" required><?=$model->text_company?></textarea>
		<p>Название кнопки:</p>
		<textarea name="MagazineAbout[name_button]" class="css_trans" required><?=$model->name_button?></textarea>
		<p>Фоновое изображение:</p>
		<div class="img_file_box">
			<div class="img_file_wrap">
				<!-- если нет файла
                    <input type="file" class="publiser_file_input"  name="file_akz" required value="">-->
				<!-- eсли файл есть -->
				<input type="file" class="publiser_file_input"  name="MagazineAbout[img]"  value="<?=$model->img?>">
				<p class="bg_color_th"></p>
			</div><div class="img_file_preview">
				<img src="">
			</div>
		</div>
		<div class="input_div">
			<p>Адрес:</p>
			<input type="text" name="MagazineAbout[address_company]" value="<?=$model->address_company?>" required>
		</div>
		<div class="input_div">
			<p>Координаты x:</p>
			<input type="text" name="MagazineAbout[point_x]" value="<?=$model->point_x?>" required>
		</div>
		<div class="input_div">
			<p>Координаты y:</p>
			<input type="text" name="MagazineAbout[point_y]" value="<?=$model->point_y?>" required>
		</div>
		<div class="input_div">
			<p>Телефон</p>
			<input type="text" name="MagazineAbout[phone_company]" value="<?=$model->phone_company?>" required>
		</div>
		<div class="input_div" >
			<p>E-mail</p>
			<input type="text" name="MagazineAbout[email_company]" value="<?=$model->email_company?>" required>
		</div>
		<button class="form_submit bg_color_th left_align">Сохранить</button>
	</form>


</div>
</div>