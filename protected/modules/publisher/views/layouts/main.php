<!DOCTYPE html>
<html lang="ru">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="language" content="ru">

    <!-- blueprint CSS framework -->
    <!--	<link rel="stylesheet" type="text/css" href="--><?php //echo Yii::app()->request->baseUrl; ?><!--/css/screen.css" media="screen, projection">-->
    <link rel="stylesheet" href="/html_source/css/all.css">
    <!-- page style-->
    <link rel="stylesheet" href="/html_source/css/all_publisher.css">

    <style>
        <?php include Yii::getPathOfAlias('webroot').'/html_source/head_style.php'?>
    </style>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
    <script src="/html_source/js/all_publisher.js"></script>
    <script src="/html_source/js/core.js"></script>
    <script src="/html_source/js/all.js"></script>

    <!-- page script-->

    <!--    <script src="/html_source/js/all_inner.js"></script>-->
    <title><?php echo CHtml::encode($this->pageTitle); ?></title>
</head>
<body>

<div id="left_bar" class="inline_style">
    <div id="left_bar_wrap">
        <a href="/" class="back color_th_hover css_trans">← Вернуться на сайт</a>
        <?php
        echo CHtml::image('/upload/' . @$this->dataModel['logo_site'], '', ['width'=> 50, 'height' => 50, 'class'=>'logo'. ($this->connectID == 36 ? ' barbers-logo' : null)]);
        ?>
        <?php $this->widget('zii.widgets.CMenu',array(
            'encodeLabel'=>false,
            'items'=>array(
                array('label'=>'Каталог<span class="bg_color_th"></span>', 'url'=>array('/publisher/MagazineCatalog/admin'), 'visible'=>(Yii::app()->user->role == WebUser::PUBLISHER)),
                array('label'=>'Акция<span class="bg_color_th"></span>', 'url'=>array('/publisher/MagazineStock/create'), 'visible'=>(Yii::app()->user->role == WebUser::PUBLISHER), 'active' => preg_match('/(MagazineStock\/create|MagazineStock\/index)/', $_SERVER['REQUEST_URI'])),
                array('label'=>'Промоматериалы<span class="bg_color_th"></span>', 'url'=>array('/publisher/MagazinePromo/create'), 'visible'=>(Yii::app()->user->role == WebUser::PUBLISHER) , 'active' => preg_match('/(MagazinePromo\/admin|MagazinePromo\/index|MagazinePromo\/create)/', $_SERVER['REQUEST_URI'])),
                array('label'=>'О компании<span class="bg_color_th"></span>', 'url'=>array('/publisher/magazineAbout/admin'), 'visible'=>(Yii::app()->user->role == WebUser::PUBLISHER)),
                array('label'=>'Редактирование главной<span class="bg_color_th"></span>', 'url'=>array('/publisher/magazineMain/admin'), 'visible'=>(Yii::app()->user->role == WebUser::PUBLISHER)),
                array('label'=>'Доставка<span class="bg_color_th"></span>', 'url'=>array('/publisher/MagazinePublisherDelivery/admin'), 'visible'=>(Yii::app()->user->role == WebUser::PUBLISHER)),
                array('label'=>'Тип продукта<span class="bg_color_th"></span>', 'url'=>array('/publisher/MagazineTypeProduct/admin'), 'visible'=>(Yii::app()->user->role == WebUser::PUBLISHER)),
                array('label'=>'Редактирование текста скидки<span class="bg_color_th"></span>', 'url'=>array('/publisher/magazineMain/discount'), 'visible'=>(Yii::app()->user->role == WebUser::PUBLISHER)),
                array('label'=>'Фильты и теги<span class="bg_color_th"></span>', 'url'=>array('/publisher/MagazineFilters/admin'), 'visible'=>(Yii::app()->user->role == WebUser::PUBLISHER)),
                array('label' => 'Настройки<span class="bg_color_th"></span>', 'url' => array('/admin/Setting/index'), 'visible' => (Yii::app()->user->role == WebUser::PUBLISHER)),
            ),
            'submenuHtmlOptions' => ['class' => 'css_trans'],
            'itemCssClass' => 'child_true css_trans'
        )); ?>
    </div>
    <a href="/site/logout" class="log_out">Выход</a>
</div><div id="conten_zone" class="inline_style <?=(strpos($_SERVER['REQUEST_URI'],'/publisher/MagazineStock') !== false || strpos($_SERVER['REQUEST_URI'],'/publisher/MagazineCatalog') !== false || strpos($_SERVER['REQUEST_URI'],'/publisher/MagazinePromo') !== false?'pub_row':'')?>">
    <?=$content?>
</div>

</body>
</html>