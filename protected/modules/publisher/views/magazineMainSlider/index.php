<?php
/* @var $this MagazineMainSliderController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Magazine Main Sliders',
);

$this->menu=array(
	array('label'=>'Create MagazineMainSlider', 'url'=>array('create')),
	array('label'=>'Manage MagazineMainSlider', 'url'=>array('admin')),
);
?>

<h1>Magazine Main Sliders</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
