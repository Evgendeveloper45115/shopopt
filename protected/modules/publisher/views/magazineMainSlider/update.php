<?php
/* @var $this MagazineMainSliderController */
/* @var $model MagazineMainSlider */

$this->breadcrumbs=array(
	'Magazine Main Sliders'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List MagazineMainSlider', 'url'=>array('index')),
	array('label'=>'Create MagazineMainSlider', 'url'=>array('create')),
	array('label'=>'View MagazineMainSlider', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage MagazineMainSlider', 'url'=>array('admin')),
);
?>

<h1>Update MagazineMainSlider <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>