<?php
/* @var $this MagazineMainSliderController */
/* @var $model MagazineMainSlider */

$this->breadcrumbs=array(
	'Magazine Main Sliders'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List MagazineMainSlider', 'url'=>array('index')),
	array('label'=>'Create MagazineMainSlider', 'url'=>array('create')),
	array('label'=>'Update MagazineMainSlider', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete MagazineMainSlider', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage MagazineMainSlider', 'url'=>array('admin')),
);
?>

<h1>View MagazineMainSlider #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'image',
		'id_main',
	),
)); ?>
