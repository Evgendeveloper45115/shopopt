<?php
/* @var $this MagazineMainSliderController */
/* @var $model MagazineMainSlider */

$this->breadcrumbs=array(
	'Magazine Main Sliders'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List MagazineMainSlider', 'url'=>array('index')),
	array('label'=>'Manage MagazineMainSlider', 'url'=>array('admin')),
);
?>

<h1>Create MagazineMainSlider</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>