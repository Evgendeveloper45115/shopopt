<?php
/* @var $this MagazineTypeProductController */
/* @var $model MagazineTypeProduct */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'magazine-type-product-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

<!--	<p class="note">Fields with <span class="required">*</span> are required.</p>-->

	<?php echo $form->errorSummary($model); ?>

	<div id="align_zone">


		<!-- заменить текст на "редактировать акцию", вслучае редактирования -->
		<p class="publisher_title">Добавить тип продукта</p>

		<div class="white_fon form_style_l">


				<div class="type_list_item">
					<?php echo $form->textField($model,'name',array('size'=>60,'maxlength'=>255)); ?>
				</div>

				<button class="form_submit bg_color_th">Сохранить</button>

		</div>




		<!--content_area end-->
	</div>



<!--	<div class="row">-->
<!--		--><?php //echo $form->labelEx($model,'site_connect_id'); ?>
<!--		--><?php //echo $form->textField($model,'site_connect_id'); ?>
<!--		--><?php //echo $form->error($model,'site_connect_id'); ?>
<!--	</div>-->


<?php $this->endWidget(); ?>

</div><!-- form -->