<?php
/* @var $this MagazineTypeProductController */
/* @var $model MagazineTypeProduct */
?>
<div id="align_zone">
<style>
	.client_shops_list_item{padding: 1.8vw 2.1%;width: 44.8%;margin:0 1% 2% 0;}
	.client_shops_list_item:nth-child(2n){margin-right: 0;}
</style>
<div>
	<p class="publisher_title">Тип продукта</p>
	<div class="white_fon form_style_l">
		<form name="add_akz" action="" method="post" >
	<?php
	if(!empty($model)) {
		foreach ($model as $md) { ?>

			<div class="type_list_item">
				<input type="text" value="<?=$md->name?>" name="name[<?=$md->id?>]">
				<a class="del_type add_new_type" href="/publisher/MagazineTypeProduct/delete/id/<?=$md->id?>" onclick="if(!confirm('Вы точно хотите удалить')){event.preventDefault();}">Удалить</a>
			</div>

		<?php }
	} else { ?>
		<div class="type_list_item">
			<input type="text" value="<?=$md->name?>" name="name[<?=$md->id?>]">
			<a class="del_type add_new_type" href="/publisher/MagazineTypeProduct/delete/id/<?=$md->id?>" onclick="if(!confirm('Вы точно хотите удалить')){event.preventDefault();}">Удалить</a>
		</div>
	<?php }

	?>
	<a class="add_new_type" href="/publisher/MagazineTypeProduct/create">Добавить новый тип</a>
	<button class="form_submit bg_color_th">Сохранить</button>
		</form>
		</div>
</div>
	</div>
<script>
	$(function(){
		console.log();
		if($('.type_list_item').length == 1 && $('.type_list_item input').val() == ''){
			$('.type_list_item input').attr('name','type[]')
		}
	})
</script>
