<?php
/* @var $this MagazineTypeProductController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Magazine Type Products',
);

$this->menu=array(
	array('label'=>'Create MagazineTypeProduct', 'url'=>array('create')),
	array('label'=>'Manage MagazineTypeProduct', 'url'=>array('admin')),
);
?>

<h1>Magazine Type Products</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
