<?php
/* @var $this MagazineTypeProductController */
/* @var $model MagazineTypeProduct */

$this->breadcrumbs=array(
	'Magazine Type Products'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List MagazineTypeProduct', 'url'=>array('index')),
	array('label'=>'Create MagazineTypeProduct', 'url'=>array('create')),
	array('label'=>'View MagazineTypeProduct', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage MagazineTypeProduct', 'url'=>array('admin')),
);
?>

<h1>Update MagazineTypeProduct <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>