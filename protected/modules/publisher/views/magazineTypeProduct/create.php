<?php
/* @var $this MagazineTypeProductController */
/* @var $model MagazineTypeProduct */

$this->breadcrumbs=array(
	'Magazine Type Products'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List MagazineTypeProduct', 'url'=>array('index')),
	array('label'=>'Manage MagazineTypeProduct', 'url'=>array('admin')),
);
?>


<?php $this->renderPartial('_form', array('model'=>$model)); ?>