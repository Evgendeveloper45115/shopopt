<?php
/**
 * @var $getProductName MagazineProduct
 */
?>
<script>
    var tree_open = '<?=($_SESSION['tree'] == null?'[0]':$_SESSION['tree'])?>';
</script>
<div id="catalog_menu">
    <ul>
        <li>
            <a href="" data-id="0" class="color_th_hover"><span>Каталог</span>
                <small>(0)</small>
            </a>
            <ul>
                <?= MagazineCatalogController::view_cat($Categories) ?>
            </ul>
        </li>
    </ul>

    <div id="contextmenu">
        <div id="contextmenu_close"></div>
        <div id="contextmenu_wrap">
            <p class="title"></p>
            <a href="" class="color_th_hover child_add" data-type="child_add"><img
                    src="/html_source/img/r_m_ico1.png"><span>Добавить</span></a>
            <a href="" class="color_th_hover edit_element" data-type="edit_element"><img
                    src="/html_source/img/r_m_ico2.png"><span>Редактировать</span></a>
            <a href="#" class="color_th_hover move_element" data-type="move_element"><img
                    src="/html_source/img/r_m_ico3.png"><span>Переместить</span></a>
            <a href="#" class="color_th_hover clone_element" data-type="clone_element"><img
                    src="/html_source/img/r_m_ico4.png"><span>Сделать копию</span></a>
            <a href="#" class="color_th_hover pub_element" data-type="pub_element"><img
                    src="/html_source/img/r_m_ico5.png"><span>Опубликовать</span></a>
            <a href="#" class="color_th_hover unpub_element" data-type="unpub_element"><img
                    src="/html_source/img/r_m_ico6.png"><span>Отменить публикацию</span></a>
            <a href="javascript://" class="color_th_hover del_element" data-type="del_element"><img
                    src="/html_source/img/r_m_ico7.png"><span>Удалить</span></a>
            <!--<a href="#" class="color_th_hover undel_element" data-type="undel_element"><img src="/html_source/img/r_m_ico8.png"><span>Востановить</span></a>-->
            <!--            <a href="javascript://" class="color_th_hover show_in_webview" data-type="show_in_webview" target="blank"><img src="/html_source/img/r_m_ico9.png"><span>Просмотр</span></a>-->
        </div>
    </div>

</div>
<div id="align_zone" <?= ($_GET['type'] ? '' : 'style="display: none;"') ?>>

    <!-- заменить текст на "редактировать ", вслучае редактирования -->
    <p class="publisher_title"><?= ($_GET['type'] === 'edit_element' ? 'Редактировать' : 'Добавить товар') ?></p>

    <div class="white_fon form_style_l publisher">

        <div id="select_type_" style="<?= ($getProductName === null ? '' : 'display: none;') ?>">
            <p>Выберите тип ресурса</p>

            <div class="radio_line">
                <input type="radio" value="1" name="form_s" class="form_s" checked><span> - Раздел</span>
            </div>
            <div class="radio_line">
                <input type="radio" value="2" name="form_s" class="form_s"><span> - Товар</span>
            </div>
            <button class="bg_color_th">Продолжить</button>
        </div>

        <div id="form_hid_tab">
            <!--            --><?php //var_dump($getProductName->type)?>
            <form class="folder" action="" method="post"
                  style="<?= ($getProductName->type === '1' ? 'display: block;' : 'display: none;') ?>">
                <p>Название раздела</p>
                <input type="text" name="folder_name" value="<?= $getProductName->title ?>">
                <button class="form_submit bg_color_th">Сохранить</button>
            </form>
            <?php
            $is_active = [];
            if ($getProductName->is_active) {
                $is_active = json_decode($getProductName->is_active);
            }
            ?>
            <form class="element" id="magazineCatalog" name="add_akz" action="" method="post"
                  enctype="multipart/form-data"
                  style="<?= ($getProductName->type === '0' ? 'display: block;' : 'display: none;') ?>">
                <div style="display: flex; justify-content: flex-end;    margin-right: 7%;">
                   <span>
                       Отображать раздел
                   </span>
                </div>
                <div class="input_div">
                    <p>Артикул:</p>
                    <input class="input_publisher" type="text" name="MagazineProduct[articul]"
                           value="<?= $getProductName->articul ?>" required>
                    <?php
                    $class = 'selector';
                    if ($is_active->articul != 'off') {
                        $class = 'selector active bg_color_th';
                    }
                    ?>
                    <input style="width: 0; height: 0; opacity: 0;" type="checkbox"
                           name="MagazineProduct[articul-check]" <?= $is_active->articul == 'off' ? null : 'checked' ?>
                           class="is_active_checkbox">

                    <div class="<?= $class ?>"></div>


                </div>
                <div class="input_div">
                    <?php
                    $class = 'selector';
                    if ($is_active->name_product != 'off') {
                        $class = 'selector active bg_color_th';
                    }
                    ?>
                    <p>Наименование:</p>
                    <input class="input_publisher" type="text" name="MagazineProduct[name_product]"
                           value="<?= htmlspecialchars($getProductName->name_product, ENT_QUOTES) ?>">
                    <input style="width: 0; height: 0; opacity: 0;" type="checkbox"
                           name="MagazineProduct[name_product-check]"
                        <?= $is_active->name_product == 'off' ? null : 'checked' ?> class="is_active_checkbox">

                    <div class="<?= $class ?>"></div>
                </div>
                <div class="input_div">
                    <?php
                    $class = 'selector';
                    if ($is_active->text_product != 'off') {
                        $class = 'selector active bg_color_th';
                    }
                    ?>
                    <p>Описание:</p>
                    <input class="input_publisher" type="text" name="MagazineProduct[text_product]"
                           value="<?= htmlspecialchars($getProductName->text_product, ENT_QUOTES) ?>">
                    <input style="width: 0; height: 0; opacity: 0;" type="checkbox"
                           name="MagazineProduct[text_product-check]"
                        <?= $is_active->text_product == 'off' ? null : 'checked' ?> class="is_active_checkbox">

                    <div class="<?= $class ?>"></div>
                </div>
                <div class="input_div">
                    <?php
                    $class = 'selector';
                    if ($is_active->type_product != 'off') {
                        $class = 'selector active bg_color_th';
                    }
                    ?>

                    <p>Упаковка:</p>
                    <select style="width: calc(44.4% - 2.2vw)!important" class="input_publisher"
                            name="MagazineProduct[type_product]" id="select_type">
                        <?php foreach ($getTypeProduct as $key => $getType) {
                            if ($getProductName->type_product == $key) {
                                echo '<option selected="' . $key . '">' . $getType . '</option>';
                            } else {
                                echo '<option value="' . $key . '">' . $getType . '</option>';
                            }
                        } ?>
                    </select>
                    <input style="width: 0; height: 0; opacity: 0;" type="checkbox"
                           name="MagazineProduct[type_product-check]"
                        <?= $is_active->type_product == 'off' ? null : 'checked' ?> class="is_active_checkbox">

                    <div class="<?= $class ?>"></div>
                </div>

                <div class="input_div">
                    <?php
                    $class = 'selector';
                    if ($is_active->price != 'off') {
                        $class = 'selector active bg_color_th';
                    }
                    ?>

                    <p>Цена:</p>
                    <input class="input_publisher" type="text" name="MagazineProduct[price]"
                           value="<?= $getProductName->price ?>">
                    <input style="width: 0; height: 0; opacity: 0;" type="checkbox" name="MagazineProduct[price-check]"
                        <?= $is_active->price == 'off' ? null : 'checked' ?> class="is_active_checkbox">

                    <div class="<?= $class ?>"></div>
                </div>
                <div class="input_div">
                    <?php
                    $class = 'selector';
                    if ($is_active->discountPrice != 'off') {
                        $class = 'selector active bg_color_th';
                    }
                    ?>

                    <p>Цена со скидкой:</p>
                    <input class="input_publisher" type="text" name="MagazineProduct[discountPrice]"
                           value="<?= $getProductName->discountPrice ?>">
                    <input style="width: 0; height: 0; opacity: 0;" type="checkbox"
                           name="MagazineProduct[discountPrice-check]"
                        <?= $is_active->discountPrice == 'off' ? null : 'checked' ?> class="is_active_checkbox">

                    <div class="<?= $class ?>"></div>
                </div>
                <div class="input_div">
                    <?php
                    $class = 'selector';
                    if ($is_active->min_count != 'off') {
                        $class = 'selector active bg_color_th';
                    }
                    ?>

                    <p>Минимальное количество:</p>
                    <input class="input_publisher" type="text" name="MagazineProduct[min_count]"
                           value="<?= $getProductName->min_count ?>">
                    <input style="width: 0; height: 0; opacity: 0;" type="checkbox"
                           name="MagazineProduct[min_count-check]"
                        <?= $is_active->min_count == 'off' ? null : 'checked' ?> class="is_active_checkbox">

                    <div class="<?= $class ?>"></div>
                </div>
                <div class="input_div">
                    <?php
                    $class = 'selector';
                    if ($is_active->balanceStock != 'off') {
                        $class = 'selector active bg_color_th';
                    }
                    ?>

                    <p>Остаток на складе:</p>
                    <input class="input_publisher" type="text" name="MagazineProduct[balanceStock]"
                           value="<?= $getProductName->balanceStock ?>">
                    <input style="width: 0; height: 0; opacity: 0;" type="checkbox"
                           name="MagazineProduct[balanceStock-check]"
                        <?= $is_active->balanceStock == 'off' ? null : 'checked' ?> class="is_active_checkbox">

                    <div class="<?= $class ?>"></div>
                </div>
                <div class="input_div">
                    <?php
                    $class = 'selector';
                    if ($is_active->recommend_ids != 'off') {
                        $class = 'selector active bg_color_th';
                    }
                    ?>

                    <p>Рекомендованные товары:</p>
                    <input class="input_publisher" type="text" name="recommend_ids" value="<?= $recommendIds ?>">
                    <input style="width: 0; height: 0; opacity: 0;" type="checkbox" name="recommend_ids-check"
                        <?= $is_active->recommend_ids == 'off' ? null : 'checked' ?> class="is_active_checkbox">

                    <div class="<?= $class ?>"></div>
                </div>
                <div id="publiser_tovar_foto" style="margin-bottom: 25px">
                    <p style="width: 34%" class="inline_style pub_form_label_p">Изображение:</p>

                    <div id="publisher_foto_cont">
                        <?php
                        if ($getProductName->image_product != null && $getProductName->image_product != '[]') {

                            $decode = json_decode($getProductName->image_product);
                            foreach ($decode as $key => $imgPath) {
                                if ($imgPath != '' && !stristr($imgPath, '[')) {
                                    ?>
                                    <div class="img_file_box">
                                    <div class="img_file_wrap">
                                        <div class="img_file_wrap_i">
                                            <input data-type="image_product" class="publiser_file_input"
                                                   data-id="<?= $getProductName->id ?>" name="MagazineProduct[]"
                                                   value="<?= $imgPath ?>">
                                            <input type="file" data-type="image_product" class="publiser_file_input"
                                                   data-id="<?= $getProductName->id ?>" name="MagazineProduct[]"
                                                   value="<?= $imgPath ?>">

                                            <p class="bg_color_th"></p>
                                        </div>
                                    </div>
                                    <div class="img_file_preview">
                                        <img src="<?= $imgPath ?>">

                                        <div class="del_this_pub_foto"></div>
                                    </div>
                                    </div><?php
                                }
                            }
                        } else {
                            ?>
                            <div class="img_file_box">
                                <div class="img_file_wrap">
                                    <div class="img_file_wrap_i">
                                        <input type="file" data-type="image_product" class="publiser_file_input"
                                               data-id="<?= $getProductName->id ?>" name="MagazineProduct[]"
                                               value="">
                                    </div>
                                </div>
                                <div class="img_file_preview">
                                    <img src="<?= '/html_source/img/save_ico.png' ?>">

                                    <div class="del_this_pub_foto"></div>
                                </div>
                            </div>

                            <?php
                        }
                        ?>
                    </div>
                </div>
                <div class="input_div">
                    <button type="button" class="add_field_publisher bg_color_th">+ Добавить поле</button>
                </div>
                <?php
                if ($getProductName->dop_fields) {
                    $array = json_decode($getProductName->dop_fields);
                    foreach ($array as $name => $value) {
                        if (stristr($name, 'duplicate_name')) {
                            $strWithoutChars = preg_replace('/[^0-9]/', '', $name);
                            ?>
                            <div class="input_div clone_fields" data-id="1" data-clone="<?= $strWithoutChars ?>">
                            <p><input placeholder="Название поля" type="text"
                                      name="duplicate_name_<?= $strWithoutChars ?>" value="<?= $value ?>"></p>
                            <?php
                        } else {
                            $strWithoutChars = preg_replace('/[^0-9]/', '', $name);
                            if (!stristr($name, '-check')) {

                                $class = 'selector';
                                if ($is_active->{'duplicate_value_' . $strWithoutChars} != 'off') {
                                    $class = 'selector active bg_color_th';
                                }
                                ?>

                                <input class="input_publisher" type="text"
                                       name="duplicate_value_<?= $strWithoutChars ?>"
                                       value="<?= $value ?>">
                                <input style="width: 0; height: 0; opacity: 0;" type="checkbox"
                                       name="duplicate_value_<?= $strWithoutChars ?>-check"
                                    <?= $is_active->{'duplicate_value_' . $strWithoutChars} == 'off' ? null : 'checked' ?>
                                       class="is_active_checkbox">
                                <div class="<?= $class ?>"></div>
                                <div class="delete_publisher color_th_hover">Удалить</div></div>
                                <?php
                            }
                        }
                    }
                }
                ?>
                <button style="display: none" type="submit" class="form_submit form_del_submit bg_color_th delete-img">
                    Сохранить
                </button>
                <button type="button" class="form_submit del_img bg_color_th delete-img second">Сохранить</button>
            </form>
        </div>
    </div>
