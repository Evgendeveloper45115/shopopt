<?php
/* @var $this MagazineMainController */
/* @var $model MagazineMain */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'magazine-main-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'text_popup'); ?>
		<?php echo $form->textArea($model,'text_popup',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'text_popup'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'text_main'); ?>
		<?php echo $form->textArea($model,'text_main',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'text_main'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'main_logo'); ?>
		<?php echo $form->textField($model,'main_logo',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'main_logo'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'site_connect_id'); ?>
		<?php echo $form->textField($model,'site_connect_id'); ?>
		<?php echo $form->error($model,'site_connect_id'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->