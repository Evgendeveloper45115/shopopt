<?php
/* @var $this MagazineMainController */
/* @var $model MagazineMain */

$this->breadcrumbs=array(
	'Magazine Mains'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List MagazineMain', 'url'=>array('index')),
	array('label'=>'Create MagazineMain', 'url'=>array('create')),
	array('label'=>'Update MagazineMain', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete MagazineMain', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage MagazineMain', 'url'=>array('admin')),
);
?>

<h1>View MagazineMain #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'text_popup',
		'text_main',
		'main_logo',
		'site_connect_id',
	),
)); ?>
