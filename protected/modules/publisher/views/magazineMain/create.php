<?php
/* @var $this MagazineMainController */
/* @var $model MagazineMain */

$this->breadcrumbs=array(
	'Magazine Mains'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List MagazineMain', 'url'=>array('index')),
	array('label'=>'Manage MagazineMain', 'url'=>array('admin')),
);
?>

<h1>Create MagazineMain</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>