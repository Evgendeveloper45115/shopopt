<?php
/* @var $this MagazineMainController */
/* @var $data MagazineMain */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('text_popup')); ?>:</b>
	<?php echo CHtml::encode($data->text_popup); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('text_main')); ?>:</b>
	<?php echo CHtml::encode($data->text_main); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('main_logo')); ?>:</b>
	<?php echo CHtml::encode($data->main_logo); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('site_connect_id')); ?>:</b>
	<?php echo CHtml::encode($data->site_connect_id); ?>
	<br />


</div>