<?php
/* @var $this MagazineMainController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Magazine Mains',
);

$this->menu=array(
	array('label'=>'Create MagazineMain', 'url'=>array('create')),
	array('label'=>'Manage MagazineMain', 'url'=>array('admin')),
);
?>

<h1>Magazine Mains</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
