<?php
/* @var $this MagazineMainController */
/* @var $model MagazineMain */

$this->breadcrumbs=array(
	'Magazine Mains'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List MagazineMain', 'url'=>array('index')),
	array('label'=>'Create MagazineMain', 'url'=>array('create')),
	array('label'=>'View MagazineMain', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage MagazineMain', 'url'=>array('admin')),
);
?>

<h1>Update MagazineMain <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>