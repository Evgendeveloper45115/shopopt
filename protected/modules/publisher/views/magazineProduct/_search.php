<?php
/* @var $this MagazineProductController */
/* @var $model MagazineProduct */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'site_connect_id'); ?>
		<?php echo $form->textField($model,'site_connect_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'price'); ?>
		<?php echo $form->textField($model,'price'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'min_count'); ?>
		<?php echo $form->textField($model,'min_count'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'text_product'); ?>
		<?php echo $form->textArea($model,'text_product',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'name_product'); ?>
		<?php echo $form->textField($model,'name_product',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'desc_product'); ?>
		<?php echo $form->textArea($model,'desc_product',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'image_product'); ?>
		<?php echo $form->textField($model,'image_product',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->