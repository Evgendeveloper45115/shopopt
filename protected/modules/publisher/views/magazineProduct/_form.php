<?php
/* @var $this MagazineProductController */
/* @var $model MagazineProduct */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'magazine-product-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'htmlOptions'=>array('enctype'=>'multipart/form-data'),
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

<!--	<div class="row">-->
<!--		--><?php //echo $form->labelEx($model,'site_connect_id'); ?>
<!--		--><?php //echo $form->textField($model,'site_connect_id'); ?>
<!--		--><?php //echo $form->error($model,'site_connect_id'); ?>
<!--	</div>-->

	<div class="row">
		<?php echo $form->labelEx($model,'price'); ?>
		<?php echo $form->textField($model,'price'); ?>
		<?php echo $form->error($model,'price'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'min_count'); ?>
		<?php echo $form->textField($model,'min_count'); ?>
		<?php echo $form->error($model,'min_count'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'text_product'); ?>
		<?php echo $form->textArea($model,'text_product',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'text_product'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'name_product'); ?>
		<?php echo $form->textField($model,'name_product',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'name_product'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'desc_product'); ?>
		<?php echo $form->textArea($model,'desc_product',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'desc_product'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'catalog_id'); ?>
		<?php echo CHtml::dropDownList('MagazineProduct[catalog_id]',$model->catalog_id, $list); ?>
		<?php echo $form->error($model,'catalog_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'image_product'); ?>
		<?php echo $form->fileField($model,'image_product'); ?>
		<?php echo $form->error($model,'image_product'); ?>
	</div>
	<div class="row">
		Тип упаковки:
		<?php echo CHtml::dropDownList('type_product','',$modelCategoryType);?>
	</div>
	<div class="row">
		<?php
		echo CHtml::dropDownList('MagazineRecommendProduction', $activeModelRecommed, $modelRecommend,  ['multiple' => 'multiple'])
		?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->