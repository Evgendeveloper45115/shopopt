<?php
/* @var $this MagazineStockController */
/* @var $model MagazineStock */
?>

<div id="publisher_left_menu">
	<a href="/publisher/MagazineStock/admin" class="add_akz color_th">Добавить акцию</a>
	<ul>
		<?php foreach($model as $md){?>
		<li>
			<a href="/publisher/MagazineStock/index/id/<?=$md->id?>" class="color_th_hover css_trans"><?=$md->header_text?></a>
		</li>
		<?php }?>
	</ul>
</div><div id="align_zone">


	<!-- заменить текст на "редактировать акцию", вслучае редактирования -->
	<p class="publisher_title">Добавить акцию</p>

	<div class="white_fon form_style_l">

		<form name="add_akz" action="" method="post"   enctype="multipart/form-data">
			<input type="hidden" name="id" value="1">
			<p>Название:</p>
			<input type="text" name="akz_name" required>
			<p>Описание акции:</p>
			<textarea name="akz_descr" class="css_trans" required></textarea>
			<p>Полное описание акции:</p>
			<textarea name="akz_content" class="css_trans" required></textarea>
			<p>Изображение:</p>
			<div class="img_file_box">
				<div class="img_file_wrap">
					<div class="img_file_wrap_i">
						<!-- если нет файла
                            <input type="file" class="publiser_file_input" name="file_akz" required value="">-->
						<!-- eсли файл есть -->
						<input type="file"  class="publiser_file_input"  name="main_logo"  value="../img/main_img.png">
						<p class="bg_color_th"></p>
					</div>
				</div><div class="img_file_preview">
					<img src="<?=$model->image?>" style="height: 200px;width:200px;display: block">
				</div>
			</div>
			<button class="form_submit bg_color_th">Сохранить</button>
		</form>
	</div>




	<!--content_area end-->
</div>