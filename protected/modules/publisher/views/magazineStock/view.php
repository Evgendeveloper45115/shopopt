<?php
/* @var $this MagazineStockController */
/* @var $model MagazineStock */

$this->breadcrumbs=array(
	'Magazine Stocks'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List MagazineStock', 'url'=>array('index')),
	array('label'=>'Create MagazineStock', 'url'=>array('create')),
	array('label'=>'Update MagazineStock', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete MagazineStock', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage MagazineStock', 'url'=>array('admin')),
);
?>

<h1>View MagazineStock #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'site_connect_id',
		'text',
		'image',
		'header_text',
	),
)); ?>
