<?php
/* @var $this MagazineStockController */
/* @var $dataProvider CActiveDataProvider */
/* @var $form CActiveForm */

$this->breadcrumbs = array(
    'Magazine Stocks',
);

$this->menu = array(
    array('label' => 'Create MagazineStock', 'url' => array('create')),
    array('label' => 'Manage MagazineStock', 'url' => array('admin')),
);
?>
    <div id="publisher_left_menu">
        <a href="/publisher/MagazineStock/create" class="add_akz color_th">Добавить акцию</a>
        <ul>
            <?php
            $ids = [];
            foreach ($catsStock as $catStock) {
                $hasCats = MagazineStockHasCat::model()->findAllByAttributes(['cat_id' => $catStock->id])
                ?>
                <ul class="category">
                    <span class="cat_name"><?= $catStock->name ?></span>
                    <?php
                    if (!empty($hasCats)) {
                        foreach ($hasCats as $hasCat) {
                            $stock = MagazineStock::model()->findByPk($hasCat->stock_id);
                            if ($stock != null) {
                                $ids[] = $stock->id;
                                ?>
                                <li style="text-align: center; display: none">
                                    <a href="/publisher/MagazineStock/index/id/<?= $stock->id ?>"
                                       data-id="<?= $stock->id ?>"
                                       class="akz_click color_th_hover css_trans"><span><?= $stock->header_text ?></span><span
                                            class="color_th">x</span></a>
                                </li>
                                <?php
                            } else {
                                ?>
                                <li style="text-align: center; display: none">Акций нет</li>
                                <?php
                            }

                        }
                    } else {
                        ?>
                        <li style="text-align: center; display: none">Акций нет</li>
                        <?php
                    }
                    ?>
                </ul>
                <?php
            }
            if(!empty($ids)){
                $notCats = MagazineStock::model()->findAll(['condition' => 'id not in (' . implode(',', $ids) . ')']);
            }else{
                $notCats = MagazineStock::model()->findAll();
            }
            foreach($notCats as $stock){
                ?>
                <li>
                    <a href="/publisher/MagazineStock/index/id/<?= $stock->id ?>"
                       data-id="<?= $stock->id ?>"
                       class="akz_click color_th_hover css_trans"><span><?= $stock->header_text ?></span><span
                            class="color_th">x</span></a>
                </li>
                <?php
            }
            ?>
        </ul>
    </div>
    <div id="align_zone">
        <p class="publisher_title">Изменить акцию</p>

        <div class="white_fon form_style_l">
            <?php $form = $this->beginWidget('CActiveForm', array(
                'id' => 'magazine-stock-form',
                'enableAjaxValidation' => false,
                'htmlOptions' => array('enctype' => 'multipart/form-data'),
            )); ?>
            <input type="hidden" name="id" value="1">

            <p>Выбрать категорию:</p>
            <?php echo CHtml::dropDownList('name_select', $selected, ['empty' => 'Выбрать категорию'] + CHtml::listData(MagazineStockCat::model()->findAll(), 'id', 'name'), array('class' => 'css_trans')); ?>
            <p style="margin-top: 0.5vw">Создать категорию:</p>
            <?php echo $form->textField(MagazineStockCat::model(), 'name', array('size' => 60, 'maxlength' => 255, 'class' => 'css_trans')); ?>
            <p>Название:</p>
            <?php echo $form->textField($model, 'header_text', array('size' => 60, 'maxlength' => 255, 'class' => 'css_trans')); ?>
            <p>Полное описание акции:</p>
            <?php echo $form->textArea($model, 'text', array('rows' => 6, 'cols' => 50, 'class' => 'css_trans')); ?>
            <p>Акция действует до:</p>
            <?php echo $form->dateField($model, 'end_date'); ?>
            <p>Изображение:</p>

            <div class="img_file_box">
                <div class="img_file_wrap">
                    <div class="img_file_wrap_i">
                        <?php echo $form->fileField($model, 'image', ['class' => 'publiser_file_input']); ?>
                        <p class="bg_color_th"></p>
                    </div>
                </div>
                <div class="img_file_preview" style="display:block;">
                    <img src="<?= $model->image ?>">
                </div>
            </div>
            <?php echo CHtml::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить', ['class' => 'form_submit bg_color_th']); ?>
            <?php $this->endWidget(); ?>
        </div>
        <!-- form -->
    </div>
<?php if (isset($_GET['fine'])) { ?>
    <script>$(function () {
            creat_lb_sv('<div style="height:200px;"><div class="vert_style" style="width:80%"><img style="display:inline_block;" src="/html_source/img/cart_ok.png" width="75px"><br/>Успешно обновлены данные</div><div class="vert_style"></div></div>');
        });</script>
<?php } ?>