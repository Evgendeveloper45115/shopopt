<?php
/* @var $this MagazineCategoriesController */
/* @var $model MagazineCategories */

$this->breadcrumbs=array(
	'Magazine Categories'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List MagazineCategories', 'url'=>array('index')),
	array('label'=>'Create MagazineCategories', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#magazine-categories-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Magazine Categories</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
