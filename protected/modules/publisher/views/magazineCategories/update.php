<?php
/* @var $this MagazineCategoriesController */
/* @var $model MagazineCategories */

$this->breadcrumbs=array(
	'Magazine Categories'=>array('index'),
	$model->title=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List MagazineCategories', 'url'=>array('index')),
	array('label'=>'Create MagazineCategories', 'url'=>array('create')),
	array('label'=>'View MagazineCategories', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage MagazineCategories', 'url'=>array('admin')),
);
?>

<h1>Update MagazineCategories <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>