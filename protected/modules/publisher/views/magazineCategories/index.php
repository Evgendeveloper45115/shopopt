<?php
/* @var $this MagazineCategoriesController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Magazine Categories',
);

$this->menu=array(
	array('label'=>'Create MagazineCategories', 'url'=>array('create')),
	array('label'=>'Manage MagazineCategories', 'url'=>array('admin')),
);
?>

<h1>Magazine Categories</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
