<?php
/* @var $this MagazineCategoriesController */
/* @var $model MagazineCategories */

$this->breadcrumbs=array(
	'Magazine Categories'=>array('index'),
	$model->title,
);

$this->menu=array(
	array('label'=>'List MagazineCategories', 'url'=>array('index')),
	array('label'=>'Create MagazineCategories', 'url'=>array('create')),
	array('label'=>'Update MagazineCategories', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete MagazineCategories', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage MagazineCategories', 'url'=>array('admin')),
);
?>

<h1>View MagazineCategories #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'title',
		'parent_id',
		'site_connect_id',
		'global_parent_id',
		'title_en',
	),
)); ?>
