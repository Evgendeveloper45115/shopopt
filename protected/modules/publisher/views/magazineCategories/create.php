<?php
/* @var $this MagazineCategoriesController */
/* @var $model MagazineCategories */

$this->breadcrumbs=array(
	'Magazine Categories'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List MagazineCategories', 'url'=>array('index')),
	array('label'=>'Manage MagazineCategories', 'url'=>array('admin')),
);
?>

<h1>Create MagazineCategories</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>