<?php
/* @var $this MagazineCategoriesController */
/* @var $data MagazineCategories */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('title')); ?>:</b>
	<?php echo CHtml::encode($data->title); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('parent_id')); ?>:</b>
	<?php echo CHtml::encode($data->parent_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('site_connect_id')); ?>:</b>
	<?php echo CHtml::encode($data->site_connect_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('global_parent_id')); ?>:</b>
	<?php echo CHtml::encode($data->global_parent_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('title_en')); ?>:</b>
	<?php echo CHtml::encode($data->title_en); ?>
	<br />


</div>