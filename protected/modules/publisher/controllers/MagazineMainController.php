<?php

class MagazineMainController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='/layouts/main';


	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new MagazineMain;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['MagazineMain']))
		{
			$model->attributes=$_POST['MagazineMain'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['MagazineMain']))
		{
			$model->attributes=$_POST['MagazineMain'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	public function actionDiscount() {

		$model = MagazineDiscountInfo::model()->find();

		if(isset($_POST['MagazineMain'])) {
			if($model == null) {
				$model = new MagazineDiscountInfo();
				$model->site_connect_id = $this->connectID;
			}
			$model->attributes = $_POST['MagazineMain'];
			$model->save();
		}
		$this->render('discount',array(
			'model'=>$model,
		));
	}
	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('MagazineMain');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{

		$model = MagazineMain::model()->find();
		$save = false;
		$i = 0;

		if(isset($_FILES['main_slaid'])) {
			$notDelete = [];
			foreach($_FILES['main_slaid']['name'] as $key => $ms) {

				$notDelete[] = $key;
			}
//			echo'<pre>';
//			var_dump($_FILES['main_slaid']['name']);
//			echo'</pre>';
//			exit;
			MagazineMainSlider::model()->deleteAll(['condition' => 'id not in ('.implode(',', $notDelete).') and id_main = ' . $model->id]);

		}

		if(isset($_FILES['main_slaid'])) {
			foreach($_FILES['main_slaid']['name'] as $key => $value ) {
				if(trim($_FILES['main_slaid']['name'][$key]) != '') {

					$path = pathinfo($_FILES['main_slaid']['name'][$key],PATHINFO_EXTENSION);
					$name ='/upload/'.uniqid().'.'.$path;

					move_uploaded_file($_FILES['main_slaid']['tmp_name'][$key], Yii::getPathOfAlias('webroot').$name);
					$modelSilder = MagazineMainSlider::model()->findByPk($key);
					if($modelSilder) {
						$modelSilder->image = $name;
						$modelSilder->id_main = $model->id;
						$modelSilder->save();
					} else {
						$modelSilder = new MagazineMainSlider();
						$modelSilder->image = $name;
						$modelSilder->id_main = $model->id;
						$modelSilder->save();
					}
				}
			}
		}

		$modelSilder = MagazineMainSlider::model()->findAll(['condition' => 'id_main = "'.$model->id.'"']);

		if(@$_FILES['MagazineMain']['name']['main_logo'] != '') {
			$model->attributes = $_FILES['MagazineMain'];
			$file = CUploadedFile::getInstance($model,'main_logo');
			$path='/upload/'.uniqid().'.'.$file->getExtensionName();
			$file->saveAs(Yii::getPathOfAlias('webroot').$path);
			$model->main_logo = $path;

			$save = true;
		}

		if(isset($_POST['MagazineMain']['text_main'])) {
			$model->text_main = $_POST['MagazineMain']['text_main'];

			$save = true;
		}

		if(isset($_POST['MagazineMain']['text_popup'])) {
			$model->text_popup = $_POST['MagazineMain']['text_popup'];

			$save = true;
		}

		if($save == true) {
			$model->save();
			$this->redirect('/publisher/magazineMain/admin');
		}

		if(!isset($model)) {
			$model = new MagazineMain();
			$model->site_connect_id = $this->connectID;
			$model->save();
			$this->redirect('/publisher/magazineMain/admin');
		}

		$this->render('admin',array(
			'model'=>$model,
			'modelSilder' => $modelSilder
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return MagazineMain the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=MagazineMain::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param MagazineMain $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='magazine-main-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
