<?php

class MagazineFiltersController extends Controller
{
    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '/layouts/main';


    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreateFilter()
    {
        if (isset($_POST['MagazineFilter'])) {
            $post = $_POST['MagazineFilter'];
            $root = MagazineFilter::model()->findByAttributes(['name' => $post['item']]);
            if (!$root) {
                $root = new MagazineFilter();
            }
            $root->name = $post['item'];
            $root->site_connect_id = $this->connectID;
            $root->saveNode(false);
            if (isset($post['subcat'])) {
                //  $root= MagazineFilter::model()->findByAttributes(['name' => $post['item']]);
                $subCat = new MagazineFilter();
                $subCat->name = $post['subcat'];
                $subCat->site_connect_id = $this->connectID;
                $subCat->appendTo($root, false);
            }
        }

        $this->render('create', array(
            'model' => new MagazineFilter(),
        ));
    }

    public function actionCreateTag()
    {
        if (isset($_POST['MagazineFilter'])) {
            $post = $_POST['MagazineFilter'];
            $root = MagazineFilter::model()->findByPk($post['item']);
            if ($root) {
                if (isset($post['subcat'])) {
                    $subCat = new MagazineFilter();
                    $subCat->name = $post['subcat'];
                    $subCat->site_connect_id = $this->connectID;
                    $subCat->appendTo($root, false);
                    $this->redirect('admin');
                }
            }
        }

        $this->render('createTag', array(
            'model' => new MagazineFilter(),
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['MagazineFilter'])) {
            $model->attributes = $_POST['MagazineFilter'];
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->id));
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    /**
     * Lists all models.
     */
    public function actionIndex()
    {
        $dataProvider = new CActiveDataProvider('MagazineFilter');
        $this->render('index', array(
            'dataProvider' => $dataProvider,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin()
    {
        /**
         * @var $magFil NestedSetBehavior
         */
        $magFil = MagazineFilter::model()->treedata();
        $this->render('admin', ['roots' => $magFil]);
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return MagazineFilter the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model = MagazineFilter::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param MagazineFilter $model the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'magazine-about-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }
}
