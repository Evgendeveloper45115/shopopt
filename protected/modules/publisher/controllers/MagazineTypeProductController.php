<?php

class MagazineTypeProductController extends Controller
{
    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '/layouts/main';

    /**
     * @return array action filters
     */

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate()
    {
        $model = new MagazineTypeProduct;

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['MagazineTypeProduct'])) {
            $_POST['MagazineTypeProduct']['site_connect_id'] = $this->connectID;
            $model->attributes = $_POST['MagazineTypeProduct'];
            if ($model->save())
                $this->redirect('/publisher/MagazineTypeProduct/admin');
        }

        $this->render('create', array(
            'model' => $model,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['MagazineTypeProduct'])) {
            $_POST['MagazineTypeProduct']['site_connect_id'] = $this->connectID;
            $model->attributes = $_POST['MagazineTypeProduct'];
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->id));
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    /**
     * Lists all models.
     */
    public function actionIndex()
    {
        $dataProvider = new CActiveDataProvider('MagazineTypeProduct');
        $this->render('index', array(
            'dataProvider' => $dataProvider,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin()
    {

        if (isset($_POST['name'])) {
            $ids = [];
            foreach ($_POST['name'] as $key => $value) {

                $ids[] = $key;
                $model = MagazineTypeProduct::model()->findByAttributes(['id' => $key, 'site_connect_id' => $this->connectID]);
                if ($model == null) {
                    $model = new MagazineTypeProduct;
                    $model->site_connect_id = $this->connectID;
                }
                $model->name = $value;
                $model->save();
            }
            MagazineTypeProduct::model()->deleteAll(['condition' => ' id not in ("' . implode('","', $ids) . '") and site_connect_id = "' . $this->connectID . '"']);
        }

        if (isset($_POST['type'])) {
            foreach ($_POST['type'] as $type) {
                $model = new MagazineTypeProduct();
                $model->name = $type;
                $model->site_connect_id = $this->connectID;
                $model->save(false);
            }
            $this->redirect('/publisher/MagazineTypeProduct/admin');
        }
        $model = MagazineTypeProduct::model()->findAll();

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return MagazineTypeProduct the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model = MagazineTypeProduct::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param MagazineTypeProduct $model the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'magazine-type-product-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }
}
