<?php

class MagazineStockController extends Controller
{
    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '/layouts/main';

    /**
     * @return array action filters
     */
//	public function filters()
//	{
//		return array(
//			'accessControl', // perform access control for CRUD operations
//			'postOnly + delete', // we only allow deletion via POST request
//		);
//	}

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
//	public function accessRules()
//	{
//		return array(
//			array('allow',  // allow all users to perform 'index' and 'view' actions
//				'actions'=>array('index','view'),
//				'users'=>array('*'),
//			),
//			array('allow', // allow authenticated user to perform 'create' and 'update' actions
//				'actions'=>array('create','update'),
//				'users'=>array('@'),
//			),
//			array('allow', // allow admin user to perform 'admin' and 'delete' actions
//				'actions'=>array('admin','delete'),
//				'users'=>array('admin'),
//			),
//			array('deny',  // deny all users
//				'users'=>array('*'),
//			),
//		);
//	}

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate()
    {
        $model = new MagazineStock;
        $allModels = MagazineStock::model()->findAll();
        $catsStock = MagazineStockCat::model()->findAll();

        $MagazineStockCat = null;
//		var_dump($allModels);
        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);
        if (isset($_POST['MagazineStock'])) {
            $_POST['MagazineStock']['site_connect_id'] = $this->connectID;
            $model->attributes = $_POST['MagazineStock'];
            $model->image = CUploadedFile::getInstance($model, 'image');

            if ($_FILES['MagazineStock']['name']['image'] != '') {

                $path = '/upload/' . uniqid() . '.' . $model->image->getExtensionName();
                $model->image->saveAs(Yii::getPathOfAlias('webroot') . $path);
                $model->image = $path;

            }
            if ($model->save()) {
                if ($_POST['name_select'] == 'empty') {
                    $MagazineStockCat = new MagazineStockCat();
                    if (!empty($_POST['MagazineStockCat'])) {
                        $MagazineStockCat->name = $_POST['MagazineStockCat']['name'];
                        $MagazineStockCat->site_connect_id = $this->connectID;
                        $MagazineStockCat->save(false);
                    }
                    $MagazineStockHasCat = new MagazineStockHasCat();
                    $MagazineStockHasCat->cat_id = $MagazineStockCat->id;
                    $MagazineStockHasCat->stock_id = $model->id;
                    $MagazineStockHasCat->site_connect_id = $MagazineStockCat->site_connect_id;
                    $MagazineStockHasCat->save(false);
                } else {
                    $MagazineStockHasCat = new MagazineStockHasCat();
                    $MagazineStockHasCat->cat_id = $_POST['name_select'];
                    $MagazineStockHasCat->stock_id = $model->id;
                    $MagazineStockHasCat->site_connect_id = $this->connectID;
                    $MagazineStockHasCat->save(false);
                }

                $this->redirect('/publisher/MagazineStock/index/id/' . $model->id . '?fine');
            }
        }

        $this->render('create', array(
            'model' => $model,
            'allModels' => $allModels,
            'catsStock' => $catsStock,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['MagazineStock'])) {
            $_POST['MagazineStock']['site_connect_id'] = $this->connectID;
            $model->attributes = $_POST['MagazineStock'];
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->id));
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    /**
     * Lists all models.
     */
    public function actionIndex($id)
    {


        $model = MagazineStock::model()->findByPk($id);
        $selected = null;
        if ($model) {
            $MagazineStockHasCat = MagazineStockHasCat::model()->findByAttributes(['stock_id' => $model->id]);
            if ($MagazineStockHasCat) {
                $MagazineStockCat = MagazineStockCat::model()->findByPk($MagazineStockHasCat->cat_id);
                if ($MagazineStockCat) {
                    $selected = $MagazineStockCat->id;
                }
            }
        }

        if (isset($_POST['MagazineStock'])) {
            $model->attributes = $_POST['MagazineStock'];
            $model->image = CUploadedFile::getInstance($model, 'image');
            if ($_POST['name_select'] == 'empty') {
                $MagazineStockCat = new MagazineStockCat();
                if (!empty($_POST['MagazineStockCat'])) {
                    $MagazineStockCat->name = $_POST['MagazineStockCat']['name'];
                    $MagazineStockCat->site_connect_id = $this->connectID;
                    $MagazineStockCat->save(false);
                }
                $MagazineStockHasCat = MagazineStockHasCat::model()->findByAttributes(['stock_id' => $id]);
                if (!$MagazineStockHasCat) {
                    $MagazineStockHasCat = new MagazineStockHasCat();
                }
                $MagazineStockHasCat->cat_id = $MagazineStockCat->id;
                $MagazineStockHasCat->stock_id = $model->id;
                $MagazineStockHasCat->site_connect_id = $MagazineStockCat->site_connect_id;
                $MagazineStockHasCat->save(false);
            } else {
                $MagazineStockHasCat = MagazineStockHasCat::model()->findByAttributes(['stock_id' => $id]);
                if (!$MagazineStockHasCat) {
                    $MagazineStockHasCat = new MagazineStockHasCat();
                }
                $MagazineStockHasCat->cat_id = $_POST['name_select'];
                $MagazineStockHasCat->stock_id = $model->id;
                $MagazineStockHasCat->site_connect_id = $this->connectID;
                $MagazineStockHasCat->save(false);
            }

            if ($_FILES['MagazineStock']['name']['image'] != '') {

                $path = '/upload/' . uniqid() . '.' . $model->image->getExtensionName();
                $model->image->saveAs(Yii::getPathOfAlias('webroot') . $path);
                $model->image = $path;

            }
            $model->save();
            $this->redirect('/publisher/MagazineStock/index/id/' . $id . '?fine');
        }
        $allModels = MagazineStock::model()->findAll();
        $catsStock = MagazineStockCat::model()->findAll();
        $this->render('index', array(
            'model' => $model,
            'allModels' => $allModels,
            'selected' => $selected,
            'catsStock' => $catsStock
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin()
    {
        $model = MagazineStock::model()->findAll();

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return MagazineStock the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model = MagazineStock::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param MagazineStock $model the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'magazine-stock-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }
}
