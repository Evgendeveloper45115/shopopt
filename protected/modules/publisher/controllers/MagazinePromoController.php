<?php

class MagazinePromoController extends Controller
{
    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '/layouts/main';

    /**
     * @return array action filters
     */
//	public function filters()
//	{
//		return array(
//			'accessControl', // perform access control for CRUD operations
//			'postOnly + delete', // we only allow deletion via POST request
//		);
//	}

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
//	public function accessRules()
//	{
//		return array(
//			array('allow',  // allow all users to perform 'index' and 'view' actions
//				'actions'=>array('index','view'),
//				'users'=>array('*'),
//			),
//			array('allow', // allow authenticated user to perform 'create' and 'update' actions
//				'actions'=>array('create','update'),
//				'users'=>array('@'),
//			),
//			array('allow', // allow admin user to perform 'admin' and 'delete' actions
//				'actions'=>array('admin','delete'),
//				'users'=>array('admin'),
//			),
//			array('deny',  // deny all users
//				'users'=>array('*'),
//			),
//		);
//	}

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate()
    {
        $model = new MagazinePromoFiles();
        $allModels = MagazinePromoFiles::model()->findAll();
        $catsPromo = MagazinePromoCat::model()->findAll();

        if (isset($_FILES['MagazinePromoFiles'])) {
            $MagazineStockCat = null;

            $model = new MagazinePromoFiles();
            $model->file = CUploadedFile::getInstance($model, 'url');
            if($model->file){
                $model->file_size = $model->file->getSize();
            }
            $model->date_create = time();
            $model->status = 0;
            $model->name_file = $model->file->getName();

            $model->site_connect_id = $this->connectID;
            $model->extension = $model->file->getExtensionName();
            $path = '/upload/' . uniqid() . '.' . $model->file->getExtensionName();
            $model->file->saveAs(Yii::getPathOfAlias('webroot') . $path);
            $model->url = $path;
            $model->name_file = CUploadedFile::getInstance($model, 'url');

            if ($_FILES['MagazineStock']['name']['image'] != '') {

                $path = '/upload/' . uniqid() . '.' . $model->name_file->getExtensionName();
                $model->name_file->saveAs(Yii::getPathOfAlias('webroot') . $path);
                $model->name_file = $path;

            }
            if ($model->save()) {
                if ($_POST['name_select'] == 'empty') {
                    $MagazineStockCat = new MagazinePromoCat();
                    if (!empty($_POST['MagazinePromoCat'])) {
                        $MagazineStockCat->name = $_POST['MagazinePromoCat']['name'];
                        $MagazineStockCat->site_connect_id = $this->connectID;
                        $MagazineStockCat->save(false);
                    }
                    $MagazineStockHasCat = new MagazinePromoHasCat();
                    $MagazineStockHasCat->cat_id = $MagazineStockCat->id;
                    $MagazineStockHasCat->files_id = $model->id;
                    $MagazineStockHasCat->site_connect_id = $this->connectID;
                    $MagazineStockHasCat->save(false);
                } else {
                    $MagazineStockHasCat = new MagazinePromoHasCat();
                    $MagazineStockHasCat->cat_id = $_POST['name_select'];
                    $MagazineStockHasCat->files_id = $model->id;
                    $MagazineStockHasCat->site_connect_id = $this->connectID;
                    $MagazineStockHasCat->save(false);
                }
                $this->refresh();
            }
        }

        $this->render('create', array(
            'model' => $model,
            'allModels' => $allModels,
            'catsPromo' => $catsPromo,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['MagazineStock'])) {
            $_POST['MagazineStock']['site_connect_id'] = $this->connectID;
            $model->attributes = $_POST['MagazineStock'];
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->id));
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    /**
     * Lists all models.
     */
    public function actionIndex($id)
    {


        $model = MagazinePromoFiles::model()->findByPk($id);

        if (isset($_POST['MagazineStock'])) {
            $model->attributes = $_POST['MagazineStock'];
            $model->image = CUploadedFile::getInstance($model, 'image');

            if ($_FILES['MagazineStock']['name']['image'] != '') {

                $path = '/upload/' . uniqid() . '.' . $model->image->getExtensionName();
                $model->image->saveAs(Yii::getPathOfAlias('webroot') . $path);
                $model->image = $path;

            }
//			$model->attributes = $_POST['MagazineStock'];
            $model->save();
            $this->redirect('/publisher/MagazineStock/index/id/' . $id);
        }
        $allModels = MagazinePromoFiles::model()->findAll();
        $this->render('index', array(
            'model' => $model,
            'allModels' => $allModels,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin()
    {
        $model = MagazinePromoFiles::model()->findAll();

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return MagazineStock the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model = MagazinePromoFiles::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param MagazineStock $model the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'magazine-stock-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }
}
