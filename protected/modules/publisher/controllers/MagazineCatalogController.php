<?php

class MagazineCatalogController extends Controller
{
    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '/layouts/main';

    /**
     * @return array action filters
     */
//	public function filters()
//	{
//		return array(
//			'accessControl', // perform access control for CRUD operations
//			'postOnly + delete', // we only allow deletion via POST request
//		);
//	}
    public function actionAjax()
    {
        //$session = Yii::app()->session;Yii::app()->;
        $session = Yii::app()->session;
        $_SESSION['tree'] = $_POST['tree'];
        session_write_close();
    }
    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
//	public function accessRules()
//	{
//		return array(
//			array('allow',  // allow all users to perform 'index' and 'view' actions
//				'actions'=>array('index','view'),
//				'users'=>array('*'),
//			),
//			array('allow', // allow authenticated user to perform 'create' and 'update' actions
//				'actions'=>array('create','update'),
//				'users'=>array('@'),
//			),
//			array('allow', // allow admin user to perform 'admin' and 'delete' actions
//				'actions'=>array('admin','delete'),
//				'users'=>array('admin'),
//			),
//			array('deny',  // deny all users
//				'users'=>array('*'),
//			),
//		);
//	}

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate()
    {

        $model = new MagazineProduct;
        $modelCategoryId = MagazineCategories::model()->findAll();
        $modelCategoryType = MagazineProduct::model()->findAll();
        $modelCategoryType = CHtml::listData($modelCategoryType, 'id', 'name');
        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);
        $list = CHtml::listData($modelCategoryId, 'id', 'title');
        $activeModelRecommed = MagazineRecommendProduction::model()->findAll();
        $modelRecommend = CHtml::listData(MagazineProduct::model()->findAll(), 'id', 'name_product');

        if (isset($_POST['MagazineProduct'])) {
            $connectID = SiteConnect::model()->findByAttributes(['url' => $_SERVER['SERVER_NAME']]);
            $_POST['MagazineProduct']['site_connect_id'] = $connectID->id;
            $model->attributes = $_POST['MagazineProduct'];
            $model->image = CUploadedFile::getInstance($model, 'image_product');

            if ($_FILES['MagazineProduct']['name']['image_product'] != '') {

                $path = '/upload/' . uniqid() . '.' . $model->image->getExtensionName();
                $model->image->saveAs(Yii::getPathOfAlias('webroot') . $path);
                $model->image_product = $path;

            }

            if ($model->save()) {

                $connectID = SiteConnect::model()->findByAttributes(['url' => $_SERVER['SERVER_NAME']]);
                if (isset($_POST['MagazineRecommendProduction'])) {
                    foreach ($_POST['MagazineRecommendProduction'] as $item) {
                        $modelRecom = new MagazineRecommendProduction();
                        $modelRecom->product_id = $model->id;
                        $modelRecom->recommend_id = $item;
                        $modelRecom->site_connect_id = $connectID->id;
                        $modelRecom->save();
                    }
                }
                $this->redirect(array('view', 'id' => $model->id));
            }
        }

        $this->render('create', array(
            'model' => $model,
            'activeModelRecommed' => $activeModelRecommed,
            'modelCategoryType' => $modelCategoryType,
            'modelRecommend' => $modelRecommend,
            'list' => $list
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id);
        $modelCategoryId = MagazineCategories::model()->findAll();
        $modelCategoryType = MagazineTypeProduct::model()->findAll();
        $modelCategoryType = CHtml::listData($modelCategoryType, 'id', 'name');
        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);
        $activeModelRecommed = MagazineRecommendProduction::model()->findAll();
        $modelRecommend = CHtml::listData(MagazineProduct::model()->findAll(), 'id', 'name_product');
        $list = CHtml::listData($modelCategoryId, 'id', 'title');
        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);
        $connectID = SiteConnect::model()->findByAttributes(['url' => $_SERVER['SERVER_NAME']]);
        if (isset($_POST['MagazineRecommendProduction'])) {
            foreach ($_POST['MagazineRecommendProduction'] as $item) {
                $modelRecom = new MagazineRecommendProduction();
                $modelRecom->product_id = $id;
                $modelRecom->recommend_id = $item;
                $modelRecom->site_connect_id = $connectID->id;
                $modelRecom->save();
            }
        }


        if (isset($_POST['MagazineProduct'])) {
            $connectID = SiteConnect::model()->findByAttributes(['url' => $_SERVER['SERVER_NAME']]);
            $_POST['MagazineProduct']['site_connect_id'] = $connectID->id;
            $model->attributes = $_POST['MagazineProduct'];
            $model->image = CUploadedFile::getInstance($model, 'image_product');

            if ($_FILES['MagazineProduct']['name']['image_product'] != '') {
                $path = '/upload/' . uniqid() . '.' . $model->image->getExtensionName();
                $model->image->saveAs(Yii::getPathOfAlias('webroot') . $path);
                $model->image_product = $path;
            }

            if ($model->save()) {
                $this->redirect(array('view', 'id' => $model->id));
            }

        }


        $this->render('update', array(
            'model' => $model,
            'modelCategoryType' => $modelCategoryType,
            'activeModelRecommed' => $activeModelRecommed,
            'modelRecommend' => $modelRecommend,
            'list' => $list,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    /**
     * Lists all models.
     */
    public function actionIndex()
    {
        $dataProvider = new CActiveDataProvider('MagazineProduct');
        $this->render('index', array(
            'dataProvider' => $dataProvider,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin()
    {
        /**
         * @var MagazineProduct $getProductName
         */
        $getProductName = null;
        $session = Yii::app()->session;
        if (isset($_POST['type_action']) && $_POST['type_action'] == 'clone') {

            $getProductName = MagazineProduct::model()->findByPk($_POST['id_el']);
            $newProduct2 = new MagazineProduct();
            $newProduct2->attributes = $getProductName->attributes;
            $newProduct2->publish = 1;
            $newProduct2->save();

            $parentss = MagazineProduct::model()->findAll(['condition' => 'catalog_id = "' . $getProductName->id . '" or parent_id = ' . $getProductName->id]);

            foreach ($parentss as $pts) {
                $newProduct = new MagazineProduct();
                $newProduct->attributes = $pts->attributes;
                $newProduct->publish = 1;
                $newProduct->catalog_id = $newProduct2->id;
                $newProduct->parent_id = $newProduct2->id;
                $newProduct->save();
            }

            $this->redirect('/publisher/MagazineCatalog/admin');
            exit;
//			}
        }

        if (isset($_POST['type_action']) && $_POST['type_action'] == 'move_el') {
            $getProductName = MagazineProduct::model()->findByPk($_POST['id_el']);
            $getProductName->catalog_id = $_POST['id_new_parent'];
            $getProductName->parent_id = $_POST['id_new_parent'];
            $getProductName->save();
            $this->redirect('/publisher/MagazineCatalog/admin');
        }

        if (isset($_GET['type']) && $_GET['type'] == 'unpub_element') {
            $getProductName = MagazineProduct::model()->findByPk($_GET['id']);
            $getProductName->publish = 1;
            $getProductName->save();
            $this->redirect('/publisher/MagazineCatalog/admin');
        }

        if (isset($_GET['type']) && $_GET['type'] == 'pub_element') {
            $getProductName = MagazineProduct::model()->findByPk($_GET['id']);
            $getProductName->publish = 0;
            $getProductName->save();
            $this->redirect('/publisher/MagazineCatalog/admin');
        }

        if (isset($_GET['type']) && $_GET['type'] == 'child_add' && isset($_POST['folder_name'])) {

            $getProductName = new MagazineProduct();
            $_POST['MagazineProduct']['site_connect_id'] = $this->connectID;
            $_POST['MagazineProduct']['catalog_id'] = $_GET['id'];
            $_POST['MagazineProduct']['parent_id'] = $_GET['id'];
            $_POST['MagazineProduct']['name_product'] = $_POST['folder_name'];
            $_POST['MagazineProduct']['title'] = $_POST['folder_name'];
            $_POST['MagazineProduct']['title_en'] = $_POST['folder_name'];
            $_POST['MagazineProduct']['type'] = 1;
            $_POST['MagazineProduct']['type_product'] = 0;
            $getProductName->attributes = $_POST['MagazineProduct'];
            $getProductName->save();

            if (isset($_FILES['MagazineProduct']['name'])) {
                array_pop($_FILES['MagazineProduct']['name']);
                $json = $this->loadImage($_FILES['MagazineProduct']['name']);
                $getProductName->image_product = $json;
            }
            if (isset($_POST['recommend_ids'])) {
                MagazineRecommendProduction::model()->deleteAll(['condition' => 'product_id = "' . $_GET['id'] . '"']);
                $types = explode(',', $_POST['recommend_ids']);
                foreach ($types as $tp) {

                    $product = new MagazineRecommendProduction();
                    $product->product_id = $_GET['id'];
                    $product->site_connect_id = $this->connectID;
                    $product->recommend_id = $tp;
                    $product->save();
                }
            }
            $getProductName->save();
            $this->redirect('/publisher/MagazineCatalog/admin');
        }

        if (isset($_GET['type']) && $_GET['type'] == 'edit_element') {
            $id = $_GET['id'];
            $typeProduct = null;
            $getProductName = MagazineProduct::model()->findByPk($id);
            if (!empty($_POST)) {
                $dopFields = [];
                $checks = [];
                foreach ($_POST as $key => $value) {
                    if (is_string($key) && stristr($key, 'duplicate') && !stristr($key, 'check') ) {
                        $dopFields[$key] = $value;
                    }
                }
                if (isset($_POST['MagazineProduct'])) {
                    foreach ($_POST['MagazineProduct'] as $key => $value) {
                        if (is_int($key)) {
                            if ($_FILES['MagazineProduct']['name'][$key] == '') {
                                $_FILES['MagazineProduct']['name'][$key] = $value;
                            }
                        }
                    }
                    foreach ($_POST['MagazineProduct'] as $attributeName => $attributeVal) {
                        if (stristr($attributeName, 'check')) {
                            $attribute = str_replace('-check', '', $attributeName);
                            $checks[$attribute] = $attribute;
                        }
                    }
                    foreach ($_POST['MagazineProduct'] as $attributeName => $attributeVal) {
                        if (!stristr($attributeName, 'check') && !in_array($attributeName, $checks)) {
                            $checks[$attributeName] = 'off';
                        }
                    }
                }
                if (!empty($_POST)) {
                    foreach ($_POST as $attributeName => $attributeVal) {
                        if (!is_array($attributeVal)) {
                            if (!stristr($attributeName, 'check') && !in_array($attributeName, $checks) && stristr($attributeName, 'duplicate_value') || stristr($attributeName, 'recommend_ids')) {
                                $checks[$attributeName] = 'off';
                            }
                        }
                    }
                    foreach ($_POST as $attributeName => $attributeVal) {
                        if (!is_array($attributeVal)) {
                            if (stristr($attributeName, 'check') && !in_array($attributeName, $checks)) {
                                $attribute = str_replace('-check', '', $attributeName);
                                $checks[$attribute] = $attribute;
                            }
                        }
                    }
                }
                if(isset($checks['recommend_ids-check'])){
                    unset($checks['recommend_ids-check']);
                }
                $getProductName->is_active = json_encode($checks);
                $getProductName->dop_fields = json_encode($dopFields);
                if (isset($_FILES['MagazineProduct']['name'])) {
                    array_pop($_FILES['MagazineProduct']['name']);
                    $json = $this->loadImage($_FILES['MagazineProduct']['name']);
                    $getProductName->image_product = $json;
                }else{
                    $json = $this->loadImage([]);
                    $getProductName->image_product = $json;
                }
            }

            if (isset($_POST['MagazineProduct']) || isset($_POST['folder_name'])) {
                if (isset($_POST['recommend_ids'])) {
                    MagazineRecommendProduction::model()->deleteAll(['condition' => 'product_id = "' . $id . '"']);
                    $types = explode(',', $_POST['recommend_ids']);
                    foreach ($types as $tp) {

                        $product = new MagazineRecommendProduction();
                        $product->product_id = $id;
                        $product->site_connect_id = $this->connectID;
                        $product->recommend_id = $tp;
                        $product->save();
                    }
                }

                if (isset($_POST['MagazineProduct']['name_product'])) {

                } else {
                    $_POST['MagazineProduct']['name_product'] = $_POST['folder_name'];
                    $_POST['MagazineProduct']['type_product'] = 0;
                }

                $_POST['MagazineProduct']['title_en'] = $_POST['MagazineProduct']['name_product'];
                $_POST['MagazineProduct']['title'] = $_POST['MagazineProduct']['name_product'];
                $getProductName->attributes = $_POST['MagazineProduct'];
                if(isset($_POST['MagazineProduct']['type_product'])){
                    $typeProduct = MagazineTypeProduct::model()->findByPk($_POST['MagazineProduct']['type_product']);
                    if(!$typeProduct){
                        $typeProduct = MagazineTypeProduct::model()->findByAttributes(['name' => $_POST['MagazineProduct']['type_product']]);
                    }
                }
                $getProductName->type_product = $typeProduct->id;

//				var_dump($_POST);

                $getProductName->save();
//				var_dump($getProductName->getErrors());
//				exit;
            }
        }

        if (isset($_GET['type']) && $_GET['type'] == 'del_element') {
            $id = $_GET['id'];
            $getProductName = MagazineProduct::model()->deleteByPk($id);
            MagazineRecommedClientTovar::model()->deleteAllByAttributes(['id_tovar' => $id]);
            $this->redirect('/publisher/MagazineCatalog/admin');
        }

        if (isset($_GET['type']) && $_GET['type'] == 'child_add') {
            $id = $_GET['id'];

            if (isset($_POST['MagazineProduct'])) {
                $getProductName = new MagazineProduct();
                $_POST['MagazineProduct']['site_connect_id'] = $this->connectID;
                $_POST['MagazineProduct']['catalog_id'] = $id;
                $_POST['MagazineProduct']['parent_id'] = $id;
                $_POST['MagazineProduct']['title'] = $_POST['MagazineProduct']['name_product'];
                $_POST['MagazineProduct']['title_en'] = $_POST['MagazineProduct']['name_product'];
                $getProductName->attributes = $_POST['MagazineProduct'];
                $getProductName->save();

                if (isset($_FILES['MagazineProduct']['name'])) {
                    array_pop($_FILES['MagazineProduct']['name']);
                    $json = $this->loadImage($_FILES['MagazineProduct']['name']);
                    $getProductName->image_product = $json;
                }

                $getProductName->save();

                $this->redirect($_SERVER['REQUEST_URI']);
            }
        }

        $recommendIds = MagazineRecommendProduction::model()->findAll(['condition' => 'product_id = "' . $id . '"']);
        $idsRec = [];

        $getTypeProduct = MagazineTypeProduct::model()->findAll();
        $getTypeProduct = CHtml::listData($getTypeProduct, 'id', 'name');

        foreach ($recommendIds as $recId) {
            $idsRec[] = $recId->recommend_id;
        }

        $recommendIds = implode(',', $idsRec);

        $Categories = Yii::app()->db->createCommand('SELECT * FROM magazine_product WHERE site_connect_id = "' . $this->connectID . '" ORDER BY parent_id ASC')->queryAll();
        $menuItems = array();
        foreach ($Categories as $value) {
            $menuItems[$value['id']] = $value;
        }
        $Categories = $this->mapTreeMy($menuItems);

        $this->render('admin', array(
            'Categories' => $Categories,
            'getProductName' => $getProductName,
            'getTypeProduct' => $getTypeProduct,
            'recommendIds' => $recommendIds
        ));
    }
    public function loadImage($imgArray)
    {

        if (!empty($imgArray)) {
            $imagesArr = [];
            $images = CUploadedFile::getInstancesByName('MagazineProduct');
           // CVarDumper::dump($imgArray,11,1);
           // CVarDumper::dump($_POST,11,1);exit;

            foreach ($images as $imgName) {
                if (in_array($imgName->name, $imgArray)) {
                    unset($imgArray[array_search($imgName->name, $imgArray)]);
                }
                $path = '/upload/' . uniqid() . '.' . $imgName->getExtensionName();
                $imgName->saveAs(Yii::getPathOfAlias('webroot') . $path);
                $imagesArr[] = $path;
            }
            foreach($imgArray as $key => $img){
                if($img == ''){
                    unset($imgArray[$key]);
                }
            }
            $arr = array_merge($imgArray, $imagesArr);

            return json_encode($arr);

        }
    }

    public function mapTreeMy($dataset)
    {

        $tree = array();

        foreach ($dataset as $id => &$node) {
            if (!$node['parent_id']) {
                $tree[$id] = &$node;
            } else {
                $dataset[$node['parent_id']]['childs'][$id] = &$node;
            }

        }
        return $tree;

    }

    static function  view_cat($dataset)
    {

        foreach ($dataset as $menu) {
//			var_dump($menu);
//			href=""   ><span>Каталог 1.1</span>
            echo '<li class="' . ($menu['publish'] == '0' ? '' : 'hiddenFolder') . '"><a href="?type=edit_element&id=' . $menu["id"] . '" data-id="' . $menu["id"] . '" class="color_th_hover ' . ($menu['publish'] == '0' ? '' : 'unpub') . '">' . $menu["title"] . '<small>(' . $menu["id"] . ')</small></a>';
            if (isset($menu['childs'])) {
                echo '<ul >';
                self::view_cat($menu['childs']);
                echo '</ul>';
            }
            echo '</li>';
        }
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return MagazineProduct the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model = MagazineProduct::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param MagazineProduct $model the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'magazine-product-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }
}
