<?php

class MagazineDocumentsController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='/layouts/main';

	/**
	 * @return array action filters
	 */

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new MagazineDocuments;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['MagazineDocuments']))
		{
			$model->attributes=$_POST['MagazineDocuments'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['MagazineDocuments']))
		{
			$model->attributes=$_POST['MagazineDocuments'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex($id)
	{
		if(isset($_FILES['MagazineDocuments']['name'])) {
			$model = new MagazineDocuments();
			$model->file = CUploadedFile::getInstance($model,'url');
			$model->size_file = $model->file->getSize();
			$model->order_id = $id;
			$model->date_add = date('Y-m-d');
			$model->status = 0;
			$model->name = $model->file->getName();
			$model->extension = $model->file->getExtensionName();
			$path='/upload/'.uniqid().'.'.$model->file->getExtensionName();
			$model->file->saveAs(Yii::getPathOfAlias('webroot').$path);
			$model->url = $path;
			$magazineOrder = MagazineOrder::model()->findByAttributes(['id' => $model->order_id]);
			$manager = null;
			$clientId = yii::app()->user->id;
			$client = MagazineUsers::model()->find(['condition' => 'id = ' . $clientId]);
			$text = 'Пользователь: ' . $client->user_email . ' Сделал изменения в заказе №:' . $id . '<br>'.
			' Добавил документ - ' . $this->createAbsoluteUrl($model->url) . '<br>';

			if ($magazineOrder) {
				$manager = MagazineUsers::model()->find(['condition' => 'id = ' . $magazineOrder->manager_id]);
			}
			if ($manager != null) {
				$name = '=?UTF-8?B?' . base64_encode('Документооборот по заказу №'.$id) . '?=';
				$subject = '=?UTF-8?B?' . base64_encode('Документооборот по заказу №'.$id) . '?=';
				$headers = "From: $name <{$_SERVER['SERVER_NAME']}>\r\n" .
					"Reply-To: {$manager->user_email}\r\n" .
					"MIME-Version: 1.0\r\n" .
					"Content-Type: text/html; charset=UTF-8";

				mail(
					$manager->user_email,
					$subject,
					$text,
					$headers
				);
			}

			$model->save();


			$this->redirect('/client/MagazineDocuments/index/id/' .$id);
		}
		$model = MagazineDocuments::model()->findAll(['condition' => 'order_id = "'.$id.'"']);
		$order = MagazineOrder::model()->find($id);

		$this->render('index',array(
			'model'=>$model,
			'order'=>$order,
			'id' => $id
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$ids = [];
		$ordersClient = MagazineOrder::model()->findAll(['condition' => 'client_id = "'.Yii::app()->user->id.'"']);
		foreach ($ordersClient as $oC) {
			$ids[] = $oC->id;
		}

		$ids = implode('","', $ids);

		$model = MagazineDocuments::model()->findAll(['condition' => 'order_id in ("'.$ids.'")']);
//		$order = MagazineOrder::model()->find($id);

		$this->render('admin',array(
			'model'=>$model,
//			'order'=>$order,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return MagazineDocuments the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=MagazineDocuments::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param MagazineDocuments $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='magazine-documents-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
