<?php

class DefaultController extends Controller
{
    public $layout = '/layouts/main';

    public function actionIndex()
    {
        $session = Yii::app()->session;
        $ids = [];
        if (isset($session['items'])) {
            foreach ($session['items'] as $key) {
                if ($key['id'] != 'undefined') {
                    $ids[] = $key['id'];
                }
            }

            $this->cart_count = 0;
            $this->cart_price = 0;

            $ids = implode(',', $ids);

            if (!empty($ids)) {

                $cart = MagazineProduct::model()->findAll(['condition' => 'id in (' . $ids . ')']);
                foreach ($cart as $cr) {
                    foreach ($session['items'] as $key => $item) {
                        if ($item['id'] == $cr->id) {
                            $this->cart_count += $item['count'];
                            $this->cart_price += $item['count'] * $cr->price;
                        }
                    }
                }
            }
        }

        $getClientOrders = MagazineOrder::model()->findAll(['condition' => 'client_id = "' . Yii::app()->user->id . '"']);
        $orderIds = [];
        foreach ($getClientOrders as $order) {
            $orderIds[] = $order->id;
        }
        $orderDocuments = MagazineDocuments::model()->findAll(['condition' => 'order_id in ("' . implode('","', $orderIds) . '")', 'limit' => '5']);

        $orderHistory = MagazineOrder::model()->findAll(['condition' => 'client_id = "' . Yii::app()->user->id . '" and status = "' . MagazineOrder::STATUS_CLOSE . '"', 'limit' => '5']);
        $orderWork = MagazineOrder::model()->findAll(['condition' => 'client_id = "' . Yii::app()->user->id . '" and (status = "' . MagazineOrder::STATUS_COLLECTED . '" or status = "' . MagazineOrder::STATUS_SHIPPED . '")', 'limit' => '5']);

        foreach ($orderWork as &$oW) {
            @$oW->date_m = date('n', strtotime($oW->time_order));
            @$oW->date_d = date('d', strtotime($oW->time_order));
            @$oW->date_y = date('y', strtotime($oW->time_order));
            $oW->magazine_address = MagazineClientsAddress::model()->findByPk($oW->magazine_shop_id);
        }

        $getUserCompany = MagazineCompany::model()->findAll(['condition' => 'client_id = "' . Yii::app()->user->id . '"']);

        $ids = [];
        foreach ($getUserCompany as $getUserComp) {
            $ids[] = $getUserComp->id;
        }
        $getRec = MagazineRecommedClientTovar::model()->findAll(['condition' => 'id_company in ("' . implode(',', $ids) . '") ']);

        $getPromoids = MagazinePromoRelations::model()->findAll(['condition' => 'id_company in ("' . implode(',', $ids) . '")']);
        $idsInfo = [];
        foreach ($getPromoids as $dt) {
            $idsInfo[] = $dt->id_files;
        }

        $filesPromo = MagazinePromoFiles::model()->findAll(['condition' => 'id in ("' . implode('","', $idsInfo) . '")']);

//		var_dump($getPromoids);
        $idss = [];
        foreach ($getRec as $gr) {
            $idss[] = $gr->id_tovar;
        }

        $getProduct = MagazineProduct::model()->findAll(['condition' => 'id in ("' . implode('","', $idss) . '")']);

        $this->render('index', [
            'orderDocuments' => $orderDocuments,
            'filesPromo' => $filesPromo,
            'idss' => $idss,
            'getProduct' => $getProduct,
            'orderHistory' => $orderHistory,
            'orderWork' => $orderWork,
        ]);
    }

    public function actionFeedback()
    {
        $manager_id = MagazineOrder::model()->find(['condition' => 'client_id = "' . Yii::app()->user->id . '" and manager_id is not null']);
        $user = MagazineUsers::model()->findByPk(Yii::app()->user->id);
        $company = MagazineCompany::model()->findByAttributes(['client_id' => $user->id]);
        $managerInfo = null;
        if ($manager_id) {
            $managerInfo = MagazineUsers::model()->find(['condition' => 'id = ' . $manager_id->manager_id]);
        }
        $text = '';
        if (isset($_POST['text'])) {
            $text = 'Почта: ' . $user->user_email . '<br>' .
                'Имя: ' . $user->user_name . ' ' . $user->user_surname . '<br>' .
                'Компания: ' . ($company != null ? ($company->name_company . '<br>') : 'Компания не найдена. <br>') .
                'Вопрос: ' . $_POST['text'];
        } elseif (isset($_POST['phone'])) {
            $text = 'телефон: ' . $_POST['phone'] . '<br>' .
                'Имя: ' . $user->user_name . ' ' . $user->user_surname . '<br>' .
                'Компания: ' . ($company != null ? $company->name_company  : 'Компания не найдена.');
        }
        if ($managerInfo != null) {
            $name = '=?UTF-8?B?' . base64_encode('Клиент просит связаться с ним - barbers.opt-project.ru') . '?=';
            $subject = '=?UTF-8?B?' . base64_encode('Клиент просит связаться с ним - barbers.opt-project.ru') . '?=';
            $headers = "From: $name <{$_SERVER['SERVER_NAME']}>\r\n" .
                "Reply-To: {$managerInfo->user_email}\r\n" .
                "MIME-Version: 1.0\r\n" .
                "Content-Type: text/html; charset=UTF-8";
            mail(
                $managerInfo->user_email,
                $subject,
                $text,
                $headers
            );
            echo 'ok';
            exit;
        }
    }
}