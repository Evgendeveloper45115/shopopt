<?php

class MagazineCompanyController extends Controller
{
    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '/layouts/main';

    /**
     * @return array action filters
     */
//	public function filters()
//	{
//		return array(
//			'accessControl', // perform access control for CRUD operations
//			'postOnly + delete', // we only allow deletion via POST request
//		);
//	}
//
//	/**
//	 * Specifies the access control rules.
//	 * This method is used by the 'accessControl' filter.
//	 * @return array access control rules
//	 */
//	public function accessRules()
//	{
//		return array(
//			array('allow',  // allow all users to perform 'index' and 'view' actions
//				'actions'=>array('index','view'),
//				'users'=>array('*'),
//			),
//			array('allow', // allow authenticated user to perform 'create' and 'update' actions
//				'actions'=>array('create','update'),
//				'users'=>array('@'),
//			),
//			array('allow', // allow admin user to perform 'admin' and 'delete' actions
//				'actions'=>array('admin','delete'),
//				'users'=>array('admin'),
//			),
//			array('deny',  // deny all users
//				'users'=>array('*'),
//			),
//		);
//	}

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate()
    {
        $model = new MagazineCompany;

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['MagazineCompany'])) {
            $connectID = SiteConnect::model()->findByAttributes(['url' => $_SERVER['SERVER_NAME']]);
            if (isset($_FILES['MagazineCompany']['name'])) {
                array_pop($_FILES['MagazineCompany']['name']);
                $json = $this->loadImage($_FILES['MagazineCompany']['name']);
                if ($model->image) {
                    $arrayAdd = array_diff(json_decode($json), json_decode($model->image));
                } else {
                    $arrayAdd = json_decode($json);
                }

                $clientId = yii::app()->user->id;
                $client = MagazineUsers::model()->find(['condition' => 'id = ' . $clientId]);
                $company = MagazineCompany::model()->find(['condition' => 'client_id = ' . $clientId]);
                $text = 'Пользователь: ' . $client->user_email . ' Сделал изменения в компании: ' . $company->name_company . '<br>';
                if (!empty($arrayAdd)) {
                    foreach ($arrayAdd as $image) {
                        $text .= ' Добавил документ - ' . $this->createAbsoluteUrl($image) . '<br>';
                    }
                }

                $magazineOrder = MagazineOrder::model()->findByAttributes(['client_id' => $clientId]);
                $manager = null;
                if ($magazineOrder) {
                    $manager = MagazineUsers::model()->find(['condition' => 'id = ' . $magazineOrder->manager_id]);
                }
                if ($manager != null) {
                    $name = '=?UTF-8?B?' . base64_encode('Редактирование компании') . '?=';
                    $subject = '=?UTF-8?B?' . base64_encode('Редактирование компании') . '?=';
                    $headers = "From: $name <{$_SERVER['SERVER_NAME']}>\r\n" .
                        "Reply-To: {$manager->user_email}\r\n" .
                        "MIME-Version: 1.0\r\n" .
                        "Content-Type: text/html; charset=UTF-8";

                    mail(
                        $manager->user_email,
                        $subject,
                        $text,
                        $headers
                    );
                }

                $model->image = $json;
            }

            $_POST['MagazineCompany']['client_id'] = Yii::app()->user->id;
            $_POST['MagazineCompany']['site_connect_id'] = $connectID->id;
            $model->attributes = $_POST['MagazineCompany'];
            if ($model->save())
                $this->redirect(array('admin'));
        }

        $this->render('create', array(
            'model' => $model,
        ));
    }

    public function loadImage($imgArray)
    {
        if (!empty($imgArray)) {
            $imagesArr = [];
            $images = CUploadedFile::getInstancesByName('MagazineCompany');

            foreach ($images as $imgName) {
                if (in_array($imgName->name, $imgArray)) {
                    unset($imgArray[array_search($imgName->name, $imgArray)]);
                }
                $path = '/upload/' . uniqid() . '.' . $imgName->getExtensionName();
                $imgName->saveAs(Yii::getPathOfAlias('webroot') . $path);
                $imagesArr[] = $path;
            }
            foreach ($imgArray as $key => $img) {
                if ($img == '') {
                    unset($imgArray[$key]);
                }
            }
            $arr = array_merge($imgArray, $imagesArr);
            return json_encode($arr);

        }
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);
        if (isset($_POST['MagazineCompany'])) {
            foreach ($_POST['MagazineCompany'] as $key => $value) {
                if (is_int($key)) {
                    if ($_FILES['MagazineCompany']['name'][$key] == '') {
                        $_FILES['MagazineCompany']['name'][$key] = $value;
                    }
                }
            }
            $connectID = SiteConnect::model()->findByAttributes(['url' => $_SERVER['SERVER_NAME']]);
            if (isset($_FILES['MagazineCompany']['name'])) {
                $json = $this->loadImage($_FILES['MagazineCompany']['name']);
                if ($model->image) {
                    $arrayAdd = array_diff(json_decode($json), json_decode($model->image));
                } else {
                    $arrayAdd = json_decode($json);
                }

                $clientId = yii::app()->user->id;
                $client = MagazineUsers::model()->find(['condition' => 'id = ' . $clientId]);
                $company = MagazineCompany::model()->find(['condition' => 'client_id = ' . $clientId]);
                $text = 'Пользователь: ' . $client->user_email . ' Сделал изменения в компании: ' . $company->name_company . '<br>';
               if (!empty($arrayAdd)) {
                    foreach ($arrayAdd as $image) {
                        $text .= ' Добавил документ - ' . $this->createAbsoluteUrl($image) . '<br>';
                    }
                }

                $magazineOrder = MagazineOrder::model()->findByAttributes(['client_id' => $clientId]);
                $manager = null;
                if ($magazineOrder) {
                    $manager = MagazineUsers::model()->find(['condition' => 'id = ' . $magazineOrder->manager_id]);
                }
                if ($manager != null) {
                    $name = '=?UTF-8?B?' . base64_encode('Редактирование компании') . '?=';
                    $subject = '=?UTF-8?B?' . base64_encode('Редактирование компании') . '?=';
                    $headers = "From: $name <{$_SERVER['SERVER_NAME']}>\r\n" .
                        "Reply-To: {$manager->user_email}\r\n" .
                        "MIME-Version: 1.0\r\n" .
                        "Content-Type: text/html; charset=UTF-8";

                    mail(
                        $manager->user_email,
                        $subject,
                        $text,
                        $headers
                    );
                }
                $model->image = $json;
            } else {
                $json = $this->loadImage([]);
                $model->image = $json;
            }

            $_POST['MagazineCompany']['client_id'] = Yii::app()->user->id;
            $_POST['MagazineCompany']['site_connect_id'] = $connectID->id;
            $model->attributes = $_POST['MagazineCompany'];
            if ($model->save())
                $this->redirect(array('admin'));
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    /**
     * Lists all models.
     */
    public function actionIndex()
    {
        $dataProvider = new CActiveDataProvider('MagazineCompany');
        $this->render('index', array(
            'dataProvider' => $dataProvider,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin()
    {
        $model = MagazineCompany::model()->findAll(['condition' => 'client_id = ' . Yii::app()->user->id]);

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return MagazineCompany the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model = MagazineCompany::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param MagazineCompany $model the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'magazine-company-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }
}
