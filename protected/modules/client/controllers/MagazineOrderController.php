<?php

class MagazineOrderController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='/layouts/main';

	/**
	 * @return array action filters
	 */

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new MagazineOrder;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['MagazineOrder']))
		{
			$model->attributes=$_POST['MagazineOrder'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['MagazineOrder']))
		{
			$model->attributes=$_POST['MagazineOrder'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex($id)
	{
		$models = MagazineOrder_2::model()->findAll(['condition' => 'order_id = '. $id]);
		$model = MagazineOrder::model()->find(['condition' => 'id = '. $id]);
		$documents = MagazineDocuments::model()->count(['condition' => 'order_id = ' . $id]);
		$this->render('index',array(
			'documents'=>$documents,
			'modelInfo'=>$models,
			'model'=>$model,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$condition = '';

		if(isset($_GET['MagazineOrder']['status'])) {
			$condition []= 'status = ' . $_GET['MagazineOrder']['status'];
		} else {
			$condition []= 'status in (0,1,2)';
		}

		if(isset($_GET['MagazineOrder']['id'])) {
			$condition []= 'id = ' . $_GET['MagazineOrder']['id'];
		}

		if(isset($_GET['MagazineOrder']['magazine_shop_id'])) {
			$condition []= 'magazine_shop_id = ' . $_GET['MagazineOrder']['magazine_shop_id'];
		}

		if($condition != '') {
			$condition []= 'client_id = '. Yii::app()->user->id;
			$condition = implode(' and ', $condition);
			$model = MagazineOrder::model()->findAll(['condition' => $condition, 'order' => 'id DESC']);
		} else {
			$condition []= 'client_id = '. Yii::app()->user->id;
			$condition = implode(' and ', $condition);
			$model = MagazineOrder::model()->findAll(['condition' => $condition, 'order' => 'id DESC']);
		}

		foreach($model as $md) {
			$md->magazine_address = MagazineClientsAddress::model()->findByPk($md->magazine_shop_id);
			$md->countFiles = MagazineDocuments::model()->count(['condition' => 'order_id = '.$md->id]);
		}

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return MagazineOrder the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=MagazineOrder::model()->findByPk(
			[
				'id' =>$id,
				'site_connect_id'=>$this->connectID
			]);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param MagazineOrder $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='magazine-order-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
