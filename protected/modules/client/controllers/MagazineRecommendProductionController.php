<?php

class MagazineRecommendProductionController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='/layouts/main';

	/**
	 * @return array action filters
	 */


	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new MagazineRecommendProduction;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['MagazineRecommendProduction']))
		{
			$model->attributes=$_POST['MagazineRecommendProduction'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['MagazineRecommendProduction']))
		{
			$model->attributes=$_POST['MagazineRecommendProduction'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('MagazineRecommendProduction');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$session = Yii::app()->session;
		$ids = [];
		if(isset($session['items'])) {
			foreach ($session['items'] as $key ) {
				if($key['id'] != 'undefined') {
					$ids[] = $key['id'];
				}
			}

			$this->cart_count = 0;
			$this->cart_price = 0;

			$ids = implode(',',$ids);

			if(!empty($ids)) {

				$cart = MagazineProduct::model()->findAll(['condition' => 'id in (' . $ids . ')']);
				foreach($cart as $cr) {
					foreach ($session['items'] as $key => $item ) {
						if($item['id'] == $cr->id) {
							$this->cart_count+=$item['count'];
							$this->cart_price+=$item['count']*$cr->price;
						}
					}
				}
			}
		}

		$getUserCompany = MagazineCompany::model()->findAll(['condition' => 'client_id = "'.Yii::app()->user->id.'"']);

		$ids = [];
		foreach($getUserCompany as $getUserComp) {
			$ids[] = $getUserComp->id;
		}
		$getRec = MagazineRecommedClientTovar::model()->findAll(['condition' => 'id_company in ("'.implode('","', $ids).'")']);

		$idss=[];
		foreach($getRec as $gr) {
			$idss[] = $gr->id_tovar;
		}

		$getProduct = MagazineProduct::model()->findAll(['condition' => 'id in ("'.implode('","',$idss).'")']);

		$getStock = MagazineRecommendStock::model()->findAll(['condition' => 'id_company in ("'.implode('","', $ids).'")']);
		$idss=[];
		foreach($getStock as $gr) {
			$idss[] = $gr->id_stock;
		}

		$getStock = MagazineStock::model()->findAll(['condition' => 'id in ("'.implode('","',$idss).'")']);

		$this->render('admin',array(
			'getProduct' => $getProduct,
			'getStock' => $getStock
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return MagazineRecommendProduction the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=MagazineRecommendProduction::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param MagazineRecommendProduction $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='magazine-recommend-production-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
