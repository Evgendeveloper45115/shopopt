<?php

class MagazinePromoFilesController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='/layouts/main';

	/**
	 * @return array action filters
	 */

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new MagazinePromoFiles;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['MagazinePromoFiles']))
		{
			$model->attributes=$_POST['MagazinePromoFiles'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['MagazinePromoFiles']))
		{
			$model->attributes=$_POST['MagazinePromoFiles'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('MagazinePromoFiles');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$ids = [];
		$ordersClient = MagazineCompany::model()->findAll(['condition' => 'client_id = "'.Yii::app()->user->id.'"']);
		foreach ($ordersClient as $oC) {
			$ids[] = $oC->id;
		}

		$ids = implode('","', $ids);

		$criteria = new CDbCriteria();

		if(isset($_POST['client_finder_str'])) {
			$str_searching = addslashes($_POST['client_finder_str']);

			$model = MagazinePromoRelations::model()->findAll(['condition' => 'id_company in ("'.$ids.'")']);
			$idsRel = [];
			foreach($model as $dk) {
				$idsRel[] = $dk->id_files;
			}

			$criteria->condition = 'name_file like "%'.$str_searching.'%" and id in ("'.implode('","', $idsRel).'")';

			$files = MagazinePromoFiles::model()->findAll($criteria);
		} else {

			$model = MagazinePromoRelations::model()->findAll(['condition' => 'id_company in ("'.$ids.'")']);
			$idsRel = [];
			foreach($model as $dk) {
				$idsRel[] = $dk->id_files;
			}
			$criteria->condition = 'id in ("'.implode('","', $idsRel).'")';

			$files = MagazinePromoFiles::model()->count($criteria);
		}



//		$pages=new CPagination($files);
		// элементов на страницу
//		$pages->pageSize=5;
//		$pages->applyLimit($criteria);

		$files = MagazinePromoFiles::model()->findAll($criteria);


		$this->render('admin',array(
			'model'=>$files,
//			'pages'=>$pages,
//			'order'=>$order,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return MagazinePromoFiles the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=MagazinePromoFiles::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param MagazinePromoFiles $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='magazine-promo-files-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
