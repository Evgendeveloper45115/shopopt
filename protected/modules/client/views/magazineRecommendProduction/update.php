<?php
/* @var $this MagazineRecommendProductionController */
/* @var $model MagazineRecommendProduction */

$this->breadcrumbs=array(
	'Magazine Recommend Productions'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List MagazineRecommendProduction', 'url'=>array('index')),
	array('label'=>'Create MagazineRecommendProduction', 'url'=>array('create')),
	array('label'=>'View MagazineRecommendProduction', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage MagazineRecommendProduction', 'url'=>array('admin')),
);
?>

<h1>Update MagazineRecommendProduction <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>