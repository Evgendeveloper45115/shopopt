<div id="client_top_bar">
    <?php $this->widget('application.extensions.client_manager.ClientManagerWidget'); ?>
    <div id="cart_info">
        <a href="/catalog/cart">
            <div id="cart_ico" class="inline_style">

            </div>
            <div id="cart_info_content" class="inline_style">
                <div id="align_cart_info">
                    <p><span id="cart_count"><?= $this->cart_count ?></span> позиций</p>

                    <p><span class="money"><?= $this->cart_price ?></span> <span class="rub_lt">a</span></p>
                </div>
            </div>
            <a href="/site/logout" class="log_out_castom" title="Выход"></a>

        </a>
    </div>
</div>

<!--content_area-->
<p class="client_title">Специальные предложения</p>

<div id="special_client_sl">
    <!-- goods item-->
    <!--single-->
    <?php foreach ($getProduct as $gp) {
        $json = json_decode($gp->image_product);
        if (is_array($json) && !empty($json)) {
            foreach ($json as $image) {
                if ($gp->image_product != '' || !stristr($gp->image_product, '[')) {
                    $img = $image;
                    break;
                }
            }
        }
        ?>
        <div class="goods_item css_trans shadow_hover">
        <a href="#">
            <div class="img_fon" style="background-image:url(<?= $img ?>)"></div>
            <p><?= $gp->title_en ?></p>
        </a>

        <div class="price inline_style color_th"><span class="money"><?= $gp->price ?></span><span
                class="rub_lt">a</span></div>
        <button class="bg_color_th" data-id="<?= $gp->id ?>" data-price="<?= $gp->price ?>"
                data-min="<?= $gp->min_count ?>">Купить
        </button>
        </div><?php } ?>
    <!-- goods_item end-->
</div>

<div id="akz_special">
    <!-- akz_special_item -->
    <!-- akz_special_item single-->
    <?php
    if (!empty($getStock)) {
        foreach ($getStock as $md) {
            if ($md->end_date != null) {
                if (date('Y-m-d', strtotime($md->end_date)) < date('Y-m-d')) {
                    continue;
                }
            }
            ?>
            <div class="akz_special_item">
            <div class="img_fon" style="background-image:url(<?= $md->image ?>)"></div>
            <div class="demo_akz_txt_cont">
                <p class="demo_akz_title"><?= $md->header_text ?></p>

                <div><p><?= $md->text ?></p></div>
                <div><p style="color: darkred">Акция до: <?= $md->end_date ? $md->end_date : 'не задано' ?></p></div>
                <a href="#" class="show_akz_lt"><span>Подробнее</span> <span class="color_th">→</span></a>
                <!-- hidden full content -->
                <div class="hid_content">
                    <?= $md->text ?>
                </div>
                <!-- hidden full content -->
            </div>
            </div><?php }
    }else{
        echo '<span>тут появятся специальные предложения от вашего менеджера</span>';
    }
    ?>
</div>