<?php
/* @var $this MagazineRecommendProductionController */
/* @var $model MagazineRecommendProduction */

$this->breadcrumbs=array(
	'Magazine Recommend Productions'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List MagazineRecommendProduction', 'url'=>array('index')),
	array('label'=>'Create MagazineRecommendProduction', 'url'=>array('create')),
	array('label'=>'Update MagazineRecommendProduction', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete MagazineRecommendProduction', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage MagazineRecommendProduction', 'url'=>array('admin')),
);
?>

<h1>View MagazineRecommendProduction #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'product_id',
		'recommend_id',
		'site_connect_id',
	),
)); ?>
