<?php
/* @var $this MagazineRecommendProductionController */
/* @var $model MagazineRecommendProduction */

$this->breadcrumbs=array(
	'Magazine Recommend Productions'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List MagazineRecommendProduction', 'url'=>array('index')),
	array('label'=>'Manage MagazineRecommendProduction', 'url'=>array('admin')),
);
?>

<h1>Create MagazineRecommendProduction</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>