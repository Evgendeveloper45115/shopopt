<?php
/* @var $this MagazineRecommendProductionController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Magazine Recommend Productions',
);

$this->menu=array(
	array('label'=>'Create MagazineRecommendProduction', 'url'=>array('create')),
	array('label'=>'Manage MagazineRecommendProduction', 'url'=>array('admin')),
);
?>

<h1>Magazine Recommend Productions</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
