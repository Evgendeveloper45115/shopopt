<?php
/* @var $this MagazineRecommendProductionController */
/* @var $model MagazineRecommendProduction */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'product_id'); ?>
		<?php echo $form->textField($model,'product_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'recommend_id'); ?>
		<?php echo $form->textField($model,'recommend_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'site_connect_id'); ?>
		<?php echo $form->textField($model,'site_connect_id'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->