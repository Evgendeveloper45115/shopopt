<?php
/* @var $this MagazineRecommendProductionController */
/* @var $model MagazineRecommendProduction */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'magazine-recommend-production-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'product_id'); ?>
		<?php echo $form->textField($model,'product_id'); ?>
		<?php echo $form->error($model,'product_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'recommend_id'); ?>
		<?php echo $form->textField($model,'recommend_id'); ?>
		<?php echo $form->error($model,'recommend_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'site_connect_id'); ?>
		<?php echo $form->textField($model,'site_connect_id'); ?>
		<?php echo $form->error($model,'site_connect_id'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->