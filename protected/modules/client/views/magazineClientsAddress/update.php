<?php
/* @var $this MagazineClientsAddressController */
/* @var $model MagazineClientsAddress */

$this->breadcrumbs=array(
	'Magazine Clients Addresses'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List MagazineClientsAddress', 'url'=>array('index')),
	array('label'=>'Create MagazineClientsAddress', 'url'=>array('create')),
	array('label'=>'View MagazineClientsAddress', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage MagazineClientsAddress', 'url'=>array('admin')),
);
?>

<h1>Обновить магазин <?php echo $model->address; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>