<?php
/* @var $this MagazineClientsAddressController */
/* @var $model MagazineClientsAddress */

$this->breadcrumbs = array(
    'Magazine Clients Addresses' => array('index'),
    'Manage',
);

$this->menu = array(
    array('label' => 'List MagazineClientsAddress', 'url' => array('index')),
    array('label' => 'Create MagazineClientsAddress', 'url' => array('create')),
);

?>
<div id="client_top_bar">

<?php $this->widget('application.extensions.client_manager.ClientManagerWidget'); ?>
<div id="cart_info">
    <a href="/catalog/cart">
        <div id="cart_ico" class="inline_style">

        </div><div id="cart_info_content" class="inline_style">
            <div id="align_cart_info">
                <p><span id="cart_count"><?=$this->cart_count?></span> позиций</p>
                <p><span class="money"><?=$this->cart_price?></span> <span class="rub_lt">a</span></p>
            </div>
        </div>
        <a href="/site/logout" class="log_out_castom" title="Выход"></a>

    </a>
</div>
</div>
<ul id="crm_menu">
    <li class="inline_style"><a href="/client/MagazineClientsAddress/admin" class="color_th_hover active css_trans">Адреса
            доставки<span class="bg_color_th css_trans"></span></a></li>
    <li class="inline_style"><a href="/client/MagazineClientsAddress/create" class="color_th_hover css_trans">Добавить
            адрес доставки<span class="bg_color_th css_trans"></span></a></li>
</ul>


<div>
    <?php if (empty($model)) { ?>
        Добавьте <a href="/client/MagazineClientsAddress/create">ваш первый магазин</a>
    <?php } ?>
    <?php foreach ($model as $md) { ?>
        <div class="client_shops_list_item white_fon inline_style">
            <div class="client_shops_title">
                <p class="color_th inline_style"><?= $md->address ?>
                </p>

                <div class="inline_style">
                    <a href="/client/magazineClientsAddress/update/id/<?= $md->id ?>"
                       class="s_a css_trans border_hover">Редактировать</a>
                    <a href="/client/magazineClientsAddress/delete/id/<?= $md->id ?>" class="s_a css_trans border_hover"
                       onclick="if(!confirm('Вы точно хотите удалить')){event.preventDefault();}">Удалить</a>
                </div>
            </div>
            <div class="client_shop_desc_wrap">
                <p class="client_shop_desc_title">Контактное лицо (приемщик):</p>

                <div class="client_shop_desc">
                    <p><?= $md->name_contact . ' ' . ($md->surname_contact ? $md->surname_contact : null) ?></p>

                    <p><?= preg_replace("/([0-9]{3})([0-9]{3})([0-9]{4})/", "($1) $2-$3", $md->phone_contact) ?></p>

                    <p><?= $md->email ?></p>
                </div>
                <div class="client_shop_s_content">Доп. информация: <?= $md->text_contact ?></div>
            </div>
            <div class="client_shop_link">
                <a href="/client/MagazineOrder/admin?MagazineOrder[magazine_shop_id]=<?= $md->id ?>"
                   class="b_a css_trans border_hover">Текущие заказы</a>
                <a href="/client/MagazineOrder/admin?MagazineOrder[magazine_shop_id]=<?= $md->id ?>&MagazineOrder[status]=<?= MagazineOrder::STATUS_CLOSE ?>"
                   class="b_a css_trans border_hover">История заказов</a>
                <?php
                $getPosition = MagazineProduct::model()->find(['condition' => 'catalog_id = 0 and publish = 0']);
                ?>
                <a href="/catalog/<?= $getPosition->name_product ?>" class="b_a css_trans border_hover">Сделать
                    заказ</a>
            </div>
        </div>
    <?php }
    ?>
</div>
