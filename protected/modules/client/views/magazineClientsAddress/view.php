<?php
/* @var $this MagazineClientsAddressController */
/* @var $model MagazineClientsAddress */

$this->breadcrumbs=array(
	'Magazine Clients Addresses'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List MagazineClientsAddress', 'url'=>array('index')),
	array('label'=>'Create MagazineClientsAddress', 'url'=>array('create')),
	array('label'=>'Update MagazineClientsAddress', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete MagazineClientsAddress', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage MagazineClientsAddress', 'url'=>array('admin')),
);
?>

<h1>View MagazineClientsAddress #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'address',
		'phone',
		'email',
		'user_id',
		'site_connect_id',
		'name_contact',
		'surname_contact',
		'phone_contact',
		'text_contact',
	),
)); ?>
