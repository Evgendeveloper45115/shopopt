<?php
/* @var $this MagazineClientsAddressController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Magazine Clients Addresses',
);

$this->menu=array(
	array('label'=>'Create MagazineClientsAddress', 'url'=>array('create')),
	array('label'=>'Manage MagazineClientsAddress', 'url'=>array('admin')),
);
?>

<h1>Magazine Clients Addresses</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
