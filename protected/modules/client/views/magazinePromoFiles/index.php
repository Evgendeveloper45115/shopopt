<?php
/* @var $this MagazinePromoFilesController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Magazine Promo Files',
);

$this->menu=array(
	array('label'=>'Create MagazinePromoFiles', 'url'=>array('create')),
	array('label'=>'Manage MagazinePromoFiles', 'url'=>array('admin')),
);
?>

<h1>Magazine Promo Files</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
