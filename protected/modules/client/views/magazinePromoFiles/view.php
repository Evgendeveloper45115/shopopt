<?php
/* @var $this MagazinePromoFilesController */
/* @var $model MagazinePromoFiles */

$this->breadcrumbs=array(
	'Magazine Promo Files'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List MagazinePromoFiles', 'url'=>array('index')),
	array('label'=>'Create MagazinePromoFiles', 'url'=>array('create')),
	array('label'=>'Update MagazinePromoFiles', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete MagazinePromoFiles', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage MagazinePromoFiles', 'url'=>array('admin')),
);
?>

<h1>View MagazinePromoFiles #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'name_file',
		'date_create',
		'file_size',
		'user_id',
		'status',
		'extension',
		'client_id',
		'company_id',
	),
)); ?>
