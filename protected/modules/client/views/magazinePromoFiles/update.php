<?php
/* @var $this MagazinePromoFilesController */
/* @var $model MagazinePromoFiles */

$this->breadcrumbs=array(
	'Magazine Promo Files'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List MagazinePromoFiles', 'url'=>array('index')),
	array('label'=>'Create MagazinePromoFiles', 'url'=>array('create')),
	array('label'=>'View MagazinePromoFiles', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage MagazinePromoFiles', 'url'=>array('admin')),
);
?>

<h1>Update MagazinePromoFiles <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>