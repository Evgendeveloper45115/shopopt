<?php
/* @var $this MagazinePromoFilesController */
/* @var $model MagazinePromoFiles */

$this->breadcrumbs=array(
	'Magazine Promo Files'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List MagazinePromoFiles', 'url'=>array('index')),
	array('label'=>'Manage MagazinePromoFiles', 'url'=>array('admin')),
);
?>

<h1>Create MagazinePromoFiles</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>