<?php
/* @var $this MagazineDocumentsController */
/* @var $dataProvider CActiveDataProvider */

?>
<script type="text/javascript">
    $(function () {
        var input = document.querySelector("input[type='file']");
        $(input).onchange = function () {
            this.form.submit();
        }
    });
</script>
<div id="client_top_bar">
    <?php $this->widget('application.extensions.client_manager.ClientManagerWidget'); ?>
    <div id="cart_info">
        <a href="/catalog/cart">
            <div id="cart_ico" class="inline_style">

            </div><div id="cart_info_content" class="inline_style">
                <div id="align_cart_info">
                    <p><span id="cart_count"><?=$this->cart_count?></span> позиций</p>
                    <p><span class="money"><?=$this->cart_price?></span> <span class="rub_lt">a</span></p>
                </div>
            </div>
            <a href="/site/logout" class="log_out_castom" title="Выход"></a>

        </a>
    </div>
</div>
<div class="manager_finder_box" style="top: 0">
    <p class="client_title">Промоматериалы
        <?php if (isset($_POST['client_finder_str'])) { ?>
            <a href="" class="show_all_promo_unic color_th_hover css_trans">
                <svg class="svg_fill_color" version="1.1" xmlns="http://www.w3.org/2000/svg"
                     xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 16 16"
                     style="enable-background:new 0 0 16 16;" xml:space="preserve">
				<g>
                    <g>
                        <path class="st0"
                              d="M0.3,3.9h3.6V0.4H0.3V3.9z M0.3,10.3h3.6V6.8H0.3V10.3z M0.3,16h3.6v-3.5H0.3V16z M9.6,0.4H6v3.6h3.6V0.4z M6.3,9.9h3.4V6.5H6.3V9.9z M6,16h3.6v-3.5H6V16z M12.4,0.4v3.6H16V0.4H12.4z M12.4,10.3H16V6.8h-3.6V10.3z M12.4,16H16v-3.5h-3.6 V16z"></path>
                    </g>
                </g>
				</svg>
                <span>Все промоматериалы</span>
            </a>
        <?php } ?>
    </p>

    <form name="manager_finder" action="" method="POST">
        <input type="text" name="client_finder_str" placeholder="Поиск по названию ..." class="">
    </form>
</div>
<div class="white_fon  client_views_file" style="margin-top: 3%">
    <!--	<div id="client_documents_upload">-->
    <!--		<form method="POST" action=""  enctype="multipart/form-data">-->
    <!--			<input type="file" name="MagazineDocuments[url]" required>-->
    <!--			<input type="hidden" name="order_id" value="--><? //=$id?><!--">-->
    <!--			<button class="bg_color_th">Загрузить документ</button>-->
    <!--		</form>-->
    <!--	</div>-->
    <div class="client_view_file_head">
        <div class="inline_style row1">Наименование</div>
        <div class="inline_style row4">Дата загрузки</div>
        <div class="inline_style row5">Размер файла</div>
        <div class="inline_style row6">Скачать</div>
    </div>
    <div class="client_view_file_body">
        <!--file line -->
        <?php
        if (!empty($model)) {
            foreach ($model as $md) {
                $arrayFormats = explode('.', $md->name_file);
                $img = '';
                if (isset($arrayFormats[1])) {
                    if ($arrayFormats[1] == 'docx') {
                        $img = '<div class="icon-format" style="background-position: -4.4vw -0.2vw;"></div>';
                    } elseif ($arrayFormats[1] == 'xlsx') {
                        $img = '<div class="icon-format" style="background-position: 4.4vw -0.2vw;"></div>';
                    } elseif ($arrayFormats[1] == 'pptx') {
                        $img = '<div class="icon-format" style="background-position: 2.2vw -0.2vw;"></div>';
                    } elseif ($arrayFormats[1] == 'pdf') {
                        $img = '<div class="icon-format" style="background-position: -0.1vw -0.2vw;"></div>';
                    } elseif ($arrayFormats[1] == 'jpg') {
                        $img = '<img src="' . $md->url . '" class="inline_style">';
                    } elseif ($arrayFormats[1] == 'png') {
                        $img = '<img src="' . $md->url . '" class="inline_style">';
                    } else {
                        $img = '<div class="icon-format-none" style="background-position: -0.6vw 0;"></div>';
                    }
                }

                ?>
                <div class="client_view_file_line">
                    <div class="inline_style row1">
                        <?= $img ?>

                        <p class="inline_style"><?= $md->name_file ?></p>
                        <!--				<span>Новый</span>-->
                    </div>
                    <div class="inline_style row4"><?= date('d-m-Y', $md->date_create) ?></div>
                    <div class="inline_style row5"><?= MagazineDocuments::HumanBytes($md->file_size) ?></div>
                    <div class="inline_style row6"><a href="<?= $md->url ?>" class="border_hover css_trans"
                                                      download=""><span class="color_th">↓</span> Скачать</a></div>
                </div>
            <?php }
        } else {
            echo 'Здесь появятся промоматериалы от вашего менеджера';
        }
        ?>
    </div>
</div>

<?= $this->widget('CLinkPager', array(
    'pages' => $pages,
    'id' => 'crm_pagination',
    'prevPageLabel' => '',
    'nextPageLabel' => '',
    'firstPageLabel' => '',
    'lastPageLabel' => '',
    'header' => '',
), true) ?>
