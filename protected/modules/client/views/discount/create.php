<?php
/* @var $this MagazineClientsAddressController */
/* @var $model MagazineClientsAddress */

$this->breadcrumbs=array(
	'Magazine Clients Addresses'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List MagazineClientsAddress', 'url'=>array('index')),
	array('label'=>'Manage MagazineClientsAddress', 'url'=>array('admin')),
);
?>

	<ul id="crm_menu">
		<li class="inline_style"><a href="/client/magazineclientsaddress/admin" class="color_th_hover css_trans">Адреса доставки<span class="bg_color_th css_trans"></span></a></li>
		<li class="inline_style"><a href="/client/magazineclientsaddress/create" class="color_th_hover active css_trans">Добавить магазин<span class="bg_color_th css_trans"></span></a></li>
	</ul>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>