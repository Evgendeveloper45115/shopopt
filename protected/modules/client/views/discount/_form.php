<?php
/* @var $this MagazineClientsAddressController */
/* @var $model MagazineClientsAddress */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'magazine-clients-address-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<div class="white_fon client_shop_form">
		<?php echo $form->errorSummary($model); ?>
			<div class="client_shop_form_wrap">
				<div class="client_shop_input_line">
					<p>Адрес магазина:</p><?php echo $form->textField($model,'address',array('maxlength'=>255, 'class' => 'css_trans')); ?>
				</div>
				<div class="client_shop_input_line">
					<p>Номер телефона</p><?php echo $form->textField($model,'phone',array('maxlength'=>255, 'class' => 'css_trans')); ?>
				</div>
				<div class="client_shop_input_line">
					<p>E-mail</p><?php echo $form->textField($model,'email',array('size'=>60,'maxlength'=>255, 'class' => 'css_trans')); ?>
				</div>
			</div>
			<div class="client_shop_form_wrap">
				<p>Контактное лицо (приемщик):</p>
				<div class="inline_style">
					<?php echo $form->textField($model,'name_contact',array('size'=>60,'maxlength'=>255, 'placeholder'=>'Имя', 'class'=>'css_trans')); ?>
					<?php echo $form->textField($model,'surname_contact',array('size'=>60,'maxlength'=>255, 'placeholder'=>'Фамилия', 'class'=>'css_trans')); ?>
					<?php $this->widget("ext.maskedInput.MaskedInput", array(
						"model" => $model,
						"attribute" => "phone_contact",
						"mask" => '+7(999) 999-9999',
						'htmlOptions' => array('size'=>60,'maxlength'=>255, 'placeholder'=>'Номер телефона', 'class'=>'css_trans')
					)); ?>
<!--					--><?php //echo $form->textField($model,'phone_contact',array('size'=>60,'maxlength'=>255, 'placeholder'=>'Номер телефона', 'class'=>'css_trans')); ?>
				</div><div class="inline_style">
					<?php echo $form->textArea($model,'text_contact',array('rows'=>6, 'cols'=>50, 'placeholder' => 'Дополнительная информация по заказу или уточнения по адресу', 'class'=>"css_trans")); ?>

				</div>
			</div>
			<button class="bg_color_th"><?=$model->isNewRecord ? 'Добавить' : 'Сохранить'?></button>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->