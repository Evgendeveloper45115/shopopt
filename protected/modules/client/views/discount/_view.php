<?php
/* @var $this MagazineClientsAddressController */
/* @var $data MagazineClientsAddress */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('address')); ?>:</b>
	<?php echo CHtml::encode($data->address); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('phone')); ?>:</b>
	<?php echo CHtml::encode($data->phone); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('email')); ?>:</b>
	<?php echo CHtml::encode($data->email); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('user_id')); ?>:</b>
	<?php echo CHtml::encode($data->user_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('site_connect_id')); ?>:</b>
	<?php echo CHtml::encode($data->site_connect_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('name_contact')); ?>:</b>
	<?php echo CHtml::encode($data->name_contact); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('surname_contact')); ?>:</b>
	<?php echo CHtml::encode($data->surname_contact); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('phone_contact')); ?>:</b>
	<?php echo CHtml::encode($data->phone_contact); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('text_contact')); ?>:</b>
	<?php echo CHtml::encode($data->text_contact); ?>
	<br />

	*/ ?>

</div>