<?php
/* @var $this MagazineClientsAddressController */
/* @var $model MagazineClientsAddress */

?>

<p class="client_title">Скидочная система</p>
<div class="white_fon discount_desc">
    <div class="discount_part1">
        <p><?= $model->text; ?></p>
    </div>
    <div class="discount_part2">
        <div class="discount_line">
            <div class="row1"><p>сумма</p></div>
            <div class="row2"><p>размер скидки</p></div>
        </div>
        <?php
        if (!empty($getDiscount)) {
            foreach ($getDiscount as $dt) {
                ?>
                <div class="discount_line">
                    <div class="row1">
                        <p>от <span class="money"><?= $dt->sum ?></span> руб</p>
                    </div>
                    <div class="row2">
                        <p><?= $dt->persent ?>%</p>
                    </div>
                </div>
                <?php
            }
        } else {
            ?>
            <div class="discount_line">Ваш менеджер еще не выставил необходимый объем закупки, при которой будет
                действовать скидка
            </div>
            <?php
        }
        ?>
    </div>
</div>
<p class="client_title" style="margin-top: 1vw">Бонусы</p>
<div class="white_fon discount_desc">
    <li class="inline_style"><span style="margin-right: 2vw"><?= 'Период от:' ?></span>
        <span class="begin_date" style="display: none"></span>
        <span class="end_date" style="display: none"></span>
        <button class="click_order_bonus" style="display: none"></button>

        <span style="margin: 0 0.5vw">С:</span> <?= $this->widget('zii.widgets.jui.CJuiDatePicker', array(
            'name' => 'from_date',
            // additional javascript options for the date picker plugin
            'language' => 'ru',
            'options' => array(
                'dateFormat' => 'yy-mm-dd',
                'showAnim' => 'fold',
                'buttonImageOnly' => true,
                'changeMonth' => true,
                'changeYear' => true,
                'showButtonPanel' => true,
                'showOtherMonths' => true,
                'onSelect' => 'js: function(dateText, inst) {
                            $(".begin_date").text(dateText)
                            $(".click_order_bonus").click()
                        }',
            ),
            'htmlOptions' => array(
                'style' => 'height:20px;'
            ),
        ), true); ?>
        <span style="margin: 0 0.5vw">До:</span> <?= $this->widget('zii.widgets.jui.CJuiDatePicker', array(
            'name' => 'publishDate2',
            // additional javascript options for the date picker plugin
            'language' => 'ru',
            'options' => array(
                'dateFormat' => 'yy-mm-dd',
                'showAnim' => 'fold',
                'buttonImageOnly' => true,
                'changeMonth' => true,
                'changeYear' => true,
                'showButtonPanel' => true,
                'showOtherMonths' => true,
                'onSelect' => 'js: function(dateText, inst) {
                            $(".end_date").text(dateText)
                            $(".click_order_bonus").click()
                        }',
            ),
            'htmlOptions' => array(
                'style' => 'height:20px;'
            ),
        ), true); ?>
    </li>
    <div class="set_bonus" style="margin-top: 1vw">
        <?php
        if (!empty($setBonuses)) {
            $activeBonus = 0;
            $used_bonus = 0;
            foreach ($setBonuses as $setBonus) {
                $activeBonus += $setBonus->active_bonus
                ?>
                <div class="order_info setBonus">
                <span class="" style="display: inline-block; width: 49%;"> Заказ № <?= $setBonus->order_id ?>
                    от <?= date('Y-m-d H:i', strtotime($setBonus->date_create)) ?></span>
                <span style="display: inline-block; width: 50%;">Начислено: <?= $setBonus->active_bonus ?>
                    руб. бонусных</span>
                </div>
                <?php
                if ($setBonus->children != null) {
                    foreach ($setBonus->children as $children) {
                        $used_bonus += $children->used_bonus;
                        ?>
                        <div class="order_info used_bonus">
                        <span
                            style="display: inline-block; width: 49%;"> Заказ № <?= $children->order_id ?>
                            от <?= date('Y-m-d H:i', strtotime($children->date_create)) ?></span>
                        <span
                            style="display: inline-block; width: 50%;)">Потрачено: <?= $children->used_bonus ?>
                            руб. бонусных</span>
                            </div>
                        <?php

                    }
                }
            }
            ?>
            <div class="active_bonus_header" style="display: flex; margin-top: 1vw;     padding-left: 1vw;">
                <div style="width: 49%">Получено бонусов:</div>
                <div style="width: 50%">Использовано бонусов:</div>
            </div>
            <div class="active_bonus" style="display: flex;;     padding-left: 1vw;">
                <div style="width: 49%;color: black"><?= $activeBonus ?> руб.</div>
                <div style="width: 50%; color: black"><?= $used_bonus ?> руб.</div>
            </div>
            <div class="result_bonus" style="padding-left: 1vw; margin-top: 1vw;">
                <div>Остаток:</div>
                <div><?= $activeBonus - $used_bonus ?> руб.</div>
            </div>
            <?php
        }
        ?>
    </div>
</div>