<?php
/* @var $this MagazineReviewController */
/* @var $model MagazineReview */

$this->breadcrumbs=array(
	'Magazine Reviews'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List MagazineReview', 'url'=>array('index')),
	array('label'=>'Create MagazineReview', 'url'=>array('create')),
	array('label'=>'View MagazineReview', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage MagazineReview', 'url'=>array('admin')),
);
?>

<h1>Update MagazineReview <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array(
	'model'=>$model,
	'getManagerUsers' => $getManagerUsers
)); ?>