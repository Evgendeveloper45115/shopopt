<?php
/* @var $this MagazineReviewController */
/* @var $model MagazineReview */

$this->breadcrumbs=array(
	'Magazine Reviews'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List MagazineReview', 'url'=>array('index')),
	array('label'=>'Create MagazineReview', 'url'=>array('create')),
);

?>

<h1>Manage Magazine Reviews</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'magazine-review-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'client_id',
		'manager_id',
		'text_review',
		'name',
		'phone',
		/*
		'email',
		'position',
		*/
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
