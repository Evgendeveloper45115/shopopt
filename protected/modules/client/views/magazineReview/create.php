<?php
/* @var $this MagazineReviewController */
/* @var $model MagazineReview */

$this->breadcrumbs=array(
	'Magazine Reviews'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List MagazineReview', 'url'=>array('index')),
	array('label'=>'Manage MagazineReview', 'url'=>array('admin')),
);
?>

<div id="conten_zone">
<?php $this->renderPartial('_form', array(
	'model'=>$model,
	'submit'=>$submit,
	'getManagerUsers' => $getManagerUsers
)); ?>
</div>
