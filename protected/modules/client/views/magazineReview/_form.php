<?php
/* @var $this MagazineReviewController */
/* @var $model MagazineReview */
/* @var $form CActiveForm */
?>
<div id="client_top_bar">
	<?php $this->widget('application.extensions.client_manager.ClientManagerWidget'); ?>
	<div id="cart_info">
		<a href="/catalog/cart">
			<div id="cart_ico" class="inline_style">

			</div><div id="cart_info_content" class="inline_style">
				<div id="align_cart_info">
					<p><span id="cart_count"><?=$this->cart_count?></span> позиций</p>
					<p><span class="money"><?=$this->cart_price?></span> <span class="rub_lt">a</span></p>
				</div>
			</div>
			<a href="/site/logout" class="log_out_castom" title="Выход"></a>

		</a>
	</div>
</div>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'magazine-review-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>


	<?php echo $form->errorSummary($model); ?>
	<p class="client_title">Отзыв о менеджере: <?=$getManagerUsers->user_name?> <?=$getManagerUsers->user_surname?></p>
	<?php if($submit == true) {?>
		<p class="client_title">Спасибо за отзыв!</p>
	<?php } else {?>
	<div id="clent_otz">
		<p class="client_otz_desc">Если у вас есть замечания или пожелания к работе нашего менеджера, оставьте сообщение и наше руководство его обязательно рассмотрит</p>
		<form name="otz_client" action="" method="POST">
			<div class="form_line">
				<p>Ваше имя:</p><?php echo $form->textField($model,'name',array('size'=>60,'maxlength'=>255, 'class' => 'css_trans')); ?>
			</div>
			<div class="form_line">
				<p>Номер телефона:</p><?php echo $form->textField($model,'phone',array('size'=>60,'maxlength'=>255, 'class' => 'css_trans')); ?>
			</div>
			<div class="form_line">
				<p>E-mail:</p><?php echo $form->textField($model,'email',array('size'=>60,'maxlength'=>255, 'class' => 'css_trans')); ?>
			</div>
			<div class="form_line">
				<p>Должность:</p><?php echo $form->textField($model,'position',array('size'=>60,'maxlength'=>255, 'class' => 'css_trans')); ?>
			</div>
			<div class="form_line">
				<p>Ваш отзыв:</p><?php echo $form->textArea($model,'text_review',array('rows'=>6, 'cols'=>50, 'class' => 'css_trans'));?>
			</div>
			<button class="bg_color_th"><?=($model->isNewRecord ? 'Отправить' : 'Отправить')?></button>
<!--			--><?php //echo CHtml::submitButton(, ['class' => '']); ?>
		</form>
	</div>
	<?php }?>
</div>

<?php $this->endWidget(); ?>

</div><!-- form -->