<?php
/* @var $this MagazineReviewController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Magazine Reviews',
);

$this->menu=array(
	array('label'=>'Create MagazineReview', 'url'=>array('create')),
	array('label'=>'Manage MagazineReview', 'url'=>array('admin')),
);
?>

<h1>Magazine Reviews</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
