<?php
/* @var $this MagazineReviewController */
/* @var $model MagazineReview */

$this->breadcrumbs=array(
	'Magazine Reviews'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List MagazineReview', 'url'=>array('index')),
	array('label'=>'Create MagazineReview', 'url'=>array('create')),
	array('label'=>'Update MagazineReview', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete MagazineReview', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage MagazineReview', 'url'=>array('admin')),
);
?>

<h1>View MagazineReview #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'client_id',
		'manager_id',
		'text_review',
		'name',
		'phone',
		'email',
		'position',
	),
)); ?>
