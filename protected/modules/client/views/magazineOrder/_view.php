<?php
/* @var $this MagazineOrderController */
/* @var $data MagazineOrder */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('time_order')); ?>:</b>
	<?php echo CHtml::encode($data->time_order); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('status')); ?>:</b>
	<?php echo CHtml::encode($data->status); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('client_id')); ?>:</b>
	<?php echo CHtml::encode($data->client_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('manager_id')); ?>:</b>
	<?php echo CHtml::encode($data->manager_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('magazine_shop_id')); ?>:</b>
	<?php echo CHtml::encode($data->magazine_shop_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('site_connect_id')); ?>:</b>
	<?php echo CHtml::encode($data->site_connect_id); ?>
	<br />


</div>