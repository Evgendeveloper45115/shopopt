<?php
/* @var $this MagazineOrderController */
/* @var $model MagazineOrder */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'magazine-order-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'time_order'); ?>
		<?php echo $form->textField($model,'time_order'); ?>
		<?php echo $form->error($model,'time_order'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'status'); ?>
		<?php echo $form->textField($model,'status'); ?>
		<?php echo $form->error($model,'status'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'client_id'); ?>
		<?php echo $form->textField($model,'client_id'); ?>
		<?php echo $form->error($model,'client_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'manager_id'); ?>
		<?php echo $form->textField($model,'manager_id'); ?>
		<?php echo $form->error($model,'manager_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'magazine_shop_id'); ?>
		<?php echo $form->textField($model,'magazine_shop_id'); ?>
		<?php echo $form->error($model,'magazine_shop_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'site_connect_id'); ?>
		<?php echo $form->textField($model,'site_connect_id'); ?>
		<?php echo $form->error($model,'site_connect_id'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->