<?php
/* @var $this MagazineOrderController */
/* @var $model MagazineOrder */

$this->breadcrumbs=array(
	'Magazine Orders'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List MagazineOrder', 'url'=>array('index')),
	array('label'=>'Create MagazineOrder', 'url'=>array('create')),
	array('label'=>'View MagazineOrder', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage MagazineOrder', 'url'=>array('admin')),
);
?>

<h1>Update MagazineOrder <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>