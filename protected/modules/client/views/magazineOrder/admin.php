<?php
/* @var $this MagazineOrderController */
/* @var $model MagazineOrder */

$this->breadcrumbs=array(
	'Magazine Orders'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List MagazineOrder', 'url'=>array('index')),
	array('label'=>'Create MagazineOrder', 'url'=>array('create')),
);

$getPosition = MagazineProduct::model()->find(['condition' => 'catalog_id = 0 and publish = 0']);
?>
<div id="client_top_bar">
	<?php $this->widget('application.extensions.client_manager.ClientManagerWidget'); ?>
	<div id="cart_info">
		<a href="/catalog/cart">
			<div id="cart_ico" class="inline_style">

			</div><div id="cart_info_content" class="inline_style">
				<div id="align_cart_info">
					<p><span id="cart_count"><?=$this->cart_count?></span> позиций</p>
					<p><span class="money"><?=$this->cart_price?></span> <span class="rub_lt">a</span></p>
				</div>
			</div>
			<a href="/site/logout" class="log_out_castom" title="Выход"></a>

		</a>
	</div>
</div>
<p class="client_title client_padd">Мои заказы</p>
<ul id="crm_menu" class="client_padd">
	<li class="inline_style"><a href="/client/MagazineOrder/admin" class="color_th_hover <?=@($_GET['MagazineOrder']['status'] < 3?'active':'');?> css_trans">Текущие заказы<span class="bg_color_th css_trans"></span></a></li>
	<li class="inline_style"><a href="/client/MagazineOrder/admin?MagazineOrder[status]=3" class="color_th_hover <?=@($_GET['MagazineOrder']['status'] == 3?'active':'');?> css_trans">История заказов<span class="bg_color_th css_trans"></span></a></li>
	<li class="inline_style"><a href="/catalog/<?=$getPosition->name_product?>" class="color_th_hover css_trans">Сделать заказ<span class="bg_color_th css_trans"></span></a></li>
</ul>
<!-- search-form -->
<div>
<?php foreach ($model as $md) {
	$order2 = MagazineOrder_2::model()->findByAttributes(['order_id' => $md->id])
	?><div class="white_fon inline_style orders_list_item">
		<div class="order_list_head">
			<p class="order_title">Заказ <b>№<?=$md->id?></b> на <b><span class="money"><?=$order2->order_price?></span> руб.</b></p>
			<div class="order_des">
				<p>Поступил: <?=date('d.m.Y H:i', strtotime($md->time_create))?></p>
				<?php if($md->time_order != null) {?>
				<p>Ожидаемое время прибытия: <?=date('d.m.Y', strtotime($md->time_order))?></p>
				<?php } ?>
				<p>Адреса магазина: <?=$md->magazine_address->address?></p>
				<p>Статус заказа: <b><?php switch($md->status) {
							case MagazineOrder::STATUS_NEW:
								echo 'новый';
								break;
							case MagazineOrder::STATUS_COLLECTED:
								echo 'собирается';
								break;
							case MagazineOrder::STATUS_SHIPPED:
								echo 'отгружен';
								break;
							case MagazineOrder::STATUS_CLOSE:
								echo 'закрыт';
								break;
						}?></b></p>
			</div>
		</div>
		<div class="order_list_buttons">
			<?php if($md->countFiles > 0) {?>
				<a href="/client/MagazineDocuments/index/id/<?=$md->id?>" class="css_trans color_th_hover border_hover">Документы по заказу</a>
			<?php } else { ?>
				<a href="/client/MagazineDocuments/index/id/<?=$md->id?>" class="css_trans color_th_hover border_hover">Документов нет</a>
			<?php } ?>
			<br>
			<a href="/client/MagazineOrder/index/id/<?=$md->id?>" class="css_trans border_hover">Подробнее <span class="color_th">→</span></a><br>
		</div>
	</div><?php } ?>
	<?php if(empty($model)) {
		if($_GET['MagazineOrder']['status'] == 3) { ?>
			<p>У вас нет завершенных заказов</p>
			<p><a href="/client/MagazineOrder/admin">Посмотреть активные заказы</a></p>
		<?php }  else { ?>
			<p>У вас нет активных заказов</p>
			<?php $getPosition = MagazineProduct::model()->find(['condition' => 'catalog_id = 0 and publish = 0']); ?>
			<p><a href="/catalog/<?=$getPosition->name_product?>">Сделать заказ</a></p>
		<?php } ?>
	<?php }?>
</div>