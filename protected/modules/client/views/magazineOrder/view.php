<?php
/* @var $this MagazineOrderController */
/* @var $model MagazineOrder */

$this->breadcrumbs=array(
	'Magazine Orders'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List MagazineOrder', 'url'=>array('index')),
	array('label'=>'Create MagazineOrder', 'url'=>array('create')),
	array('label'=>'Update MagazineOrder', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete MagazineOrder', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage MagazineOrder', 'url'=>array('admin')),
);
?>

<h1>View MagazineOrder #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'time_order',
		'status',
		'client_id',
		'manager_id',
		'magazine_shop_id',
		'site_connect_id',
	),
)); ?>
