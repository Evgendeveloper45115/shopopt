<?php
/* @var $this MagazineOrderController */
/* @var $model MagazineOrder */

$this->breadcrumbs=array(
	'Magazine Orders'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List MagazineOrder', 'url'=>array('index')),
	array('label'=>'Manage MagazineOrder', 'url'=>array('admin')),
);
?>

<h1>Create MagazineOrder</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>