<?php
/* @var $this MagazineOrderController */
/* @var $dataProvider CActiveDataProvider */
$getPosition = MagazineProduct::model()->find(['condition' => 'catalog_id = 0 and publish = 0']);
?>

<p class="client_title client_padd">Мои заказы</p>
<ul id="crm_menu" class="client_padd">
    <li class="inline_style"><a href="/client/MagazineOrder/admin"
                                class="color_th_hover css_trans <?= (stristr($_SERVER['HTTP_REFERER'], '/admin') && !stristr($_SERVER['HTTP_REFERER'], '[status]=3') ? 'active' : '') ?>">Текущие
            заказы<span class="bg_color_th css_trans"></span></a></li>
    <li class="inline_style"><a href="/client/MagazineOrder/admin?MagazineOrder[status]=3"
                                class="color_th_hover css_trans <?= (stristr($_SERVER['HTTP_REFERER'], '[status]=3') ? 'active' : '') ?>">История
            заказов<span class="bg_color_th css_trans"></span></a></li>
    <li class="inline_style"><a href="/catalog/<?= $getPosition->name_product ?>" class="color_th_hover css_trans">Сделать
            заказ<span class="bg_color_th css_trans"></span></a></li>
</ul>


<div class="white_fon order_det_view">
    <div id="order_head">
        <p class="order_title">Заказ <b>№<?= $modelInfo[0]->order_id ?></b> на <b><span class="money"><?= $modelInfo[0]->order_price ?></span>
                руб.</b></p>

        <div class="order_des">
            <!--			<p>Клинет: ООО "торговая компания"</p>-->
            <p>Поступил: <?= $model->time_create ?></p>
            <?php if ($model->time_order != '') { ?>
                <p>Ожидаемое прибытие: <?= date('d.m.Y', strtotime($model->time_order)) ?></p>
            <?php } ?>
            <p>Статус заказа: <b><?php switch ($model->status) {
                        case MagazineOrder::STATUS_NEW:
                            echo 'новый';
                            break;
                        case MagazineOrder::STATUS_COLLECTED:
                            echo 'собирается';
                            break;
                        case MagazineOrder::STATUS_SHIPPED:
                            echo 'отгружен';
                            break;
                        case MagazineOrder::STATUS_CLOSE:
                            echo 'закрыт';
                            break;
                    } ?></b></p>
        </div>
    </div>

    <div id="manager_button_box">
        <?php if ($documents > 0) { ?>
            <a href="/client/MagazineDocuments/index/id/<?= $model->id ?>" class="s_a css_trans border_hover">Документы
                по заказу</a>
            <a style="margin-top: 10px" href="/site/DownloadReport/id/<?= $model->id ?>"
               class="s_a css_trans border_hover">Выгурзить в xls</a>
        <?php } else { ?>
            <a href="/client/MagazineDocuments/index/id/<?= $model->id ?>" class="s_a css_trans border_hover">Документов
                по заказу нет</a>
            <a style="margin-top: 10px" href="/site/DownloadReport/id/<?= $model->id ?>"
               class="s_a css_trans border_hover">Выгурзить в xls</a>
        <?php } ?>
    </div>

    <!--cart_order-->
    <div id="cart_item_table">
        <div id="cart_item_head">
            <div class="row1 inline_style">
                Фото
            </div>
            <div class="row2 inline_style">
                Наименование
            </div>
            <div class="row3 inline_style">
                Цена
            </div>
            <div class="row4 inline_style">
                Количество
            </div>
            <div class="row5 inline_style">
                Стоимость
            </div>
        </div>
        <div id="cart_item_body">
            <!-- cart_item_line-->
            <!-- single cart_item_line-->
            <?php
            $allCost = 0;
            foreach ($modelInfo as $mIn) {
                $allCost += $mIn->with_bonus_sum;
                $img = null;
                if (!empty($mIn->image)) {
                    $json = json_decode($mIn->image);
                    if (is_array($json)) {
                        foreach ($json as $image) {
                          if($image != '' || !stristr($image,'[')){
                              $img = $image;
                              break;
                          }
                        }
                    }
                }
                ?>
                <div class="cart_item_line" data-id="1">
                    <div class="row1 inline_style img_fon" style="background-image:url(<?= $img ?>)">

                    </div>
                    <div class="row2 inline_style">
                        <?= $mIn->name ?>
                    </div>
                    <div class="row3 inline_style">
                        <span class="money"><?= $mIn->cost_item ?></span><span class="rub_lt">a</span>
                    </div>
                    <div class="row4 inline_style">
                        <?= $mIn->count_item ?>
                    </div>
                    <div class="row5 inline_style">
                        <span class="money"><?= $mIn->with_bonus_sum ?></span><span
                            class="rub_lt">c</span>
                    </div>
                </div>
            <?php }
            ?>
            <!--end single cart_item_line-->


            <!--end cart_item_line-->
        </div>
        <div id="cart_item_footer">
            <div class="inline_style cart_itog" style="display: block">
                <span><span class="cart_itog_dop"> Скидка</span>: </span><b><span
                        class="money_disc select_money"><?= isset($modelInfo[0]) ? $modelInfo[0]->discount : 0 ?></span><span
                        class="rub_lt">c</span></b>
            </div>
            <div class="inline_style cart_itog">
                <span>Итого<span class="cart_itog_dop"> со скидкой и бонусом</span>: </span><b><span
                        class="money select_money"><?= $allCost - $modelInfo[0]->discount?></span><span class="rub_lt">c</span></b>
            </div>
        </div>
    </div>
    <!--cart_order-->
</div>