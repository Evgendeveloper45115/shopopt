<?php
/* @var $this MagazineCompanyController */
/* @var $model MagazineCompany */

$this->breadcrumbs=array(
	'Magazine Companies'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List MagazineCompany', 'url'=>array('index')),
	array('label'=>'Create MagazineCompany', 'url'=>array('create')),
);

?>

<div id="client_top_bar">
	<?php $this->widget('application.extensions.client_manager.ClientManagerWidget'); ?>
	<div id="cart_info">
		<a href="/catalog/cart">
			<div id="cart_ico" class="inline_style">

			</div><div id="cart_info_content" class="inline_style">
				<div id="align_cart_info">
					<p><span id="cart_count"><?=$this->cart_count?></span> позиций</p>
					<p><span class="money"><?=$this->cart_price?></span> <span class="rub_lt">a</span></p>
				</div>
			</div>
			<a href="/site/logout" class="log_out_castom" title="Выход"></a>

		</a>
	</div>
</div>
<ul id="crm_menu">
	<li class="inline_style"><a href="/client/MagazineCompany/admin" class="color_th_hover active css_trans">Моя компания<span class="bg_color_th css_trans"></span></a></li>
	<?php if(count($model) == 0) {?>
	<li class="inline_style"><a href="/client/MagazineCompany/create" class="color_th_hover css_trans">Добавить компанию<span class="bg_color_th css_trans"></span></a></li>
	<?php }?>
</ul>

<div>
	<?php
	if(count($model) == 0) {

	} else {


	foreach ($model as $md) { ?>
		<div class="client_shops_list_item white_fon inline_style">
			<div class="client_shops_title">
				<p class="color_th inline_style"><?=$md['name_company']?>
				</p><div class="inline_style">
					<a href="/client/MagazineCompany/update/id/<?=$md['id']?>" class="s_a css_trans border_hover" >Редактировать</a>
					<a href="/client/MagazineCompany/delete/id/<?=$md['id']?>"  class="s_a css_trans border_hover" onclick="if(!confirm('Вы точно хотите удалить')){event.preventDefault();}">Удалить</a>
				</div>
			</div>
		</div>
	<?php }
	}
	?>
</div>

