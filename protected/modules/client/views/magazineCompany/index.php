<?php
/* @var $this MagazineCompanyController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Magazine Companies',
);

$this->menu=array(
	array('label'=>'Create MagazineCompany', 'url'=>array('create')),
	array('label'=>'Manage MagazineCompany', 'url'=>array('admin')),
);
?>

<h1>Magazine Companies</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
