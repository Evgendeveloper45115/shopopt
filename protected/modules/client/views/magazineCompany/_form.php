<?php
/* @var $this MagazineCompanyController */
/* @var $model MagazineCompany */
/* @var $form CActiveForm */
?>
<div class="form">

    <?php $form = $this->beginWidget('CActiveForm', array(
        'id' => 'magazine-clients-address-form',
        'htmlOptions' => [
            'enctype' => 'multipart/form-data',
        ],
        // Please note: When you enable ajax validation, make sure the corresponding
        // controller action is handling ajax validation correctly.
        // There is a call to performAjaxValidation() commented in generated controller code.
        // See class documentation of CActiveForm for details on this.
        'enableAjaxValidation' => false,
    )); ?>

    <div class="white_fon client_shop_form">
        <?php echo $form->errorSummary($model, ''); ?>
        <div class="client_shop_form_wrap">
            <div class="client_shop_input_line">
                <p>Название
                    компании:</p><?php echo $form->textField($model, 'name_company', array('disabled' => $role ? true : false, 'maxlength' => 255, 'class' => 'css_trans')); ?>
            </div>
            <div class="client_shop_input_line">
                <p>
                    ИНН:</p><?php echo $form->textField($model, 'inn', array('disabled' => $role ? true : false, 'maxlength' => 255, 'class' => 'css_trans')); ?>
            </div>
            <div class="client_shop_input_line">
                <p>
                    КПП:</p><?php echo $form->textField($model, 'kpp', array('disabled' => $role ? true : false, 'maxlength' => 255, 'class' => 'css_trans')); ?>
            </div>
            <div class="client_shop_input_line">
                <p>
                    ОГРН:</p><?php echo $form->textField($model, 'ogrn', array('disabled' => $role ? true : false, 'maxlength' => 255, 'class' => 'css_trans')); ?>
            </div>
            <div class="client_shop_input_line">
                <p>Юридический
                    адрес:</p><?php echo $form->textField($model, 'ur_addr', array('disabled' => $role ? true : false, 'maxlength' => 255, 'class' => 'css_trans')); ?>
            </div>
            <div class="client_shop_input_line">
                <p>Расчетный
                    счет:</p><?php echo $form->textField($model, 'chet', array('disabled' => $role ? true : false, 'maxlength' => 255, 'class' => 'css_trans')); ?>
            </div>
            <div class="client_shop_input_line">
                <p>
                    Банк:</p><?php echo $form->textField($model, 'bank', array('disabled' => $role ? true : false, 'maxlength' => 255, 'class' => 'css_trans')); ?>
            </div>
            <div class="client_shop_input_line">
                <p>
                    К/сч:</p><?php echo $form->textField($model, 'k_ch', array('disabled' => $role ? true : false, 'maxlength' => 255, 'class' => 'css_trans')); ?>
            </div>
            <div class="client_shop_input_line">
                <p>Генеральный
                    директор:</p><?php echo $form->textField($model, 'gen_dir', array('disabled' => $role ? true : false, 'maxlength' => 255, 'class' => 'css_trans')); ?>
            </div>
            <div id="publiser_tovar_foto" style="margin-bottom: 25px">
                <p style="width: 34%" class="inline_style pub_form_label_p">Изображение:</p>

                <div id="publisher_foto_cont">
                    <?php
                    if ($model->image != null && $model->image != '[]') {
                        $decode = json_decode($model->image);
                        foreach ($decode as $key => $imgPath) {
                            ?>
                            <div class="img_file_box">
                            <div class="img_file_wrap">
                                <div class="img_file_wrap_i">
                                    <input <?= $role ? 'disabled' : false ?> data-type="image_product"
                                                                             class="publiser_file_input"
                                                                             data-id="<?= $model->id ?>"
                                                                             name="MagazineCompany[]"
                                                                             value="<?= $imgPath ?>">
                                    <input <?= $role ? 'disabled' : false ?> type="file" data-type="image_product"
                                                                             class="publiser_file_input"
                                                                             data-id="<?= $model->id ?>"
                                                                             name="MagazineCompany[]"
                                                                             value="<?= $imgPath ?>">

                                    <p class="bg_color_th"></p>
                                </div>
                            </div>
                            <div class="img_file_preview">
                                <img src="<?= $imgPath ?>">

                                <div class="del_this_pub_foto"></div>
                            </div>
                            </div><?php
                        }
                    } else {
                        ?>
                        <div class="img_file_box">
                            <div class="img_file_wrap">
                                <div class="img_file_wrap_i">
                                    <input type="file" data-type="image_product" class="publiser_file_input"
                                           data-id="<?= $model->id ?>" name="MagazineCompany[]"
                                           value="">
                                </div>
                            </div>
                            <div class="img_file_preview">
                                <img src="<?= '/html_source/img/save_ico.png' ?>">

                                <div class="del_this_pub_foto"></div>
                            </div>
                        </div>

                        <?php
                    }
                    ?>
                </div>
            </div>
        </div>
        <?php
        if (!$role) {
            ?>
            <button class="bg_color_th"><?= $model->isNewRecord ? 'Добавить' : 'Сохранить' ?></button>
            <?php
        }
        ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->