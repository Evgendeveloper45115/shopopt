<?php /* @var $this Controller */ ?>
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="language" content="ru">

    <!-- blueprint CSS framework -->
    <!--	<link rel="stylesheet" type="text/css" href="-->
    <?php //echo Yii::app()->request->baseUrl; ?><!--/css/screen.css" media="screen, projection">-->
    <link rel="stylesheet" href="/html_source/css/all.css">
    <!--    <link rel="stylesheet" href="/html_source/css/all_inner.css">-->
    <link rel="stylesheet" href="/html_source/css/all_client.css">
    <!--    <link rel="stylesheet" href="/html_source/css/all_crm.css">-->

    <style>
        <?php include Yii::getPathOfAlias('webroot').'/html_source/head_style.php'?>
    </style>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
    <script src="/html_source/js/jquery.maskedinput.min.js"></script>
    <script src="/html_source/js/core.js"></script>
    <script src="/html_source/js/all.js"></script>

    <!-- page script-->

    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/print.css"
          media="print">
    <!--    <script src="/html_source/js/all_inner.js"></script>-->
    <script src="/html_source/js/all_client.js"></script>
    <script src="/html_source/js/jquery.slides.min.js"></script>
    <title><?php echo CHtml::encode($this->pageTitle); ?></title>
</head>
<body>
<div id="left_bar" class="inline_style">
    <div id="left_bar_wrap">
        <?php
        $getPosition = MagazineProduct::model()->find(['condition' => 'catalog_id = 0 and publish = 0']);

        echo CHtml::image('/upload/' . @$this->dataModel['logo_site'], '', ['width' => 50, 'height' => 50, 'class' => 'logo' . ($this->connectID == 36 ? ' barbers-logo' : null)]);
        ?>
        <?php
        $firstRaw = 'Сделать заказ';
        if ($this->connectID == 36) {
            $firstRaw = 'Каталог';
        }
        $items = array(
            ['label' => $firstRaw . '<span class="bg_color_th"></span>', 'url' => '/catalog/' . $getPosition->name_product, 'visible' => (Yii::app()->user->role == WebUser::CLIENT)],
            array('label' => 'Рабочий стол<span class="bg_color_th"></span>', 'url' => array('/client/default/index'), 'visible' => (Yii::app()->user->role == WebUser::CLIENT)),
            array('label' => 'Мои Заказы<span class="bg_color_th"></span>', 'url' => array('/client/MagazineOrder/admin'), 'visible' => (Yii::app()->user->role == WebUser::CLIENT)),
            array('label' => 'Моя компания<span class="bg_color_th"></span>', 'url' => array('/client/MagazineCompany/admin'), 'visible' => (Yii::app()->user->role == WebUser::CLIENT)),
            array('label' => 'Адреса доставки<span class="bg_color_th"></span>', 'url' => array('/client/magazineClientsAddress/admin'), 'visible' => (Yii::app()->user->role == WebUser::CLIENT), 'active' => stristr($_SERVER['REQUEST_URI'], '/client/magazineClientsAddress/')),
            array('label' => 'Специальные предложения<span class="bg_color_th"></span>', 'url' => array('/client/magazineRecommendProduction/admin'), 'visible' => (Yii::app()->user->role == WebUser::CLIENT)),
            array('label' => 'Документооборот<span class="bg_color_th"></span>', 'url' => array('/client/MagazineDocuments/admin'), 'visible' => (Yii::app()->user->role == WebUser::CLIENT)),
            array('label' => 'Промоматериалы<span class="bg_color_th"></span>', 'url' => array('/client/MagazinePromoFiles/admin'), 'visible' => (Yii::app()->user->role == WebUser::CLIENT)),
            array('label' => 'Скидки и бонусы<span class="bg_color_th"></span>', 'url' => array('/client/discount/admin'), 'visible' => (Yii::app()->user->role == WebUser::CLIENT)),
            array('label' => 'Настройки клиента<span class="bg_color_th"></span>', 'url' => array('/client/MagazineUsers/admin'), 'visible' => (Yii::app()->user->role == WebUser::CLIENT)),
        );
        if (Yii::app()->user->role == WebUser::MANAGER) {
            $items = [
                array('label' => 'Клиенты<span class="bg_color_th"></span>', 'url' => array('/manager/MagazineClientInfo/index'), 'visible' => (Yii::app()->user->role == WebUser::MANAGER)),
                array('label' => 'Заказы<span class="bg_color_th"></span>', 'url' => array('/manager/MagazineOrder/admin'), 'visible' => (Yii::app()->user->role == WebUser::MANAGER)),
                array('label' => 'Документооборот<span class="bg_color_th"></span>', 'url' => array('/manager/MagazineDocuments/admin'), 'visible' => (Yii::app()->user->role == WebUser::MANAGER)),
                array('label' => 'Промоматериалы<span class="bg_color_th"></span>', 'url' => array('/manager/Promo/index'), 'visible' => (Yii::app()->user->role == WebUser::MANAGER)),
                array('label' => 'Акции и спецпредложения<span class="bg_color_th"></span>', 'url' => array('/manager/MagazineAction/admin'), 'visible' => (Yii::app()->user->role == WebUser::MANAGER)),
                array('label' => 'Настройки<span class="bg_color_th"></span>', 'url' => array('/manager/MagazineUsers/admin'), 'visible' => (Yii::app()->user->role == WebUser::MANAGER)),
            ];
        }
        $this->widget('zii.widgets.CMenu', array(
            'encodeLabel' => false,
            'items' => $items,
            'submenuHtmlOptions' => ['class' => 'css_trans'],
            'itemCssClass' => 'child_true css_trans'
        ));

        ?>
    </div>
</div><div id="conten_zone" class="inline_style"><div id="align_zone">
        <?= $content ?>
        <div id="footer_margin"></div>
        <div id="footer">
            <p class="copyright">© 2015 opt prodject</p>

            <div class="inline_style">
                <a href="#" class="mail_rss color_th_hover css_trans border_hover"><span>Подписаться на рассылку</span>
                    <span class="color_th">→</span></a>
            </div>
            <div class="inline_style">
                <ul class="soc_li">
                    <li><a href="#" target="blank"><img src="/html_source/img/soz1.png" alt=""></a></li>
                    <li><a href="#" target="blank"><img src="/html_source/img/soz2.png" alt=""></a></li>
                    <li><a href="#" target="blank"><img src="/html_source/img/soz3.png" alt=""></a></li>
                </ul>
            </div>
            <div class="inline_style">
                <ul class="map_site">
                    <li><a href="/site/about" class="color_th_hover css_trans">О магазине</a></li>
                    <li><a href="/site/delivery" class="color_th_hover css_trans">Доставка</a></li>
                    <li><a href="/client" class="color_th_hover css_trans">Личный кабинет</a></li>
                </ul>
            </div>
        </div>
    </div>

    <!--content_area end-->
    <!--footer start-->

    <!--footer_end-->
</div>
<?php
if(Yii::app()->controller->id == 'magazineReview' && Yii::app()->controller->action->id == 'create'){
    ?>
    <style>
        #conten_zone{
            position: static;
        }
        #footer{
            top: 50vw;
        }
    </style>
<?php
}
?>
</body>
</html>
