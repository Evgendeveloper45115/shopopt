<?php
/* @var $this MagazineDocumentsController */
/* @var $dataProvider CActiveDataProvider */

?>
<script type="text/javascript">
	$(function(){
		var input = document.querySelector("input[type='file']");
		input.onchange = function () {
			this.form.submit();
		}
	});
</script>
<div id="client_top_bar">
	<?php $this->widget('application.extensions.client_manager.ClientManagerWidget'); ?>
	<div id="cart_info">
		<a href="/catalog/cart">
			<div id="cart_ico" class="inline_style">

			</div><div id="cart_info_content" class="inline_style">
				<div id="align_cart_info">
					<p><span id="cart_count"><?=$this->cart_count?></span> позиций</p>
					<p><span class="money"><?=$this->cart_price?></span> <span class="rub_lt">a</span></p>
				</div>
			</div>
			<a href="/site/logout" class="log_out_castom" title="Выход"></a>

		</a>
	</div>
</div>

<p class="client_title">Документооборот по заказу № <?=$id?> от <?=date('H.i d.m.Y',strtotime($order->time_create))?></p>
<div class="white_fon  client_views_file">
	<div id="client_documents_upload">
		<form method="POST" action=""  enctype="multipart/form-data">
			<input type="file" name="MagazineDocuments[url]" required>
			<input type="hidden" name="order_id" value="<?=$id?>">
			<button class="bg_color_th">Загрузить документ</button>
		</form>
	</div>
	<div class="client_view_file_head">
		<div class="inline_style row1">Наименование</div>
		<div class="inline_style row3">К заказу</div>
		<div class="inline_style row4">Дата загрузки</div>
		<div class="inline_style row5">Размер файла</div>
		<div class="inline_style row6">Скачать</div>
	</div>
	<div class="client_view_file_body">
		<!--file line -->
		<?php foreach($model as $md) {?><div class="client_view_file_line">
			<div class="inline_style row1">
				<img src="<?=$md->url?>" class="inline_style">
				<p class="inline_style"><?=$md->name?></p>
<!--				<span>Новый</span>-->
			</div>
			<div class="inline_style row3"><?=$md->order_id?></div>
			<div class="inline_style row4"><?=$md->date_add?></div>
			<div class="inline_style row5"><?=MagazineDocuments::HumanBytes($md->size_file)?></div>
			<div class="inline_style row6"><a href="<?=$md->url?>"  class="border_hover css_trans" download=""><span class="color_th">↓</span> Скачать</a></div>
		</div><?php }?>
		<!-- file line one end-->
		<!--file line end-->

	</div>
</div>

<!--<div id="crm_pagination">-->
<!--	<!-- active == class="bg_color_th"-->
<!--	<a href="#" class="bg_color_th css_trans"><span>1</span></a>-->
<!--	<a href="#" class="bg_color_th_hover css_trans"><span>2</span></a>-->
<!--	<a href="#" class="bg_color_th_hover css_trans"><span>3</span></a>-->
<!--	<a href="#" class="bg_color_th_hover v"><span>Все</span></a>-->
<!--</div>-->