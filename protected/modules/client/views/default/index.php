<?php
/* @var $this DefaultController */

$this->breadcrumbs = array(
    $this->module->id,
);
?>
<div id="client_top_bar">
    <?php $this->widget('application.extensions.client_manager.ClientManagerWidget'); ?>
    <div id="cart_info">
        <a href="/catalog/cart">
            <div id="cart_ico" class="inline_style">

            </div>
            <div id="cart_info_content" class="inline_style">
                <div id="align_cart_info">
                    <p><span id="cart_count"><?= $this->cart_count ?></span> позиций</p>

                    <p><span class="money"><?= $this->cart_price ?></span> <span class="rub_lt">a</span></p>
                </div>
            </div>
            <a href="/site/logout" class="log_out_castom" title="Выход"></a>

        </a>
    </div>
</div>
<p class="client_title">Рекомендуемые предложения</p>
<div id="special_client_sl"><?php
    $test = null;
    foreach ($idss as $id) {

        foreach ($getProduct as $key => $gp) {
            if ($gp->id == $id) {
                $test = $gp;
            }
            $magUrl = null;
            $magUrlParent = null;
            if ($test->parent_id) {
                $magUrl = MagazineProduct::model()->findByPk($test->parent_id);
                if ($magUrl && $magUrl->parent_id != 0) {
                    $magUrlParent = MagazineProduct::model()->findByPk($magUrl->parent_id);
                }
            }
        }
        $json = null;
        if ($test->image_product) {
            $json = json_decode($test->image_product);
        }
        if (!empty($json[0])) {
            if (!stristr($json[0], '[')) {
                $json = $json[0];
            } else {
                $json = $json[1];
            }
        }
        if ($json == '') {
            $json = json_decode($test->image_product);
            if ($json[0] != '') {
                $json = json_decode($json[0]);
                $json = $json[0];
            }
        }
        $buttonText = 'Купить';
        $app = Yii::app();
        if (isset($app->session['items'])) {
            foreach ($app->session['items'] as $items) {
                if ($test->id == $items['id']) {
                    $buttonText = 'В корзине';
                }
            }
        }
        if ($test->price == null) {
            continue;
        }
        ?>
        <div class="goods_item css_trans shadow_hover">
        <a href="/catalog/<?= $magUrlParent->title ?>/<?= urldecode($magUrl->title) ?>/<?= $test->id ?>">
            <div class="img_fon" style="background-image:url(<?= $json != '' ? $json : null ?>)"></div>
            <p><?= $test->title_en ?></p>
        </a>

        <div class="price inline_style color_th"><span class="money"><?= $test->price ?></span><span
                class="rub_lt">a</span></div>
        <button class="bg_color_th" data-id="<?= $test->id ?>" data-price="<?= $test->price ?>"
                data-min="<?= $test->min_count ?>"><?= $buttonText ?></button>
        </div><?php } ?>
</div>

<div class="client_order_dashboard">
    <div class="inline_style dashboard_l">
        <p class="client_title">Календарь заказов (в работе)</p>

        <div class="white_fon">
            <div class="inline_style dashboard_l_1">
                <?php foreach ($orderWork as $md) { ?>
                    <div class="dashboard_line">
                    <p class="dashboard_title">Заказ <b>№<?= $md->id ?></b> на <b><span
                                class="money"><?= $md->price ?></span> руб.</b> <a
                            href="/client/MagazineOrder/index/id/<?= $md->id ?>" class="s_a border_hover css_trans">Подробнее
                            <span class="color_th">→</span></a></p>

                    <div class="dashboard_content">
                        <p>Поступил: <?= date('d.m.Y H:s', strtotime($md->time_create)) ?></p>
                        <?php if ($md->time_order != null) { ?>
                            <p>Ожидаемое
                                прибытие: <?= $md->time_order != null ? date('d.m.Y H:s', strtotime($md->time_order)) : 'Не задано' ?></p>
                        <?php } ?>
                        <p>Адреса магазинов: <?= $md->magazine_address->address ?></p>

                        <p>Статус заказа: <b><?php
                                switch ($md->status) {
                                    case MagazineOrder::STATUS_NEW:
                                        echo 'новый';
                                        break;
                                    case MagazineOrder::STATUS_COLLECTED:
                                        echo 'собирается';
                                        break;
                                    case MagazineOrder::STATUS_SHIPPED:
                                        echo 'отгружен';
                                        break;
                                    case MagazineOrder::STATUS_CLOSE:
                                        echo 'закрыт';
                                        break;
                                } ?></b></p>
                    </div>
                    </div><?php } ?><?php if (count($orderWork) > 0) { ?><a href="/client/MagazineOrder/admin"
                                                                            class="s_a border_hover css_trans">Посмотреть
                    весь календарь заказов <span class="color_th">→</span></a>
                <?php } ?>
            </div>
            <div class="inline_style dashboard_l_2">
                <?php
                $dataTest = [];

                foreach ($orderWork as $md) {

                    $dataTest[] = [
                        'data_d' => isset($md->date_d) ? $md->date_d : '',
                        'data_m' => isset($md->date_m) ? $md->date_m : '',
                        'data_y' => isset($md->date_y) ? '20' . $md->date_y : '',
                        'href' => '/client/MagazineOrder/index/id/' . $md->id
                    ];
                }
                $out = json_encode($dataTest);

                ?>
                <script>
                    var data_arr = <?=$out?>;
                </script>
                <!--				--><?php //if(!empty($dataTest)) {?>
                <table id="calendar2">
                    <thead class="color_th">
                    <tr>
                        <td>‹
                        <td colspan="5" id="calendar_data">
                        <td>›
                    </tr>
                    <tr>
                        <td>Пн
                        <td>Вт
                        <td>Ср
                        <td>Чт
                        <td>Пт
                        <td>Сб
                        <td>Вс
                    </tr>
                    </thead>
                    <tbody></tbody>
                </table>
                <div id="calendar_legend">
                    <span class="legend_today"></span><span> - сегодня </span><span
                        class="legend_active bg_color_th"></span><span> - дни заказов</span>
                </div>
                <!--				--><?php //}?>
            </div>
        </div>
    </div>
    <div class="inline_style dashboard_r">
        <p class="client_title">История заказов</p>

        <div class="white_fon">
            <?php foreach ($orderHistory as $orderHis) { ?>
                <div class="client_dash_hist">
                    <div class="inline_style">
                        <p>Заказ <b>№<?= $orderHis->id ?></b> на <b><span class="money"><?= $orderHis->price ?></span>
                                руб.</b></p>

                        <p>Поступил: <?= $orderHis->time_order ?></p>
                    </div>
                    <div class="inline_style">
                        <a href="/client/MagazineOrder/index/id/<?= $orderHis->id ?>"
                           class="s_a border_hover css_trans">Подробнее <span class="color_th">→</span></a>
                    </div>
                </div>
            <?php } ?>

            <a href="/client/MagazineOrder/admin?MagazineOrder[status]=3"
               class="s_a all_dash_h_link  border_hover css_trans">Посмотреть вcю историю заказов <span
                    class="color_th">→</span></a>

        </div>
    </div>
</div>


<div class="manager_box_row">
    <div class="inline_style">
        <p class="client_title">Документооборот</p>

        <div class="white_fon">
            <?php foreach ($orderDocuments as $orderDocument) {
                $arrayFormats = explode('.', $orderDocument->url);
                $img = '';
                if (isset($arrayFormats[1])) {
                    if ($arrayFormats[1] == 'docx') {
                        $img = '<div class="icon-format" style="background-position: -4.4vw -0.2vw;"></div>';
                    } elseif ($arrayFormats[1] == 'xlsx') {
                        $img = '<div class="icon-format" style="background-position: 4.4vw -0.2vw;"></div>';
                    } elseif ($arrayFormats[1] == 'pptx') {
                        $img = '<div class="icon-format" style="background-position: 2.2vw -0.2vw;"></div>';
                    } elseif ($arrayFormats[1] == 'pdf') {
                        $img = '<div class="icon-format" style="background-position: -0.1vw -0.2vw;"></div>';
                    } elseif ($arrayFormats[1] == 'jpg') {
                        $img = '<img src="' . $orderDocument->url . '" class="d_inline">';
                    } elseif ($arrayFormats[1] == 'png') {
                        $img = '<img src="' . $orderDocument->url . '" class="d_inline">';
                    } else {
                        $img = '<div class="icon-format-none" style="background-position: -0.6vw 0;"></div>';
                    }
                }

                ?>
                <div
                    class="manager_file_view <?= (in_array($orderDocument->extension, ['png', 'jpeg', 'jpg', 'PNG']) ? 'manager_file_view2' : '') ?>">
                    <?= $img ?>
                    <p class="d_inline"><?= $orderDocument->name ?></p>
                    <a href="<?= $orderDocument->url ?>" class="s_a border_hover css_trans"
                       download="<?= $orderDocument->name ?>"><span class="color_th">↓</span> Скачать</a>
                </div>
                <?php
            }
            ?>
            <div class="manager_file_link">
                <a href="/client/MagazineDocuments/admin" class="s_a border_hover css_trans">Посмотреть все документы
                    <span class="color_th">→</span></a>
            </div>
        </div>
    </div>
    <div class="inline_style">
        <p class="client_title">Промоматериалы</p>

        <div class="white_fon">
            <?php foreach ($filesPromo as $orderDocument) {
                $arrayFormats = explode('.', $orderDocument->url);
                $img = '';
                if (isset($arrayFormats[1])) {
                    if ($arrayFormats[1] == 'docx') {
                        $img = '<div class="icon-format" style="background-position: -4.4vw -0.2vw;"></div>';
                    } elseif ($arrayFormats[1] == 'xlsx') {
                        $img = '<div class="icon-format" style="background-position: 4.4vw -0.2vw;"></div>';
                    } elseif ($arrayFormats[1] == 'pptx') {
                        $img = '<div class="icon-format" style="background-position: 2.2vw -0.2vw;"></div>';
                    } elseif ($arrayFormats[1] == 'pdf') {
                        $img = '<div class="icon-format" style="background-position: -0.1vw -0.2vw;"></div>';
                    } elseif ($arrayFormats[1] == 'jpg') {
                        $img = '<img src="' . $orderDocument->url . '" class="d_inline">';
                    } elseif ($arrayFormats[1] == 'png') {
                        $img = '<img src="' . $orderDocument->url . '" class="d_inline">';
                    } else {
                        $img = '<div class="icon-format-none" style="background-position: -0.6vw 0;"></div>';
                    }
                }

                ?>
                <div
                    class="manager_file_view <?= (in_array($orderDocument->extension, ['png', 'jpeg', 'jpg', 'PNG']) ? 'manager_file_view2' : '') ?>">
                    <?= $img ?>
                    <p class="d_inline"><?= $orderDocument->name_file ?></p>
                    <a href="<?= $orderDocument->url ?>" class="s_a border_hover css_trans"
                       download="<?= $orderDocument->name_file ?>"><span class="color_th">↓</span> Скачать</a>
                </div>
                <?php
            }
            ?>
            <div class="manager_file_link">
                <a href="/client/MagazinePromoFiles/admin" class="s_a border_hover css_trans cl_page_top_align">Посмотреть
                    все промоматериалы <span class="color_th">→</span></a>
            </div>
        </div>
    </div>
</div>