<div id="align_zone">
    <a href="/admin/MagazineUsers/newUsers" class="back_to_orders" style="margin-top: 2.5vw;">← Вернуться к списку не
        подтверждённых клиентов</a>

    <div class="white_fon client_info_line">
        <div class="inline_style img_cont">
            <!--            <img src="/html_source/img/logo_client.png">-->
        </div>
        <div class="inline_style title">
            <?= $companyInfo->name_company ?>
        </div>
        <div class="inline_style link_box">
            <!--            <a href="#" class="inline_style show_client_cart border_hover css_trans">Показать карточку клиента</a>-->
            <div class="short_line inline_style">
                <p><?= $clinetInfo->user_name ?> <?= $clinetInfo->user_surname ?></p>

                <p><?= $clinetInfo->user_phone ?></p>

                <p>e-mail: <a href="mailto:<?= $clinetInfo->user_email ?>"><?= $clinetInfo->user_email ?></a></p>
            </div>
            <a href="/admin/MagazineUsers/update/id/<?= $clinetInfo->id ?>"
               class="inline_style cd_client border_hover css_trans">Изменить <span class="color_th">→</span></a>
        </div>
        <div id="select_manager" style="top: 3.4vw;">
            <span style="margin-right: 0.5vw">Выберите менеджера</span>
            <select style="width: 12.3vw;!important" class="input_publisher"
                    name="MagazineUsers[manager_id]" id="select_new_manager">
                <?php foreach (MagazineUsers::model()->findAllByAttributes(['user_role' => WebUser::MANAGER]) as $key => $manager) {
                        echo '<option value="' . $manager->id . '">' . $manager->user_email . '</option>';
                } ?>
            </select>
        </div>
        <div id="manager_dop_info" style="top: 3vw;">
            <a href="#" class="bg_color_th active_on" name="on" data-id="<?= $id ?>">Подтвердить</a>
            <a href="#" class="bg_color_th active_off" name="off" data-id="<?= $id ?>">Отказать</a>
        </div>
    </div>
    <div class=" manager_box_row">
        <div class="inline_style" style="color: #5d5d5d;">
            <p class="manager_title">О компании:</p>

            <div class="white_fon">
                <div>
                    <span> - ИНН:</span><span><?= $companyInfo->inn ?></span>
                </div>
                <div>
                    <span> - Документы:</span>

                    <div class="imgCompany">
                        <?php
                        if ($companyInfo->image) {
                            $jsonImg = json_decode($companyInfo->image);
                            if (!empty($jsonImg)) {
                                foreach ($jsonImg as $key => $img) {
                                    if ($key > 3) {
                                        break;
                                    } else {
                                        ?>
                                        <a class="preload" href="<?= $img ?>"><img
                                                style="width: 8vw; vertical-align: top; margin-right: 0.3vw"
                                                src="<?= $img ?>"></a>
                                        <?php
                                    }
                                }
                            }
                        }
                        ?>
                    </div>
                    <div style="height: 27px; padding-bottom: 1.3vw">
                    <span>
                        <a href="/admin/MagazineCompany/update/id/<?= $companyInfo->id ?>"
                           class="link_in_page_order border_hover css_trans">Посмотреть всю информацию</a>
                    </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>