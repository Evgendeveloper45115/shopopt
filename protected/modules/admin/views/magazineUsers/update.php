<?php
/* @var $this MagazineUsersController */
/* @var $model MagazineUsers */

$this->breadcrumbs=array(
	'Magazine Users'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List MagazineUsers', 'url'=>array('index')),
	array('label'=>'Create MagazineUsers', 'url'=>array('create')),
	array('label'=>'View MagazineUsers', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage MagazineUsers', 'url'=>array('admin')),
);
?>


<?php $this->renderPartial('_form', array('model'=>$model)); ?>