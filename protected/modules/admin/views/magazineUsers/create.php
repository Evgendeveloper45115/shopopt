<?php
/* @var $this MagazineUsersController */
/* @var $model MagazineUsers */

$this->breadcrumbs=array(
	'Magazine Users'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List MagazineUsers', 'url'=>array('index')),
	array('label'=>'Manage MagazineUsers', 'url'=>array('admin')),
);
?>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>