<?php
/* @var $this MagazineUsersController */
/* @var $model MagazineUsers */
/* @var $form CActiveForm */
?>

<!--<div class="form">-->

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'magazine-users-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>
<p class="crm_title">Создать менеджера</p>

<div id="creat_manager">

		<p>Добавьте менеджера и на его электронный адрес придет вся информация<br/> для входа в систему</p>
		<div class="input_wrap">
			<label>Имя:</label>
			<input type="text" name="MagazineUsers[user_name]" required>
		</div>
		<div class="input_wrap">
			<label>Фамилия:</label>
			<input type="text" name="MagazineUsers[user_surname]" required>
		</div>
		<div class="input_wrap">
			<label>E-mail:</label>
			<?php echo $form->textField($model, 'user_email', ['required' => 'required'])?>
			<?php echo $form->error($model,'user_email'); ?>
		</div>
		<div class="input_wrap">
			<label>Номер телефона:</label>
			<?php $this->widget("ext.maskedInput.MaskedInput", array(
				"model" => $model,
				"attribute" => "user_phone",
				"mask" => '+7 (999) 999-9999',
				'htmlOptions' => array('size'=>60,'maxlength'=>255, 'placeholder'=>'Номер телефона')
			)); ?>
<!--			<input type="text" name="MagazineUsers[user_phone]" required>-->
		</div>
	<div class="input_wrap">
	<?php echo $form->labelEx($model,'user_role'); ?>
	<?php echo $form->dropDownList($model,'user_role',['2'=>'Менеджер']); ?>
	<?php echo $form->error($model,'user_role'); ?>
		</div>

		<button class="bg_color_th">Добавить</button>

</div>
<?php $this->endWidget(); ?>

<!--</div><!-- form -->