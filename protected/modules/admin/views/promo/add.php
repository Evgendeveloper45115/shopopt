<!---->
<p class="crm_title">Добавить для клиентов файл: <?=$file->name_file?></p>
<div id="select_all">
    <div class="selector bg_color_th "></div><span class="select_all_title">Выбрать всех</span>
</div>
<form action="" method="post">
    <button id="save_add_client_file" class="bg_color_th">Сохранить</button>

    <div id="client_list_select">
        <!-- start client list-->
        <!--client item single-->

        <?php foreach($UserCompany as $comp) {?><div class="client_item" data-id="<?=$comp->id?>" data-file-id="<?=$file->id?>">
            <div class="selector  <?=(in_array($comp->id,$relations)?'active bg_color_th':'')?>"></div>
<!--            <img src="/html_source/img/logo_client.png" alt="" class="client_logo">-->
            <p class="client_title"><?=$comp->name_company?></p>
            <div class="client_info">
                <p><?=$comp->client_id->user_surname?> <?=$comp->client_id->user_name?></p>
                <p><?=$comp->client_id->user_phone?></p>
                <a href="mailto:<?=$comp->client_id->user_email?>"><?=$comp->client_id->user_email?></a>
            </div>
            <a href="/admin/Clients/index/id/<?=$comp->id?>"  class="more_info color_th_hover css_trans border_hover see_detail">Подробнее <span class="color_th">→</span></a>
    <!--        <a href="#"  class="more_info color_th_hover css_trans border_hover del_item">Удалить</a>-->

        </div><?php } ?>
        <!-- end client list-->
    </div>
</form>