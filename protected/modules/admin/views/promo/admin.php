<div id="align_zone">
    <p class="crm_title">Промоматериалы</p>

    <div id="crm_file_promo">

        <div id="crm_documents_upload">
            <form name="manager_documents_upload" method="POST" action="" enctype="multipart/form-data">
                <input type="file" name="MagazinePromoFiles[url]" required="" class="">
                <button class="bg_color_th">Загрузить материал</button>
            </form>
        </div>

        <div id="crm_file_head">
            <div class="row1 inline_style">Название</div>
            <div class="row2 inline_style">Дата загрузки</div>
            <div class="row3 inline_style">Размер файла</div>
            <div class="row4 inline_style">Скачать</div>
            <div class="row5 inline_style">Удалить</div>
        </div>
        <div id="crm_file_body">
            <?php foreach ($model as $md) {
                $arrayFormats = explode('.', $md->name_file);
                $img = '';
                if (isset($arrayFormats[1])) {
                    if ($arrayFormats[1] == 'docx') {
                        $img = '<div class="icon-format" style="background-position: -4.4vw -0.2vw; margin-right: 1.5vw;"></div>';
                    } elseif ($arrayFormats[1] == 'xlsx') {
                        $img = '<div class="icon-format" style="background-position: 4.4vw -0.2vw;margin-right: 1.5vw;"></div>';
                    } elseif ($arrayFormats[1] == 'pptx') {
                        $img = '<div class="icon-format" style="background-position: 2.2vw -0.2vw;margin-right: 1.5vw;"></div>';
                    } elseif ($arrayFormats[1] == 'pdf') {
                        $img = '<div class="icon-format" style="background-position: -0.1vw -0.2vw;margin-right: 1.5vw;"></div>';
                    } elseif ($arrayFormats[1] == 'jpg') {
                        $img = '<img src="' . $md->url . '" class="inline_style">';
                    } elseif ($arrayFormats[1] == 'png') {
                        $img = '<img src="' . $md->url . '" class="inline_style">';
                    } else {
                        $img = '<div class="icon-format-none" style="background-position: -0.6vw 0;margin-right: 1.5vw;"></div>';
                    }
                }
                ?>
                <div class="crm_file_line">
                <div class="row1 inline_style">
                    <?= $img ?><p class="inline_style"><?= $md->name_file ?></p>
                </div>
                <div class="row2 inline_style">
                    <?= date('d-m-Y', $md->date_create) ?>
                </div>
                <div class="row3 inline_style">
                    <?= MagazineDocuments::HumanBytes($md->file_size) ?>
                </div>
                <div class="row4 inline_style">
                    <a href="<?= $md->url ?>" class="s_a css_trans border_hover" download=""><span
                            class="color_th">↓</span> Скачать</a>
                </div>
                <div class="row5 inline_style">
                    <svg class="svg_stroke_color_hover css_trans" preserveAspectRatio="xMidYMid meet"
                         viewBox="0 0 194 194" xmlns="http://www.w3.org/2000/svg">
                        <g display="inline">
                            <line id="svg_7" y2="2.500002" x2="191.699994" y1="190.900002" x1="2.499994"
                                  stroke-linecap="null" stroke-linejoin="null"></line>
                            <line id="svg_6" y2="191.700002" x2="191.699994" y1="2.500002" x1="2.499994"
                                  stroke-linecap="null" stroke-linejoin="null"></line>
                        </g>
                    </svg>
                </div>
                </div><?php } ?>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(function () {
        var input = document.querySelector("input[type='file']");
        input.onchange = function () {
            this.form.submit();
        }
    });
</script>