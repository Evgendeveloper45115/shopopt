<?php
/* @var $this MagazineDiscountController */
/* @var $model MagazineDiscount */

$this->breadcrumbs=array(
	'Magazine Discounts'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List MagazineDiscount', 'url'=>array('index')),
	array('label'=>'Manage MagazineDiscount', 'url'=>array('admin')),
);
?>

<h1>Create MagazineDiscount</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>