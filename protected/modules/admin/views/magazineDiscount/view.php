<?php
/* @var $this MagazineDiscountController */
/* @var $model MagazineDiscount */

$this->breadcrumbs=array(
	'Magazine Discounts'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List MagazineDiscount', 'url'=>array('index')),
	array('label'=>'Create MagazineDiscount', 'url'=>array('create')),
	array('label'=>'Update MagazineDiscount', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete MagazineDiscount', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage MagazineDiscount', 'url'=>array('admin')),
);
?>

<h1>View MagazineDiscount #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'client_id',
		'persent',
		'sum',
	),
)); ?>
