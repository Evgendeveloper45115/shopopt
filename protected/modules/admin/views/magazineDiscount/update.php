<?php
/* @var $this MagazineDiscountController */
/* @var $model MagazineDiscount */

$this->breadcrumbs=array(
	'Magazine Discounts'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List MagazineDiscount', 'url'=>array('index')),
	array('label'=>'Create MagazineDiscount', 'url'=>array('create')),
	array('label'=>'View MagazineDiscount', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage MagazineDiscount', 'url'=>array('admin')),
);
?>

<h1>Update MagazineDiscount <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>