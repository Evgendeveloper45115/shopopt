<?php
/* @var $this MagazineDiscountController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Magazine Discounts',
);

$this->menu=array(
	array('label'=>'Create MagazineDiscount', 'url'=>array('create')),
	array('label'=>'Manage MagazineDiscount', 'url'=>array('admin')),
);
?>

<h1>Magazine Discounts</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
