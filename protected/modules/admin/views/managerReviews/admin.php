<div id="align_zone">
    <p class="crm_title">Отзывы о менеджерах</p>
    <div>
        <div class="white_fon review_box">
            <?php if(!empty($maganersReviews)){?>
            <?php foreach($maganersReviews as $manReview) {?>
                <div class="review_box_item">
            <div class="inline_style review_l">
                <div class="review_info_line">
                    <div class="row1 inline_style">
                        О менеджере:
                    </div><div class="row2 inline_style">
                        <div class="revie_img inline_style">
                            <div class="img_fon" style="background-image:url(<?=$manReview->info->user_avatar?>)"></div>
                        </div><p class="inline_style"><?=$manReview->info->user_name?><br/><?=$manReview->info->user_surname?></p>

                    </div>

                </div>
                <div class="review_info_line">
                    <div class="row1 inline_style">
                        От кого:
                    </div><div class="row2 inline_style">
                        <?=$manReview->name?> <br/><?=$manReview->phone?>
                    </div>
                </div>
            </div><div class="inline_style review_r">
                <p class="review_title">Отзыв:</p>
                <div class="review_content">
                    <?=$manReview->text_review?>
                </div>
                    <?php if($manReview->text_answer) { ?>
                        <form name="review" class="review_form">

                            <div class="box_review inline_style"></div>
                            <textarea class="inline_style" name="review_answer" required placeholder="Введите текст"><?=$manReview->text_answer?></textarea>
                        </form>
                <?php }  else {?>
                <form name="review" class="review_form" action="" method="post">
                    <input type="hidden" name="review_id" value="<?=$manReview->id?>">
                    <div class="box_review inline_style"></div>
                    <textarea class="inline_style" name="review_answer" required placeholder="Введите текст"></textarea>
                    <button class="bg_color_th">Отправить клиенту</button>
                </form>
                <?php }?>
                    </div>
            </div>
            <?php }?>
            <?php } else {?>
                <p>О менеджере еще не оставили отзыв</p>
            <?php }?>
        </div>
    </div>



</div>
