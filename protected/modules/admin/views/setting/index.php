<?php
/* @var $this SettingController */
/* @var $dataModel array */
/* @var $modelSaveSettings array */

//echo  CHtml::errorSummary($modelSaveSettings);

?>
<p class="crm_title">Настройки сайта</p>
<div class="white_fon form_style" style="margin-left:25px;width: 500px; ">
    <?php
    echo CHtml::beginForm('', 'post', ['enctype' => 'multipart/form-data']);
    if (array_key_exists('color_site', $dataModel)) {
        echo '<div class="site_color"><p>Цвет сайта</p>' . CHtml::colorField('color_site', $dataModel['color_site'], ['class' => 'css_trans']) . '</div>';
    } else {
        echo '<div class="site_color"><p>Цвет сайта</p>' . CHtml::colorField('color_site', '', ['class' => 'css_trans']) . '</div>';
    }
    ?>
    <div id="akz_file_input_img">
        <p class="inline_style pub_form_label_p">Логотип</p>
        <div id="wrap_eto">
            <div class="img_file_wrap">
                <div class="img_file_wrap_i">
                    <?php
                    //                var_dump($dataModel);
                    if (!array_key_exists('logo_site', $dataModel)) {
                        ?>
                        <input type="file" class="publiser_file_input" name="logo_site" required value="">
                    <?php } else { ?>
                        <input type="file" class="publiser_file_input" name="logo_site"
                               value="/upload/<?= $dataModel['logo_site'] ?>">
                        <p class="bg_color_th"></p>
                    <?php } ?>
                </div>
            </div>
            <div class="img_file_preview">
                <?php if (!array_key_exists('logo_site', $dataModel)) { ?>
                    <img src="/html_source/img/pub_preview2.png">
                <?php } else { ?>
                    <img src="/upload/<?= $dataModel['logo_site'] ?>">
                <?php } ?>
            </div></div></div>
    <?= CHtml::submitButton('Сохранить', ['name' => 'save', 'class' => 'bg_color_th sett_save']);
    echo CHtml::endForm();
    ?>
</div>


