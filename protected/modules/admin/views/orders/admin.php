<p class="crm_title">Заказы</p>
<ul id="crm_menu">
    <li class="inline_style"><a href="/admin/orders/admin" class="color_th_hover <?=($_GET['status']?'':'active ')?>css_trans">Новые заказы<span class="bg_color_th css_trans"></span></a></li>
    <li class="inline_style"><a href="/admin/orders/admin?status=1,2" class="color_th_hover <?=($_GET['status'] == '1,2'?'active':'')?>  css_trans">Заказы в работе<span class="bg_color_th css_trans"></span></a></li>
    <li class="inline_style"><a href="/admin/orders/admin?status=3" class="color_th_hover <?=($_GET['status'] == '3'?'active':'')?> css_trans">История заказов<span class="bg_color_th css_trans"></span></a></li>
    <li class="inline_style" style="float: right"><span style="margin-right: 2vw">За Период:</span>
        <span class="begin_date" style="display: none"></span>
        <span class="end_date" style="display: none"></span>
        <button class="click_admin" style="display: none"></button>

        <span style="margin: 0 0.5vw">С:</span> <?= $this->widget('zii.widgets.jui.CJuiDatePicker', array(
            'name' => 'from_date',
            // additional javascript options for the date picker plugin
            'language'=>'ru',
            'options' => array(
                'dateFormat'=>'yy-mm-dd',
                'showAnim' => 'fold',
                'buttonImageOnly' => true,
                'changeMonth' => true,
                'changeYear' => true,
                'showButtonPanel' => true,
                'showOtherMonths' => true,
                'onSelect'=> 'js: function(dateText, inst) {
                            $(".begin_date").text(dateText)
                            $(".click_admin").click()
                        }',
            ),
            'htmlOptions' => array(
                'style' => 'height:20px;'
            ),
        ), true); ?>
        <span style="margin: 0 0.5vw">По:</span> <?= $this->widget('zii.widgets.jui.CJuiDatePicker', array(
            'name' => 'publishDate2',
            // additional javascript options for the date picker plugin
            'language'=>'ru',
            'options' => array(
                'dateFormat'=>'yy-mm-dd',
                'showAnim' => 'fold',
                'buttonImageOnly' => true,
                'changeMonth' => true,
                'changeYear' => true,
                'showButtonPanel' => true,
                'showOtherMonths' => true,
                'onSelect'=> 'js: function(dateText, inst) {
                            $(".end_date").text(dateText)
                            $(".click_admin").click()
                        }',
            ),
            'htmlOptions' => array(
                'style' => 'height:20px;'
            ),
        ), true); ?>
    </li>

</ul>

<div id="order_wrap">
    <?php foreach ($model as $md) { ?>
        <div class="order_item inline_style">
            <div class="inline_style order_left_row">
                <p class="order_title">Заказ <b>№<?=$md->id?></b> на <b><span class="money"><?=$md->price?></span> руб.</b></p>
                <p>Поступил: <?=date('d.m.Y H:s',strtotime($md->time_create))?></p>
                <p>Адреса магазинов: <?=$md->magazine_address->address?></p>
                <p>Статус заказа: <b><?php switch($md->status) {
                            case MagazineOrder::STATUS_NEW:
                                echo 'новый';
                                break;
                            case MagazineOrder::STATUS_COLLECTED:
                                echo 'собирается';
                                break;
                            case MagazineOrder::STATUS_SHIPPED:
                                echo 'отгружен';
                                break;
                            case MagazineOrder::STATUS_CLOSE:
                                echo 'закрыт';
                                break;
                        }?></b></p>
                <?php if($md->manager_id != null) {?>
                <div class="order_manager_info">
                    <div class="img_fon" style="background-image:url(<?=$md->managerInfo->user_avatar?>)"></div>
                    <div class="inline_style">
                        <p>Менеджер:</p>
                        <p><?=$md->managerInfo->user_name?> <?=$md->managerInfo->user_surname?></p>
                    </div>
                </div>
                <?php }?>
            </div><div class="inline_style order_right_row">
                <?php if($md->countFiles > 0) {?>
                <a href="/admin/MagazineDocuments/index/id/<?=$md->id?>" class="css_trans color_th_hover border_hover">Документы по заказу</a>
                <?php } else { ?>
                    <a href="/admin/MagazineDocuments/index/id/<?=$md->id?>" class="css_trans color_th_hover border_hover">Документов нет</a>
                <?php } ?>
                            <a href="/admin/orders/index/id/<?=$md->id?>" class="css_trans color_th_hover border_hover">Подробнее <span class="color_th">→</span></a>
            </div>
        </div>
    <?php } ?>
</div>

    <!-- order list end-->