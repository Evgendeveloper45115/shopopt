<script type="text/javascript">
    $(function(){
        var input = document.querySelector("input[type='file']");
        input.onchange = function () {
            this.form.submit();
        }
    });
</script>
<div id="align_zone">
    <div id="manager_short_info">
        <div class="img_fon inline_style" style="background-image:url(<?=$manager_info->user_avatar?>)"></div>
        <div class="inline_style">
            <p><?=$manager_info->user_name?></p>
            <p><?=$manager_info->user_surname?></p>
        </div>
        <div class="inline_style">
            <p><?=$manager_info->user_phone?></p>
            <p>e-mail: <?=$manager_info->user_email?></p>
        </div>
    </div>

    <a href="/admin/orders/admin" class="back_to_orders">← Вернуться к списку заказов</a>
    <div>
        <div class="inline_style" id="manager_orders_l">
            <div class="white_fon">
                <div id="order_head">
                    <p class="order_title">Заказ <b>№<?=$model->id?></b> на <b><span class="money"><?=$model->price?></span> руб.</b> </p>
                    <div class="order_des">
                        <p>Клиент: ООО "торговая компания"</p>
                        <p>Поступил: <?=date('d.m.Y H:s',strtotime($model->time_create))?></p>
                        <?php if($model->time_order != ''){?>
                            <p>Ожидаемое прибытие:<?=$model->time_order?></p>
                        <?php } ?>
                        <p>Статус заказа: <b><?php switch($model->status) {
                                    case MagazineOrder::STATUS_NEW:
                                        echo 'новый';
                                        break;
                                    case MagazineOrder::STATUS_COLLECTED:
                                        echo 'собирается';
                                        break;
                                    case MagazineOrder::STATUS_SHIPPED:
                                        echo 'отгружен';
                                        break;
                                    case MagazineOrder::STATUS_CLOSE:
                                        echo 'закрыт';
                                        break;
                                }?></b></p>

                    </div>
                </div>
                <div id="manager_button_box">
                    <div id="hid_status" class="hid_box">
<!--                        <form name="cd_status" action="" method="post">-->
<!--                            <div class="hid_cd_status">-->
<!--                                <input type="radio" name="status" value="0">-->
<!--                                <p class="color_th_hover ">Новый</p>-->
<!--                            </div>-->
<!--                            <div class="hid_cd_status">-->
<!--                                <input type="radio" name="status" value="1">-->
<!--                                <p  class="color_th_hover css_trans">Собирается</p>-->
<!--                            </div>-->
<!--                            <div class="hid_cd_status">-->
<!--                                <input type="radio" name="status" value="2">-->
<!--                                <p  class="color_th_hover css_trans">Отгружен</p>-->
<!--                            </div>-->
<!--                            <div class="hid_cd_status">-->
<!--                                <input type="radio" name="status" value="3">-->
<!--                                <p  class="color_th_hover css_trans">Закрыт</p>-->
<!--                            </div>-->
<!--                        </form>-->
                    </div>
<!--                    --><?php //if($model->time_come == ''){?>
<!--                        <button id="marker_data"  class="css_trans">Уведомить клиента о том, что заказ принят</button>-->
<!--                    --><?php //}?>
                    <div id="hid_marker_form" class="hid_box">
                        <form name="manager_order_data" action="" method="post">
                            <input type="hidden" name="data_manager_order">
                        </form>
                        <p id="calendar_title">Выберите дату поставки</p>
                        <table id="calendar2">
                            <thead class="color_th">
                            <tr><td>‹<td colspan="5"><td>›</tr>
                            <tr><td>Пн<td>Вт<td>Ср<td>Чт<td>Пт<td>Сб<td>Вс</tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                        <div id="calendar_legend">
                            <span class="legend_today"></span><span> - сегодня </span><span class="legend_active bg_color_th"></span><span> - выбранный день</span>
                        </div>
                        <button class="border_hover css_trans">Уведомить клиента</button>
                    </div>
                </div>

                <!--cart_order-->
                <div id="cart_item_table">
                    <div id="cart_item_head">
                        <div class="row1 inline_style">
                            Фото
                        </div><div class="row2 inline_style">
                            Наименование
                        </div><div class="row3 inline_style">
                            Цена
                        </div><div class="row4 inline_style">
                            Количество
                        </div><div class="row5 inline_style">
                            Стоимость
                        </div>
                    </div>
                    <div id="cart_item_body">
                        <!-- cart_item_line-->
                        <!-- single cart_item_line-->
                        <?php foreach ($orderInfo as $info) {
                            if ($info->image_product) {
                                $json = json_decode($info->image_product);
                                $img = null;
                                if (!empty($json)) {
                                    foreach ($json as $image) {
                                        if ($image != '' || stristr($image, '[')) {
                                            $img = $image;
                                            break;
                                        }
                                    }
                                }
                            }
                            ?>


                            <div class="cart_item_line" data-id="<?=$info->id?>">
                                <div class="row1 inline_style img_fon" style="background-image:url(<?=$img?>)">

                                </div><div class="row2 inline_style">
                                    <?=$info->name_product?>
                                </div><div class="row3 inline_style">
                                    <span class="money"><?=$info->price?></span><span class="rub_lt">a</span>
                                </div><div class="row4 inline_style">
                                    <?php
                                    $magazineProduct = MagazineProduct::model()->findByPk($info->goods_id);
                                    echo $info->goods_count /* echo $model->status == MagazineOrder::STATUS_NEW ||  $model->status == MagazineOrder::STATUS_COLLECTED || $model->status == MagazineOrder::STATUS_SHIPPED? $this->widget(
                                        'booster.widgets.TbEditableField',
                                        array(
                                            'model' => $info,
                                            'emptytext' => 'Не назначено',
                                            'title' => 'Изменить количество товара',
                                            'htmlOptions' => [
                                                'style' => 'border-bottom: none;'
                                            ],
                                            'attribute' => 'goods_count',
                                            'text' => $info->goods_count ? $info->goods_count : null,
                                            //  'url' => '/manager/MagazineOrder/setNewGoodsCount/client_id/'.$model->client_id,
                                            'options' => array(
                                                'send' => false
                                            ),

                                            'validate' => 'js: function(value) {
                                                 if($.trim(value) < '.$magazineProduct->min_count.') return "количество должно быть не меньше '.$magazineProduct->min_count.'";
                                            }',
                                            'onSave' => 'js: function(e, params) {
                                                $(this).closest(".cart_item_line").addClass("edit_item");
                                                var allCost = $(this).closest(".cart_item_line").find(".row3 .money").text();
                                                 var t = allCost.replace(" ","");
                                                 var summa = t * params.newValue;
                                                 var summa = String(summa);
                                                summa = summa.replace(/(\s)+/g, "").replace(/(\d{1,3})(?=(?:\d{3})+$)/g, "$1 ");
                                                 $(this).closest(".cart_item_line").find(".row5 .money").text(summa);
                                                 var allCost = 0;
                                                 $(".cart_item_line").each(function(){
                                                 var noPr = $(this).find(".row5 .money").text();
                                                 noPr = noPr.replace(" ","");
                                                 noPr = noPr.replace(" ","");
                                                 allCost = (Number(allCost) + Number(noPr));
                                                 allCost = String(allCost)
                                                 })
                                                allCost = allCost.replace(/(\s)+/g, "").replace(/(\d{1,3})(?=(?:\d{3})+$)/g, "$1 ");
                                                 var all = $("#cart_item_footer .inline_style b .money").text(allCost);
                                             }'
                                        ), true
                                    ) : $info->goods_count;*/

                                    ?>

                                </div><div class="row5 inline_style">
                                    <span class="money"><?=$info->price*$info->goods_count?></span><span class="rub_lt">c</span>
                                </div>
                            </div>
                        <?php }
                        ?>
                        <!--end single cart_item_line-->


                        <!--end cart_item_line-->
                    </div>
                    <div id="cart_item_footer">
                        <div class="inline_style cart_itog">
                            <span>Итого<span class="cart_itog_dop"> со скидкой</span>:</span><b><span class="money"><?=$model->price?></span><span class="rub_lt">c</span></b>
                        </div>

                    </div>
                    <div style="text-align: end">
                        <button id="save_result" class="css_trans save_button">Сохранить</button>
                    </div>

                </div>
                <!--cart_order-->
            </div>

            <p class="manager_title">Документооборот</p>
            <div id="manager_order_doc_view" class="white_fon">
                <?php foreach($documents as $document) {?>
                    <!-- file list-->
                    <div class="manager_order_doc_line">
                        <img src="<?=$document->url?>"><div class="inline_style">
                            <p><?=$document->name?></p>
                            <!--                        <span class="new">Новый</span>-->
                        </div><a href="<?=$document->url?>"  download=""><span class="color_th">↓</span> Скачать</a>
                    </div>
                <?php }?>
                <!-- file list end-->
                <div id="manager_order_file_load">
                    <form name="manager_order_file_load" action="" method="POST"   enctype="multipart/form-data">
                        <input type="file" name="MagazineDocuments[url]">
                        <button class="border_hover">Прикрепить документ</button>
                    </form>
                </div>
            </div>

        </div><div class="inline_style" id="manager_orders_r">
            <p class="manager_title">Магазин для поставки</p>
            <!--magaz_box-->
            <div class="magaz_info_box white_fon">
                <p class="magaz_info_box_title color_th"><?=$magazineInfo->address?></p>
                <p class="magaz_info_box_contact_title">Контактное лицо  (приемщик):</p>
                <div class="magaz_info_box_contact_wrap">
                    <p><?=$magazineInfo->name_contact?> <?=$magazineInfo->surname_contact?></p>
                    <p><?=$magazineInfo->phone?></p>
                    <p>e-mail: <?=$magazineInfo->email?></p>
                </div>
                <div class="magaz_info_dop_content">
                    <?=$magazineInfo->text_contact?>
                </div>
            </div>
            <!--magaz_box end-->
        </div>
    </div>

</div>