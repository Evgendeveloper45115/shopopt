<p class="crm_title">ABC-анализ</p>
<ul id="crm_menu">
    <li class="inline_style"><a href="/admin/Abc/admin?all_time"
                                class="color_th_hover <?= (isset($_GET['all_time']) ? 'active' : '') ?> css_trans">За
            все время<span class="bg_color_th css_trans"></span></a></li>
    <li class="inline_style" style="float: right"><span style="margin-right: 2vw"><?= 'За период:' ?></span>
        <span class="begin_date" style="display: none"></span>
        <span class="end_date" style="display: none"></span>
        <button class="click_abc" style="display: none"></button>

        <span style="margin: 0 0.5vw">С:</span> <?= $this->widget('zii.widgets.jui.CJuiDatePicker', array(
            'name' => 'from_date',
            // additional javascript options for the date picker plugin
            'language' => 'ru',
            'options' => array(
                'dateFormat' => 'yy-mm-dd',
                'showAnim' => 'fold',
                'buttonImageOnly' => true,
                'changeMonth' => true,
                'changeYear' => true,
                'showButtonPanel' => true,
                'showOtherMonths' => true,
                'onSelect' => 'js: function(dateText, inst) {
                            $(".begin_date").text(dateText)
                            $(".click_abc").click()
                        }',
            ),
            'htmlOptions' => array(
                'style' => 'height:20px;'
            ),
        ), true); ?>
        <span style="margin: 0 0.5vw">По:</span> <?= $this->widget('zii.widgets.jui.CJuiDatePicker', array(
            'name' => 'publishDate2',
            // additional javascript options for the date picker plugin
            'language' => 'ru',
            'options' => array(
                'dateFormat' => 'yy-mm-dd',
                'showAnim' => 'fold',
                'buttonImageOnly' => true,
                'changeMonth' => true,
                'changeYear' => true,
                'showButtonPanel' => true,
                'showOtherMonths' => true,
                'onSelect' => 'js: function(dateText, inst) {
                            $(".end_date").text(dateText)
                            $(".click_abc").click()
                        }',
            ),
            'htmlOptions' => array(
                'style' => 'height:20px;'
            ),
        ), true); ?>
    </li>
</ul>
<div id="abc_table">
    <div id="abc_head_table">
        <div class="inline_style row1">Наименование</div>
        <div class="inline_style row2 sort-false" data-name="count">Количество</div>
        <div class="inline_style row3 sort-false" data-name="price">Цена</div>
        <div class="inline_style row4 sort-false" data-name="allPrice">Стоимость</div>
    </div>
    <div id="abc_body_table">
        <?php
        $i = 0;
        foreach ($allTovars as $tv) {
            if (($tv['price'] * $tv['count']) > $countPrices) {
                $class = 'abc_line';
            } else {
                $class = 'abc_line_no';
            }
            if ($i == 0 && $class == 'abc_line_no') {
                $class = 'abc_line';
            }
            $i++;
            ?>
            <div class="<?= $class ?> item color_th_hover svg_fill_color_hover">
                <div class="inline_style css_trans row1"><?= $tv['name_product'] ?></div>
                <div class="inline_style css_trans row2"><?= $tv['count'] ?></div>
                <div class="inline_style css_trans row3"><?= $tv['price'] ?>
                    <svg preserveAspectRatio="xMidYMid meet" viewBox="0 0 3839 5077" xmlns="http://www.w3.org/2000/svg">


                        <metadata
                            transform="translate(10,0) translate(1,1) translate(7,7) scale(0.9891065359115601,1) translate(-7,-7) "
                            stroke="null" id="svg_2"/>
                        <path stroke="null" fill="" fill-rule="nonzero" id="svg_11"
                              d="m3668.416992,797.991333c-80.117676,-209 -196.832031,-333 -320.470215,-450c-123.638672,-117 -343.220215,-230 -525.21582,-269c-131.55127,-34 -324.426758,-49 -571.703125,-49l-1922.823242,0l0,2319.999878l-314.536011,0l0,520l314.536011,0l0,211l-314.536011,0l0,520l314.536011,0l0,1437l550.932129,0l0,-1437l1974.256836,0l0,-520l-1974.256836,0l0,-211l34.618652,0l1380.792969,0c531.149902,0 911.956055,-150 1149.341797,-442c226.505371,-279 364.97998,-523 364.97998,-951c0,-248 -68.248047,-476 -141.441895,-679.999878l0.98877,1zm-724.025879,1313.999878l0,0c-138.475098,160 -321.459473,238 -641.930176,238l-1387.716309,0l0,0l-34.618896,0l0,-1786.999878l1411.455322,0c225.516113,0 381.794922,15 461.912598,44c132.540039,85 236.396484,151 313.546875,295.999878c80.117676,151 119.681641,330 119.681641,534c0,291 -111.769043,520 -243.320313,675l0.989258,0z"/>

                    </svg>
                </div>
                <div class="inline_style css_trans row4">
                    <span class="money"><?= $tv['price'] * $tv['count'] ?></span>
                    <svg preserveAspectRatio="xMidYMid meet" viewBox="0 0 3839 5077" xmlns="http://www.w3.org/2000/svg">


                        <metadata
                            transform="translate(10,0) translate(1,1) translate(7,7) scale(0.9891065359115601,1) translate(-7,-7) "
                            stroke="null" id="svg_2"/>
                        <path stroke="null" fill="" fill-rule="nonzero" id="svg_11"
                              d="m3668.416992,797.991333c-80.117676,-209 -196.832031,-333 -320.470215,-450c-123.638672,-117 -343.220215,-230 -525.21582,-269c-131.55127,-34 -324.426758,-49 -571.703125,-49l-1922.823242,0l0,2319.999878l-314.536011,0l0,520l314.536011,0l0,211l-314.536011,0l0,520l314.536011,0l0,1437l550.932129,0l0,-1437l1974.256836,0l0,-520l-1974.256836,0l0,-211l34.618652,0l1380.792969,0c531.149902,0 911.956055,-150 1149.341797,-442c226.505371,-279 364.97998,-523 364.97998,-951c0,-248 -68.248047,-476 -141.441895,-679.999878l0.98877,1zm-724.025879,1313.999878l0,0c-138.475098,160 -321.459473,238 -641.930176,238l-1387.716309,0l0,0l-34.618896,0l0,-1786.999878l1411.455322,0c225.516113,0 381.794922,15 461.912598,44c132.540039,85 236.396484,151 313.546875,295.999878c80.117676,151 119.681641,330 119.681641,534c0,291 -111.769043,520 -243.320313,675l0.989258,0z"/>

                    </svg>
                </div>
            </div>
        <?php } ?>
        <!--abc line content end-->
    </div>
</div>
<!--<div id="crm_pagination">-->
<!--    <!-- active == class="bg_color_th"-->
<!--    <a href="#" class="bg_color_th css_trans"><span>1</span></a>-->
<!--    <a href="#" class="bg_color_th_hover css_trans"><span>2</span></a>-->
<!--    <a href="#" class="bg_color_th_hover css_trans"><span>3</span></a>-->
<!--    <a href="#" class="bg_color_th_hover v"><span>Все</span></a>-->
<!--</div>-->