<script type="text/javascript">
    <!-- php json_encode(array())-->
    var content_item = '<?=addslashes($contentTovar)?>';

    var contet_akz = '<?=$contentStock?>';
    var idCompany = <?=$id?>;
</script>
<div id="align_zone">
    <div id="manager_short_info">
        <div class="img_fon inline_style" style="background-image:url(<?= $mangerInfo->user_avatar ?>)"></div>
        <div class="inline_style">
            <p><?= $mangerInfo->user_name ?></p>

            <p><?= $mangerInfo->user_surname ?></p>
        </div>
        <div class="inline_style">
            <p><?= $mangerInfo->user_phone ?></p>

            <p>e-mail: <?= $mangerInfo->user_email ?></p>
        </div>
    </div>
    <?php if (Yii::app()->user->getRole() == WebUser::ADMIN) { ?>
        <a href="/admin/Clients/admin" class="back_to_orders">← Вернуться к списку клиентов</a>
    <?php } else { ?>
        <a href="/manager/MagazineClientInfo/index" class="back_to_orders">← Вернуться к списку клиентов</a>
    <?php } ?>
    <div class="white_fon client_info_line">
        <div class="inline_style img_cont">
            <!--            <img src="/html_source/img/logo_client.png">-->
        </div>
        <div class="inline_style title">
            <?= $companyInfo->name_company ?>
        </div>
        <div class="inline_style link_box">
            <!--            <a href="#" class="inline_style show_client_cart border_hover css_trans">Показать карточку клиента</a>-->
            <div class="short_line inline_style">
                <p><?= $clinetInfo->user_name ?> <?= $clinetInfo->user_surname ?></p>

                <p><?= $clinetInfo->user_phone ?></p>

                <p>e-mail: <a href="mailto:<?= $clinetInfo->user_email ?>"><?= $clinetInfo->user_email ?></a></p>
            </div>
            <a href="/admin/MagazineUsers/update/id/<?= $clinetInfo->id ?>"
               class="inline_style cd_client border_hover css_trans">Изменить <span class="color_th">→</span></a>
        </div>
        <div class="set_manager_block" style="position: relative; bottom: 2.2vw;">
            <div id="manager_dop_info_set" style="top: 3vw; float:right;">
                <a href="#" class="bg_color_th set_manager" name="on" data-id="<?= $id ?>">Подтвердить</a>
            </div>
            <div id="select_manager" style="right: 0; position: relative; bottom: 0.6vw; margin-right: 1.5vw; float: right">
                <span style="margin-right: 0.5vw">Выберите менеджера</span>
                <select style="width: 12.3vw;!important" class="input_publisher"
                        name="MagazineUsers[manager_id]" id="select_new_manager">
                    <?php foreach (MagazineUsers::model()->findAllByAttributes(['user_role' => WebUser::MANAGER]) as $key => $manager) {
                        echo '<option value="' . $manager->id . '">' . $manager->user_email . '</option>';
                    } ?>
                </select>
            </div>

        </div>

    </div>

    <div class="manager_box_row">
        <div class="inline_style">
            <p class="manager_title">Новые заказы:</p>

            <div class="white_fon">
                <!--orders_item-->
                <?php if (!empty($orderNews)) {
                    ?>
                    <?php foreach ($orderNews as $key => $order) { ?>
                        <div class="manager_client_page_order">
                            <p class="order_title">Заказ <b>№<?= $order->id ?></b> на <b><span
                                        class="money"><?= $order->price ?></span> руб.</b>
                                <?php if (Yii::app()->user->getRole() == WebUser::MANAGER) { ?>
                                    <a href="/manager/MagazineOrder/ShowOrder/id/<?= $order->id ?>"
                                       class="s_a border_hover css_trans">Подробнее <span class="color_th">→</span></a>
                                <?php } else { ?>
                                    <a href="/admin/orders/index/id/<?= $order->id ?>"
                                       class="s_a border_hover css_trans">Подробнее <span class="color_th">→</span></a>
                                <?php } ?>
                            </p>
                            <?php
                            ?>
                            <div class="order_des">
                                <p>Поступил: <?= date('d.m.Y H:i', strtotime($order->time_create)) ?></p>

                                <p>Адрес доставки: <?= $companyInfo->name_company ?></p>

                                <p>Статус заказа: <b><?php switch ($order->status) {
                                            case MagazineOrder::STATUS_NEW:
                                                echo 'новый';
                                                break;
                                            case MagazineOrder::STATUS_COLLECTED:
                                                echo 'собирается';
                                                break;
                                            case MagazineOrder::STATUS_SHIPPED:
                                                echo 'отгружен';
                                                break;
                                            case MagazineOrder::STATUS_CLOSE:
                                                echo 'закрыт';
                                                break;
                                        } ?></b></p>
                            </div>
                            <?php
                            if (Yii::app()->user->getRole() == WebUser::MANAGER) { ?>
                                <a href="/manager/MagazineDocuments/index/id/<?= $order->id ?>"
                                   class="link_in_page_order border_hover css_trans">Документы по заказу</a>
                            <?php } else { ?>
                                <a href="/admin/MagazineDocuments/index/id/<?= $order->id ?>"
                                   class="link_in_page_order border_hover css_trans">Документы по заказу</a>
                            <?php } ?>
                        </div>
                    <?php } ?>
                <?php } else { ?>
                    <p>у клиента нет новых заказов</p>
                <?php } ?>
                <?php if (!empty($orderNews)) { ?>
                    <a href="/admin/Orders/admin" class="show_more_orders  color_th_hover css_trans">Показать еще ↓</a>
                <?php } ?>
            </div>
        </div>
        <div class="inline_style">
            <p class="manager_title">Заказы в работе:</p>

            <div class="white_fon">
                <!--orders item-->
                <?php if (!empty($orderWork)) { ?>
                    <?php foreach ($orderWork as $key => $order) { ?>
                        <div class="manager_client_page_order">
                            <p class="order_title">Заказ <b>№<?= $order->id ?></b> на <b><span
                                        class="money"><?= $order->price ?></span>
                                    руб.</b> <?php if (Yii::app()->user->getRole() == WebUser::MANAGER) { ?>
                                    <a href="/manager/MagazineOrder/ShowOrder/id/<?= $order->id ?>"
                                       class="s_a border_hover css_trans">Подробнее <span class="color_th">→</span></a>
                                <?php } else { ?>
                                    <a href="/admin/orders/index/id/<?= $order->id ?>"
                                       class="s_a border_hover css_trans">Подробнее <span class="color_th">→</span></a>
                                <?php } ?></p>

                            <div class="order_des">
                                <p>Поступил: <?= date('d.m.Y H:s', strtotime($order->time_order)) ?></p>

                                <p>Адрес доставки: <?= $companyInfo->name_company ?></p>

                                <p>Статус заказа: <b><?php switch ($order->status) {
                                            case MagazineOrder::STATUS_NEW:
                                                echo 'новый';
                                                break;
                                            case MagazineOrder::STATUS_COLLECTED:
                                                echo 'собирается';
                                                break;
                                            case MagazineOrder::STATUS_SHIPPED:
                                                echo 'отгружен';
                                                break;
                                            case MagazineOrder::STATUS_CLOSE:
                                                echo 'закрыт';
                                                break;
                                        } ?></b></p>
                            </div>
                            <?php
                            if (Yii::app()->user->getRole() == WebUser::MANAGER) { ?>
                                <a href="/manager/MagazineDocuments/index/id/<?= $order->id ?>"
                                   class="link_in_page_order border_hover css_trans">Документы по заказу</a>
                            <?php } else { ?>
                                <a href="/admin/MagazineDocuments/index/id/<?= $order->id ?>"
                                   class="link_in_page_order border_hover css_trans">Документы по заказу</a>
                            <?php } ?>
                        </div>
                    <?php } ?>
                <?php } else { ?>
                    <p>у клиента нет активных заказов</p>
                <?php } ?>
                <!--orders_item one end-->
                <!--order item end-->
                <?php if (!empty($orderWork)) { ?>
                    <a href="/admin/orders/admin?status=1,2" class="show_more_orders color_th_hover css_trans">Показать
                        еще ↓</a>
                <?php } ?>
            </div>
        </div>
    </div>
    <div class="manager_box_row">
        <div class="inline_style" style="color: #5d5d5d;">
            <p class="manager_title">О компании:</p>

            <div class="white_fon">
                <div>
                    <span> - ИНН:</span><span><?= $companyInfo->inn ?></span>
                </div>
                <div>
                    <span> - Документы:</span>

                    <div class="imgCompany">
                        <?php
                        if ($companyInfo->image) {
                            $jsonImg = json_decode($companyInfo->image);
                            if (!empty($jsonImg)) {
                                foreach ($jsonImg as $key => $img) {
                                    if ($key > 3) {
                                        break;
                                    } else {
                                        ?>
                                        <a class="preload" href="<?= $img ?>"><img
                                                style="width: 8vw; vertical-align: top; margin-right: 0.3vw"
                                                src="<?= $img ?>"></a>
                                        <?php
                                    }
                                }
                            }

                        }
                        ?>
                    </div>
                    <div style="height: 27px; padding-bottom: 1.3vw">
                    <span>
                        <a href="/admin/MagazineCompany/update/id/<?= $companyInfo->id ?>"
                           class="link_in_page_order border_hover css_trans">Посмотреть всю информацию</a>
                    </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="manager_box_row">
        <div class="inline_style">
            <p class="manager_title">Документооборот</p>

            <div class="white_fon">
                <?php if (!empty($documents)) { ?>
                    <?php foreach ($documents as $doc) {
                        $arrayFormats = explode('.', $doc->name);
                        $img = '';
                        if (isset($arrayFormats[1])) {
                            if ($arrayFormats[1] == 'docx') {
                                $img = '<img src="' . $doc->url . '" style="display: none"> <div class="icon-format" style="background-position: -4.4vw -0.2vw;margin-right: 0.7%"></div>';
                            } elseif ($arrayFormats[1] == 'xlsx') {
                                $img = '<div class="icon-format" style="background-position: 4.4vw -0.2vw; margin-right: 0.7%"></div>';
                            } elseif ($arrayFormats[1] == 'pptx') {
                                $img = '<div class="icon-format" style="background-position: 2.2vw -0.2vw;margin-right: 0.7%"></div>';
                            } elseif ($arrayFormats[1] == 'pdf') {
                                $img = '<div class="icon-format" style="background-position: -0.1vw -0.2vw;margin-right: 0.7%"></div>';
                            } elseif ($arrayFormats[1] == 'jpg') {
                                $img = '<img src="' . $doc->url . '" class="d_inline">';
                            } elseif ($arrayFormats[1] == 'png') {
                                $img = '<img src="' . $doc->url . '" class="d_inline">';
                            } else {
                                $img = '<div class="icon-format-none" style="background-position: -0.6vw 0;margin-right: 0.4%"></div>';
                            }
                        }

                        ?>
                        <div class="manager_file_view">
                            <?= $img ?>

                            <p class="d_inline"><?= $doc->name ?></p>
                            <a href="<?= $doc->url ?>" class="s_a border_hover css_trans"><span
                                    class="color_th">↓</span> Скачать</a>
                        </div>
                    <?php } ?>
                <?php } else { ?>
                    <p>У клиента нет документов</p>
                <?php } ?>
                <div class="manager_file_link">
                    <a href="/admin/MagazineDocuments/index/id/" class="s_a border_hover css_trans">Посмотреть все
                        документы <span class="color_th">→</span></a>

                    <div class="manager_all_file_in">
                    </div>
                </div>
            </div>
        </div>
        <div class="inline_style">
            <p class="manager_title">История заказов</p>

            <div class="white_fon">
                <!--hist order-->
                <?php if (!empty($orderHistory)) { ?>
                    <?php foreach ($orderHistory as $key => $order) { ?>
                        <div class="manager_client_page_order">
                            <p class="order_title">Заказ <b>№<?= $order->id ?></b> на <b><span
                                        class="money"><?= $order->price ?></span> руб.</b>
                                <?php if (Yii::app()->user->getRole() == WebUser::MANAGER) { ?>
                                    <a href="/manager/MagazineOrder/ShowOrder/id/<?= $order->id ?>"
                                       class="s_a border_hover css_trans">Подробнее <span class="color_th">→</span></a>
                                <?php } else { ?>
                                    <a href="/admin/orders/index/id/<?= $order->id ?>"
                                       class="s_a border_hover css_trans">Подробнее <span class="color_th">→</span></a>
                                <?php } ?> </p>

                            <div class="order_des">
                                <p>Поступил: <?= date('d.m.Y H:s', strtotime($order->time_order)) ?></p>

                                <p>Адрес доставки: <?= $companyInfo->name_company ?></p>

                                <p>Статус заказа: <b><?php switch ($order->status) {
                                            case MagazineOrder::STATUS_NEW:
                                                echo 'новый';
                                                break;
                                            case MagazineOrder::STATUS_COLLECTED:
                                                echo 'собирается';
                                                break;
                                            case MagazineOrder::STATUS_SHIPPED:
                                                echo 'отгружен';
                                                break;
                                            case MagazineOrder::STATUS_CLOSE:
                                                echo 'закрыт';
                                                break;
                                        } ?></b></p>
                            </div>
                            <?php if (Yii::app()->user->getRole() == WebUser::MANAGER) { ?>
                                <a href="/manager/MagazineDocuments/index/id/<?= $order->id ?>"
                                   class="link_in_page_order border_hover css_trans">Документы по заказу</a>
                            <?php } else { ?>
                                <a href="/admin/MagazineDocuments/index/id/<?= $order->id ?>"
                                   class="link_in_page_order border_hover css_trans">Документы по заказу</a>
                            <?php } ?>

                            <!--                    <a href="#" class="to_work link_in_page_order">В работу</a>-->
                        </div>
                    <?php } ?>
                <?php } else { ?>
                    У клиента нет выполненых заказов
                <?php } ?>
                <!--hist order one end-->
                <!--hist order end-->
                <?php if (!empty($orderHistory)) { ?>
                    <a href="/admin/orders/admin?status=3" class="show_more_orders color_th_hover css_trans">Показать
                        еще ↓</a>
                <?php } ?>
            </div>
        </div>
    </div>

    <div class="manager_box_row">
        <div class="inline_style">
            <p class="manager_title">Промоматериалы</p>

            <div class="white_fon">
                <?php if (!empty($PromoFiles)) {
                    $i = 0;
                    ?>
                    <?php foreach ($PromoFiles as $promoFile) {
                        $i++;
                        if ($i > 5) {
                            break;
                        }
                        $arrayFormats = explode('.', $promoFile->name_file);
                        $img = '';
                        if (isset($arrayFormats[1])) {
                            if ($arrayFormats[1] == 'docx') {
                                $img = '<img src="' . $promoFile->url . '" style="display: none"> <div class="icon-format" style="background-position: -4.4vw -0.2vw;margin-right: 0.7%"></div>';
                            } elseif ($arrayFormats[1] == 'xlsx') {
                                $img = '<div class="icon-format" style="background-position: 4.4vw -0.2vw;margin-right: 0.7%"></div>';
                            } elseif ($arrayFormats[1] == 'pptx') {
                                $img = '<div class="icon-format" style="background-position: 2.2vw -0.2vw;margin-right: 0.7%"></div>';
                            } elseif ($arrayFormats[1] == 'pdf') {
                                $img = '<div class="icon-format" style="background-position: -0.1vw -0.2vw;margin-right: 0.7%"></div>';
                            } elseif ($arrayFormats[1] == 'jpg') {
                                $img = '<img src="' . $promoFile->url . '" class="d_inline">';
                            } elseif ($arrayFormats[1] == 'png') {
                                $img = '<img src="' . $promoFile->url . '" class="d_inline">';
                            } else {
                                $img = '<div class="icon-format-none" style="background-position: -0.6vw 0;margin-right: 0.4%"></div>';
                            }
                        }

                        ?>
                        <div class="manager_file_view">
                            <?= $img ?>

                            <p class="d_inline"><?= $promoFile->name_file ?></p>
                            <a href="<?= $promoFile->url ?>" class="s_a border_hover css_trans"><span
                                    class="color_th">↓</span> Скачать</a>
                        </div>
                    <?php } ?>
                <?php } else { ?>
                    <p>у клиента нет промоматериалов</p>
                <?php } ?>
                <div class="manager_file_link">
                    <?php if (Yii::app()->user->getRole() == WebUser::MANAGER) { ?>
                        <a href="/manager/Promo/index" class="s_a border_hover css_trans">Посмотреть все промоматериалы
                            <span class="color_th">→</span></a>
                    <?php } else { ?>
                        <a href="/admin/Promo/index" class="s_a border_hover css_trans">Посмотреть все промоматериалы
                            <span class="color_th">→</span></a>
                    <?php } ?>

                    <!--                    <div class="manager_all_file_in">-->
                    <!--                        <form name="promo_in" method="POST" action=""  enctype="multipart/form-data">-->
                    <!--                            <input type="file" name="promo_in_in">-->
                    <!--                            <a href="#" class="s_a css_trans">Прикрепить материал</a>-->
                    <!--                        </form>-->
                    <!--                    </div>-->
                </div>
            </div>
        </div>
        <div class="inline_style">
            <p class="manager_title">Адреса доставки</p>

            <div class="white_fon">
                <?php if (!empty($magazineUsers)) { ?>
                    <?php foreach ($magazineUsers as $mag) { ?>
                        <div class="hist_order_cl_page shop_list_cl_page">
                            <p class="hist_l"><?= $mag->address ?></p><a
                                href="/admin/Orders/admin/?idmagazine=<?= $mag->id ?>"
                                class="hist_r s_a border_hover css_trans">Список заказов</a>
                        </div>
                    <?php } ?>
                <?php } else { ?>
                    <p>у клиента нет магазинов</p>
                <?php } ?>
                <!--                <a href="#" class="s_a border_hover css_trans cl_page_top_align">Посмотреть все магазины <span class="color_th">→</span></a>-->
            </div>
        </div>
    </div>

    <div class="manager_box_row">
        <div class="inline_style">
            <p class="manager_title">Скидка клиента</p>

            <div class="white_fon discont_line">
                <p><?= ($getClientDiscount->persent == null ? '0' : $getClientDiscount->persent) ?>% скидка</p>
                <a href="/admin/Clients/Discount/id/<?= $clinetInfo->id ?>" class="s_a border_hover css_trans">Изменить
                    скидку
                    <span class="color_th">→</span></a>

                <p style="font-size: 0.9vw; margin-top: 0.4vw;">Общая сумма заказов за все время: <span
                        class="money"><?= $sBalance ?></span> руб.</p>
            </div>
        </div>
        <div class="inline_style">
            <form action="" method="POST" id="form_disc">
                <p class="manager_title">Минимальный заказ</p>

                <div class="white_fon discont_line">
                    <input value="<?= $clinetInfo->min_order ? $clinetInfo->min_order : null ?>" type="text"
                           name="MagazineUser[min_order]"
                           style="font-size: 1vw;border: 1px solid #c2c2c2;border-radius: 5px; width: 70%; padding: 0.3vw 3%;"
                           class="min_order">
                    <button style="position: initial; float: right;margin: 0;" type="submit"
                            class="manager_select_link bg_color_th">Сохранить
                    </button>
                </div>
            </form>

        </div>
    </div>
    <p class="manager_title">Рекомендуемые предложения<a href="#" class=" goods_add manager_select_link bg_color_th">ДОБАВИТЬ
            ИЗ КАТАЛОГА</a></p>

    <div class="w_c" id="tovari_tut">

        <!--recomend start-->

        <!--single-->
        <?php foreach ($CompanyRecommendTovar as $tv) {
            $links = MagazineProduct::getTree($tv->info, false);
            $json = null;
            if ($tv->info->image_product) {
                $json = json_decode($tv->info->image_product);
            }
            if (!empty($json[0])) {
                if (!stristr($json[0], '[')) {
                    $json = $json[0];
                } else {
                    $json = $json[1];
                }
            }
            if (is_array($json)) {
                $json = $json[1];
            }
            $href = '/catalog/';
            foreach ($links as $link) {
                if ($link != null) {
                    $href .= $link . '/';
                }
            }
            $href .= $tv->info->id;
            $json = json_decode($tv->info->image_product);
            $image = null;
            if (is_array($jsonA)) {
                foreach ($json as $img) {
                    if ($img != '' || !stristr($img, '[')) {
                        $image = $img;
                        break;
                    }

                }
            }
            if ($tv->info->price == null) {
                continue;
            }
            ?>
            <div class="goods_item css_trans shadow_hover" data-id="<?= $tv->info->id ?>">
                <a href="<?= $href ?>">
                    <div class="img_fon" style="background-image:url(<?= $image ?>)"></div>
                    <p><?= $tv->info->name_product ?></p>
                </a>

                <div class="price inline_style color_th"><span class="money"><?= $tv->info->price ?></span><span
                        class="rub_lt">a</span></div>
                <button class="border_hover css_trans color_th_hover" data-id="<?= $tv->info->id ?>">
                    <svg class="svg_stroke_color" preserveAspectRatio="xMidYMid meet" viewBox="0 0 194 194"
                         xmlns="http://www.w3.org/2000/svg">
                        <g display="inline">
                            <line id="svg_7" y2="2.500002" x2="191.699994" y1="190.900002" x1="2.499994"
                                  stroke-linecap="null" stroke-linejoin="null"></line>
                            <line id="svg_6" y2="191.700002" x2="191.699994" y1="2.500002" x1="2.499994"
                                  stroke-linecap="null" stroke-linejoin="null"></line>
                        </g>
                    </svg>
                    Удалить
                </button>
            </div>
        <?php } ?>
        <!--recomend end-->
    </div>
    <!--akz start-->
    <p class="manager_title">Акции клиента<a href="#" class="manager_select_link bg_color_th akz_add">ДОБАВИТЬ АКЦИЮ</a>
    </p>

    <div class="w_c" id="akz_full_wrp">

        <!-- akz_special_item single-->
        <?php foreach ($CompanyRecommendStock as $compStock) {
            if ($compStock->info->header_text == null) {
                continue;
            }
            ?>
        <div class="akz_special_item" data-id="<?= $compStock->info->id ?>">
            <div class="img_fon" style="background-image:url(<?= $compStock->info->image ?>)"></div>
            <div class="demo_akz_txt_cont">
                <p class="demo_akz_title"><?= $compStock->info->header_text ?></p>

                <div><p><?= $compStock->info->text ?></p></div>
                <button class="border_hover css_trans color_th_hover del_akz" data-id="<?= $compStock->info->id ?>">
                    <svg class="svg_stroke_color" preserveAspectRatio="xMidYMid meet" viewBox="0 0 194 194"
                         xmlns="http://www.w3.org/2000/svg">
                        <g display="inline">
                            <line id="svg_7" y2="2.500002" x2="191.699994" y1="190.900002" x1="2.499994"
                                  stroke-linecap="null" stroke-linejoin="null"></line>
                            <line id="svg_6" y2="191.700002" x2="191.699994" y1="2.500002" x1="2.499994"
                                  stroke-linecap="null" stroke-linejoin="null"></line>
                        </g>
                    </svg>
                    Удалить
                </button>
            </div>
            </div><?php } ?>
        <!--akz end-->
    </div>
    <button class="bg_color_th" id="save_client_page">Сохранить</button>

</div>
<script>
    $(function () {
        $('.imgCompany img').each(function () {
            var imgExt = $(this).attr('src').split('.');
            if (imgExt[1] == 'docx') {
                $(this).attr('src', '/html_source/img/docx-icon.png')
            } else if (imgExt[1] == 'xlsx') {
                $(this).attr('src', '/html_source/img/xlsx-icon.png')
            } else if (imgExt[1] == 'pptx') {
                $(this).attr('src', '/html_source/img/pptx-icon.png')
            } else if (imgExt[1] == 'pdf') {
                $(this).attr('src', '/html_source/img/pdf-icon.png')
            }
        });
        $(".preload").attr("rel", "gallery").fancybox({
            afterLoad: function () {
                this.title = this.title ?
                '<a href="' + this.href.replace("press_web", "download")
                    .replace("_web.jpg", ".jpg.zip") +
                '">Скачать</a> ' + this.title
                    :
                '<a download href="' + this.href.replace("press_web", "download")
                    .replace("_web.jpg", ".jpg.zip") +
                '">Скачать</a>';
            },
            helpers: {
                title: {
                    type: 'inside'
                }
            }
        });
    })
</script>
