<?php
/**
 * @var $app CWebApplication
 * @var $bonus MagazineBonus
 */
$app = Yii::app();
?>
<div id="align_zone" style="float: left;width: 44.5%;">
    <p style="padding-top: 15px;padding-bottom: 15px;">Скидка</p>

    <div id="discont_table" class="white_fon">
        <div class="discont_head">
            <div class="row1">Сумма скидки</div>
            <div class="row2">Процент скидки</div>
            <div class="row3"></div>
        </div>
        <div class="discont_body">
            <form action="" method="POST" id="form_disc">
                <!-- если нет скидок оставить тут пустоту, иначае для каждого правила создать html, где {{index}} заменить на порядковый номер начиная с 0 -->
                <?php foreach ($getUserDiscount as $getUsD) { ?>
                    <div class="discont_form_line">
                        <div class="row1"><input type="text" name="MagazineDiscount[sum][]" value="<?= $getUsD->sum ?>"
                                                 required></div>
                        <div class="row2"><input type="text" name="MagazineDiscount[persent][]"
                                                 value="<?= $getUsD->persent ?>" required></div>
                        <div class="row3"><span class="del_line border_hover css_trans"
                                                onclick="$(this).parent().parent().remove();">Удалить<span></div>
                    </div>
                <?php } ?>

                <?php
                if (empty($getUserDiscount)) { ?>
                    <div class="discont_form_line">
                        <div class="row1"><input type="text" name="MagazineDiscount[sum][]" value="" required></div>
                        <div class="row2"><input type="text" name="MagazineDiscount[persent][]" value="" required></div>
                        <div class="row3"><span class="del_line border_hover css_trans"
                                                onclick="$(this).parent().parent().remove();">Удалить<span></div>
                    </div>
                <?php }
                ?>
                <button id="add_disc_line" class="css_trans border_hover">Добавить правило</button>
                <button id="save_disc" name="pay" class="bg_color_th">Сохранить</button>

            </form>
        </div>
    </div>
    <p style="padding-top: 15px;padding-bottom: 15px;">Скидка за предоплату</p>

    <div id="discont_table" class="white_fon">
        <div class="discont_head">
            <div class="row2" style="width: 100%">Процент скидки за предоплату</div>
        </div>
        <div class="discont_body">
            <form action="" method="POST" id="form_disc">
                <!-- если нет скидок оставить тут пустоту, иначае для каждого правила создать html, где {{index}} заменить на порядковый номер начиная с 0 -->
                <div class="discont_form_line_bonus">
                    <div class="row2" style="width: 100%"><input type="text" name="MagazinePrepay[percent]"
                                                                 value="<?= $prepay->percent ?>" required></div>
                </div>
                <button id="save_disc" name="prepay" class="bg_color_th">Сохранить</button>

            </form>
        </div>
    </div>
    <p style="padding-top: 15px;padding-bottom: 15px;">Бонус</p>

    <div id="discont_table" class="white_fon" style="margin-bottom: 11.8vw;">
        <div class="discont_head">
            <div class="row2" style="width: 70%">Бонус</div>
            <div class="row2" style="width: 30%;float:right">Заказ №</div>
        </div>
        <div class="discont_body">
            <form action="" method="POST" id="form_disc">
                <!-- если нет скидок оставить тут пустоту, иначае для каждого правила создать html, где {{index}} заменить на порядковый номер начиная с 0 -->
                <div class="discont_form_line_bonus">
                    <div class="row2" style="width: 70%">
                        <input placeholder="Ввести сумму бонусов" type="text" name="MagazineBonus[active_bonus]"
                               value="<?= $bonus->active_bonus ? $bonus->active_bonus : null ?>" required>

                        <p style="padding-top: 15px;">Бонусов у клиента:
                            <span><?= MagazineBonus::getUserBonus($user_id) ?> р.</span></p>
                    </div>
                    <div class="row2" style="width: 30%;float: right">
                        <input placeholder="Заказ №" type="text" name="MagazineBonus[order_id]"
                               value="<?= $bonus->order_id ? $bonus->order_id : null ?>" required>
                    </div>
                </div>
                <button id="save_disc" name="bonus" class="bg_color_th">Отправить</button>
            </form>
        </div>
    </div>
</div>
<div id="align_zone" style="width: 47.5%;float: right;">
    <p class="client_title">Бонусы клиента <?=$company != null ? '"'.$company->name_company.'"' : null?></p>

    <div class="white_fon discount_desc" style="width: 96%">
        <li class="inline_style"><span style="margin-right: 2vw"><?= 'Период от:' ?></span>
            <span class="begin_date" style="display: none"></span>
            <span class="end_date" style="display: none"></span>
            <input class="user_id" type="hidden" style="display: none" value="<?= $user_id ?>">
            <button class="click_order_bonus_manager" style="display: none"></button>

            <span style="margin: 0 0.5vw">С:</span> <?= $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                'name' => 'from_date',
                // additional javascript options for the date picker plugin
                'language' => 'ru',
                'options' => array(
                    'dateFormat' => 'yy-mm-dd',
                    'showAnim' => 'fold',
                    'buttonImageOnly' => true,
                    'changeMonth' => true,
                    'changeYear' => true,
                    'showButtonPanel' => true,
                    'showOtherMonths' => true,
                    'onSelect' => 'js: function(dateText, inst) {
                            $(".begin_date").text(dateText)
                            $(".click_order_bonus_manager").click()
                        }',
                ),
                'htmlOptions' => array(
                    'style' => 'height:20px;'
                ),
            ), true); ?>
            <span style="margin: 0 0.5vw">До:</span> <?= $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                'name' => 'publishDate2',
                // additional javascript options for the date picker plugin
                'language' => 'ru',
                'options' => array(
                    'dateFormat' => 'yy-mm-dd',
                    'showAnim' => 'fold',
                    'buttonImageOnly' => true,
                    'changeMonth' => true,
                    'changeYear' => true,
                    'showButtonPanel' => true,
                    'showOtherMonths' => true,
                    'onSelect' => 'js: function(dateText, inst) {
                            $(".end_date").text(dateText)
                            $(".click_order_bonus_manager").click()
                        }',
                ),
                'htmlOptions' => array(
                    'style' => 'height:20px;'
                ),
            ), true); ?>
        </li>
        <div class="set_bonus" style="margin-top: 1vw">
            <?php
            if (!empty($setBonuses)) {
                $activeBonus = 0;
                $used_bonus = 0;
                foreach ($setBonuses as $setBonus) {
                    $activeBonus += $setBonus->active_bonus
                    ?>
                    <div class="order_info setBonus">
                <span style="display: inline-block; width: 49%;"> Заказ № <?= $setBonus->order_id ?>
                    от <?= date('Y-m-d H:i', strtotime($setBonus->date_create)) ?></span>
                <span style="display: inline-block; width: 50%;">Начислено: <?= $setBonus->active_bonus ?>
                    руб. бонусных</span>
                </div>
                    <?php
                    if ($setBonus->children != null) {
                        foreach ($setBonus->children as $children) {
                            $used_bonus += $children->used_bonus;
                            ?>
                            <div class="order_info used_bonus">
                        <span
                            style="display: inline-block; width: 49%;"> Заказ № <?= $children->order_id ?>
                            от <?= date('Y-m-d H:i', strtotime($children->date_create)) ?></span>
                        <span
                            style="display: inline-block; width: 50%;">Потрачено: <?= $children->used_bonus ?>
                            руб. бонусных</span>
                            </div>
                            <?php

                        }
                    }
                }
                ?>
                <div class="active_bonus_header" style="display: flex; margin-top: 1vw;     padding-left: 1vw;">
                    <div style="width: 49%">Получено бонусов:</div>
                    <div style="width: 50%">Использовано бонусов:</div>
                </div>
                <div class="active_bonus" style="display: flex;;     padding-left: 1vw;">
                    <div style="width: 49%;color: black"><?= $activeBonus ?> руб.</div>
                    <div style="width: 50%; color: black"><?= $used_bonus ?> руб.</div>
                </div>
                <div class="result_bonus" style="padding-left: 1vw; margin-top: 1vw;">
                    <div>Остаток:</div>
                    <div><?= $activeBonus - $used_bonus ?> руб.</div>
                </div>
                <?php
            } else {
                echo 'Нет заказов';
            }
            ?>
        </div>
    </div>
    <script type="text/javascript">
        $(function () {
            $('#add_disc_line').on('click', function (event) {
                event.preventDefault();
                var $clone = $('.discont_form_line:last').clone();
                $('.discont_form_line:last').after($clone);
            });
        });
    </script>
    <?php
    if ($save === true) { ?>

        <script>$(function () {
                creat_lb_sv('<div style="height:200px;"><div class="vert_style" style="width:80%"><img style="display:inline_block;" src="/html_source/img/cart_ok.png" width="75px"><br/>Размеры скидок сохранены</div><div class="vert_style"></div></div>');
            });</script>
    <?php }elseif ($save === 'bonus'){
    ?>
        <script>$(function () {
                creat_lb_sv('<div style="height:200px;"><div class="vert_style" style="width:80%"><img style="display:inline_block;" src="/html_source/img/cart_ok.png" width="75px"><br/>Бонус отправлен</div><div class="vert_style"></div></div>');
            });</script>
        <?php
    } ?>
