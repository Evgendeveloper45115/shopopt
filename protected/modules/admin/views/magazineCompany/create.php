<?php
/* @var $this MagazineCompanyController */
/* @var $model MagazineCompany */

$this->breadcrumbs = array(
    'Magazine Companies' => array('index'),
    'Create',
);

$this->menu = array(
    array('label' => 'List MagazineCompany', 'url' => array('index')),
    array('label' => 'Manage MagazineCompany', 'url' => array('admin')),
);
?>
    <div id="client_top_bar">
        <?php $this->widget('application.extensions.client_manager.ClientManagerWidget'); ?>
        <div id="cart_info">
            <a href="/catalog/cart">
                <div id="cart_ico" class="inline_style">

                </div><div id="cart_info_content" class="inline_style">
                    <div id="align_cart_info">
                        <p><span id="cart_count"><?=$this->cart_count?></span> позиций</p>
                        <p><span class="money"><?=$this->cart_price?></span> <span class="rub_lt">a</span></p>
                    </div>
                </div>
                <a href="/site/logout" class="log_out_castom" title="Выход"></a>

            </a>
        </div>
    </div>
    <ul id="crm_menu">
        <li class="inline_style"><a href="/client/MagazineCompany/admin" class="color_th_hover  css_trans">Моя
                компания<span class="bg_color_th css_trans"></span></a></li>
        <li class="inline_style"><a href="/client/MagazineCompany/create" class="color_th_hover active css_trans">Добавить
                компания<span class="bg_color_th css_trans"></span></a></li>
    </ul>

    <h1>Создать компанию</h1>

<?php $this->renderPartial('_form', array('model' => $model, 'role' => false)); ?>