<?php
/* @var $this MagazineCompanyController */
/* @var $model MagazineCompany */

$this->breadcrumbs = array(
    'Magazine Companies' => array('index'),
    $model->id => array('view', 'id' => $model->id),
    'Update',
);

$this->menu = array(
    array('label' => 'List MagazineCompany', 'url' => array('index')),
    array('label' => 'Create MagazineCompany', 'url' => array('create')),
    array('label' => 'View MagazineCompany', 'url' => array('view', 'id' => $model->id)),
    array('label' => 'Manage MagazineCompany', 'url' => array('admin')),
);
?>
    <div id="client_top_bar">
        <?php $this->widget('application.extensions.client_manager.ClientManagerWidget'); ?>
        <div id="cart_info">
            <a href="/catalog/cart">
                <div id="cart_ico" class="inline_style">

                </div><div id="cart_info_content" class="inline_style">
                    <div id="align_cart_info">
                        <p><span id="cart_count"><?=$this->cart_count?></span> позиций</p>
                        <p><span class="money"><?=$this->cart_price?></span> <span class="rub_lt">a</span></p>
                    </div>
                </div>
                <a href="/site/logout" class="log_out_castom" title="Выход"></a>

            </a>
        </div>
    </div>
<?php
$text = '<h1>Редактировать компанию</h1>';
$role = false;
if (Yii::app()->user->getRole() == WebUser::MANAGER || Yii::app()->user->getRole() == WebUser::ADMIN) {
    $text = '<p class="manager_title" style="    font-size: 1.4vw;color: #5d5d5d; margin-bottom: 1.5vw">'.$model->name_company.'</p>';
    $role = true;
}
echo $text;
?>

<?php $this->renderPartial('_form', array('model' => $model, 'role' => $role)); ?>