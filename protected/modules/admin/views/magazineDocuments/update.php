<?php
/* @var $this MagazineDocumentsController */
/* @var $model MagazineDocuments */

$this->breadcrumbs=array(
	'Magazine Documents'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List MagazineDocuments', 'url'=>array('index')),
	array('label'=>'Create MagazineDocuments', 'url'=>array('create')),
	array('label'=>'View MagazineDocuments', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage MagazineDocuments', 'url'=>array('admin')),
);
?>

<h1>Update MagazineDocuments <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>