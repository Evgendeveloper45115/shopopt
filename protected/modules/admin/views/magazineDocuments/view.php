<?php
/* @var $this MagazineDocumentsController */
/* @var $model MagazineDocuments */

$this->breadcrumbs=array(
	'Magazine Documents'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List MagazineDocuments', 'url'=>array('index')),
	array('label'=>'Create MagazineDocuments', 'url'=>array('create')),
	array('label'=>'Update MagazineDocuments', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete MagazineDocuments', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage MagazineDocuments', 'url'=>array('admin')),
);
?>

<h1>View MagazineDocuments #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'url',
		'date_add',
		'order_id',
		'size_file',
		'status',
	),
)); ?>
