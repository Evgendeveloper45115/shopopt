<?php
/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
class MakeController extends CController
{
	/**
	 * @var string the default layout for the controller view. Defaults to '//layouts/column1',
	 * meaning using a single column layout. See 'protected/views/layouts/column1.php'.
	 */
	public function filters()
	{
		return array(
			'accessControl',
		);
	}

	public function accessRules()
	{
		/**
		 * @var CWebApplication $app
		 */
		$app = Yii::app();
		if(Yii::app()->user->getRole() == WebUser::ADMIN || Yii::app()->user->getRole() == WebUser::MANAGER) {
			return array(

				array('allow',
					'users'=>['@'],
				),
				array('deny',
					'users'=>['?'],
				),

			);
		}elseif(Yii::app()->user->getRole() == WebUser::PUBLISHER &&
			$app->getController()->action->id == 'index'
			&& $app->getController()->id == 'setting') {
			return array(
				array('allow',
					'users'=>['@'],
				),
				array('deny',
					'users'=>['?'],
				),

			);
		} else {
			$this->redirect('/');
			return array(
				array('deny'),
			);
		}

	}

	public $layout='/layouts/column1';
	public $dataModel='';
	public $connectID='';
	/**
	 * @var array context menu items. This property will be assigned to {@link CMenu::items}.
	 */
	public $menu=array();
	/**
	 * @var array the breadcrumbs of the current page. The value of this property will
	 * be assigned to {@link CBreadcrumbs::links}. Please refer to {@link CBreadcrumbs::links}
	 * for more details on how to specify this property.
	 */
	public $breadcrumbs=array();

	public function init()
	{
		parent::init();
		$connectID = SiteConnect::model()->findByAttributes(['url' => $_SERVER['SERVER_NAME']]);
		$this->connectID = $connectID->id;
		$model = MagazineKeySettings::model()->findAll();
		$this->dataModel = CHtml::listData($model,'key','value');
	}
}