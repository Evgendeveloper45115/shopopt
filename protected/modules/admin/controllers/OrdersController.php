<?php

class OrdersController extends MakeController
{
    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '/layouts/main';

    /**
     * @return array action filters
     */
//	public function filters()
//	{
//		return array(
//			'accessControl', // perform access control for CRUD operations
//			'postOnly + delete', // we only allow deletion via POST request
//		);
//	}

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
//	public function accessRules()
//	{
//		return array(
//			array('allow',  // allow all users to perform 'index' and 'view' actions
//				'actions'=>array('index','view'),
//				'users'=>array('*'),
//			),
//			array('allow', // allow authenticated user to perform 'create' and 'update' actions
//				'actions'=>array('create','update'),
//				'users'=>array('@'),
//			),
//			array('allow', // allow admin user to perform 'admin' and 'delete' actions
//				'actions'=>array('admin','delete'),
//				'users'=>array('admin'),
//			),
//			array('allow',  // deny all users
//				'users'=>array('*'),
//			),
//		);
//	}

    public function actionEditOrder()
    {

        if (Yii::app()->request->isAjaxRequest) {
            $order = null;
            $user = null;
            $text = null;
            if (isset($_POST['id'])) {
                MagazineInfoOrder::model()->deleteAllByAttributes(['order_id' => $_POST['id'], 'site_connect_id' => $this->connectID]);
                $order = MagazineOrder::model()->findByAttributes(['id' => $_POST['id']]);
                if ($order) {
                    $orderId = $order->id;
                    $user = MagazineUsers::model()->findByPk($order->client_id);
                    if ($order->delete()) {
                        $text = 'Ваш заказ № ' . $orderId . ' был удалён';
                    }
                }
                $name = '=?UTF-8?B?' . base64_encode('Удаление заказа') . '?=';
                $subject = '=?UTF-8?B?' . base64_encode('Удаление заказа') . '?=';
                $headers = "From: $name <{$_SERVER['SERVER_NAME']}>\r\n" .
                    "Reply-To: {$user->user_email}\r\n" .
                    "MIME-Version: 1.0\r\n" .
                    "Content-Type: text/html; charset=UTF-8";
                mail(
                    $user->user_email,
                    $subject,
                    $text,
                    $headers
                );

                echo json_encode(['success' => 'redirect']);
                exit;
            }
            if (isset($_POST['items'])) {
                $oldValues = [];
                $orderId = null;
                $elmCome = [];
                $str = '';
                $price = ['edit' => [0 => 0], 'no_edit' => [0 => 0]];
                foreach ($_POST['items'] as $id => $item) {
                    if ($item != '') {
                        $arrayValues = explode('/', $item);
                        $order_info = MagazineInfoOrder::model()->findByPk($id);
                        $magazineProduct = MagazineProduct::model()->findByPk($order_info->goods_id);
                        $order = MagazineOrder::model()->findByAttributes(['id' => $order_info->order_id]);
                        $getShopName = MagazineClientsAddress::model()->findByPk($order->magazine_shop_id);
                        $getCompany = MagazineCompany::model()->findByPk($order->client_company_id);
                        if ($arrayValues[0] != 'no_edit') {
                            if ($order) {
                                $user = MagazineUsers::model()->findByPk($order->client_id);
                            }
                            $oldValues[] = $order_info->goods_count;
                            $order_info->goods_count = $arrayValues[0];
                            $order_info->price = $arrayValues[1];
                            $orderId = $order_info->order_id;
                            $order_info->save(false);
                            $elmCome[] = $order_info->id;
                            if (stristr($order_info->price, ' ')) {
                                $order_info->price = str_replace(' ', '', $order_info->price);
                            }
                            $price['edit'][0] = $price['edit'][0] + ((float)$order_info->price * (float)$order_info->goods_count);
                        } else {
                            $arrayValues = explode('/', $item);
                            $order_info->price = $arrayValues[1];
                            if (stristr($order_info->price, ' ')) {
                                $order_info->price = str_replace(' ', '', $order_info->price);
                            }
                            $price['no_edit'][0] = $price['no_edit'][0] + ((float)$order_info->price * (float)$order_info->goods_count);
                            $orderId = $order_info->order_id;
                            $elmCome[] = $order_info->id;
                        }
                        $order->price = ($price['edit'][0] + $price['no_edit'][0]);
                        $order->save(false);
                        $image = null;
                        if ($magazineProduct->image_product) {
                            $json = json_decode($magazineProduct->image_product);
                            if (!empty($json)) {
                                foreach ($json as $img) {
                                    if ($img != '' || !stristr($img, '[')) {
                                        $image = $img;
                                        break;
                                    }
                                }
                            }
                            $color = 'black';
                            if (!in_array($order_info->goods_count, $oldValues)) {
                                $color = 'red';
                            }
                        }
                        $str .= '<tr>
<td style="border: 1px solid #c2c2c2;font-size: 16px;padding: 5px 2%;width: 20%;"><img src="http://' . $_SERVER['SERVER_NAME'] . $image . '" style="width:100%;height:auto;"/></td>
		<td style="border: 1px solid #c2c2c2;font-size: 16px;padding: 5px 2%;width: 39%;">' . $magazineProduct->name_product . '</td>
		<td style="border: 1px solid #c2c2c2;font-size: 16px;padding: 5px 2%;width: 15%;">' . $magazineProduct->price . '</td>
		<td style=" color: ' . $color . ';border: 1px solid #c2c2c2;font-size: 16px;padding: 5px 2%;width: 15%;">' . $order_info->goods_count . '</td>
		<td style="border: 1px solid #c2c2c2;font-size: 16px;padding: 5px 2%;width: 15%">' . (float)$order_info->price * (float)$order_info->goods_count . '</td>
	</tr>';

                    }
                }
                $name = '=?UTF-8?B?' . base64_encode('Изменения в заказе') . '?=';
                $subject = '=?UTF-8?B?' . base64_encode('Изменения в заказе') . '?=';
                $headers = "From: $name <{$_SERVER['SERVER_NAME']}>\r\n" .
                    "Reply-To: {$user->user_email}\r\n" .
                    "MIME-Version: 1.0\r\n" .
                    "Content-Type: text/html; charset=UTF-8";

                mail(
                    'evgendeveloper7@gmail.com',
                    $subject,
                    '
					<p>Ваш заказ №' . $orderId . ' был изменен, новые количества товара и сумма указаны ниже</p>
					<table style=" border-collapse: collapse;border: 1px solid #c2c2c2; width: 100%;">
						<tr>
						<td style="border: 1px solid #c2c2c2;font-size: 12px;text-transform: uppercase;color: #c2c2c2;padding: 5px 2%;width: 15%;">Вид товара</td>
							<td style="border: 1px solid #c2c2c2;font-size: 12px;text-transform: uppercase;color: #c2c2c2;padding: 5px 2%;width: 39%;">Наименование</td>
							<td style="border: 1px solid #c2c2c2;font-size: 12px;text-transform: uppercase;color: #c2c2c2;padding: 5px 2%;width: 15%;">Цена</td>
							<td style="border: 1px solid #c2c2c2;font-size: 12px;text-transform: uppercase;color: #c2c2c2;padding: 5px 2%;width: 15%;">кол-во</td>
							<td style="border: 1px solid #c2c2c2;font-size: 12px;text-transform: uppercase;color: #c2c2c2;padding: 5px 2%;width: 15%">Стоимость</td>
						</tr>
						' . $str . '
					</table>
					<div style="width: 100%;text-align: right;font-weight: bold;"><p style="padding-right: 50px;">Итого: ' . $order->price . '</p></div>
					<p>Заказ будет доставлен по адресу:' . $getShopName->address . '</p>
					<p>Принимающий: ' . $getShopName->surname_contact . '  ' . $getShopName->name_contact . ' тел. ' . $getShopName->phone_contact . '</p>
					<p>Компания: ' . $getCompany->name_company . '</p>
					<p>Спасибо за внимание!</p>
					',
                    $headers
                );
                $order_info = MagazineInfoOrder::model()->findAllByAttributes(['order_id' => $orderId]);
                foreach ($order_info as $key => $model) {
                    if (!in_array($model->id, $elmCome)) {
                        $model->delete();
                    }
                }
                echo json_encode(['success' => 'ok']);
                exit;
            }
        }
    }

    public function actionIndex($id)
    {
        if (isset($_FILES['MagazineDocuments']['name'])) {
            $model = new MagazineDocuments();
            $model->file = CUploadedFile::getInstance($model, 'url');
            $model->size_file = $model->file->getSize();
            $model->order_id = $id;
            $model->date_add = date('Y-m-d');
            $model->status = 0;
            $model->name = $model->file->getName();
            $model->extension = $model->file->getExtensionName();
            $path = '/upload/' . uniqid() . '.' . $model->file->getExtensionName();
            $model->file->saveAs(Yii::getPathOfAlias('webroot') . $path);
            $model->url = $path;
            $model->save();
            $model = null;
        }

        $model = MagazineOrder::model()->findByPk([
            'id' => $id,
            'site_connect_id' => $this->connectID
        ]);
        if (isset($_POST['status'])) {
            $model->status = $_POST['status'];
            $model->save();
            $this->redirect('/manager/MagazineOrder/ShowOrder/id/' . $id);
        }

        if (isset($_POST['data_manager_order'])) {
            $model->time_order = $_POST['data_manager_order'];
            $model->save();

            $getClientInfo = MagazineUsers::model()->findByPk($model->client_id);
            mail($getClientInfo->user_email, 'Изменение статуса заказа', 'Ваш заказ №' . $model->id . ' на сумму ' . $model->price . ' р принят и отправлен в работу');
            $this->redirect('/manager/MagazineOrder/ShowOrder/id/' . $id);
        }

        $magazineInfo = MagazineClientsAddress::model()->findByPk($model->magazine_shop_id);

        $orderInfo = MagazineInfoOrder::model()->findAll(['condition' => 'order_id = "' . $id . '"']);
        foreach ($orderInfo as $orderInf) {
            $imgProduct = MagazineProduct::model()->findByPk($orderInf->goods_id);
            $orderInf->image_product = $imgProduct->image_product;
            $orderInf->name_product = $imgProduct->name_product;
            $orderInf->price = $imgProduct->price;
        }

        $documents = MagazineDocuments::model()->findAll(['condition' => 'order_id = "' . $id . '"']);

        $this->render('index', array(
            'model' => $model,
            'magazineInfo' => $magazineInfo,
            'orderInfo' => $orderInfo,
            'documents' => $documents,
            'manager_info' => Yii::app()->user->getModel()
        ));
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */


    /**
     * Manages all models.
     */
    public function actionAdmin()
    {
        if (isset($_GET['status'])) {
            $ids = $_GET['status'];
        } else {
            $ids = '0';
        }
        $condition = null;
        if (isset($_POST['dates'])) {
            if ($_POST['dates'][0] != '' && $_POST['dates'][1] != '') {
                $condition = 'time_create >= "' . date('Y-m-d H:i:s', strtotime($_POST['dates'][0])) . '" and time_create <= "' . date('Y-m-d H:i:s', strtotime($_POST['dates'][1])) . '" and ';
            } elseif ($_POST['dates'][0] != '') {
                $condition = 'time_create >= "' . date('Y-m-d H:i:s', strtotime($_POST['dates'][0])) . '" and ';

            } elseif ($_POST['dates'][1] != '') {
                $condition = 'time_create <= "' . date('Y-m-d H:i:s', strtotime($_POST['dates'][1])) . '" and ';

            }
        }

        $model = MagazineOrder::model()->findAll(['condition' => $condition . 'status in (' . $ids . ')', 'order' => 'id DESC']);

        if (isset($_GET['idmagazine'])) {
            $model = MagazineOrder::model()->findAll(['condition' => $condition . 'magazine_shop_id = ' . $_GET['idmagazine'], 'order' => 'id DESC']);
        }
        foreach ($model as $md) {
            if (isset($md->manager_id) && $md->manager_id != null) {
                $md->managerInfo = MagazineUsers::model()->findByPk($md->manager_id);
            }
            $md->countFiles = MagazineDocuments::model()->count(['condition' => 'order_id = ' . $md->id]);
            $md->magazine_address = MagazineClientsAddress::model()->findByPk($md->magazine_shop_id);
        }

        $this->render('admin', ['model' => $model]);
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return MagazineUsers the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model = MagazineUsers::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param MagazineUsers $model the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'magazine-users-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }
}
