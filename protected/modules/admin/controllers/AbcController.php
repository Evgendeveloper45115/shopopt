<?php

class AbcController extends MakeController
{
    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '/layouts/main';

    /**
     * @return array action filters
     */
//	public function filters()
//	{
//		return array(
//			'accessControl', // perform access control for CRUD operations
//			'postOnly + delete', // we only allow deletion via POST request
//		);
//	}

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
//	public function accessRules()
//	{
//		return array(
//			array('allow',  // allow all users to perform 'index' and 'view' actions
//				'actions'=>array('index','view'),
//				'users'=>array('*'),
//			),
//			array('allow', // allow authenticated user to perform 'create' and 'update' actions
//				'actions'=>array('create','update'),
//				'users'=>array('@'),
//			),
//			array('allow', // allow admin user to perform 'admin' and 'delete' actions
//				'actions'=>array('admin','delete'),
//				'users'=>array('admin'),
//			),
//			array('allow',  // deny all users
//				'users'=>array('*'),
//			),
//		);
//	}

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate()
    {
        $model = new MagazineUsers;

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['MagazineUsers'])) {
            $connectID = SiteConnect::model()->findByAttributes(['url' => $_SERVER['SERVER_NAME']]);

            $_POST['MagazineUsers']['user_password'] = uniqid();
            $_POST['MagazineUsers']['site_connect_id'] = $connectID->id;

            $model->attributes = $_POST['MagazineUsers'];
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->id));
        }

        $this->render('create', array(
            'model' => $model,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['MagazineUsers'])) {

            $connectID = SiteConnect::model()->findByAttributes(['url' => $_SERVER['SERVER_NAME']]);

            $_POST['MagazineUsers']['site_connect_id'] = $connectID->id;

            $model->attributes = $_POST['MagazineUsers'];
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->id));
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    /**
     * Lists all models.
     */
    public function actionIndex()
    {
        $dataProvider = new CActiveDataProvider('MagazineUsers');
        $this->render('index', array(
            'dataProvider' => $dataProvider,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin()
    {
        $connection = Yii::app()->db; // так можно делать, если в конфигурации настроен компонент соединения "db"
// В противном случае можно создать соединение явно:
// $connection=new CDbConnection($dsn,$username,$password);
        /*
         * ;
         * */
        $sort = "count2 ";
        $position = "DESC";
        if (isset($_POST['sort'])) {
            if ($_POST['sort'] == 'count') {
                $sort = "count ";
            } elseif ($_POST['sort'] == 'price') {
                $sort = "magazine_product.price ";
            }
        }
        if (isset($_POST['position']) && $_POST['position'] == 'DESC') {
            $position = "ASC";
        }
        $condition = null;
        if (isset($_POST['dates'])) {
            if ($_POST['dates'][0] != '' && $_POST['dates'][1] != '') {
                $condition = 'magazine_order.time_create >= "' . date('Y-m-d H:i:s', strtotime($_POST['dates'][0])) . '" and magazine_order.time_create <= "' . date('Y-m-d H:i:s', strtotime($_POST['dates'][1])) . '" and ';
            } elseif ($_POST['dates'][0] != '') {
                $condition = 'magazine_order.time_create >= "' . date('Y-m-d H:i:s', strtotime($_POST['dates'][0])) . '" and ';

            } elseif ($_POST['dates'][1] != '') {
                $condition = 'magazine_order.time_create <= "' . date('Y-m-d H:i:s', strtotime($_POST['dates'][1])) . '" and ';

            }
        }
        $sql = '
		SELECT
  magazine_product.price,
  magazine_product.image_product,
  SUM(m.goods_count) as count,
  magazine_product.name_product,
  magazine_product.min_count,
  (SUM(m.goods_count)*magazine_product.price) as count2

FROM magazine_info_order as m
  LEFT JOIN magazine_product ON magazine_product.id = m.goods_id
  LEFT JOIN magazine_order ON magazine_order.id = m.order_id
WHERE ' . $condition . ' magazine_product.site_connect_id = "' . $this->connectID . '"
GROUP BY magazine_product.id ORDER BY ' . $sort . $position;
        $criteria = new CDbCriteria;
        $criteria->select = 'sum(price) as price';  // подходит только то имя поля, которое уже есть в модели
        $countPrices = MagazineOrder::model()->find($criteria)->getAttribute('price');

        $command = $connection->createCommand($sql);
        $allTovars = $command->queryAll();
        $countTovar = count($allTovars);

        $this->render('admin', ['countPrices' => ($countPrices / 100 * 20), 'allTovars' => $allTovars, 'countTovar' => $countTovar]);
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return MagazineUsers the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model = MagazineUsers::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param MagazineUsers $model the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'magazine-users-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }
}
