<?php

class PromoController extends MakeController
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='/layouts/main';

	/**
	 * @return array action filters
	 */
//	public function filters()
//	{
//		return array(
//			'accessControl', // perform access control for CRUD operations
//			'postOnly + delete', // we only allow deletion via POST request
//		);
//	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
//	public function accessRules()
//	{
//		return array(
//			array('allow',  // allow all users to perform 'index' and 'view' actions
//				'actions'=>array('index','view'),
//				'users'=>array('*'),
//			),
//			array('allow', // allow authenticated user to perform 'create' and 'update' actions
//				'actions'=>array('create','update'),
//				'users'=>array('@'),
//			),
//			array('allow', // allow admin user to perform 'admin' and 'delete' actions
//				'actions'=>array('admin','delete'),
//				'users'=>array('admin'),
//			),
//			array('allow',  // deny all users
//				'users'=>array('*'),
//			),
//		);
//	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new MagazineUsers;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['MagazineUsers']))
		{
			$connectID = SiteConnect::model()->findByAttributes(['url' => $_SERVER['SERVER_NAME']]);

			$_POST['MagazineUsers']['user_password'] = uniqid();
			$_POST['MagazineUsers']['site_connect_id'] = $connectID->id;

			$model->attributes=$_POST['MagazineUsers'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['MagazineUsers']))
		{

			$connectID = SiteConnect::model()->findByAttributes(['url' => $_SERVER['SERVER_NAME']]);

			$_POST['MagazineUsers']['site_connect_id'] = $connectID->id;

			$model->attributes=$_POST['MagazineUsers'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	public function actionSave()
	{
		MagazinePromoRelations::model()->deleteAll(['condition' => 'id_files = ' . $_POST['file_id'] ]);
		$_POST['data'] = explode(',', $_POST['data']);
		foreach($_POST['data'] as $data) {
			$model = new MagazinePromoRelations();
			$model->id_company = $data;
			$model->id_files = $_POST['file_id'];
			$model->save();
		}
		echo 'ok';
	}

	/**
	 * Manages all models.
	 */
	public function actionIndex()
	{
		if(isset($_FILES['MagazinePromoFiles']['name'])) {
			$model = new MagazinePromoFiles();
			$model->file = CUploadedFile::getInstance($model,'url');
			$model->file_size = $model->file->getSize();
			$model->date_create = time();
			$model->status = 0;
			$model->name_file = $model->file->getName();

			$model->site_connect_id = $this->connectID;
			$model->extension = $model->file->getExtensionName();
			$path='/upload/'.uniqid().'.'.$model->file->getExtensionName();
			$model->file->saveAs(Yii::getPathOfAlias('webroot').$path);
			$model->url = $path;
			$model->save();
//			exit;
			$this->redirect('/admin/Promo/index');
		}
		$model = MagazinePromoFiles::model()->findAll();
//		$order = MagazineOrder::model()->find($id);

		$this->render('admin',array(
			'model'=>$model,
//			'order'=>$order,
//			'id' => $id
		));
	}

	public function actionAdd($id)
	{

		$UserCompany = MagazineCompany::model()->findAll();
		foreach($UserCompany as $userCom) {
			$userCom->client_id = MagazineUsers::model()->findByPk($userCom->client_id);
		}
		$file = MagazinePromoFiles::model()->findByPk($id);
		$relations = MagazinePromoRelations::model()->findAll(['condition' => 'id_files = ' . $id]);
		$ids= [];
		foreach($relations as $rel) {
			$ids [] = $rel->id_company;
		}

		$this->render('add',array(
			'UserCompany'=>$UserCompany,
			'relations'=>$ids,
			'file'=>$file,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return MagazineUsers the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=MagazineUsers::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param MagazineUsers $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='magazine-users-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
