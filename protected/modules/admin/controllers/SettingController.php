<?php

class SettingController extends MakeController
{


    public function actionIndex()
    {

        $modelSaveSettings = null;
        if(isset($_POST)) {
            foreach($_POST as $key => $value) {
                if($key == 'save') {
                    continue;
                }
                $modelFind = MagazineKeySettings::model()->find('`key` = "'.$key.'"');
                if($modelFind) {
                    $modelFind->delete();
                }
                $modelSaveSettings = new MagazineKeySettings;
                $modelSaveSettings->key = $key;
                $modelSaveSettings->value = $value;
                $modelSaveSettings->site_connect_id = $this->connectID;
                $modelSaveSettings->save();

            }
        }

        if(isset($_FILES)) {
            foreach($_FILES as $key => $value) {
                if(isset($value['tmp_name']) && $value['tmp_name'] == '') {
                    continue;
                }
                if($key == 'save') {
                    continue;
                }

                $modelFind = MagazineKeySettings::model()->find('`key` = "'.$key.'"');
                if($modelFind) {
                    $modelFind->delete();
                }

                $modelSaveSettings = new MagazineKeySettings;
                $modelSaveSettings->key = $key;
                $file_extension = pathinfo($value['name'], PATHINFO_EXTENSION);
                $modelSaveSettings->value = uniqid().'.'.$file_extension;
                $modelSaveSettings->site_connect_id = $this->connectID;
                if(move_uploaded_file($value['tmp_name'], '/var/www/magazineOpt/upload/'.$modelSaveSettings->value)) {
                    $modelSaveSettings->save();
                }
            }
        }

        if(isset($_POST['save'])) {
            $this->redirect(array('index'));
        }

        $model = MagazineKeySettings::model()->findAll();
        $dataModel = CHtml::listData($model,'key','value');

        // renders the view file 'protected/views/site/index.php'
        // using the default layout 'protected/views/layouts/main.php'
        $this->render('index', ['dataModel' => $dataModel, 'modelSaveSettings' => $modelSaveSettings]);
    }
}