<?php

class ClientsController extends MakeController
{
    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '/layouts/main';

    /**
     * @return array action filters
     */

    public function actionIndex($id)
    {
        $comp = MagazineCompany::model()->findByPk($id);
        if (isset($_POST['MagazineUser']['min_order']) && !empty($_POST['MagazineUser']['min_order'])) {
            $comp = MagazineCompany::model()->findByPk($id);
            if ($comp != null) {
                $user = MagazineUsers::model()->findByPk($comp->client_id);
                if ($user) {
                    $user->min_order = $_POST['MagazineUser']['min_order'];
                    $user->save(false);
                }
            }
        }

        $allTovar = MagazineProduct::model()->findAll(['condition' => 'type = 0']);
        $allStock = MagazineStock::model()->findAll();
        $CompanyRecommendTovar = MagazineRecommedClientTovar::model()->findAll(['condition' => 'id_company = "' . $id . '" ']);
        $CompanyRecommendStock = MagazineRecommendStock::model()->findAll(['condition' => 'id_company = "' . $id . '" ']);

        foreach ($CompanyRecommendStock as $rTovar2) {
            foreach ($allStock as $td) {
                if ($rTovar2->id_stock == $td->id) {
                    $rTovar2->info = $td;
                }
            }
        }

        foreach ($CompanyRecommendTovar as $rTovar) {
            foreach ($allTovar as $td) {
                if ($rTovar->id_tovar == $td->id) {
                    $rTovar->info = $td;
                }
            }
        }
//		$CompanyRecommendStock = MagazineRecommendStock::model()->findAll();
        $client = null;
        $mangerInfo = null;
        if($comp->client_id){
            $client = MagazineOrder::model()->findByAttributes(['client_id' => $comp->client_id]);
        }
        if($client != null && $client->manager_id){
            $mangerInfo = MagazineUsers::model()->findByPk($client->manager_id);
        }
        $orderNews = MagazineOrder::model()->findAll(['condition' => 'client_company_id = "' . $id . '" and status = 0']);
        $ids = [];
        $idsCompany = [];
        $clientIds = [];

        foreach ($orderNews as $order) {
            $ids[] = $order->id;
            $clientIds[] = $order->client_id;
            $idsCompany[] = $order->client_company_id;
            $order->companyInfo = MagazineCompany::model()->findAll($order->client_company_id);
        }

        $cond = '';

        if (Yii::app()->user->getRole() == WebUser::MANAGER) {
            $cond = 'and (manager_id = "' . Yii::app()->user->id . '" or manager_id is null) ';
        }
        $orderWork = MagazineOrder::model()->findAll(['condition' => 'client_company_id = "' . $id . '" and status in (1,2) ' . $cond]);
        foreach ($orderWork as $order) {

            $ids[] = $order->id;
            $clientIds[] = $order->client_id;
            $idsCompany[] = $order->client_company_id;
            $order->companyInfo = MagazineCompany::model()->findAll($order->client_company_id);
        }

        $orderHistory = MagazineOrder::model()->findAll(['condition' => 'client_company_id = "' . $id . '" and status = 3']);
        foreach ($orderHistory as $order) {
            $ids[] = $order->id;
            $clientIds[] = $order->client_id;
            $idsCompany[] = $order->client_company_id;
            $order->companyInfo = MagazineCompany::model()->findAll($order->client_company_id);
        }

        if(!empty($clientIds)){
            $getClientDiscount = MagazineDiscount::model()->find(['condition' => 'client_id = ' . $clientIds[0], 'order' => 'id ASC']);
        }

        $companyInfo = MagazineCompany::model()->findByPk($id);
        $clinetInfo = MagazineUsers::model()->findByPk($companyInfo->client_id);

        $documents = MagazineDocuments::model()->findAll(['condition' => 'order_id in ("' . implode('","', $ids) . '")']);

        $PromoRelations = MagazinePromoRelations::model()->findAll(['condition' => 'id_company in ("' . implode('","', $idsCompany) . '")']);
        $idsPromo = [];
        foreach ($PromoRelations as $promoRel) {
            $idsPromo[] = $promoRel->id_files;
        }

        $PromoFiles = MagazinePromoFiles::model()->findAll(['condition' => 'id in ("' . implode('","', $idsPromo) . '")']);

        $magazineUsers = MagazineClientsAddress::model()->findAll(['condition' => 'user_id in ("' . implode('","', $clientIds) . '")']);

        $criteria = new CDbCriteria;
        $criteria->select = 'sum(price) as price';  // подходит только то имя поля, которое уже есть в модели
        $criteria->condition = 'client_id in ("' . implode('","', $clientIds) . '")';
        $sBalance = MagazineOrder::model()->find($criteria)->getAttribute('price');
        //[{"img":"..\/img\/demo_akz.png","title":"title akz","descr":"description akz","content":"content_akz","id":"id"}]
        $PCREpattern = '/\r\n|\r|\n/u';

        $contentStock = [];
        foreach ($allStock as $tv) {
            $contentStock[] = [
                'id' => $tv->id,
                'img' => $tv->image,
                'title' => $tv->header_text,
                'descr' => preg_replace($PCREpattern, '<br/>', $tv->text),
            ];
        }
        $contentTovar = [];
        foreach ($allTovar as $tv) {
            $json = null;
            if ($tv->image_product) {
                $json = json_decode($tv->image_product);
            }
            if(is_array($json)){
                foreach ($json as $image) {
                    if (!stristr($image, '[')) {
                        if ($image == '') {
                            continue;
                        } else {
                            $json = $image;
                        }
                    }
                    break;
                }
            }

            $contentTovar[] = [
                'id' => $tv->id,
                'title' => $tv->title_en,
                'img' => $json,
                'price' => $tv->price
            ];
        }
        $contentTovar = json_encode($contentTovar, JSON_UNESCAPED_UNICODE);
        $contentStock = json_encode($contentStock, JSON_UNESCAPED_UNICODE);

//		echo '<pre>';
//		var_dump($CompanyRecommendStock);
//		echo '</pre>';
        $this->render('index', array(
            'mangerInfo' => $mangerInfo,
            'getClientDiscount' => $getClientDiscount,
            'id' => $id,
            'sBalance' => $sBalance,
            'orderNews' => $orderNews,
            'orderWork' => $orderWork,
            'orderHistory' => $orderHistory,
            'companyInfo' => $companyInfo,
            'clinetInfo' => $clinetInfo,
            'documents' => $documents,
            'PromoFiles' => $PromoFiles,
            'magazineUsers' => $magazineUsers,
            'contentTovar' => $contentTovar,
            'allStock' => $allStock,
            'contentStock' => $contentStock,
            'CompanyRecommendTovar' => $CompanyRecommendTovar,
            'CompanyRecommendStock' => $CompanyRecommendStock,
        ));
    }

    public function actionClientChange()
    {
    }


    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
//	public function accessRules()
//	{
//		return array(
//			array('allow',  // allow all users to perform 'index' and 'view' actions
//				'actions'=>array('index','view'),
//				'users'=>array('*'),
//			),
//			array('allow', // allow authenticated user to perform 'create' and 'update' actions
//				'actions'=>array('create','update'),
//				'users'=>array('@'),
//			),
//			array('allow', // allow admin user to perform 'admin' and 'delete' actions
//				'actions'=>array('admin','delete'),
//				'users'=>array('admin'),
//			),
//			array('allow',  // deny all users
//				'users'=>array('*'),
//			),
//		);
//	}

    public function actionDiscount($id)
    {
        $countPost = count($_POST['MagazineDiscount']['sum']);
        $save = false;
        $prepay = MagazinePrepay::model()->findByAttributes(['user_id' => $id]);
        if (Yii::app()->request->isAjaxRequest) {
            $condition = null;
            if (isset($_POST['dates'])) {
                if ($_POST['dates'][0] != '' && $_POST['dates'][1] != '') {
                    $condition = 'date_create >= "' . date('Y-m-d H:i:s', strtotime($_POST['dates'][0])) . '" and date_create <= "' . date('Y-m-d H:i:s', strtotime($_POST['dates'][1])) . '" and ';
                } elseif ($_POST['dates'][0] != '') {
                    $condition = 'date_create >= "' . date('Y-m-d H:i:s', strtotime($_POST['dates'][0])) . '" and ';

                } elseif ($_POST['dates'][1] != '') {
                    $condition = 'date_create <= "' . date('Y-m-d H:i:s', strtotime($_POST['dates'][1])) . '" and ';
                }
            }
            $setBonuses = MagazineBonus::model()->findAll(['condition' => $condition . 'user_id = "' . $id . '"']);
            if (!empty($setBonuses)) {
                foreach ($setBonuses as $setBonus) {
                    $orderBonuses = MagazineBonusOrder::model()->findAll(['condition' => $condition . 'bonus_id ="' . $setBonus->id . '"']);
                    if (!empty($orderBonuses)) {
                        $setBonus->children = $orderBonuses;
                    }
                }
            }
        } else {
            $setBonuses = MagazineBonus::model()->findAllByAttributes(['user_id' => $id]);
            if (!empty($setBonuses)) {
                foreach ($setBonuses as $setBonus) {
                    $orderBonuses = MagazineBonusOrder::model()->findAllByAttributes(['bonus_id' => $setBonus->id]);
                    if (!empty($orderBonuses)) {
                        $setBonus->children = $orderBonuses;
                    }
                }
            }
        }
        if ($prepay == null) {
            $prepay = new MagazinePrepay();
        }
        if (isset($_POST['MagazinePrepay'])) {
            $prepay->user_id = $id;
            $prepay->site_connect_id = $this->connectID;
            $prepay->percent = $_POST['MagazinePrepay']['percent'];
            $prepay->save();
            $save = true;
        }
        $criteria = new CDbCriteria();
        $criteria->condition = 'user_id = ' . $id . (isset($_POST['MagazineBonus']['order_id']) ? ' AND order_id = ' . $_POST['MagazineBonus']['order_id'] : null);
        $criteria->order = "id DESC";
        $criteria->limit = 1;

        $bonus = MagazineBonus::model()->find($criteria);
        if ($bonus == null) {
            $bonus = new MagazineBonus();
        }
        $allBonus = MagazineBonus::model()->findAllByAttributes(['user_id' => $id]);
        if (isset($_POST['MagazineBonus']) && isset($_POST['MagazineBonus']['active_bonus'])) {
            $bonus->user_id = $id;
            $bonus->site_connect_id = $this->connectID;
            $bonus->active_bonus = $_POST['MagazineBonus']['active_bonus'];
            $bonus->date_create = date('Y-m-d H:i:s');
            $bonus->order_id = isset($_POST['MagazineBonus']['order_id']) ? $_POST['MagazineBonus']['order_id'] : null;
            $bonus->save();
            $save = true;
            $user = MagazineUsers::model()->findByPk($id);
            if ($user != null) {
                $name = '=?UTF-8?B?' . base64_encode('Начисление бонусов!') . '?=';
                $subject = '=?UTF-8?B?' . base64_encode('Начисление бонусов!') . '?=';
                $headers = "From: $name <{$_SERVER['SERVER_NAME']}>\r\n" .
                    "Reply-To: {$user->user_email}\r\n" .
                    "MIME-Version: 1.0\r\n" .
                    "Content-Type: text/html; charset=UTF-8";

                mail(
                    $user->user_email,
                    $subject,
                    'Вам начислен денежный бонус в размере ' . $bonus->active_bonus . 'руб.',
                    $headers
                );
            }
            $save = 'bonus';
        }
        if ($countPost > 0) {
            MagazineDiscount::model()->deleteAll(['condition' => 'client_id = "' . $id . '"']);
            for ($i = 0; $i < $countPost; $i++) {
                $model = new MagazineDiscount();
                $model->persent = $_POST['MagazineDiscount']['persent'][$i];
                $model->sum = $_POST['MagazineDiscount']['sum'][$i];
                $model->client_id = $id;
                $model->save();
            }
            $save = true;
        }
        $getUserDiscount = MagazineDiscount::model()->findAll(['condition' => 'client_id = "' . $id . '"']);
        $company = MagazineCompany::model()->findByAttributes(['client_id' => $id]);
        $this->render('discount', [
                'getUserDiscount' => $getUserDiscount,
                'save' => $save,
                'prepay' => $prepay,
                'allBonus' => $allBonus,
                'user_id' => $id,
                'setBonuses' => $setBonuses,
                'company' => $company,
                'bonus' => $bonus
            ]
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */

    /**
     * Manages all models.
     */
    public function actionAdmin()
    {
        $connection = Yii::app()->db; // так можно делать, если в конфигурации настроен компонент соединения "db"
// В противном случае можно создать соединение явно:
// $connection=new CDbConnection($dsn,$username,$password);
        /*
         * ;
         * */
        $condition = null;
        if (isset($_POST['dates'])) {
            if ($_POST['dates'][0] != '' && $_POST['dates'][1] != '') {
                $condition = 'magazine_order.time_create >= "' . date('Y-m-d H:i:s', strtotime($_POST['dates'][0])) . '" and magazine_order.time_create <= "' . date('Y-m-d H:i:s', strtotime($_POST['dates'][1])) . '" and ';
            } elseif ($_POST['dates'][0] != '') {
                $condition = 'magazine_order.time_create >= "' . date('Y-m-d H:i:s', strtotime($_POST['dates'][0])) . '" and ';

            } elseif ($_POST['dates'][1] != '') {
                $condition = 'magazine_order.time_create <= "' . date('Y-m-d H:i:s', strtotime($_POST['dates'][1])) . '" and ';

            }
        }

        if (isset($_GET['price'])) {
            $sql = '
		SELECT
  magazine_order.client_company_id,
  magazine_company.client_id,
  magazine_company.name_company,
  SUM(if(magazine_order.status = 1 or magazine_order.status = 2,1,0)) AS count_in_work,
  SUM(if(magazine_order.status = 1 or magazine_order.status = 2,magazine_order.price,0)) as sum_in_work,
  SUM(if(magazine_order.status = 3,magazine_order.price,0))                AS sum
FROM magazine_order
  LEFT JOIN magazine_company ON magazine_company.id = magazine_order.client_company_id
  WHERE ' . $condition . ' magazine_order.site_connect_id = "' . $this->connectID . '"
GROUP BY magazine_order.client_company_id ORDER BY sum DESC';
//			echo $sql;
        } else {
            $managerid = '';
            if (isset($_GET['manager_id'])) {
                $managerid = 'and manager_id = ' . $_GET['manager_id'];
            }
            $sql = '
		SELECT
  magazine_order.client_company_id,
  magazine_company.client_id,
  magazine_company.name_company,
  SUM(if(magazine_order.status = 1 or magazine_order.status = 2,1,0)) AS count_in_work,
  SUM(if(magazine_order.status = 1 or magazine_order.status = 2,magazine_order.price,0)) as sum_in_work,
  SUM(if(magazine_order.status = 3,magazine_order.price,0))                AS sum
FROM magazine_order
  LEFT JOIN magazine_company ON magazine_company.id = magazine_order.client_company_id
  WHERE ' . $condition . ' magazine_order.site_connect_id = "' . $this->connectID . '" ' . $managerid . '
GROUP BY magazine_order.client_company_id ORDER BY magazine_company.name_company';
        }
//		echo $sql;
        $command = $connection->createCommand($sql);
        $allCompanies = $command->queryAll();
        foreach ($allCompanies as &$comp) {
//			var_dump($comp);
//			exit;
            $getUserInfo = MagazineUsers::model()->find(['condition' => 'id = "' . $comp['client_id'] . '"']);

            $comp['user_name'] = $getUserInfo->user_name;
            $comp['user_surname'] = $getUserInfo->user_surname;
            $comp['user_phone'] = $getUserInfo->user_phone;
            $comp['user_email'] = $getUserInfo->user_email;
        }
//		var_dump($allCompanies);
        $this->render('admin', ['allCompanies' => $allCompanies]);
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return MagazineUsers the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model = MagazineUsers::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param MagazineUsers $model the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'magazine-users-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }
}
