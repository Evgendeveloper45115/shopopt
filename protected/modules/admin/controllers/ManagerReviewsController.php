<?php

class ManagerReviewsController extends MakeController
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='/layouts/main';

	/**
	 * @return array action filters
	 */
//	public function filters()
//	{
//		return array(
//			'accessControl', // perform access control for CRUD operations
//			'postOnly + delete', // we only allow deletion via POST request
//		);
//	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
//	public function accessRules()
//	{
//		return array(
//			array('allow',  // allow all users to perform 'index' and 'view' actions
//				'actions'=>array('index','view'),
//				'users'=>array('*'),
//			),
//			array('allow', // allow authenticated user to perform 'create' and 'update' actions
//				'actions'=>array('create','update'),
//				'users'=>array('@'),
//			),
//			array('allow', // allow admin user to perform 'admin' and 'delete' actions
//				'actions'=>array('admin','delete'),
//				'users'=>array('admin'),
//			),
//			array('allow',  // deny all users
//				'users'=>array('*'),
//			),
//		);
//	}


	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{

		if(isset($_POST['review_answer'])) {
			$maganersReviews = MagazineReview::model()->find(['condition' => 'id = "'.$_POST['review_id'].'"']);
			$maganersReviews->text_answer = $_POST['review_answer'];
			$maganersReviews->save();
			mail($maganersReviews->email, 'Ответили на ваш отзыв', $_POST['review_answer']);
			$this->redirect('/admin/ManagerReviews/admin');
		}

		$cond = '';
		if($cond == '' && $_GET['manager_id']) {
			$maganersReviews = MagazineReview::model()->findAll(['condition'=>'manager_id = "'.$_GET['manager_id'].'"', 'order' => 'id DESC']);
		} else {
			$maganersReviews = MagazineReview::model()->findAll(['order' => 'id DESC']);
		}



		foreach($maganersReviews as $md) {
			$md->info = MagazineUsers::model()->findByPk($md->manager_id);
		}

		$this->render('admin', ['maganersReviews' => $maganersReviews]);
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return MagazineUsers the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=MagazineUsers::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param MagazineUsers $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='magazine-users-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
