<?php

class MagazineUsersController extends MakeController
{
    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '/layouts/main';

    /**
     * @return array action filters
     */
//	public function filters()
//	{
//		return array(
//			'accessControl', // perform access control for CRUD operations
//			'postOnly + delete', // we only allow deletion via POST request
//		);
//	}

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
//	public function accessRules()
//	{
//		return array(
//			array('allow',  // allow all users to perform 'index' and 'view' actions
//				'actions'=>array('index','view'),
//				'users'=>array('*'),
//			),
//			array('allow', // allow authenticated user to perform 'create' and 'update' actions
//				'actions'=>array('create','update'),
//				'users'=>array('@'),
//			),
//			array('allow', // allow admin user to perform 'admin' and 'delete' actions
//				'actions'=>array('admin','delete'),
//				'users'=>array('admin'),
//			),
//			array('allow',  // deny all users
//				'users'=>array('*'),
//			),
//		);
//	}

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    public function actionNewUsers()
    {
        $users = MagazineUsers::model()->findAllByAttributes(['is_active' => 0]);
        $this->render('newUsers', ['users' => $users]);
    }

    public function actionNewUserView($id)
    {
        $user = MagazineUsers::model()->findByPk($id);

        $companyInfo = MagazineCompany::model()->findByAttributes(['client_id' => $id]);
        $this->render('newUserView', array(
            'companyInfo' => $companyInfo,
            'clinetInfo' => $user,
            'id' => $id,
        ));
    }

    public function actionChangeActive()
    {
        if (isset($_POST['user_id']) && $_POST['name'] == 'on') {
            $user = MagazineUsers::model()->findByPk($_POST['user_id']);
            if ($user != null) {
                $user->is_active = 1;
                $user->manager_id = $_POST['manager_id'];
                if ($user->save(false)) {
                    $subject = "Подтверждение пользователя";
                    $header = "Content-type: text/html; charset=\"utf8\"";
                    $header .= "From: Evgen <evgen@" . $_SERVER['SERVER_NAME'] . ">";
                    $header .= "Subject: " . $subject;
                    $msg = 'Ваша учетная запись прошла модерацию';
                    mail($user->user_email, $subject, $msg, $header);
                }
                echo 'Пользователь подтверждён';
                exit;
            }
        } elseif (isset($_POST['user_id']) && $_POST['name'] == 'off') {
            $user = MagazineUsers::model()->findByPk($_POST['user_id']);
            if ($user != null) {
                $email = $user->user_email;
                if ($user->delete()) {
                    $subject = "Подтверждение пользователя";
                    $header = "Content-type: text/html; charset=\"utf8\"";
                    $header .= "From: Evgen <evgen@" . $_SERVER['SERVER_NAME'] . ">";
                    $header .= "Subject: " . $subject;
                    $msg = 'Ваша учетная запись не прошла модерацию';
                    mail($email, $subject, $msg, $header);
                }
                echo 'Пользователю отказано';
                exit;
            }
        }
    }

    public function actionChangePass()
    {
        $model = MagazineUsers::model()->find(['condition' => 'id = "' . Yii::app()->user->id . '"']);
        if (isset($_POST['MagazineUsers'])) {
            $error = [];
            if ($_POST['MagazineUsers']['passw_old'] != $model->user_password) {
                $error[] = 'не правильный старый пароль';
            }

            if ($_POST['MagazineUsers']['passw_repeat'] != $_POST['MagazineUsers']['passw_repeat_2']) {
                $error[] = 'не правильный повтор пароля';
            }

            $model->attributes = $_POST['MagazineUsers'];
            $model->user_password = $_POST['MagazineUsers']['passw_repeat'];

            if (empty($error)) {
                if ($model->save()) {
                    $this->redirect('/admin/MagazineUsers/ChangePass?fine');
                }
            }
        }

        if (isset($_POST['MagazineUsers'])) {
            if (isset($_POST['MagazineUsers']['user_phone'])) {
                $model->user_phone = $_POST['MagazineUsers']['user_phone'];
            }

            if (isset($_POST['MagazineUsers']['user_email'])) {
                $model->user_email = $_POST['MagazineUsers']['user_email'];
            }

            if (isset($_FILES['MagazineUsers']['name']['user_avatar'])) {
                $model->file = CUploadedFile::getInstance($model, 'user_avatar');
                if ($model->file != null) {
                    $path = '/upload/' . uniqid() . '.' . $model->file->getExtensionName();
                    $model->file->saveAs(Yii::getPathOfAlias('webroot') . $path);
                    $model->user_avatar = $path;
                }
            }


            $model->save();
//			$this->redirect('/client/MagazineUsers/admin?fine');
        }

        $model = MagazineUsers::model()->find(['condition' => 'id = "' . Yii::app()->user->id . '"']);

        $this->render('changeadmin', array(
            'model' => $model,
            'error' => $error
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate()
    {
        $model = new MagazineUsers;

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['MagazineUsers'])) {
            $connectID = SiteConnect::model()->findByAttributes(['url' => $_SERVER['SERVER_NAME']]);

            $_POST['MagazineUsers']['user_password'] = uniqid();
            $_POST['MagazineUsers']['site_connect_id'] = $connectID->id;

            $model->attributes = $_POST['MagazineUsers'];
            $roles = ['1' => 'Администратор', '2' => 'Менеджер', '3' => 'Паблишер', '4' => 'Менеджер-Паблишер', '5' => 'Клиент'];

            if ($model->save()) {
                mail($_POST['MagazineUsers']['user_email'], 'Создан новый пользователь',
                    "
					\n
					Логин: {$_POST['MagazineUsers']['user_email']} \n
					Пароль: {$_POST['MagazineUsers']['user_password']} \n
					Роль: {$roles[$model->user_role]} \n
					Изменить пароль вы можете в настойках.\n
					Спасибо!"
                );
                $this->redirect('/admin/MagazineUsers/admin?show');
            }

        }

        $this->render('create', array(
            'model' => $model,
        ));
    }

    public function actionCreateManager()
    {
        $model = new MagazineUsers;

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['MagazineUsers'])) {
            $connectID = SiteConnect::model()->findByAttributes(['url' => $_SERVER['SERVER_NAME']]);

            $_POST['MagazineUsers']['user_password'] = uniqid();
            $_POST['MagazineUsers']['site_connect_id'] = $connectID->id;

            $model->attributes = $_POST['MagazineUsers'];
            $roles = ['1' => 'Администратор', '2' => 'Менеджер', '3' => 'Паблишер', '4' => 'Менеджер-Паблишер', '5' => 'Клиент'];
            if ($model->save()) {
                mail($_POST['MagazineUsers']['user_email'], 'Создан новый пользователь',
                    "
					\n
					Логин: {$_POST['MagazineUsers']['user_email']} \n
					Пароль: {$_POST['MagazineUsers']['user_password']} \n
					Роль: {$roles[$model->user_role]} \n
					Изменить пароль вы можете в настойках.\n
					Спасибо!"
                );
                $this->redirect('/admin/MagazineUsers/admin');
            }

        }

        $this->render('createManager', array(
            'model' => $model,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['MagazineUsers'])) {

            $connectID = SiteConnect::model()->findByAttributes(['url' => $_SERVER['SERVER_NAME']]);

            $_POST['MagazineUsers']['site_connect_id'] = $connectID->id;

            $model->attributes = $_POST['MagazineUsers'];
            if ($model->save())
                $this->redirect('/admin/MagazineUsers/admin');
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    /**
     * Lists all models.
     */
    public function actionIndex()
    {
        $dataProvider = new CActiveDataProvider('MagazineUsers');
        $this->render('index', array(
            'dataProvider' => $dataProvider,
        ));
    }

    public function actionAdminManager()
    {
        $model = MagazineUsers::model()->findAll(['condition' => 'user_role = ' . WebUser::MANAGER]);

        $criteria = new CDbCriteria;
        $criteria->select = 'sum(price) as price';  // подходит только то имя поля, которое уже есть в модели
        $countPrices = MagazineOrder::model()->find($criteria)->getAttribute('price');
        $condition = null;
        if (isset($_POST['dates'])) {
            if ($_POST['dates'][0] != '' && $_POST['dates'][1] != '') {
                $condition = 'time_order >= "' . date('Y-m-d H:i:s', strtotime($_POST['dates'][0])) . '" and time_order <= "' . date('Y-m-d H:i:s', strtotime($_POST['dates'][1])) . '"';
            } elseif ($_POST['dates'][0] != '') {
                $condition = 'time_order >= "' . date('Y-m-d H:i:s', strtotime($_POST['dates'][0])) . '"';

            } elseif ($_POST['dates'][1] != '') {
                $condition = 'time_order <= "' . date('Y-m-d H:i:s', strtotime($_POST['dates'][1])) . '"';

            }
        }

        foreach ($model as $manager) {
            if ($condition != null) {
                @$manager->clients = MagazineOrder::model()->count(['condition' => 'manager_id = ' . $manager->id . ' and ' . $condition, 'group' => 'client_id']);
                @$manager->in_work = MagazineOrder::model()->count(['condition' => 'status in (' . MagazineOrder::STATUS_COLLECTED . ',' . MagazineOrder::STATUS_SHIPPED . ' ) and ' . $condition . ' and manager_id = "' . $manager->id . '"']);
            } else {
                @$manager->clients = MagazineOrder::model()->count(['condition' => 'manager_id = ' . $manager->id, 'group' => 'client_id']);
                @$manager->in_work = MagazineOrder::model()->count(['condition' => 'status in (' . MagazineOrder::STATUS_COLLECTED . ',' . MagazineOrder::STATUS_SHIPPED . ' ) and manager_id = "' . $manager->id . '"']);
                @$manager->in_review = MagazineReview::model()->count(['condition' => 'manager_id = ' . $manager->id]);
            }

            $criteria = new CDbCriteria;
            $criteria->select = 'sum(price) as price';  // подходит только то имя поля, которое уже есть в модели
            if ($condition != null) {
                $criteria->condition = $condition . ' and manager_id = ' . $manager->id;
            } else {
                $criteria->condition = 'manager_id = ' . $manager->id;
            }
            $manager->price = MagazineOrder::model()->find($criteria)->getAttribute('price');

            @$manager->in_review = MagazineReview::model()->count(['condition' => 'manager_id = ' . $manager->id]);
            $criteria = new CDbCriteria;
            if ($condition != null) {
                $criteria->condition = $condition . 'and manager_id = ' . $manager->id . ' and status in ( "' . MagazineOrder::STATUS_COLLECTED . '", "' . MagazineOrder::STATUS_SHIPPED . '")';
            } else {
                $criteria->condition = 'manager_id = ' . $manager->id . ' and status in ( "' . MagazineOrder::STATUS_COLLECTED . '", "' . MagazineOrder::STATUS_SHIPPED . '")';
            }
            $criteria->select = 'sum(price) as price';  // подходит только то имя поля, которое уже есть в модели
            $manager->price_work = MagazineOrder::model()->find($criteria)->getAttribute('price');
        }
        $position = "ASC";

        if (isset($_POST['position']) && $_POST['position'] == 'ASC') {
            $position = "DESC";
        }

        if (isset($_POST['sort'])) {
            if ($_POST['sort'] == 'clients') {
                if ($position == 'DESC') {
                    usort($model, function ($a, $b) {
                        return ($b['clients'] - $a['clients']);
                    });
                } else {
                    usort($model, function ($a, $b) {
                        return ($a['clients'] - $b['clients']);
                    });
                }
            }
            if ($_POST['sort'] == 'in_work') {
                if ($position == 'DESC') {
                    usort($model, function ($a, $b) {
                        return ($b['in_work'] - $a['in_work']);
                    });
                } else {
                    usort($model, function ($a, $b) {
                        return ($a['in_work'] - $b['in_work']);
                    });
                }
            }
            if ($_POST['sort'] == 'price') {
                if ($position == 'DESC') {
                    usort($model, function ($a, $b) {
                        return ($b['price'] - $a['price']);
                    });
                } else {
                    usort($model, function ($a, $b) {
                        return ($a['price'] - $b['price']);
                    });
                }
            }
            if ($_POST['sort'] == 'price_work') {
                if ($position == 'DESC') {
                    usort($model, function ($a, $b) {
                        return ($b['price_work'] - $a['price_work']);
                    });
                } else {
                    usort($model, function ($a, $b) {
                        return ($a['price_work'] - $b['price_work']);
                    });
                }
            }
        }

        $this->render('adminManagers', array(
            'model' => $model,
            'countPrices' => $countPrices,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin()
    {
        $model = MagazineUsers::model()->findAll();
        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return MagazineUsers the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model = MagazineUsers::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param MagazineUsers $model the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'magazine-users-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    public function actionChangeManager()
    {
        if (isset($_POST['manager_id'])) {
            $company = MagazineCompany::model()->findByPk((int)$_POST['user_id']);
            if ($company) {
                $user = MagazineUsers::model()->findByPk($company->client_id);
                if ($user) {
                    $user->manager_id = $_POST['manager_id'];
                    $user->save(false);
                }
                $orders = MagazineOrder::model()->findAll(['condition' => 'client_id = ' . $user->id . ' and status <> 3']);
                if (!empty($orders)) {
                    foreach ($orders as $order) {
                        $order->manager_id = $user->manager_id;
                        $order->save(false);
                    }
                }
            }
            echo 'ok';
            exit;
        }
    }

}
