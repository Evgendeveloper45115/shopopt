<?php /* @var $this Controller */
$users = MagazineUsers::model()->count('is_active = 0');
$items = array(
    array('label' => 'Клиенты<span class="bg_color_th"></span>', 'url' => array('/admin/Clients/admin'), 'visible' => (Yii::app()->user->role == WebUser::ADMIN), 'active' => preg_match('/(Clients\/index|Clients\/admin)/', $_SERVER['REQUEST_URI'])),
    array('label' => 'Пользователи<span class="bg_color_th"></span>', 'url' => array('/admin/MagazineUsers/admin'), 'visible' => (Yii::app()->user->role == WebUser::ADMIN), 'linkOptions' => ['class' => 'css_trans color_th_hover active']),
    array('label' => 'Менеджеры<span class="bg_color_th"></span>', 'url' => array('/admin/MagazineUsers/AdminManager'), 'visible' => (Yii::app()->user->role == WebUser::ADMIN)),
    array('label' => 'ABC-анализ<span class="bg_color_th"></span>', 'url' => array('/admin/Abc/admin'), 'visible' => (Yii::app()->user->role == WebUser::ADMIN)),
    array('label' => 'Заказы<span class="bg_color_th"></span>', 'url' => array('/admin/Orders/admin'), 'visible' => (Yii::app()->user->role == WebUser::ADMIN), 'active' => stristr($_SERVER['REQUEST_URI'], '/admin/Orders/')),
    array('label' => 'Отзывы о менеджерах<span class="bg_color_th"></span>', 'url' => array('/admin/ManagerReviews/admin'), 'visible' => (Yii::app()->user->role == WebUser::ADMIN)),
    array('label' => 'Клиенты<span class="bg_color_th"></span>', 'url' => array('/manager/MagazineClientInfo/index'), 'visible' => (Yii::app()->user->role == WebUser::MANAGER), 'active' => preg_match('/(clients\/index|clients\/admin)/', $_SERVER['REQUEST_URI'])),
    array('label' => 'Заказы<span class="bg_color_th"></span>', 'url' => array('/manager/MagazineOrder/admin'), 'visible' => (Yii::app()->user->role == WebUser::MANAGER), 'active' => stristr($_SERVER['REQUEST_URI'], '/admin/Orders/')),
    array('label' => 'Документооборот<span class="bg_color_th"></span>', 'url' => array('/manager/MagazineDocuments/admin'), 'visible' => (Yii::app()->user->role == WebUser::MANAGER)),
    array('label' => 'Промоматериалы<span class="bg_color_th"></span>', 'url' => array('/manager/Promo/index'), 'visible' => (Yii::app()->user->role == WebUser::MANAGER)),
    array('label' => 'Акции и спецпредложения<span class="bg_color_th"></span>', 'url' => array('/manager/MagazineAction/admin'), 'visible' => (Yii::app()->user->role == WebUser::MANAGER)),
    array('label' => 'Настройки профиля<span class="bg_color_th"></span>', 'url' => array('/admin/MagazineUsers/ChangePass'), 'visible' => (Yii::app()->user->role == WebUser::ADMIN)),
    array('label' => 'Новые клиенты<span class="bg_color_th"></span><div class="new_users bg_color_th" style="color: white">' . $users . '</div>', 'url' => array('/admin/MagazineUsers/newUsers'), 'visible' => (Yii::app()->user->role == WebUser::ADMIN)),
    array('label' => 'Настройки<span class="bg_color_th"></span>', 'url' => array('/manager/MagazineUsers/admin'), 'visible' => (Yii::app()->user->role == WebUser::MANAGER)),
);
if (Yii::app()->user->role == WebUser::PUBLISHER) {
    $items = array(
        array('label' => 'Каталог<span class="bg_color_th"></span>', 'url' => array('/publisher/MagazineCatalog/admin'), 'visible' => (Yii::app()->user->role == WebUser::PUBLISHER)),
        array('label' => 'Акция<span class="bg_color_th"></span>', 'url' => array('/publisher/MagazineStock/create'), 'visible' => (Yii::app()->user->role == WebUser::PUBLISHER), 'active' => preg_match('/(MagazineStock\/create|MagazineStock\/index)/', $_SERVER['REQUEST_URI'])),
        array('label' => 'Промоматериалы<span class="bg_color_th"></span>', 'url' => array('/publisher/MagazinePromo/create'), 'visible' => (Yii::app()->user->role == WebUser::PUBLISHER), 'active' => preg_match('/(MagazinePromo\/admin|MagazinePromo\/index|MagazinePromo\/create)/', $_SERVER['REQUEST_URI'])),
        array('label' => 'О компании<span class="bg_color_th"></span>', 'url' => array('/publisher/magazineAbout/admin'), 'visible' => (Yii::app()->user->role == WebUser::PUBLISHER)),
        array('label' => 'Редактирование главной<span class="bg_color_th"></span>', 'url' => array('/publisher/magazineMain/admin'), 'visible' => (Yii::app()->user->role == WebUser::PUBLISHER)),
        array('label' => 'Доставка<span class="bg_color_th"></span>', 'url' => array('/publisher/MagazinePublisherDelivery/admin'), 'visible' => (Yii::app()->user->role == WebUser::PUBLISHER)),
        array('label' => 'Тип продукта<span class="bg_color_th"></span>', 'url' => array('/publisher/MagazineTypeProduct/admin'), 'visible' => (Yii::app()->user->role == WebUser::PUBLISHER)),
        array('label' => 'Редактирование текста скидки<span class="bg_color_th"></span>', 'url' => array('/publisher/magazineMain/discount'), 'visible' => (Yii::app()->user->role == WebUser::PUBLISHER)),
        array('label' => 'Фильты и теги<span class="bg_color_th"></span>', 'url' => array('/publisher/MagazineFilters/admin'), 'visible' => (Yii::app()->user->role == WebUser::PUBLISHER)),
        array('label' => 'Настройки<span class="bg_color_th"></span>', 'url' => array('/admin/Setting/index'), 'visible' => (Yii::app()->user->role == WebUser::PUBLISHER)),
    );
}
?>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="language" content="en">

    <!-- blueprint CSS framework -->
    <link rel="stylesheet" href="/html_source/css/all.css">
    <!-- page style-->
    <link rel="stylesheet" href="/html_source/css/all_crm.css">
    <link rel="stylesheet" href="/html_source/css/jquery.fancybox.css">
    <!--	<link rel="stylesheet" href="/html_source/css/all_client.css">-->
    <style>
        <?php include Yii::getPathOfAlias('webroot').'/html_source/head_style.php'?>
    </style>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
    <script src="/html_source/js/jquery.fancybox.js"></script>
    <script src="/html_source/js/core.js"></script>
    <script src="/html_source/js/all.js"></script>
    <script src="/html_source/js/admin.js"></script>
    <script src="/html_source/js/all_crm.js"></script>
    <script src="/html_source/js/crm_add_client_file.js"></script>
    <title><?php echo CHtml::encode($this->pageTitle); ?></title>

</head>
<body>
<div id="left_bar" class="inline_style">
    <div id="left_bar_wrap">
        <?php
        echo CHtml::image('/upload/' . @$this->dataModel['logo_site'], '', ['width' => 50, 'height' => 50, 'class' => 'logo' . ($this->connectID == 36 ? ' barbers-logo' : null)]);
        ?>
        <?php $this->widget('zii.widgets.CMenu', array(
            'encodeLabel' => false,
            'items' => $items,
            'submenuHtmlOptions' => ['class' => 'css_trans'],
            'itemCssClass' => 'child_true css_trans'
        )); ?>
    </div>
</div><div id="conten_zone" class="inline_style">
    <!--content_area-->
    <?= $content ?>
    <!--	</div>-->

    <!--content_area end-->
    <!--footer start-->
    <div id="footer_margin"></div>
    <div id="footer">
        <p class="copyright">© 2015 opt prodject</p>

        <div class="inline_style">
            <a href="#" class="mail_rss color_th_hover css_trans border_hover"><span>Подписаться на рассылку</span>
                <span class="color_th">→</span></a>
        </div>
        <div class="inline_style">
            <ul class="soc_li">
                <li><a href="#" target="blank"><img src="/html_source/img/soz1.png" alt=""></a></li>
                <li><a href="#" target="blank"><img src="/html_source/img/soz2.png" alt=""></a></li>
                <li><a href="#" target="blank"><img src="/html_source/img/soz3.png" alt=""></a></li>
            </ul>
        </div>
        <div class="inline_style">
            <ul class="map_site">
                <li><a href="/site/about" class="color_th_hover css_trans">О магазине</a></li>
                <li><a href="/site/delivery" class="color_th_hover css_trans">Доставка</a></li>
                <li><a href="#" class="color_th_hover css_trans">Личный кабинет</a></li>
            </ul>
        </div>
    </div>
    <!--footer_end-->
</div>
</body>
</html>