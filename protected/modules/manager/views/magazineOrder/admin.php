<?php
/* @var $this MagazineOrderController */
/* @var $model MagazineOrder */

$this->breadcrumbs = array(
    'Magazine Orders' => array('index'),
    'Manage',
);

$this->menu = array(
    array('label' => 'List MagazineOrder', 'url' => array('index')),
    array('label' => 'Create MagazineOrder', 'url' => array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#magazine-order-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>
<div id="align_zone cont">
    <div id="manager_short_info">
        <div class="img_fon inline_style" style="background-image:url(<?= $manager_info->user_avatar ?>)"></div>
        <div class="inline_style">
            <p><?= $manager_info->user_name ?></p>
            <p><?= $manager_info->user_surname ?></p>
        </div>
        <div class="inline_style">
            <p><?= $manager_info->user_phone ?></p>

            <p>e-mail: <?= $manager_info->user_email ?></p>
        </div>
        <div class="inline_style">
            <a href="/site/logout" class="log_out_castom_manager" title="Выход"></a>
        </div>

    </div>

    <p class="manager_title">Заказы</p>
    <ul id="crm_menu">
        <li class="inline_style"><a href="/manager/MagazineOrder/admin"
                                    class="color_th_hover  css_trans <?= (isset($_GET['status']) ? '' : 'active') ?>">Новые
                заказы<span class="bg_color_th css_trans"></span></a></li>
        <li class="inline_style"><a href="/manager/MagazineOrder/admin?status=1,2"
                                    class="color_th_hover css_trans <?= ($_GET['status'] == '1,2' ? 'active' : '') ?>">Заказы
                в работе<span class="bg_color_th css_trans"></span></a></li>
        <li class="inline_style"><a href="/manager/MagazineOrder/admin?status=3"
                                    class="color_th_hover css_trans <?= ($_GET['status'] == '3' ? 'active' : '') ?> ">История
                заказов<span class="bg_color_th css_trans"></span></a></li>
        <li class="inline_style" style="float: right"><span style="margin-right: 2vw">За Период:</span>
            <span class="begin_date" style="display: none"></span>
            <span class="end_date" style="display: none"></span>
            <button class="click" style="display: none"></button>

            <span style="margin: 0 0.5vw">С:</span> <?= $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                'name' => 'from_date',
                // additional javascript options for the date picker plugin
                'language' => 'ru',
                'options' => array(
                    'dateFormat' => 'yy-mm-dd',
                    'showAnim' => 'fold',
                    'buttonImageOnly' => true,
                    'changeMonth' => true,
                    'changeYear' => true,
                    'showButtonPanel' => true,
                    'showOtherMonths' => true,
                    'onSelect' => 'js: function(dateText, inst) {
                            $(".begin_date").text(dateText)
                            $(".click").click()
                        }',
                ),
                'htmlOptions' => array(
                    'style' => 'height:20px;'
                ),
            ), true); ?>
            <span style="margin: 0 0.5vw">По:</span> <?= $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                'name' => 'publishDate2',
                // additional javascript options for the date picker plugin
                'language' => 'ru',
                'options' => array(
                    'dateFormat' => 'yy-mm-dd',
                    'showAnim' => 'fold',
                    'buttonImageOnly' => true,
                    'changeMonth' => true,
                    'changeYear' => true,
                    'showButtonPanel' => true,
                    'showOtherMonths' => true,
                    'onSelect' => 'js: function(dateText, inst) {
                            $(".end_date").text(dateText)
                            $(".click").click()
                        }',
                ),
                'htmlOptions' => array(
                    'style' => 'height:20px;'
                ),
            ), true); ?>
        </li>
    </ul>

    <div class="content-list ">
        <!-- orders_list-->
        <?php
        $client_id = null;
        $min_order = 0;
        foreach ($model as $md) {
            $order2 = MagazineOrder_2::model()->findByAttributes(['order_id' => $md->id]);

            if ($client_id == null) {
                $client_id = $md->client_id;
                $user = MagazineUsers::model()->findByPk($client_id);
                if($user){
                    $min_order = $user->min_order;
                }
            }
            ?>
            <div class="white_fon inline_style orders_list_item">
            <div class="order_list_head">
                <p class="order_title">Заказ <b>№<?= $md->id ?></b> на <b><span class="money"><?= $order2->order_price ?></span>
                        руб.</b>
                    <?php if ($md->status == 0) { ?>
                        <span class="new">Новый</span>
                    <?php } ?>
                    <?php if ($md->price < $min_order) { ?>
                        <span title="Сумма заказа меньше минимальной суммы заказа" class="min_order"></span>
                    <?php } ?>
                </p>

                <div class="order_des">
                    <p>Клиент: <?= $md->company_name ?></p>

                    <p>Поступил: <?= date('d.m.Y H:i', strtotime($md->time_create)) ?></p>
                    <!--					<p>Ожидаемое прибытие 25 октября 2015</p>-->
                    <p>Статус заказа: <b><?php switch ($md->status) {
                                case MagazineOrder::STATUS_NEW:
                                    echo 'новый';
                                    break;
                                case MagazineOrder::STATUS_COLLECTED:
                                    echo 'собирается';
                                    break;
                                case MagazineOrder::STATUS_SHIPPED:
                                    echo 'отгружен';
                                    break;
                                case MagazineOrder::STATUS_CLOSE:
                                    echo 'закрыт';
                                    break;
                            } ?></b></p>
                </div>
            </div>
            <div class="order_list_buttons">
                <?php if ($md->countFiles > 0) { ?>
                    <a href="/manager/MagazineDocuments/index/id/<?= $md->id ?>"
                       class="css_trans color_th_hover border_hover">Документы по заказу</a>
                <?php } else { ?>
                    <a href="/manager/MagazineDocuments/index/id/<?= $md->id ?>"
                       class="css_trans color_th_hover border_hover">Документов нет</a>
                <?php } ?><br>
                <a href="/manager/MagazineOrder/ShowOrder/id/<?= $md->id ?>" class="css_trans border_hover">Подробнее
                    <span class="color_th">→</span></a><br>
                <?php if ($md->status == 0) { ?>
                    <a href="/manager/MagazineOrder/UpdateStatus/id/<?= $md->id ?>" class="send_order_to_work">Отправить
                        в работу</a>
                <?php } ?>

            </div>
            </div><?php } ?>
        <!-- orders_list end-->
    </div>

</div>
