<script type="text/javascript">
    $(function () {
        var input = document.querySelector("input[type='file']");
        input.onchange = function () {
            this.form.submit();
        }
    });
</script>
<script type="text/javascript">
    var discont_rules =  <?=$discount?>;
</script>
<div id="align_zone">
    <div id="manager_short_info">
        <div class="img_fon inline_style" style="background-image:url(<?= $manager_info->user_avatar ?>)"></div>
        <div class="inline_style">
            <p><?= $manager_info->user_name ?></p>

            <p><?= $manager_info->user_surname ?></p>
        </div>
        <div class="inline_style">
            <p><?= $manager_info->user_phone ?></p>

            <p>e-mail: <?= $manager_info->user_email ?></p>
        </div>
        <div class="inline_style">
            <a href="/site/logout" class="log_out_castom_manager" title="Выход"></a>
        </div>

    </div>

    <a href="/manager/MagazineOrder/admin" class="back_to_orders">← Вернуться к списку заказов</a>

    <div>
        <div class="inline_style" id="manager_orders_l">
            <div class="white_fon">
                <div id="order_head">
                    <p class="order_title">Заказ <b>№<?= $order2[0]->order_id ?></b> на <b><span
                                class="money"><?= $order2[0]->order_price ?></span> руб.</b></p>

                    <div class="order_des">
                        <p>Клиент: <?= $model->company_name ?></p>

                        <p>Поступил: <?= date('d.m.Y H:i', strtotime($model->time_create)) ?></p>
                        <?php if ($model->time_order != '') { ?>
                            <p>Ожидаемое прибытие: <?= date('d.m.Y', strtotime($model->time_order)) ?></p>
                        <?php } ?>
                        <p>Статус заказа: <b><?php switch ($model->status) {
                                    case MagazineOrder::STATUS_NEW:
                                        echo 'новый';
                                        break;
                                    case MagazineOrder::STATUS_COLLECTED:
                                        echo 'собирается';
                                        break;
                                    case MagazineOrder::STATUS_SHIPPED:
                                        echo 'отгружен';
                                        break;
                                    case MagazineOrder::STATUS_CLOSE:
                                        echo 'закрыт';
                                        break;
                                } ?></b></p>

                    </div>
                </div>
                <div id="manager_button_box">
                    <button id="cd_order_status" class="css_trans">Изменить статус заказа</button>
                    <br>

                    <div id="hid_status" class="hid_box">
                        <form name="cd_status" action="" method="post">
                            <div class="hid_cd_status">
                                <input type="radio" name="status" value="0">

                                <p class="color_th_hover ">Новый</p>
                            </div>
                            <div class="hid_cd_status">
                                <input type="radio" name="status" value="1">

                                <p class="color_th_hover css_trans">Собирается</p>
                            </div>
                            <div class="hid_cd_status">
                                <input type="radio" name="status" value="2">

                                <p class="color_th_hover css_trans">Отгружен</p>
                            </div>
                            <div class="hid_cd_status">
                                <input type="radio" name="status" value="3">

                                <p class="color_th_hover css_trans">Закрыт</p>
                            </div>
                        </form>
                    </div>
                    <?php if ($model->status == MagazineOrder::STATUS_NEW || $model->status == MagazineOrder::STATUS_COLLECTED)
                    {
                        $text = 'Уведомить клиента о том, что заказ принят';
                        if($model->accepted == 1){
                            $text = 'Дата поставки выбрана';
                        }
                        ?>
                        <button id="marker_data" class="css_trans"><?=$text?></button>
                        <br>
                    <?php } ?>
                    <a style="margin-top: 10px;color: #565b6d;" href="/site/DownloadReport/id/<?= $model->id ?>"
                       class="s_a css_trans border_hover">Выгурзить в xls</a>
                    <div id="hid_marker_form" class="hid_box">
                        <form name="manager_order_data" action="" method="post">
                            <input type="hidden" name="data_manager_order">
                        </form>
                        <p id="calendar_title">Выберите дату поставки</p>
                        <script>
                            <?php
                                $timeChange = strtotime($model->time_order);
                                $data_d = date('d',$timeChange );
                                $data_m = date('n',$timeChange );
                                $data_y = date('Y',$timeChange );
                            ?>
                            var data_arr = [{
                                "data_d": "<?=$data_d?>",
                                "data_m": "<?=$data_m?>",
                                "data_y": "<?=$data_y?>",
                                "href": "/"
                            }];
                        </script>
                        <table id="calendar2">
                            <thead class="color_th">
                            <tr>
                                <td>‹
                                <td colspan="5" id="calendar_data">
                                <td>›
                            </tr>
                            <tr>
                                <td>Пн
                                <td>Вт
                                <td>Ср
                                <td>Чт
                                <td>Пт
                                <td>Сб
                                <td>Вс
                            </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                        <div id="calendar_legend">
                            <span class="legend_today"></span><span> - сегодня </span><span
                                class="legend_active bg_color_th"></span><span> - выбранный день</span>
                        </div>
                        <button class="border_hover css_trans">Уведомить клиента</button>
                    </div>
                </div>

                <!--cart_order-->
                <div id="cart_item_table">
                    <div id="cart_item_head">
                        <div class="row1 inline_style">
                            Фото
                        </div>
                        <div class="row2 inline_style">
                            Наименование
                        </div>
                        <div class="row3 inline_style">
                            Цена
                        </div>
                        <div class="row4 inline_style">
                            Количество
                        </div>
                        <div class="row5 inline_style">
                            Стоимость
                        </div>
                        <div class="row6 inline_style">
                            Удалить
                        </div>
                    </div>
                    <div id="cart_item_body">
                        <!-- cart_item_line-->
                        <!-- single cart_item_line-->
                        <?php
                        $allCost = 0;
                        foreach ($order2 as $info) {
                            $allCost += $info->with_bonus_sum;
                            if ($info->image) {
                                $json = json_decode($info->image);
                                $img = null;
                                if (!empty($json)) {
                                    foreach ($json as $image) {
                                        $img = $image;
                                        break;
                                    }
                                }
                            }

                            ?>
                            <div class="cart_item_line" data-id="<?= $info->id ?>">
                                <div class="row1 inline_style img_fon" style="background-image:url(<?= $img ?>)">

                                </div>
                                <div class="row2 inline_style">
                                    <?= $info->name ?>
                                </div>
                                <div class="row3 inline_style">
                                    <span class="money select_money"><?= $info->cost_item ?></span><span class="rub_lt">a</span>
                                </div>
                                <div class="row4 inline_style">
                                    <?php
                                    $magazineProduct = MagazineProduct::model()->findByPk($info->goods_id);
                                    echo $model->status == MagazineOrder::STATUS_NEW ? $this->widget(
                                        'booster.widgets.TbEditableField',
                                        array(
                                            'model' => $info,
                                            'emptytext' => 'Не назначено',
                                            'title' => 'Изменить количество товара',
                                            'htmlOptions' => [
                                                'style' => 'border-bottom: none;'
                                            ],
                                            'attribute' => 'count_item',
                                            'text' => $info->count_item ? $info->count_item : null,
                                            //  'url' => '/manager/MagazineOrder/setNewGoodsCount/client_id/'.$model->client_id,
                                            'options' => array(
                                                'send' => false
                                            ),

                                            'validate' => 'js: function(value) {
                                                 if($.trim(value) < ' . $magazineProduct->min_count != null ? $magazineProduct->min_count : 0 . ') return "количество должно быть не меньше ' . $magazineProduct->min_count != null ? $magazineProduct->min_count : 0 . '";
                                            }',
                                            'onSave' => 'js: function(e, params) {
                                                $(this).closest(".cart_item_line").addClass("edit_item");
                                                var allCost = $(this).closest(".cart_item_line").find(".row3 .money").text();
                                                 var t = allCost.replace(" ","");
                                                 var summa = t * params.newValue;
                                                 var summa = String(summa);
                                                summa = summa.replace(/(\s)+/g, "").replace(/(\d{1,3})(?=(?:\d{3})+$)/g, "$1 ");
                                                 $(this).closest(".cart_item_line").find(".row5 .money").text(summa);
                                                 var allCost = 0;
                                                 $(".cart_item_line").each(function(){
                                                 var noPr = $(this).find(".row5 .money").text();
                                                 noPr = noPr.replace(" ","");
                                                 noPr = noPr.replace(" ","");
                                                 allCost = (Number(allCost) + Number(noPr));
                                                 allCost = String(allCost)
                                                 })
                                                allCost = allCost.replace(/(\s)+/g, "").replace(/(\d{1,3})(?=(?:\d{3})+$)/g, "$1 ");
                                                 var all = $("#cart_item_footer .inline_style b .money").text(allCost);
                                             }'
                                        ), true
                                    ) : $info->count_item;
                                    ?>
                                </div>
                                <div class="row5 inline_style">
                                    <?php
                                    ?>
                                    <span class="money select_money"><?= $info->with_bonus_sum ?></span><span
                                        class="rub_lt">c</span>
                                    <span class="set_bonus" style="display: none"><?= $info->bonus ?></span>

                                </div>
                                <div class="row6 inline_style">
                                    <span class="delete_item" data-id="<?= $info->id ?>"
                                          style="margin-left: 1vw">Х</span>
                                </div>
                            </div>
                        <?php }
                        ?>
                        <!--end single cart_item_line-->


                        <!--end cart_item_line-->
                    </div>
                    <div id="cart_item_footer">
                        <div class="inline_style cart_itog" style="display: block">
                            <span><span class="cart_itog_dop"> Скидка</span>:</span><b><span
                                    class="money_disc select_money"><?= isset($order2[0]) ? $order2[0]->discount : 0 ?></span><span
                                    class="rub_lt">c</span></b>
                        </div>
                        <div class="inline_style cart_itog">
                            <span>Итого<span class="cart_itog_dop"> со скидкой и бонусом</span>:</span><b><span
                                    class="money select_money"><?= $allCost - $order2[0]->discount ?></span><span
                                    class="rub_lt">c</span></b>
                        </div>
                    </div>
                    <div style="text-align: end">
                        <input type="hidden" class="bonus_val" value="<?= $bonus ?>">
                        <button id="save_result" class="css_trans save_button">Сохранить</button>
                    </div>

                </div>
                <!--cart_order-->
            </div>

            <p class="manager_title">Документооборот</p>

            <div id="manager_order_doc_view" class="white_fon">
                <?php
                if (isset($documents) && !empty($documents)) {
                    foreach ($documents as $document) {
                        $arrayFormats = explode('.', $document->name);
                        $img = '';
                        if (isset($arrayFormats[1])) {
                            if ($arrayFormats[1] == 'docx') {
                                $img = '<div class="icon-format order_icon" style="background-position: -4.4vw -0.2vw;"></div>';
                            } elseif ($arrayFormats[1] == 'xlsx') {
                                $img = '<div class="icon-format order_icon" style="background-position: 4.4vw -0.2vw;"></div>';
                            } elseif ($arrayFormats[1] == 'pptx') {
                                $img = '<div class="icon-format order_icon" style="background-position: 2.2vw -0.2vw;"></div>';
                            } elseif ($arrayFormats[1] == 'pdf') {
                                $img = '<div class="icon-format order_icon" style="background-position: -0.1vw -0.2vw;"></div>';
                            } elseif ($arrayFormats[1] == 'jpg') {
                                $img = '<img src="' . $document->url . '">';
                            } elseif ($arrayFormats[1] == 'png') {
                                $img = '<img src="' . $document->url . '"">';
                            } else {
                                $img = '<div class="icon-format-none order_icon_none" style="background-position: -0.6vw 0;"></div>';
                            }
                        }
                        ?>
                        <!-- file list-->
                        <div class="manager_order_doc_line">
                            <?= $img ?>

                            <div class="inline_style order_icons">
                                <p><?= $document->name ?></p>
                                <!--                        <span class="new">Новый</span>-->
                            </div>
                            <a href="<?= $document->url ?>" download=""><span class="color_th">↓</span> Скачать</a>
                        </div>
                    <?php }
                }
                ?>
                <!-- file list end-->
                <div id="manager_order_file_load">
                    <form name="manager_order_file_load" action="" method="POST" enctype="multipart/form-data">
                        <input type="file" name="MagazineDocuments[url]">
                        <button class="border_hover">Прикрепить документ</button>
                    </form>
                </div>
            </div>

        </div>
        <div class="inline_style" id="manager_orders_r">
            <p class="manager_title">Магазин для поставки</p>
            <!--magaz_box-->
            <div class="magaz_info_box white_fon">
                <p class="magaz_info_box_title color_th"><?= $magazineInfo->address ?></p>

                <p class="magaz_info_box_contact_title">Контактное лицо(приемщик):</p>

                <div class="magaz_info_box_contact_wrap">
                    <p><?= $magazineInfo->name_contact ?> <?= $magazineInfo->surname_contact ?></p>

                    <p><?= $magazineInfo->phone ?></p>

                    <p>e-mail: <?= $magazineInfo->email ?></p>
                </div>
                <div class="magaz_info_dop_content">
                    <?= $magazineInfo->text_contact ?>
                </div>
            </div>
            <!--magaz_box end-->
        </div>
    </div>

</div>
<script>
    $(function () {
        var summa = $('.money_disc').text();
        summa = String(summa).replace(/(\s)+/g, "").replace(/(\d{1,3})(?=(?:\d{3})+$)/g, "$1 ");
        $('.money_disc').text(summa);
    });
</script>
<?php if ($popup == true) { ?>
    <script type="text/javascript">
        $(function () {
            creat_lb_sv('<div style="height:200px;"><div class="vert_style" style="width:80%"><img src="/html_source/img/cart_ok.png">Заказ в архиве</div><div class="vert_style"></div></div>');
        });
    </script>
<?php } ?>
