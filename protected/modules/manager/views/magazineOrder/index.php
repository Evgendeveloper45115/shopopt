<?php
/* @var $this MagazineOrderController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Magazine Orders',
);

$this->menu=array(
	array('label'=>'Create MagazineOrder', 'url'=>array('create')),
	array('label'=>'Manage MagazineOrder', 'url'=>array('admin')),
);
?>

<h1>Magazine Orders</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
