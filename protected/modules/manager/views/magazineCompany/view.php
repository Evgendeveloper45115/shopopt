<?php
/* @var $this MagazineCompanyController */
/* @var $model MagazineCompany */

$this->breadcrumbs=array(
	'Magazine Companies'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List MagazineCompany', 'url'=>array('index')),
	array('label'=>'Create MagazineCompany', 'url'=>array('create')),
	array('label'=>'Update MagazineCompany', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete MagazineCompany', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage MagazineCompany', 'url'=>array('admin')),
);
?>

<h1>View MagazineCompany #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'name_company',
		'site_connect_id',
		'client_id',
	),
)); ?>
