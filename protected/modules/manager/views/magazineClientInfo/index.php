<div id="manager_short_info">
    <div class="img_fon inline_style" style="background-image:url(<?= $manager_info->user_avatar ?>)"></div>
    <div class="inline_style">
        <p><?= $manager_info->user_name ?></p>

        <p><?= $manager_info->user_surname ?></p>
    </div>
    <div class="inline_style">
        <p><?= $manager_info->user_phone ?></p>

        <p>e-mail: <?= $manager_info->user_email ?></p>
    </div>
    <div class="inline_style">
        <a href="/site/logout" class="log_out_castom_manager" title="Выход"></a>
    </div>

</div>

<div class="manager_finder_box">
    <p class="manager_title">Клиенты</p>

    <form name="manager_finder" action="" method="POST">
        <input type="text" name="manager_finder_str" placeholder="Поиск по названию или номеру телефона...">
    </form>
</div>

<div class="manager_clients_box">
    <?php foreach ($allCompanies as $compani) {
        $count = $orders = MagazineOrder::model()->count(['condition' => 'client_company_id = ' . $compani['client_company_id'] . ' and status = "' . MagazineOrder::STATUS_NEW . '" and manager_id =' . Yii::app()->user->id]);
        ?>
        <div class="client_item">
        <!--        <img src="/html_source/img/logo_client.png" alt="" class="client_logo">-->
        <p class="client_title"><a
                href="/admin/Clients/index/id/<?= $compani['client_company_id'] ?>"><?= $compani['name_company'] ?></a>
        </p>

        <div class="client_info">
            <p><?= $compani['user_name'] ?> <?= $compani['user_surname'] ?></p>

            <p><?= $compani['user_phone'] ?></p>
            <a href="mailto:<?= $compani['user_email'] ?>"><?= $compani['user_email'] ?></a>
        </div>
        <a href="/manager/clients/index/id/<?= $compani['client_company_id'] ?>"
           class="more_info color_th_hover css_trans border_hover">Подробнее <span class="color_th">→</span></a>
        <?php if ($count > 0) { ?>
            <a href="/manager/MagazineOrder/admin?comp_id=<?= $compani['client_company_id'] ?>"
               class="new_order border_hover color_th_hover css_trans">Новый заказ

                <span><?= $count ?></span>

            </a>
        <?php } ?> <p>Всего получено:</p>

        <p class="money_style"><span class="money"><?= $compani['sum'] ?></span><span>руб.</span></p>

        <p><?= $compani['count_in_work'] ?> заказов в работе на сумму:</p>

        <p class="money_style"><span class="money"><?= $compani['sum_in_work'] ?></span><span>руб.</span></p>
        </div><?php
    }
    ?>

</div>