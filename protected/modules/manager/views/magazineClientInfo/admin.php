<script type="text/javascript">
	<!-- page script-->
	<!-- php json_encode(array())-->
	var content_item = '<?=addslashes($contentTovar)?>';

	var contet_akz = '<?=$contentStock?>';
	var idCompany = <?=$id?>;
</script>
<div id="align_zone">
	<div id="manager_short_info">
		<div class="img_fon inline_style" style="background-image:url(<?=$mangerInfo->user_avatar?>)"></div>
		<div class="inline_style">
			<p><?=$mangerInfo->user_name?></p>
			<p><?=$mangerInfo->user_surname?></p>
		</div>
		<div class="inline_style">
			<p><?=$mangerInfo->user_phone?></p>
			<p>e-mail: <?=$mangerInfo->user_email?></p>
		</div>
		<div class="inline_style">
			<a href="/site/logout" class="log_out_castom_manager" title="Выход"></a>
		</div>
	</div>

	<a href="/manager/MagazineClientInfo/index" class="back_to_orders">← Вернуться к списку клиентов</a>

	<div class="white_fon client_info_line">
		<div class="inline_style img_cont">
			<img src="/html_source/img/logo_client.png">
		</div><div class="inline_style title">
			<?=$companyInfo->name_company?>
		</div><div class="inline_style link_box">
			<!--            <a href="#" class="inline_style show_client_cart border_hover css_trans">Показать карточку клиента</a>-->
			<div class="short_line inline_style">
				<p><?=$clinetInfo->user_name?> <?=$clinetInfo->user_surname?></p>
				<p><?=$clinetInfo->user_phone?></p>
				<p>e-mail: <a href="mailto:<?=$clinetInfo->user_email?>"><?=$clinetInfo->user_email?></a></p>
			</div>
			<a href="/admin/MagazineUsers/update/id/<?=$clinetInfo->id?>" class="inline_style cd_client border_hover css_trans">Изменить <span class="color_th">→</span></a>
		</div>

	</div>

	<div class="manager_box_row">
		<div class="inline_style">
			<p class="manager_title">Новые заказы:</p>

			<div class="white_fon">
				<!--orders_item-->
				<?php foreach($orderNews as $order){?>
					<div class="manager_client_page_order">
						<p class="order_title">Заказ <b>№<?=$order->id?></b> на <b><span class="money"><?=$order->price?></span> руб.</b> <a href="/admin/orders/index/id/<?=$order->id?>" class="s_a border_hover css_trans">Подробнее <span class="color_th">→</span></a> </p>
						<div class="order_des">
							<p>Поступил: <?=$order->price?></p>
							<p>Адреса магазинов: <?=$order->companyInfo->name_company?></p>
							<p>Статус заказа: <b><?php switch($order->status) {
										case MagazineOrder::STATUS_NEW:
											echo 'новый';
											break;
										case MagazineOrder::STATUS_COLLECTED:
											echo 'собирается';
											break;
										case MagazineOrder::STATUS_SHIPPED:
											echo 'отгружен';
											break;
										case MagazineOrder::STATUS_CLOSE:
											echo 'закрыт';
											break;
									}?></b></p>
						</div>
						<a href="/admin/MagazineDocuments/index/id/<?=$order->id?>" class="link_in_page_order border_hover css_trans">Документы по заказу</a>
						<!--                    <a href="#" class="to_work link_in_page_order">В работу</a>-->
					</div>
				<?php }?>
				<?php if(!empty($orderNews)) { ?>
					<a href="/admin/Orders/admin" class="show_more_orders  color_th_hover css_trans">Показать еще ↓</a>
				<?php } ?>
			</div>
		</div><div class="inline_style">
			<p class="manager_title">Заказы в работе:</p>
			<div class="white_fon">
				<!--orders item-->
				<?php foreach($orderWork as $order){?>
				<div class="manager_client_page_order">
					<p class="order_title">Заказ <b>№<?=$order->id?></b> на <b><span class="money"><?=$order->price?></span> руб.</b> <a href="/admin/orders/index/id/<?=$order->id?>" class="s_a border_hover css_trans">Подробнее <span class="color_th">→</span></a> </p>
					<div class="order_des">
						<p>Поступил: <?=$order->price?></p>
						<p>Адреса магазинов: <?=$order->companyInfo->name_company?></p>
						<p>Статус заказа: <b><?php switch($order->status) {
									case MagazineOrder::STATUS_NEW:
										echo 'новый';
										break;
									case MagazineOrder::STATUS_COLLECTED:
										echo 'собирается';
										break;
									case MagazineOrder::STATUS_SHIPPED:
										echo 'отгружен';
										break;
									case MagazineOrder::STATUS_CLOSE:
										echo 'закрыт';
										break;
								}?></b></p>
					</div>
					<a href="/admin/MagazineDocuments/index/id/<?=$order->id?>" class="link_in_page_order border_hover css_trans">Документы по заказу</a>
				</div>
				<?php }?><!--orders_item one end-->
				<!--order item end-->
				<?php if(!empty($orderWork)) {?>
					<a href="/admin/orders/admin?status=1,2" class="show_more_orders color_th_hover css_trans">Показать еще ↓</a>
				<?php }?>
			</div>
		</div>
	</div>

	<div class="manager_box_row">
		<div class="inline_style">
			<p class="manager_title">Документооборот</p>
			<div class="white_fon">
				<?php foreach($documents as $doc){?>
					<div class="manager_file_view">
						<img src="<?=$doc->url?>" class="d_inline">
						<p class="d_inline"><?=$doc->name?></p>
						<a href="<?=$doc->url?>" class="s_a border_hover css_trans"><span class="color_th">↓</span> Скачать</a>
					</div>
				<?php } ?>
				<div class="manager_file_link">
<!--					<a href="#" class="s_a border_hover css_trans">Посмотреть все документы <span class="color_th">→</span></a>-->
					<div class="manager_all_file_in">
					</div>
				</div>
			</div>
		</div><div class="inline_style">
			<p class="manager_title">История заказов</p>
			<div class="white_fon">
				<!--hist order-->
				<?php foreach($orderHistory as $order){?>
					<div class="manager_client_page_order">
						<p class="order_title">Заказ <b>№<?=$order->id?></b> на <b><span class="money"><?=$order->price?></span> руб.</b> <a href="/admin/orders/index/id/<?=$order->id?>" class="s_a border_hover css_trans">Подробнее <span class="color_th">→</span></a> </p>
						<div class="order_des">
							<p>Поступил: <?=$order->price?></p>
							<p>Адреса магазинов: <?=$order->companyInfo->name_company?></p>
							<p>Статус заказа: <b><?php switch($order->status) {
										case MagazineOrder::STATUS_NEW:
											echo 'новый';
											break;
										case MagazineOrder::STATUS_COLLECTED:
											echo 'собирается';
											break;
										case MagazineOrder::STATUS_SHIPPED:
											echo 'отгружен';
											break;
										case MagazineOrder::STATUS_CLOSE:
											echo 'закрыт';
											break;
									}?></b></p>
						</div>
						<a href="/admin/MagazineDocuments/index/id/<?=$order->id?>" class="link_in_page_order border_hover css_trans">Документы по заказу</a>
						<!--                    <a href="#" class="to_work link_in_page_order">В работу</a>-->
					</div>
				<?php }?>
				<!--hist order one end-->
				<!--hist order end-->
				<?php if(!empty($orderHistory)) {?>
					<a href="/admin/orders/admin?status=3" class="show_more_orders color_th_hover css_trans">Показать еще ↓</a>
				<?php } ?>
			</div>
		</div>
	</div>

	<div class="manager_box_row">
		<div class="inline_style">
			<p class="manager_title">Промоматериалы</p>
			<div class="white_fon">
				<?php foreach($PromoFiles as $promoFile){?>
					<div class="manager_file_view">
						<img src="<?=$promoFile->url?>" class="d_inline">
						<p class="d_inline"><?=$promoFile->name_file?></p>
						<a href="<?=$promoFile->url?>" class="s_a border_hover css_trans"><span class="color_th">↓</span> Скачать</a>
					</div>
				<?php }?>
				<div class="manager_file_link">
					<a href="/admin/Promo/index" class="s_a border_hover css_trans">Посмотреть все промоматериалы <span class="color_th">→</span></a>
					<!--                    <div class="manager_all_file_in">-->
					<!--                        <form name="promo_in" method="POST" action=""  enctype="multipart/form-data">-->
					<!--                            <input type="file" name="promo_in_in">-->
					<!--                            <a href="#" class="s_a css_trans">Прикрепить материал</a>-->
					<!--                        </form>-->
					<!--                    </div>-->
				</div>
			</div>
		</div><div class="inline_style">
			<p class="manager_title">Магазины</p>
			<div class="white_fon">
				<?php foreach($magazineUsers as $mag){?>
					<div class="hist_order_cl_page shop_list_cl_page">
						<p class="hist_l"><?=$mag->address?></p><a href="/admin/Orders/admin/?idmagazine=<?=$mag->id?>" class="hist_r s_a border_hover css_trans">Список заказов</a>
					</div>
				<?php }?>
				<!--                <a href="#" class="s_a border_hover css_trans cl_page_top_align">Посмотреть все магазины <span class="color_th">→</span></a>-->
			</div>
		</div>
	</div>


	<p class="manager_title">Скидка клиента</p>
	<div class="white_fon discont_line">
		<!--        <p>7% скидка</p>-->
		<a href="/admin/Clients/Discount/id/<?=$clinetInfo->id?>" class="s_a border_hover css_trans">Изменить скидку <span class="color_th">→</span></a>
		<p>Общая сумма заказов за все время: <span class="money"><?=$sBalance?></span> руб.</p>
	</div>

	<p class="manager_title">Рекомендуемые предложения<a href="#" class=" goods_add manager_select_link bg_color_th">ДОБАВИТЬ ИЗ КАТАЛОГА</a></p>
	<div class="w_c" id="tovari_tut">

		<!--recomend start-->

		<!--single-->
		<?php foreach($CompanyRecommendTovar as $tv){?>
			<div class="goods_item css_trans shadow_hover">
				<a href="#">
					<div class="img_fon" style="background-image:url(<?=$tv->info->image_product?>)"></div>
					<p><?=$tv->info->name_product?></p>
				</a>
				<div class="price inline_style color_th"><span class="money"><?=$tv->info->price?></span><span class="rub_lt">a</span></div>
				<button class="border_hover css_trans color_th_hover"  data-id="<?=$tv->info->id?>">
					<svg class="svg_stroke_color" preserveAspectRatio="xMidYMid meet" viewBox="0 0 194 194" xmlns="http://www.w3.org/2000/svg"><g display="inline"><line id="svg_7" y2="2.500002" x2="191.699994" y1="190.900002" x1="2.499994" stroke-linecap="null" stroke-linejoin="null" ></line><line id="svg_6" y2="191.700002" x2="191.699994" y1="2.500002" x1="2.499994" stroke-linecap="null" stroke-linejoin="null" ></line></g></svg>
					Удалить</button>
			</div>
		<?php }?>
		<!--recomend end-->
	</div>
	<!--akz start-->
	<p class="manager_title">Акции клиента<a href="#" class="manager_select_link bg_color_th akz_add">ДОБАВИТЬ АКЦИЮ</a></p>
	<div class="w_c" id="akz_full_wrp">

		<!-- akz_special_item single-->
		<?php foreach($CompanyRecommendStock as $compStock) {?><div class="akz_special_item">
			<div class="img_fon" style="background-image:url(<?=$compStock->info->image?>)"></div><div class="demo_akz_txt_cont">
				<p class="demo_akz_title"><?=$compStock->info->header_text?></p>
				<div><p><?=$compStock->info->text?></p></div>
				<button class="border_hover css_trans color_th_hover del_akz"  data-id="<?=$compStock->info->id?>">
					<svg class="svg_stroke_color" preserveAspectRatio="xMidYMid meet" viewBox="0 0 194 194" xmlns="http://www.w3.org/2000/svg"><g display="inline"><line id="svg_7" y2="2.500002" x2="191.699994" y1="190.900002" x1="2.499994" stroke-linecap="null" stroke-linejoin="null" ></line><line id="svg_6" y2="191.700002" x2="191.699994" y1="2.500002" x1="2.499994" stroke-linecap="null" stroke-linejoin="null" ></line></g></svg>
					Удалить</button>
			</div>
			</div><?php } ?>
		<!--akz end-->
	</div>
	<button class="bg_color_th"  id="save_client_page">Сохранить</button>

</div>