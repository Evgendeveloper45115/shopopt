
<p class="crm_title">Клиенты</p>
<ul id="crm_menu">
    <li class="inline_style"><a href="/admin/Clients/admin" class="color_th_hover <?=($_GET['price'] == null?'active':'')?> css_trans">По алфавиту<span class="bg_color_th css_trans"></span></a></li>
    <li class="inline_style"><a href="/admin/Clients/admin?price=1" class="color_th_hover  <?=($_GET['price']== 1?'active':'')?>  css_trans">Рейтинг по оборотам<span class="bg_color_th  css_trans"></span></a></li>
    <li class="inline_style" style="float: right"><span style="margin-right: 2vw"><?='За период:'?></span>
        <span class="begin_date" style="display: none"></span>
        <span class="end_date" style="display: none"></span>
        <button class="click_clients" style="display: none"></button>

        <span style="margin: 0 0.5vw">С:</span> <?= $this->widget('zii.widgets.jui.CJuiDatePicker', array(
            'name' => 'from_date',
            // additional javascript options for the date picker plugin
            'language'=>'ru',
            'options' => array(
                'dateFormat'=>'yy-mm-dd',
                'showAnim' => 'fold',
                'buttonImageOnly' => true,
                'changeMonth' => true,
                'changeYear' => true,
                'showButtonPanel' => true,
                'showOtherMonths' => true,
                'onSelect'=> 'js: function(dateText, inst) {
                            $(".begin_date").text(dateText)
                            $(".click_clients").click()
                        }',
            ),
            'htmlOptions' => array(
                'style' => 'height:20px;'
            ),
        ), true); ?>
        <span style="margin: 0 0.5vw">По:</span> <?= $this->widget('zii.widgets.jui.CJuiDatePicker', array(
            'name' => 'publishDate2',
            // additional javascript options for the date picker plugin
            'language'=>'ru',
            'options' => array(
                'dateFormat'=>'yy-mm-dd',
                'showAnim' => 'fold',
                'buttonImageOnly' => true,
                'changeMonth' => true,
                'changeYear' => true,
                'showButtonPanel' => true,
                'showOtherMonths' => true,
                'onSelect'=> 'js: function(dateText, inst) {
                            $(".end_date").text(dateText)
                            $(".click_clients").click()
                        }',
            ),
            'htmlOptions' => array(
                'style' => 'height:20px;'
            ),
        ), true); ?>
    </li>

</ul>

<div id="klient_list">
    <?php foreach ($allCompanies as $compani) { ?>
        <div class="client_item">
<!--        <img src="/html_source/img/logo_client.png" alt="" class="client_logo">-->
        <p class="client_title"><a href="/admin/Clients/index/id/<?=$compani['client_company_id']?>"><?=$compani['name_company']?></a></p>
        <div class="client_info">
            <p><?=$compani['user_name']?> <?=$compani['user_surname']?></p>
            <p><?=$compani['user_phone']?></p>
            <a href="mailto:<?=$compani['user_email']?>"><?=$compani['user_email']?></a>
        </div>
        <a href="/admin/Clients/index/id/<?=$compani['client_company_id']?>"  class="more_info color_th_hover css_trans border_hover">Подробнее <span class="color_th">→</span></a>
        <p>Всего получено:</p>
        <p class="money_style"><span class="money"><?=$compani['sum']?></span><span>руб.</span></p>
        <p><?=$compani['count_in_work']?> заказов в работе на сумму:</p>
        <p class="money_style"><span class="money"><?=$compani['sum_in_work']?></span><span>руб.</span></p>
    </div><?php
    }
    ?>

</div>


