<?php
/* @var $this MagazineDocumentsController */
/* @var $dataProvider CActiveDataProvider */

?>
<script type="text/javascript">
	$(function(){
		var input = document.querySelector("input[type='file']");
		input.onchange = function () {
			this.form.submit();
		}
	});
</script>
<div id="client_top_bar">
	<?php $this->widget('application.extensions.client_manager.ClientManagerWidget'); ?>

<!--	<div id="cart_info">-->
<!--		<a href="#">-->
<!--			<div id="cart_ico" class="inline_style">-->
<!---->
<!--			</div><div id="cart_info_content" class="inline_style">-->
<!--				<div id="align_cart_info">-->
<!--					<p><span id="cart_count">46</span> позиций</p>-->
<!--					<p><span class="money">580200</span> <span class="rub_lt">a</span></p>-->
<!--				</div>-->
<!--			</div>-->
<!--		</a>-->
<!--	</div>-->
</div>

<div id="manager_short_info">
	<div class="img_fon inline_style" style="background-image:url(<?=$managerInfo->user_avatar?>)"></div>
	<div class="inline_style">
		<p><?=$managerInfo->user_name?></p>
		<p><?=$managerInfo->user_surname?></p>
	</div>
	<div class="inline_style">
		<p><?=$managerInfo->user_phone?></p>
		<p>e-mail: <?=$managerInfo->user_email?></p>
	</div>
	<div class="inline_style">
		<a href="/site/logout" class="log_out_castom_manager" title="Выход"></a>
	</div>

</div>

<p class="manager_title">Документооборот</p>

<div id="manager_documents_sort">
	<p>Показать документы:</p>
	<form method="GET" action="" name="manager_view_user_document">
		<select name="select_user_documents">
			<option value="all" <?=($_GET['select_user_documents'] == 'all'?'selected="selected"':'')?>>Всех клиентов</option>
			<?php foreach($clientsCompanies as $key => $cl) {?>
				<option value="<?=$key?>" <?=(isset($_GET['select_user_documents']) && $_GET['select_user_documents'] == $key?'selected="selected"':'' )?>><?=$cl?></option>
			<?php }?>
		</select>
	</form>
</div>
<div class="white_fon manager_views_file">
	<div class="manager_view_file_head">
		<div class="inline_style row1">Наименование</div>
		<div class="inline_style row2">Клиент</div>
		<div class="inline_style row3">К заказу</div>
		<div class="inline_style row4">Дата загрузки</div>
		<div class="inline_style row5">Размер файла</div>
		<div class="inline_style row6">Скачать</div>
	</div>
	<div class="manager_view_file_body">
		<!--file line -->
		<?php foreach($model as $md){
			$arrayFormats = explode('.', $md->url);
			$img = '';
			if (isset($arrayFormats[1])) {
				if ($arrayFormats[1] == 'docx') {
					$img = '<div class="icon-format" style="background-position: -4.4vw -0.2vw;"></div>';
				} elseif ($arrayFormats[1] == 'xlsx') {
					$img = '<div class="icon-format" style="background-position: 4.4vw -0.2vw;"></div>';
				} elseif ($arrayFormats[1] == 'pptx') {
					$img = '<div class="icon-format" style="background-position: 2.2vw -0.2vw;"></div>';
				} elseif ($arrayFormats[1] == 'pdf') {
					$img = '<div class="icon-format" style="background-position: -0.1vw -0.2vw;"></div>';
				} elseif ($arrayFormats[1] == 'jpg') {
					$img = '<img src="' . $md->url . '" class="inline_style">';
				} elseif ($arrayFormats[1] == 'png') {
					$img = '<img src="' . $md->url . '" class="inline_style">';
				}else{
					$img = '<div class="icon-format-none" style="background-position: -0.6vw 0;"></div>';
				}
			}

			?>
		<div class="manager_view_file_line">
			<div class="inline_style row1">
				<?=$img?>
				<p class="inline_style"><?=$md->name?></p>
			</div>
			<div class="inline_style row2"><?=$md->comp->name_company?></div>
			<div class="inline_style row3">№<?=$md->order_id?></div>
			<div class="inline_style row4"><?=$md->date_add?></div>
			<div class="inline_style row5"><?=MagazineDocuments::HumanBytes($md->size_file)?></div>
			<div class="inline_style row6"><a href="<?=$md->url?>" target="_blank" class="border_hover css_trans" download=""><span class="color_th">↓</span> Скачать</a></div>
		</div>
		<?php }?>
		<!--file line end-->
	</div>
</div>


<!--<div id="crm_pagination">-->
<!--	<!-- active == class="bg_color_th"-->
<!--	<a href="#" class="bg_color_th css_trans"><span>1</span></a>-->
<!--	<a href="#" class="bg_color_th_hover css_trans"><span>2</span></a>-->
<!--	<a href="#" class="bg_color_th_hover css_trans"><span>3</span></a>-->
<!--	<a href="#" class="bg_color_th_hover v"><span>Все</span></a>-->
<!--</div>-->