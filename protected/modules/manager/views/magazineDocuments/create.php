<?php
/* @var $this MagazineDocumentsController */
/* @var $model MagazineDocuments */

$this->breadcrumbs=array(
	'Magazine Documents'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List MagazineDocuments', 'url'=>array('index')),
	array('label'=>'Manage MagazineDocuments', 'url'=>array('admin')),
);
?>

<h1>Create MagazineDocuments</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>