<?php
/* @var $this MagazineDocumentsController */
/* @var $dataProvider CActiveDataProvider */

?>
<script type="text/javascript">
	$(function(){
		var input = document.querySelector("input[type='file']");
		input.onchange = function () {
			this.form.submit();
		}
	});
</script>
<div id="manager_short_info">
	<div class="img_fon inline_style" style="background-image:url(<?=$ManagerInfo->user_avatar?>)"></div>
	<div class="inline_style">
		<p><?=$ManagerInfo->user_name?></p>
		<p><?=$ManagerInfo->user_surname?></p>
	</div>
	<div class="inline_style">
		<p><?=$ManagerInfo->user_phone?></p>
		<p>e-mail: <?=$ManagerInfo->user_email?></p>
	</div>
	<div class="inline_style">
		<a href="/site/logout" class="log_out_castom_manager" title="Выход"></a>
	</div>

</div>
<?php

?>
<!--<p class="manager_title">Документооборот</p>-->

<!--<div id="manager_documents_sort">-->
<!--	<p>Показать документы:</p>-->
<!--	<form method="POST" action="" name="manager_view_user_document">-->
<!--		<select name="select_user_documents">-->
<!--			--><?php //foreach($idsComp as $key => $comp) {
//				echo '<option value="'.$key.'">'.$comp.'</option>';
//			}?>
<!--		</select>-->
<!--	</form>-->
<!--</div>-->
<a href="/manager/MagazineOrder/ShowOrder/id/<?=$id?>" class="back_to_orders">← Вернутся к заказу №<?=$id?></a>
<p class="manager_title">Документооборот по заказу № <?=$id?> от <?=date('H.i d.m.Y',strtotime($order->time_create))?></p>
<div class="white_fon  manager_views_file">
	<div id="manager_documents_upload">
		<form method="POST" action=""  enctype="multipart/form-data">
			<input type="file" name="MagazineDocuments[url]" required>
			<input type="hidden" name="order_id" value="<?=$id?>">
			<button class="bg_color_th">Загрузить документ</button>
		</form>
	</div>
	<div class="manager_view_file_head">
		<div class="inline_style row1">Наименование</div>
		<div class="inline_style row2">Клиент</div>
		<div class="inline_style row3">К заказу</div>
		<div class="inline_style row4">Дата загрузки</div>
		<div class="inline_style row5">Размер файла</div>
		<div class="inline_style row6">Скачать</div>
		<div class="inline_style row7">Удалить</div>
	</div>
	<div class="manager_view_file_body">
		<!--file line -->
		<?php foreach($model as $md) {
			$arrayFormats = explode('.', $md->url);
			$img = '';
			if (isset($arrayFormats[1])) {
				if ($arrayFormats[1] == 'docx') {
					$img = '<img src="' . $md->url . '" style="display: none"> <div class="icon-format" style="background-position: -4.4vw -0.2vw;margin-right: 0.7%"></div>';
				} elseif ($arrayFormats[1] == 'xlsx') {
					$img = '<div class="icon-format" style="background-position: 4.4vw -0.2vw;margin-right: 0.7%"></div>';
				} elseif ($arrayFormats[1] == 'pptx') {
					$img = '<div class="icon-format" style="background-position: 2.2vw -0.2vw;margin-right: 0.7%"></div>';
				} elseif ($arrayFormats[1] == 'pdf') {
					$img = '<div class="icon-format" style="background-position: -0.1vw -0.2vw;margin-right: 0.7%"></div>';
				} elseif ($arrayFormats[1] == 'jpg') {
					$img = '<img src="' . $md->url . '" class="d_inline">';
				} elseif ($arrayFormats[1] == 'png') {
					$img = '<img src="' . $md->url . '" class="d_inline">';
				} else {
					$img = '<div class="icon-format-none" style="background-position: -0.6vw 0;margin-right: 0.4%"></div>';
				}
			}

			?>
			<div class="manager_view_file_line">
			<div class="inline_style row1">
				<?=$img?>
				<p class="inline_style"><?=$md->name?></p>
<!--				<span>Новый</span>-->
			</div>
			<div class="inline_style row2"><?=$magaComp->name_company?></div>
			<div class="inline_style row3"><?=$md->order_id?></div>
			<div class="inline_style row4"><?=$md->date_add?></div>
			<div class="inline_style row5"><?=MagazineDocuments::HumanBytes($md->size_file)?></div>
			<div class="inline_style row6"><a href="<?=$md->url?>"  class="border_hover css_trans" download=""><span class="color_th">↓</span> Скачать</a></div>
			<div class="inline_style row7"><svg data-id="<?=$md->id?>" class="svg_stroke_color_hover css_trans" preserveAspectRatio="xMidYMid meet" viewBox="0 0 194 194" xmlns="http://www.w3.org/2000/svg"><g display="inline"><line id="svg_7" y2="2.500002" x2="191.699994" y1="190.900002" x1="2.499994" stroke-linecap="null" stroke-linejoin="null"></line><line id="svg_6" y2="191.700002" x2="191.699994" y1="2.500002" x1="2.499994" stroke-linecap="null" stroke-linejoin="null"></line></g></svg></div>
		</div><?php }?>
		<!-- file line one end-->
		<!--file line end-->

	</div>
</div>

<!--<div id="crm_pagination">-->
<!--	<!-- active == class="bg_color_th"-->
<!--	<a href="#" class="bg_color_th css_trans"><span>1</span></a>-->
<!--	<a href="#" class="bg_color_th_hover css_trans"><span>2</span></a>-->
<!--	<a href="#" class="bg_color_th_hover css_trans"><span>3</span></a>-->
<!--	<a href="#" class="bg_color_th_hover v"><span>Все</span></a>-->
<!--</div>-->