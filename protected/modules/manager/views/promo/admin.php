<div id="align_zone">
    <div id="manager_short_info">
        <div class="img_fon inline_style" style="background-image:url(<?= $managerInfo->user_avatar ?>)"></div>
        <div class="inline_style">
            <p><?= $managerInfo->user_name ?></p>

            <p><?= $managerInfo->user_surname ?></p>
        </div>
        <div class="inline_style">
            <p><?= $managerInfo->user_phone ?></p>

            <p>e-mail: <?= $managerInfo->user_email ?></p>
        </div>
        <div class="inline_style">
            <a href="/site/logout" class="log_out_castom_manager" title="Выход"></a>
        </div>
    </div>


    <p class="manager_title">Промоматериалы</p>

    <div class="white_fon">
        <div id="promo_file_box">
            <form name="promo_file_apload" method="POST" action="" enctype="multipart/form-data">
                <input type="file" name="MagazinePromoFiles[url]" required>
                <button class="bg_color_th">Загрузить материал</button>
            </form>
        </div>
        <div id="promo_table">
            <div class="table_head">
                <div class="row1">Наименование</div>
                <div class="row2">Дата загрузки</div>
                <div class="row3">Размер файла</div>
                <div class="row4">Скачать</div>
                <div class="row6">Доступно для клиентов</div>
                <div class="row5">Удалить</div>

            </div>
            <div class="table_body">
                <!--table_line-->
                <?php foreach ($model as $md) {
                    $arrayFormats = explode('.', $md->name_file);
                    $img = '';
                    if (isset($arrayFormats[1])) {
                        if ($arrayFormats[1] == 'docx') {
                            $img = '<img src="' . $md->url . '" style="display: none"> <div class="icon-format" style="background-position: -4.4vw -0.2vw;"></div>';
                        } elseif ($arrayFormats[1] == 'xlsx') {
                            $img = '<div class="icon-format" style="background-position: 4.4vw -0.2vw;"></div>';
                        } elseif ($arrayFormats[1] == 'pptx') {
                            $img = '<div class="icon-format" style="background-position: 2.2vw -0.2vw;"></div>';
                        } elseif ($arrayFormats[1] == 'pdf') {
                            $img = '<img src="' . $md->url . '" style="display: none"> <div class="icon-format" style="background-position: -0.1vw -0.2vw;"></div>';
                        } elseif ($arrayFormats[1] == 'jpg') {
                            $img = '<img src="' . $md->url . '">';
                        } elseif ($arrayFormats[1] == 'png') {
                            $img = '<img src="' . $md->url . '">';
                        }else{
                            $img = '<div class="icon-format-none" style="background-position: -0.6vw 0;"></div>';
                        }
                    }
                    ?>
                    <div class="table_tr">
                        <div class="row1">
                            <?= $img ?>
                            <p><?= $md->name_file ?></p>
                            <!--                        <span>Новый</span>-->
                        </div>
                        <div class="row2"><?= date('d-m-Y', $md->date_create) ?>
                        </div>
                        <div class="row3"><?= MagazineDocuments::HumanBytes($md->file_size) ?>
                        </div>
                        <div class="row4"><a href="<?= $md->url ?>" download="" class="border_hover css_trans"><span
                                    class="color_th">↓</span> Скачать</a>
                        </div>
                        <div class="row6"><a href="/manager/Promo/add/id/<?=$md->id?>" class="s_a css_trans border_hover">
                                <span class="crm_let_plus color_th">+</span><span class="inline_style">Добавить клиентов</span>
                            </a>

                        </div>
                        <div class="row5">
                            <svg class="svg_stroke_color_hover css_trans" preserveAspectRatio="xMidYMid meet"
                                 viewBox="0 0 194 194" xmlns="http://www.w3.org/2000/svg">
                                <g display="inline">
                                    <line id="svg_7" y2="2.500002" x2="191.699994" y1="190.900002" x1="2.499994"
                                          stroke-linecap="null" stroke-linejoin="null"></line>
                                    <line id="svg_6" y2="191.700002" x2="191.699994" y1="2.500002" x1="2.499994"
                                          stroke-linecap="null" stroke-linejoin="null"></line>
                                </g>
                            </svg>
                        </div>
                    </div>
                <?php } ?>
                <!--table_line 1-->
            </div>
        </div>
    </div>
</div>