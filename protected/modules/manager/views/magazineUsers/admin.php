<?php
/* @var $this MagazineUsersController */
/* @var $model MagazineUsers */
?>


<div id="manager_short_info">
	<div class="img_fon inline_style" style="background-image:url(<?=$model->user_avatar?>)"></div>
	<div class="inline_style">
		<p><?=$model->user_name?></p>
		<p><?=$model->user_surname?></p>
	</div>
	<div class="inline_style">
		<p><?=$model->user_phone?></p>
		<p>e-mail: <?=$model->user_email?></p>
	</div>
	<div class="inline_style">
		<a href="/site/logout" class="log_out_castom_manager" title="Выход"></a>
	</div>

</div>

<p class="manager_title">Настройки профиля</p>
<div class="manager_settings_box">
	<?php $form=$this->beginWidget('CActiveForm', array(
		'id'=>'magazine-users-form',
		// Please note: When you enable ajax validation, make sure the corresponding
		// controller action is handling ajax validation correctly.
		// There is a call to performAjaxValidation() commented in generated controller code.
		// See class documentation of CActiveForm for details on this.
		'htmlOptions'=>array('enctype'=>'multipart/form-data'),
		'enableAjaxValidation'=>false,
	)); ?>
		<div class="manager_settings_line avatar_line">
			<p>Аватар:<br/>100x100</p>
			<img src="<?=$model->user_avatar?>">
			<input type="file" name="MagazineUsers[user_avatar]">
		</div>
		<div class="manager_settings_line">
			<p>Номер телефона:</p>
			<?php $this->widget("ext.maskedInput.MaskedInput", array(
				"model" => $model,
				"attribute" => "user_phone",
				"mask" => '+7 (999) 999-9999',
				'htmlOptions' => array('size'=>60,'maxlength'=>255, 'placeholder'=>'Номер телефона')
			)); ?>
		</div>
		<div class="manager_settings_line">
			<p>E-mail:</p>
			<input type="text" name="MagazineUsers[user_email]" value="<?=$model->user_email?>" required>
			<?php echo $form->error($model,'user_email'); ?>
		</div>
		<button class="bg_color_th" name="saveTop">Сохранить</button>
		<?php $this->endWidget(); ?>
</div>

<p class="manager_title">Cменить пароль</p>
<?php if($error != null){
	echo '<p class="manager_title" style="color: red;">'.$error[0].'</p>';
}?>
<div class="manager_settings_box">
	<form name="profile_settings" action="" method="POST" onsubmit="">
		<div class="manager_settings_line">
			<p>Старый пароль:</p>
			<input type="password" name="MagazineUsers[passw_old]" required>
		</div>
		<div class="manager_settings_line">
			<p>Новый пароль:</p>
			<input type="password" name="MagazineUsers[passw_repeat]" class="chek_p_1" required>
		</div>
		<div class="manager_settings_line">
			<p>Повторите новый пароль:</p>
			<input type="password" name="MagazineUsers[passw_repeat_2]" class="chek_p_2" required>
		</div>
		<button class="bg_color_th" id="save_new_pass" name="save_new_pass">Сохранить</button>
	</form>
</div>


