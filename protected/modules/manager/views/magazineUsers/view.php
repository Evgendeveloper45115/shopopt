<?php
/* @var $this MagazineUsersController */
/* @var $model MagazineUsers */

$this->breadcrumbs=array(
	'Magazine Users'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List MagazineUsers', 'url'=>array('index')),
	array('label'=>'Create MagazineUsers', 'url'=>array('create')),
	array('label'=>'Update MagazineUsers', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete MagazineUsers', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage MagazineUsers', 'url'=>array('admin')),
);
?>

<h1>View MagazineUsers #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'user_email',
		'user_password',
		'user_phone',
		'user_name',
		'user_surname',
		'user_role',
		'site_connect_id',
		'user_avatar',
	),
)); ?>
