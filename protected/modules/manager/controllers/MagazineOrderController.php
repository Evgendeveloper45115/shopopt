<?php

class MagazineOrderController extends Controller
{
    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '/layouts/main';

    /**
     * @return array action filters
     */

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */


    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }


    public function actionUpdateStatus($id)
    {
        $order = MagazineOrder::model()->findByPk([
                'id' => $id,
                'site_connect_id' => $this->connectID
            ]
        );
        $order->status = MagazineOrder::STATUS_COLLECTED;
        $order->manager_id = Yii::app()->user->id;
        $getUserEmail = MagazineUsers::model()->findByPk($order->client_id);
        $order->save();

        $status = [0 => 'Новый', 1 => 'Собирается', 2 => 'Отгружен', 3 => 'Закрыт'];
        $old_status = &$order->status;
        $get_client = MagazineUsers::model()->findByPk($order->client_id)->user_surname;
        $get_address = MagazineClientsAddress::model()->findByPk($order->magazine_shop_id)->address;
        $get_company = MagazineCompany::model()->findByPk($order->client_company_id)->name_company;


        $magazine_info_order = MagazineInfoOrder::model()->findAll(['condition' => 'order_id = "' . $order->id . '"']);
        $ids = [];
        $counts = [];
        foreach ($magazine_info_order as $mio) {
            $ids[] = $mio->goods_id;
            $counts[$mio->goods_id] = $mio->goods_count;
        }
        $cart = MagazineProduct::model()->findAll(['condition' => 'id in (' . implode(',', $ids) . ')']);

        foreach ($cart as $cr) {
            foreach ($counts as $key => $value) {
                if ($key == $cr->id) {
                    $cr->count = $value;
                }
            }
        }
        $str = '';
        foreach ($cart as $key => $cr) {
            $str .= '<tr>
		<td style="border: 1px solid #c2c2c2;font-size: 16px;padding: 5px 2%;width: 39%;">' . $cr->name_product . '</td>
		<td style="border: 1px solid #c2c2c2;font-size: 16px;padding: 5px 2%;width: 15%;">' . $cr->price . '</td>
		<td style="border: 1px solid #c2c2c2;font-size: 16px;padding: 5px 2%;width: 15%;">' . $cr->count . '</td>
		<td style="border: 1px solid #c2c2c2;font-size: 16px;padding: 5px 2%;width: 15%">' . $cr->price * $cr->count . '</td>
	</tr>';
        }
        $getShopName = MagazineClientsAddress::model()->findByPk($order->magazine_shop_id);

        $name = '=?UTF-8?B?' . base64_encode('Смена статуса заказа') . '?=';
        $headers = "From: $name <{$_SERVER['SERVER_NAME']}>\r\n" .
            "Reply-To: {$getUserEmail->user_email}\r\n" .
            "MIME-Version: 1.0\r\n" .
            "Content-Type: text/html; charset=UTF-8";
        mail($getUserEmail->user_email, 'Смена статуса заказа', 'Заказ №' . $order->id . ' перешел из статуса ' . $status[0] . ' в статус ' . $status[MagazineOrder::STATUS_COLLECTED] . ' <br/>
			 <table style=" border-collapse: collapse;border: 1px solid #c2c2c2; width: 100%;">
						<tr>
							<td style="border: 1px solid #c2c2c2;font-size: 12px;text-transform: uppercase;color: #c2c2c2;padding: 5px 2%;width: 39%;">Наименование</td>
							<td style="border: 1px solid #c2c2c2;font-size: 12px;text-transform: uppercase;color: #c2c2c2;padding: 5px 2%;width: 15%;">Цена</td>
							<td style="border: 1px solid #c2c2c2;font-size: 12px;text-transform: uppercase;color: #c2c2c2;padding: 5px 2%;width: 15%;">кол-во</td>
							<td style="border: 1px solid #c2c2c2;font-size: 12px;text-transform: uppercase;color: #c2c2c2;padding: 5px 2%;width: 15%">Стоимость</td>
						</tr>
						' . $str . '
					</table>
			 Заказ будет доставлен по адресу: ' . $get_address . '<br/>
					<p>Принимающий: ' . $getShopName->surname_contact . '  ' . $getShopName->name_contact . ' тел. ' . $getShopName->phone_contact . '</p>
			  Компания:' . $get_company . '<br/> Спасибо за заказ!', $headers);
        $this->redirect('/manager/MagazineOrder/admin');
    }

    /**
     * Lists all models.
     */
    public function actionIndex()
    {
        $dataProvider = new CActiveDataProvider('MagazineOrder');
        $this->render('index', array(
            'dataProvider' => $dataProvider,
        ));
    }

    public function actionEditOrder()
    {

        if (Yii::app()->request->isAjaxRequest) {
            $order = null;
            $user = null;
            $text = null;
            if (isset($_POST['id'])) {
                MagazineInfoOrder::model()->deleteAllByAttributes(['order_id' => $_POST['id'], 'site_connect_id' => $this->connectID]);
                MagazineOrder_2::model()->deleteAllByAttributes(['order_id' => $_POST['id']]);
                $order = MagazineOrder::model()->findByAttributes(['id' => $_POST['id']]);
                if ($order) {
                    $orderId = $order->id;
                    $user = MagazineUsers::model()->findByPk($order->client_id);
                    if ($order->delete()) {
                        $text = 'Ваш заказ № ' . $orderId . ' был удалён';
                    }
                    $name = '=?UTF-8?B?' . base64_encode('Удаление заказа') . '?=';
                    $subject = '=?UTF-8?B?' . base64_encode('Удаление заказа') . '?=';
                    $headers = "From: $name <{$_SERVER['SERVER_NAME']}>\r\n" .
                        "Reply-To: {$user->user_email}\r\n" .
                        "MIME-Version: 1.0\r\n" .
                        "Content-Type: text/html; charset=UTF-8";
                    mail(
                        $user->user_email,
                        $subject,
                        $text,
                        $headers
                    );
                    echo json_encode(['success' => 'redirect']);
                    exit;
                }
            }
            if (isset($_POST['items'])) {
                $oldValues = [];
                $orderId = null;
                $elmCome = [];
                $str = '';
                $price = ['edit' => [0 => 0], 'no_edit' => [0 => 0]];

                foreach ($_POST['items'] as $id => $item) {
                    $order2 = MagazineOrder_2::model()->findByPk($id);
                    $order_info = MagazineInfoOrder::model()->findByAttributes(['goods_id' => $order2->goods_id, 'order_id' => $order2->order_id]);
                    $magazineProduct = MagazineProduct::model()->findByPk($order_info->goods_id);
                    $order = MagazineOrder::model()->findByAttributes(['id' => $order_info->order_id]);
                    $getShopName = MagazineClientsAddress::model()->findByPk($order->magazine_shop_id);
                    $getCompany = MagazineCompany::model()->findByPk($order->client_company_id);

                    if ($item[0] != 'no_edit') {
                        if ($order) {
                            $user = MagazineUsers::model()->findByPk($order->client_id);
                        }
                        $oldValues[] = $order_info->goods_count;
                        $order_info->goods_count = $item[0];
                        $order_info->price = $item[1];
                        $orderId = $order_info->order_id;
                        !isset($_POST['is_del']) ? $order_info->save(false) : null;
                        $elmCome[] = $order_info->id;
                        if (stristr($order_info->price, ' ')) {
                            $order_info->price = str_replace(' ', '', $order_info->price);
                        }
                        $price['edit'][0] = $price['edit'][0] + ((float)$order_info->price * (float)$order_info->goods_count);
                    } else {
                        $order_info->price = $item[1];
                        if (stristr($order_info->price, ' ')) {
                            $order_info->price = str_replace(' ', '', $order_info->price);
                        }
                        $price['no_edit'][0] = $price['no_edit'][0] + ((float)$order_info->price * (float)$order_info->goods_count);
                        $orderId = $order_info->order_id;
                        $elmCome[] = $order_info->id;
                    }
                    if ($price['edit'][0] != 0) {
                        $order2->order_price = round($price['edit'][0] + $price['no_edit'][0]);
                        $order2->count_item = $order_info->goods_count;
                        $order2->cost_item = $order_info->price;
                        $order2->item_sum = $order2->count_item * $order2->cost_item;
                        !isset($_POST['is_del']) ? $order2->save(false) : null;
                    }

                    $image = null;
                    if ($magazineProduct->image_product) {
                        $json = json_decode($magazineProduct->image_product);
                        if (!empty($json)) {
                            foreach ($json as $img) {
                                if ($img != '' || !stristr($img, '[')) {
                                    $image = $img;
                                    break;
                                }
                            }
                        }
                        $color = 'black';
                        if (!in_array($order_info->goods_count, $oldValues)) {
                            $color = 'red';
                        }
                    }

                    $str .= '<tr>
                    <td style="border: 1px solid #c2c2c2;font-size: 16px;padding: 5px 2%;width: 20%;"><img src="http://' . $_SERVER['SERVER_NAME'] . $image . '" style="width:100%;height:auto;"/></td>
                        <td style="border: 1px solid #c2c2c2;font-size: 16px;padding: 5px 2%;width: 39%;">' . $magazineProduct->name_product . '</td>
                        <td style="border: 1px solid #c2c2c2;font-size: 16px;padding: 5px 2%;width: 15%;">' . $magazineProduct->price . '</td>
                        <td style=" color: ' . $color . ';border: 1px solid #c2c2c2;font-size: 16px;padding: 5px 2%;width: 15%;">' . $order_info->goods_count . '</td>
                        <td style="border: 1px solid #c2c2c2;font-size: 16px;padding: 5px 2%;width: 15%">' . (float)$order_info->price * (float)$order_info->goods_count . '</td>
                    </tr>';

                }
                $name = '=?UTF-8?B?' . base64_encode('Изменения в заказе') . '?=';
                $subject = '=?UTF-8?B?' . base64_encode('Изменения в заказе') . '?=';
                $headers = "From: $name <{$_SERVER['SERVER_NAME']}>\r\n" .
                    "Reply-To: {$user->user_email}\r\n" .
                    "MIME-Version: 1.0\r\n" .
                    "Content-Type: text/html; charset=UTF-8";

                mail(
                    $user->user_email,
                    $subject,
                    '
					<p>Ваш заказ №' . $orderId . ' был изменен, новые количества товара и сумма указаны ниже</p>
					<table style=" border-collapse: collapse;border: 1px solid #c2c2c2; width: 100%;">
						<tr>
						<td style="border: 1px solid #c2c2c2;font-size: 12px;text-transform: uppercase;color: #c2c2c2;padding: 5px 2%;width: 15%;">Вид товара</td>
							<td style="border: 1px solid #c2c2c2;font-size: 12px;text-transform: uppercase;color: #c2c2c2;padding: 5px 2%;width: 39%;">Наименование</td>
							<td style="border: 1px solid #c2c2c2;font-size: 12px;text-transform: uppercase;color: #c2c2c2;padding: 5px 2%;width: 15%;">Цена</td>
							<td style="border: 1px solid #c2c2c2;font-size: 12px;text-transform: uppercase;color: #c2c2c2;padding: 5px 2%;width: 15%;">кол-во</td>
							<td style="border: 1px solid #c2c2c2;font-size: 12px;text-transform: uppercase;color: #c2c2c2;padding: 5px 2%;width: 15%">Стоимость</td>
						</tr>
						' . $str . '
					</table>
					<div style="width: 100%;text-align: right;font-weight: bold;"><p style="padding-right: 50px;">Итого: ' . $order->price . '</p></div>
					<p>Заказ будет доставлен по адресу:' . $getShopName->address . '</p>
					<p>Принимающий: ' . $getShopName->surname_contact . '  ' . $getShopName->name_contact . ' тел. ' . $getShopName->phone_contact . '</p>
					<p>Компания: ' . $getCompany->name_company . '</p>
					<p>Спасибо за внимание!</p>
					',
                    $headers
                );

                $order_info = MagazineInfoOrder::model()->findAllByAttributes(['order_id' => $orderId]);
                $discount = [];
                $idscountClient = MagazineDiscount::model()->findAll(['condition' => 'client_id = "' . $order->client_id . '"']);

                foreach ($idscountClient as $clientDiscount) {
                    $discount[] = [$clientDiscount->persent, $clientDiscount->sum];
                }

                $del = false;
                foreach ($order_info as $key => $model) {

                    if (!in_array($model->id, $elmCome)) {
                        if (isset($_POST['bonus'])) {
                            $order2 = MagazineOrder_2::model()->findByAttributes(['goods_id' => $model->goods_id, 'order_id' => $orderId]);
                            $bonus = MagazineBonusOrder::model()->findByAttributes(['order_id' => $order2->order_id]);
                            if ($bonus != null) {
                                $bonus->used_bonus -= $order2->bonus;
                                !isset($_POST['is_del']) ? $bonus->save(false) : null;
                            }
                            !isset($_POST['is_del']) ? $order2->delete() : null;
                            $del = true;

                        }
                        !isset($_POST['is_del']) ? $model->delete() : null;
                    }
                }

                if ($del) {
                    $magOrders2 = MagazineOrder_2::model()->findAllByAttributes(['order_id' => $orderId]);
                    if (isset($_POST['is_del'])) {
                        $magOrders2 = MagazineOrder_2::model()->findAll(['condition' => 'id in (' . implode(',', $_POST['items']) . ')']);
                    }
                    $all_cost = 0;
                    if (!empty($magOrders2)) {
                        $cost = 0;
                        foreach ($magOrders2 as $magOrder2) {
                            if ($magOrder2->status_item != 1) {
                                $cost += $magOrder2->item_sum;
                            }
                        }
                        foreach ($magOrders2 as $magOrder2) {
                            if ($magOrder2->status_item != 1) {
                                $magOrder2->bonus = round(($magOrder2->item_sum / $cost * (float)$_POST['bonus']));
                                if (!empty($discount)) {
                                    if ((float)$discount[0][1] > (float)$cost) {
                                        $magOrder2->discount = 0;
                                    } else {
                                        $magOrder2->discount = round($cost * ($discount[0][0] / 100));
                                    }
                                }
                                $magOrder2->with_bonus_sum = $magOrder2->item_sum - $magOrder2->bonus;
                                $all_cost += ($magOrder2->with_bonus_sum);
                            } else {
                                $magOrder2->bonus = 0;
                                $magOrder2->discount = 0;
                            }
                            !isset($_POST['is_del']) ? $magOrder2->save(false) : null;
                        }
                        $all_cost -= $magOrders2[0]->discount;
                        foreach ($magOrders2 as $magOrder2) {
                            $magOrder2->order_price = round($all_cost);
                            !isset($_POST['is_del']) ? $magOrder2->save(false) : null;
                        }
                    }
                }
                $result = null;
                !isset($_POST['is_del']) ? $result = 'json' : $result = 'render';
                if ($result == 'json') {
                    echo json_encode(['success' => 'ok']);
                    exit;
                } else {
                    $this->render('order', ['order2' => $magOrders2]);
                }
            }
        }
    }

    public function actionShowOrder($id)
    {

        if (isset($_FILES['MagazineDocuments']['name'])) {
            $model = new MagazineDocuments();
            $model->file = CUploadedFile::getInstance($model, 'url');
            $model->size_file = $model->file->getSize();
            $model->order_id = $id;
            $model->date_add = date('Y-m-d');
            $model->status = 0;
            $model->name = $model->file->getName();
            $model->extension = $model->file->getExtensionName();
            $path = '/upload/' . uniqid() . '.' . $model->file->getExtensionName();
            $model->file->saveAs(Yii::getPathOfAlias('webroot') . $path);
            $model->url = $path;
            $model->save();
            $model = null;
        }

        $model = MagazineOrder::model()->findByPk([
            'id' => $id,
            'site_connect_id' => $this->connectID
        ]);

        $model->company_name = MagazineCompany::model()->find(['condition' => 'id = "' . $model->client_company_id . '"'])->name_company;
        if ($model->show == 0 && $model->status == MagazineOrder::STATUS_CLOSE) {
            $popup = true;
            $model->show = 1;
            $model->save();
        } else {
            $popup = false;
        }

        $status = [0 => 'Новый', 1 => 'Собирается', 2 => 'Отгружен', 3 => 'Закрыт'];
        // $old_status = &$model->status;
        $get_client = MagazineUsers::model()->findByPk($model->client_id)->user_email;
        $get_address = MagazineClientsAddress::model()->findByPk($model->magazine_shop_id)->address;
        $get_company = MagazineCompany::model()->findByPk($model->client_company_id)->name_company;


        $magazine_info_order = MagazineInfoOrder::model()->findAll(['condition' => 'order_id = "' . $model->id . '"']);
        $ids = [];
        $counts = [];
        foreach ($magazine_info_order as $mio) {
            $ids[] = $mio->goods_id;
            $counts[$mio->goods_id] = $mio->goods_count;
        }
        $cart = [];
        if (!empty($ids)) {
            $cart = MagazineProduct::model()->findAll(['condition' => 'id in (' . implode(',', $ids) . ')']);
        }

        foreach ($cart as $cr) {
            foreach ($counts as $key => $value) {
                if ($key == $cr->id) {
                    $cr->count = $value;
                }
            }
        }
        $str = '';
        foreach ($cart as $key => $cr) {
            $str .= '<tr>
<td style="border: 1px solid #c2c2c2;font-size: 16px;padding: 5px 2%;width: 20%;"><img src="http://' . $_SERVER['SERVER_NAME'] . $cr->image_product . '" style="width:100%;height:auto;"/></td>
		<td style="border: 1px solid #c2c2c2;font-size: 16px;padding: 5px 2%;width: 39%;">' . $cr->name_product . '</td>
		<td style="border: 1px solid #c2c2c2;font-size: 16px;padding: 5px 2%;width: 15%;">' . $cr->price . '</td>
		<td style="border: 1px solid #c2c2c2;font-size: 16px;padding: 5px 2%;width: 15%;">' . $cr->count . '</td>
		<td style="border: 1px solid #c2c2c2;font-size: 16px;padding: 5px 2%;width: 15%">' . $cr->price * $cr->count . '</td>
	</tr>';
        }
        $getShopName = MagazineClientsAddress::model()->findByPk($model->magazine_shop_id);
        if (isset($_POST['status'])) {
            $model->status = $_POST['status'];
            $model->save();
            $name = '=?UTF-8?B?' . base64_encode('Смена статуса заказа') . '?=';
            $headers = "From: $name <{$_SERVER['SERVER_NAME']}>\r\n" .
                "Reply-To: {$get_client}\r\n" .
                "MIME-Version: 1.0\r\n" .
                "Content-Type: text/html; charset=UTF-8";
            mail($get_client, 'Смена статуса заказа', 'Заказ №' . $model->id . ' перешел из статуса ' . $status[$old_status] . ' в статус ' . $status[$_POST['status']] . ' <br/>
			 <table style=" border-collapse: collapse;border: 1px solid #c2c2c2; width: 100%;">
						<tr>
							<td style="border: 1px solid #c2c2c2;font-size: 12px;text-transform: uppercase;color: #c2c2c2;padding: 5px 2%;width: 15%;">Вид товара</td>
							<td style="border: 1px solid #c2c2c2;font-size: 12px;text-transform: uppercase;color: #c2c2c2;padding: 5px 2%;width: 39%;">Наименование</td>
							<td style="border: 1px solid #c2c2c2;font-size: 12px;text-transform: uppercase;color: #c2c2c2;padding: 5px 2%;width: 15%;">Цена</td>
							<td style="border: 1px solid #c2c2c2;font-size: 12px;text-transform: uppercase;color: #c2c2c2;padding: 5px 2%;width: 15%;">кол-во</td>
							<td style="border: 1px solid #c2c2c2;font-size: 12px;text-transform: uppercase;color: #c2c2c2;padding: 5px 2%;width: 15%">Стоимость</td>
						</tr>
						' . $str . '
					</table>
			 Заказ будет доставлен по адресу: ' . $get_address . '<br/>
					<p>Принимающий: ' . $getShopName->surname_contact . '  ' . $getShopName->name_contact . ' тел. ' . $getShopName->phone_contact . '</p>
			  Компания:' . $get_company . '<br/> Спасибо за заказ!', $headers);
            $this->redirect('/manager/MagazineOrder/ShowOrder/id/' . $id);
        }

        if (isset($_POST['data_manager_order'])) {

            $model->time_order = $_POST['data_manager_order'];
            $model->accepted = 1;
            $model->save();


            $getClientInfo = MagazineUsers::model()->findByPk($model->client_id);
            $headers = "From: $name <{$_SERVER['SERVER_NAME']}>\r\n" .
                "Reply-To: {$get_client}\r\n" .
                "MIME-Version: 1.0\r\n" .
                "Content-Type: text/html; charset=UTF-8";
            mail($get_client, 'Смена статуса заказа', 'Заказ №' . $model->id . ' перешел из статуса ' . $status[$old_status] . ' в статус ' . $status[$_POST['status']] . '<br/>
			 <table style=" border-collapse: collapse;border: 1px solid #c2c2c2; width: 100%;">
						<tr>
						<td style="border: 1px solid #c2c2c2;font-size: 12px;text-transform: uppercase;color: #c2c2c2;padding: 5px 2%;width: 15%;">Вид товара</td>
							<td style="border: 1px solid #c2c2c2;font-size: 12px;text-transform: uppercase;color: #c2c2c2;padding: 5px 2%;width: 39%;">Наименование</td>
							<td style="border: 1px solid #c2c2c2;font-size: 12px;text-transform: uppercase;color: #c2c2c2;padding: 5px 2%;width: 15%;">Цена</td>
							<td style="border: 1px solid #c2c2c2;font-size: 12px;text-transform: uppercase;color: #c2c2c2;padding: 5px 2%;width: 15%;">кол-во</td>
							<td style="border: 1px solid #c2c2c2;font-size: 12px;text-transform: uppercase;color: #c2c2c2;padding: 5px 2%;width: 15%">Стоимость</td>
						</tr>
						' . $str . '
					</table>
			 Заказ будет доставлен по адресу: ' . $get_address . '<br/>
					<p>Принимающий: ' . $getShopName->surname_contact . '  ' . $getShopName->name_contact . ' тел. ' . $getShopName->phone_contact . '</p>
			  Компания:' . $get_company . '<br/> Спасибо за заказ!', $headers);
            $this->redirect('/manager/MagazineOrder/ShowOrder/id/' . $id);
        }

        $magazineInfo = MagazineClientsAddress::model()->findByPk($model->magazine_shop_id);

        $orderInfo = MagazineInfoOrder::model()->findAll(['condition' => 'order_id = "' . $id . '"']);
        foreach ($orderInfo as $orderInf) {
            $imgProduct = MagazineProduct::model()->findByPk($orderInf->goods_id);
            $orderInf->image_product = $imgProduct->image_product;
            $orderInf->name_product = $imgProduct->name_product;
            $orderInf->price = $imgProduct->price;
        }
        $idscountClient = MagazineDiscount::model()->findAll(['condition' => 'client_id = "' . $model->client_id . '"']);
        $discount = [];
        foreach ($idscountClient as $clientDiscount) {
            $discount [] = [$clientDiscount->persent, $clientDiscount->sum];
        }
        $discount = json_encode($discount);

        $documents = MagazineDocuments::model()->findAll(['condition' => 'order_id = "' . $id . '"']);
        $order2 = MagazineOrder_2::model()->findAllByAttributes(['order_id' => $id]);
        $this->render('order', array(
            'model' => $model,
            'discount' => $discount,
            'popup' => $popup,
            'order2' => $order2,
            'magazineInfo' => $magazineInfo,
            'orderInfo' => $orderInfo,
            'documents' => $documents,
            'manager_info' => Yii::app()->user->getModel()
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin()
    {
        $condition = null;
        if (isset($_POST['dates'])) {
            if ($_POST['dates'][0] != '' && $_POST['dates'][1] != '') {
                $condition = 'time_create >= "' . date('Y-m-d H:i:s', strtotime($_POST['dates'][0])) . '" and time_create <= "' . date('Y-m-d H:i:s', strtotime($_POST['dates'][1])) . '" and ';
            } elseif ($_POST['dates'][0] != '') {
                $condition = 'time_create >= "' . date('Y-m-d H:i:s', strtotime($_POST['dates'][0])) . '" and ';

            } elseif ($_POST['dates'][1] != '') {
                $condition = 'time_create <= "' . date('Y-m-d H:i:s', strtotime($_POST['dates'][1])) . '" and ';

            }
        }
        if (isset($_GET['status']) && $_GET['status'] != 'null') {
            if ($_GET['status'] == '1,2') {
                $model = MagazineOrder::model()->with(['magazine_users' => ['condition' => 'magazine_users.manager_id = "' . Yii::app()->user->id . '"']])->findAll(['condition' => $condition . ' status in (1,2)', 'order' => 'magazine_order.id DESC']);
            } else {
                $model = MagazineOrder::model()->findAll(['condition' => '(' . $condition . 'manager_id = "' . Yii::app()->user->id . '") and status = 3', 'order' => 'id DESC']);
            }

        } else {
            if (isset($_GET['comp_id'])) {
                $model = MagazineOrder::model()->with(['magazine_users' => ['condition' => 'magazine_users.manager_id = "' . Yii::app()->user->id . '"']])->findAll(['condition' => $condition . ' status = 0 and client_company_id = "' . $_GET['comp_id'] . '"', 'order' => 'magazine_order.id DESC']);
            } else {
                $model = MagazineOrder::model()->with(['magazine_users' => ['condition' => 'magazine_users.manager_id = "' . Yii::app()->user->id . '"']])->findAll(['condition' => $condition . ' status = 0', 'order' => 'magazine_order.id DESC']);
            }
        }
        foreach ($model as $md) {
            $md->company_name = MagazineCompany::model()->find(['condition' => 'id = "' . $md->client_company_id . '"'])->name_company;
            $md->countFiles = MagazineDocuments::model()->count(['condition' => 'order_id = ' . $md->id]);
        }
        $this->render('admin', array(
            'model' => $model,
            'manager_info' => Yii::app()->user->getModel()
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return MagazineOrder the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model = MagazineOrder::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param MagazineOrder $model the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'magazine-order-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }
}
