<?php

class MagazineDocumentsController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='/layouts/main';

	/**
	 * @return array action filters
	 */

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$ManagerInfo = MagazineUsers::model()->findAll(['condition' => 'id = "'.Yii::app()->user->id.'"']);
		$OrderCompanies = MagazineOrder::model()->findAll(['condition' => 'manager_id = "'.Yii::app()->user->id.'"']);
		$idsComp = ['all' => 'Все компании'];
		foreach($OrderCompanies as $comp) {
			$getCompanyName = MagazineCompany::model()->find(['condition' => 'id = "'.$comp->client_company_id.'"']);
			$idsComp[$comp->client_company_id] = $getCompanyName->name_company ;
		}


		$this->render('view',array(
			'model'=>$this->loadModel($id),
			'ManagerInfo'=>$ManagerInfo,
			'idsComp'=>$idsComp,
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new MagazineDocuments;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['MagazineDocuments']))
		{
			$model->attributes=$_POST['MagazineDocuments'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['MagazineDocuments']))
		{
			$model->attributes=$_POST['MagazineDocuments'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex($id)
	{
		if(isset($_FILES['MagazineDocuments']['name'])) {
			$model = new MagazineDocuments();
			$model->file = CUploadedFile::getInstance($model,'url');
			$model->size_file = $model->file->getSize();
			$model->order_id = $id;
			$model->date_add = date('Y-m-d H:i:s');
			$model->status = 0;
			$model->name = $model->file->getName();
			$model->extension = $model->file->getExtensionName();
			$path='/upload/'.uniqid().'.'.$model->file->getExtensionName();
			$model->file->saveAs(Yii::getPathOfAlias('webroot').$path);
			$model->url = $path;
			$model->save();
			$this->redirect('/manager/magazineDocuments/index/id/' .$id);
		}
		$model = MagazineDocuments::model()->findAll(['condition' => 'order_id = "'.$id.'"']);
		$order = MagazineOrder::model()->findByPk(	[
			'id' =>$id,
			'site_connect_id'=>$this->connectID
		]);

		$magaComp = MagazineCompany::model()->findByPk($order->client_company_id);
//		var_dump($order->client_company_id);
		$managerInfo = MagazineUsers::model()->findByPk(Yii::app()->user->id);

		$this->render('index',array(
			'model'=>$model,
			'magaComp' => $magaComp,
			'ManagerInfo'=>$managerInfo,
			'order'=>$order,
			'id' => $id
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$ids = [];
		$ordersClient = MagazineOrder::model()->findAll(['condition' => 'manager_id = "'.Yii::app()->user->id.'"']);

		$clientsCompanies = [];
		foreach ($ordersClient as $oC) {

			$getCompanyInfo = MagazineCompany::model()->findByPk($oC->client_company_id);
			$clientsCompanies[$oC->client_company_id] = $getCompanyInfo->name_company;
			if($_GET['select_user_documents'] == 'all' || $_GET['select_user_documents'] == null) {
				$ids[] = $oC->id;
			} elseif($_GET['select_user_documents'] == $getCompanyInfo->id) {
				$ids[] = $oC->id;
			}
		}

		$ids = implode('","', $ids);

		$model = MagazineDocuments::model()->findAll(['condition' => 'order_id in ("'.$ids.'")']);
		foreach($model as $md ) {
			$order = MagazineOrder::model()->findByPk(
				[
				'id' =>$md->order_id,
				'site_connect_id'=>$this->connectID
			]
			);
			$md->comp = MagazineCompany::model()->findByPk($order->client_company_id);
		}

		$managerInfo = MagazineUsers::model()->findByPk(Yii::app()->user->id);

//		var_dump($ManagerInfo);
		$this->render('admin',array(
			'model'=>$model,
			'managerInfo'=>$managerInfo,
			'clientsCompanies'=>$clientsCompanies,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return MagazineDocuments the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=MagazineDocuments::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param MagazineDocuments $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='magazine-documents-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
