<?php

class MagazineUsersController extends Controller
{
    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '/layouts/main';

    /**
     * @return array action filters
     */

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate()
    {
        $model = new MagazineUsers;

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['MagazineUsers'])) {
            $model->attributes = $_POST['MagazineUsers'];
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->id));
        }

        $this->render('create', array(
            'model' => $model,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['MagazineUsers'])) {
            $model->attributes = $_POST['MagazineUsers'];
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->id));
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    /**
     * Lists all models.
     */
    public function actionIndex()
    {
        $dataProvider = new CActiveDataProvider('MagazineUsers');
        $this->render('index', array(
            'dataProvider' => $dataProvider,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin()
    {
        $model = MagazineUsers::model()->find(['condition' => 'id = "' . Yii::app()->user->id . '"']);
        $error = null;

        if (isset($_POST['saveTop'])) {
            if (isset($_POST['MagazineUsers']['user_phone'])) {
                $model->user_phone = $_POST['MagazineUsers']['user_phone'];
            }

            if (isset($_POST['MagazineUsers']['user_email'])) {
                $model->user_email = $_POST['MagazineUsers']['user_email'];
            }

            if (isset($_FILES['MagazineUsers']['name']['user_avatar'])) {
                $model->file = CUploadedFile::getInstance($model, 'user_avatar');
                if ($model->file != null) {
                    $path = '/upload/' . uniqid() . '.' . $model->file->getExtensionName();
                    $model->file->saveAs(Yii::getPathOfAlias('webroot') . $path);
                    $model->user_avatar = $path;
                }
            }

            if ($model->save()) {
                $this->redirect('/manager/MagazineUsers/admin?fine');
            }
        }

        if (isset($_POST['save_new_pass'])) {
            $error = [];

            if ($_POST['MagazineUsers']['passw_old'] != $model->user_password) {
                $error[] = 'не правильный старый пароль';
            }

            if ($_POST['MagazineUsers']['passw_repeat'] != $_POST['MagazineUsers']['passw_repeat_2']) {
                $error[] = 'не правильный повтор пароля';
            }

            $model->attributes = $_POST['MagazineUsers'];
            $model->user_password = $_POST['MagazineUsers']['passw_repeat'];

            if (empty($error)) {
                if ($model->save()) {
                    $this->redirect('/manager/MagazineUsers/admin?fine');
                }
            }
        }

        $this->render('admin', array(
            'model' => $model,
            'error' => $error
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return MagazineUsers the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model = MagazineUsers::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param MagazineUsers $model the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'magazine-users-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

}
