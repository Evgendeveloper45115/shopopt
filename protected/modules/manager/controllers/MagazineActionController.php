<?php

class MagazineActionController extends Controller
{
    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '/layouts/main';

    /**
     * @return array action filters
     */

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */


    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }


    public function actionUpdateStatus($id)
    {
        $order = MagazineOrder::model()->findByPk([
            'id' => $id,
            'site_connect_id' => $this->connectID
        ]);
        $order->status = MagazineOrder::STATUS_COLLECTED;
        $order->manager_id = Yii::app()->user->id;
        $order->save();
        $orders = MagazineOrder::model()->findAll(['condition' => 'client_id = ' . $order->client_id]);
        foreach ($orders as $rd) {
            $rd->manager_id = Yii::app()->user->id;
            $rd->save();
        }

        $this->redirect('/manager/MagazineOrder/admin');
    }

    /**
     * Lists all models.
     */
    public function actionIndex()
    {


        $userInfo = MagazineUsers::model()->findByPk(Yii::app()->user->id);


        $this->render('index', array(

            'userInfo' => $userInfo,
            'manager_info' => Yii::app()->user->getModel()
        ));
    }

    public function actionShowOrder($id)
    {

        if (isset($_FILES['MagazineDocuments']['name'])) {
            $model = new MagazineDocuments();
            $model->file = CUploadedFile::getInstance($model, 'url');
            $model->size_file = $model->file->getSize();
            $model->order_id = $id;
            $model->date_add = date('Y-m-d');
            $model->status = 0;
            $model->name = $model->file->getName();
            $model->extension = $model->file->getExtensionName();
            $path = '/upload/' . uniqid() . '.' . $model->file->getExtensionName();
            $model->file->saveAs(Yii::getPathOfAlias('webroot') . $path);
            $model->url = $path;
            $model->save();
            $model = null;
        }

        $model = MagazineOrder::model()->findByPk([
            'id' => $id,
            'site_connect_id' => $this->connectID
        ]);
        if (isset($_POST['status'])) {
            $model->status = $_POST['status'];
            $model->save();
            $this->redirect('/manager/MagazineOrder/ShowOrder/id/' . $id);
        }

        if (isset($_POST['data_manager_order'])) {
            $model->time_order = $_POST['data_manager_order'];
            $model->save();

            $getClientInfo = MagazineUsers::model()->findByPk($model->client_id);
            mail($getClientInfo->user_email, 'Изменение статуса заказа', 'Ваш заказ принят и отправлен в работу');
            $this->redirect('/manager/MagazineOrder/ShowOrder/id/' . $id);
        }

        $magazineInfo = MagazineClientsAddress::model()->findByPk($model->magazine_shop_id);

        $orderInfo = MagazineInfoOrder::model()->findAll(['condition' => 'order_id = "' . $id . '"']);
        foreach ($orderInfo as $orderInf) {
            $imgProduct = MagazineProduct::model()->findByPk($orderInf->goods_id);
            $orderInf->image_product = $imgProduct->image_product;
            $orderInf->text_product = $imgProduct->text_product;
            $orderInf->price = $imgProduct->price;
        }

        $documents = MagazineDocuments::model()->findAll(['condition' => 'order_id = "' . $id . '"']);

        $this->render('order', array(
            'model' => $model,
            'magazineInfo' => $magazineInfo,
            'orderInfo' => $orderInfo,
            'documents' => $documents,
            'manager_info' => Yii::app()->user->getModel()
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin()
    {
        $allTovar = MagazineProduct::model()->findAll(['condition' => 'type = 0']);
        $allStock = MagazineStock::model()->findAll();
        $CompanyRecommendTovar = MagazineRecommedClientTovar::model()->findAll();
        $CompanyRecommendStock = MagazineRecommendStock::model()->findAll();

        foreach ($CompanyRecommendStock as $rTovar) {
            foreach ($allStock as $td) {
                if ($rTovar->id == $td->id) {
                    $rTovar->info = $td;
                }
            }
        }

        foreach ($CompanyRecommendTovar as $rTovar) {
            foreach ($allTovar as $td) {
                if ($rTovar->id_tovar == $td->id) {
                    $rTovar->info = $td;
                }
            }
        }

        $PCREpattern = '/\r\n|\r|\n/u';

        $contentStock = [];
        foreach ($allStock as $tv) {
            $contentStock[] = [
                'id' => $tv->id,
                'img' => $tv->image,
                'title' => $tv->header_text,
                'descr' => preg_replace($PCREpattern, '<br/>', $tv->text),
            ];
        }
        $contentTovar = [];
        foreach ($allTovar as $tv) {
            $image = null;
            if ($tv->image_product) {
                $json = json_decode($tv->image_product);
                foreach ($json as $img) {
                    if ($img != '' || stristr($img, '[')) {
                        $image = $img;
                    }
                }
            }
            $contentTovar[] = [
                'id' => $tv->id,
                'title' => $tv->title_en,
                'img' => $image,
                'price' => $tv->price
            ];
        }
        $contentTovar = json_encode($contentTovar);
        $contentStock = json_encode($contentStock);

        $orders = MagazineOrder::model()->findAll(['condition' => 'manager_id = "' . Yii::app()->user->id . '"']);
        $idsCompany = [];

        $userInfo = MagazineUsers::model()->findByPk(Yii::app()->user->id);
//		var_dump($userInfo);
        foreach ($orders as $order) {
            $idsCompany[] = $order->client_id;
        }

        $companies = MagazineCompany::model()->findAll(['condition' => 'client_id in ("' . implode('","', $idsCompany) . '")']);


        foreach ($companies as $comp) {
            $comp->count = $orders = MagazineOrder::model()->count(['condition' => 'client_company_id = ' . $comp->id . ' and status = "' . MagazineOrder::STATUS_NEW . '"']);
            $comp->info = MagazineUsers::model()->findByPk($comp->client_id);
            $comp->recTovar = MagazineRecommedClientTovar::model()->findAll(['condition' => 'id_company = "' . $comp->id . '"']);

            if (!empty($comp->recTovar)) {
                $ids = [];
                foreach ($comp->recTovar as $tovar) {
                    $ids[] = $tovar->id_tovar;
                }
            }
            $comp->recTovar = json_encode($ids);

            $comp->recAkz = MagazineRecommendStock::model()->findAll(['condition' => 'id_company = "' . $comp->id . '"']);

            if (!empty($comp->recAkz)) {
                $ids = [];
                foreach ($comp->recAkz as $tovar) {
                    $ids[] = $tovar->id_stock;
                }
            }
            $comp->recAkz = json_encode($ids);
        }

        $this->render('admin', array(
            'companies' => $companies,
            'contentTovar' => $contentTovar,
            'contentStock' => $contentStock,
            'managerInfo' => Yii::app()->user->getModel(),
            'CompanyRecommendTovar' => $CompanyRecommendTovar,
            'CompanyRecommendStock' => $CompanyRecommendStock,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return MagazineOrder the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model = MagazineOrder::model()->findByPk(
            [
                'id' => $id,
                'site_connect_id' => $this->connectID
            ]
        );
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param MagazineOrder $model the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'magazine-order-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }
}
