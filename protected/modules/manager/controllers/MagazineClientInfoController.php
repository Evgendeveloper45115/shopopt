<?php

class MagazineClientInfoController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='/layouts/main';

	/**
	 * @return array action filters
	 */

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */


	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}


	public function actionUpdateStatus($id)
	{
		$order = MagazineOrder::model()->findByPk([
			'id' => $id,
			'site_connect_id' => $this->connectID
		]);
		$order->status = MagazineOrder::STATUS_COLLECTED;
		$order->manager_id = Yii::app()->user->id;
		$order->save();

		$orders = MagazineOrder::model()->findAll(['condition' => 'client_id = ' . $order->client_id]);
		foreach($orders as $rd) {
			$rd->manager_id = Yii::app()->user->id;
			$rd->save();
		}

		$this->redirect('/manager/MagazineOrder/admin');
	}
	/**
	 * Lists all models.
	 */

	public function actionAdmin($id)
	{

		$allTovar = MagazineProduct::model()->findAll(['condition' => 'type = 0']);
		$allStock = MagazineStock::model()->findAll();
		$CompanyRecommendTovar = MagazineRecommedClientTovar::model()->findAll();
		$CompanyRecommendStock = MagazineRecommendStock::model()->findAll();

		foreach($CompanyRecommendStock as $rTovar) {
			foreach($allStock as $td) {
				if($rTovar->id == $td->id) {
					$rTovar->info =	$td;
				}
			}
		}

		foreach($CompanyRecommendTovar as $rTovar) {
			foreach($allTovar as $td) {
				if($rTovar->id_tovar == $td->id) {
					$rTovar->info =	$td;
				}
			}
		}

		$mangerInfo = MagazineUsers::model()->findByPk(Yii::app()->user->id);
		$orderNews = MagazineOrder::model()->findAll(['condition' => 'client_company_id = "'.$id.'" and status = 0']);
		$ids = [];
		$idsCompany = [];
		$clientIds = [];

		foreach($orderNews as $order) {
			$ids[] = $order->id;
			$clientIds[] = $order->client_id;
			$idsCompany[] = $order->client_company_id;
			$order->companyInfo = MagazineCompany::model()->findAll($order->client_company_id);
		}

		$orderWork = MagazineOrder::model()->findAll(['condition' => 'client_company_id = "'.$id.'" and status in (1,2)']);
		foreach($orderWork as $order) {
			$ids[] = $order->id;
			$clientIds[] = $order->client_id;
			$idsCompany[] = $order->client_company_id;
			$order->companyInfo = MagazineCompany::model()->findAll($order->client_company_id);
		}

		$orderHistory = MagazineOrder::model()->findAll(['condition' => 'client_company_id = "'.$id.'" and status = 3']);
		foreach($orderHistory as $order) {
			$ids[] = $order->id;
			$clientIds[] = $order->client_id;
			$idsCompany[] = $order->client_company_id;
			$order->companyInfo = MagazineCompany::model()->findAll($order->client_company_id);
		}

		$companyInfo = MagazineCompany::model()->findByPk($id);
		$clinetInfo = MagazineUsers::model()->findByPk($companyInfo->client_id);

		$documents = MagazineDocuments::model()->findAll(['condition' => 'order_id in ("'.implode('","', $ids).'")']);

		$PromoRelations = MagazinePromoRelations::model()->findAll(['condition' => 'id_company in ("'.implode('","', $idsCompany).'")']);
		$idsPromo = [];
		foreach($PromoRelations as $promoRel) {
			$idsPromo[] = $promoRel->id_files;
		}

		$PromoFiles = MagazinePromoFiles::model()->findAll(['condition' => 'id in ("'.implode('","', $idsPromo).'")']);

		$magazineUsers = MagazineClientsAddress::model()->findAll(['condition' => 'user_id in ("'.implode('","', $clientIds).'")']);

		$criteria=new CDbCriteria;
		$criteria->select='sum(price) as price';  // подходит только то имя поля, которое уже есть в модели
		$criteria->condition= 'client_id in ("'.implode('","', $clientIds).'")';
		$sBalance = MagazineOrder::model()->find($criteria)->getAttribute('price');
		//[{"img":"..\/img\/demo_akz.png","title":"title akz","descr":"description akz","content":"content_akz","id":"id"}]
		$contentStock = [];
		foreach($allStock as $tv) {
			$contentStock[] = [
				'id'=>$tv->id,
				'img'=>$tv->image,
				'title'=>$tv->header_text,
				'descr'=>$tv->text,
			];
		}
		$contentTovar = [];
		foreach($allTovar as $tv) {
			$contentTovar[] = [
				'id'=>$tv->id,
				'title' => $tv->title_en,
				'img' =>$tv->image_product,
				'price' => $tv->price
			];
		}
		$contentTovar = json_encode($contentTovar);
		$contentStock = json_encode($contentStock);
		$this->render('admin',array(
			'mangerInfo'=>$mangerInfo,
			'id'=>$id,
			'sBalance'=>$sBalance,
			'orderNews'=>$orderNews,
			'orderWork'=>$orderWork,
			'orderHistory'=>$orderHistory,
			'companyInfo'=>$companyInfo,
			'clinetInfo'=>$clinetInfo,
			'documents'=>$documents,
			'PromoFiles'=>$PromoFiles,
			'magazineUsers' => $magazineUsers,
			'contentTovar' => $contentTovar,
			'allStock' => $allStock,
			'contentStock' => $contentStock,
			'CompanyRecommendTovar' => $CompanyRecommendTovar,
			'CompanyRecommendStock' => $CompanyRecommendStock,
		));
	}

	public function actionIndex()
	{
		$connection=Yii::app()->db;
		$userInfo = MagazineUsers::model()->findByPk(Yii::app()->user->id);


		$managerid = 'and manager_id = '.Yii::app()->user->id;
		$sql ='
		SELECT
 	 magazine_order.client_company_id,
  magazine_company.client_id,
  magazine_company.name_company,
  SUM(if(magazine_order.status = 1 or magazine_order.status = 2,1,0)) AS count_in_work,
  SUM(if(magazine_order.status = 1 or magazine_order.status = 2,magazine_order.price,0)) as sum_in_work,
  SUM(if(magazine_order.status = 3,magazine_order.price,0))                AS sum
FROM magazine_order
  LEFT JOIN magazine_company ON magazine_company.id = magazine_order.client_company_id
  WHERE magazine_order.status <> 3 and magazine_order.site_connect_id = "'.$this->connectID.'" '.$managerid.'
GROUP BY magazine_order.client_company_id ORDER BY magazine_company.name_company';
		$command=$connection->createCommand($sql);
		$allCompanies = $command->queryAll();
		foreach($allCompanies as &$comp) {
//			var_dump($comp);
//			exit;
			$getUserInfo = MagazineUsers::model()->find(['condition' => 'id = "'.$comp['client_id'].'"']);

			$comp['user_name'] = $getUserInfo->user_name;
			$comp['user_surname'] = $getUserInfo->user_surname;
			$comp['user_phone'] = $getUserInfo->user_phone;
			$comp['user_email'] = $getUserInfo->user_email;
		}

		$this->render('index',array(
			'allCompanies' => $allCompanies,
			'userInfo' => $userInfo,
			'manager_info' => Yii::app()->user->getModel()
		));
	}

	public function actionShowOrder($id)
	{

		if(isset($_FILES['MagazineDocuments']['name'])) {
			$model = new MagazineDocuments();
			$model->file = CUploadedFile::getInstance($model,'url');
			$model->size_file = $model->file->getSize();
			$model->order_id = $id;
			$model->date_add = date('Y-m-d');
			$model->status = 0;
			$model->name = $model->file->getName();
			$model->extension = $model->file->getExtensionName();
			$path='/upload/'.uniqid().'.'.$model->file->getExtensionName();
			$model->file->saveAs(Yii::getPathOfAlias('webroot').$path);
			$model->url = $path;
			$model->save();
			$model = null;
		}

		$model = MagazineOrder::model()->findByPk([
			'id' => $id,
			'site_connect_id' => $this->connectID
		]);
		if(isset($_POST['status'])) {
			$model->status = $_POST['status'];
			$model->save();
			$this->redirect('/manager/MagazineOrder/ShowOrder/id/'.$id);
		}

		if(isset($_POST['data_manager_order'])) {
			$model->time_order = $_POST['data_manager_order'];
			$model->save();

			$getClientInfo = MagazineUsers::model()->findByPk($model->client_id);
			mail($getClientInfo->user_email, 'Изменение статуса заказа', 'Ваш заказ принят и отправлен в работу');
			$this->redirect('/manager/MagazineOrder/ShowOrder/id/'.$id);
		}

		$magazineInfo = MagazineClientsAddress::model()->findByPk($model->magazine_shop_id);

		$orderInfo = MagazineInfoOrder::model()->findAll(['condition' => 'order_id = "'.$id.'"']);
		foreach($orderInfo as $orderInf) {
			$imgProduct = MagazineProduct::model()->findByPk($orderInf->goods_id);
			$orderInf->image_product = $imgProduct->image_product;
			$orderInf->text_product = $imgProduct->text_product;
			$orderInf->price = $imgProduct->price;
		}

		$documents = MagazineDocuments::model()->findAll(['condition' => 'order_id = "'.$id.'"']);

		$this->render('order',array(
			'model' => $model,
			'magazineInfo' => $magazineInfo,
			'orderInfo' => $orderInfo,
			'documents' => $documents,
			'manager_info' => Yii::app()->user->getModel()
		));
	}

	/**
	 * Manages all models.
	 */


	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return MagazineOrder the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=MagazineOrder::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param MagazineOrder $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='magazine-order-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
