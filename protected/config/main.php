<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>$_SERVER['SERVER_NAME'],
	'timeZone' => 'Europe/Moscow',
	// preloading 'log' component
	'preload'=>array('log'),

	'aliases' => array(
		'booster' => 'application.components.YiiBooster',
	),

	// autoloading model and component classes
	'import'=>array(
		'application.models.*',
		'application.components.*',
		'application.components.QueryPhp.phpquery.phpQuery.phpQuery',
		'application.components.PhpExcel.PHPExcel.Classes.PHPExcel',
		'ext.imperavi-redactor-widget.imperaviredactorwidget',
		'ext.nested-set-behavior-master.NestedSetBehavior',
		'application.components.booster.*',
		'application.components.YiiBooster.*',
		'booster.helpers.TbHtml',
		'booster.components.Booster',
	),

	'modules'=>array(
		// uncomment the following to enable the Gii tool

		'gii'=>array(
			'class'=>'system.gii.GiiModule',
			'password'=>'Enter Your Password Here',
			// If removed, Gii defaults to localhost only. Edit carefully to taste.
			'ipFilters'=>array('178.215.110.4','::1'),
		),
		'publisher',
		'admin'  => [
			'components' => [
				'booster' => [
					'class' => 'booster.components.Booster'
				]
			]
		],
		'client',
		'manager' => [
			'components' => [
				'booster' => [
					'class' => 'booster.components.Booster'
				]
			]
		]
	),

	// application components
	'components'=>array(

		'user'=>array(
			'class' => 'WebUser',
			// enable cookie-based authentication
			'allowAutoLogin'=>true,
			'loginUrl'=>'/',
		),

		// uncomment the following to enable URLs in path-format

		'urlManager'=>array(
			'showScriptName' => false,
			'caseSensitive' => true,
			'urlFormat'=>'path',
			'rules'=>array(
				'catalog/cart' => 'catalog/cart',
				'catalog/ajax' => 'catalog/ajax',
				'catalog/Unajax' => 'catalog/Unajax',
				'catalog/*' => 'catalog/View',
				'catalog' => 'catalog/index',
				'<controller:\w+>/<id:\d+>'=>'<controller>/view',
				'<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
				'<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
				'<module:\w+>/<controller:\w+>/<action:\w+>' => '<module>/<controller>/<action>',
			),
		),


		// database settings are configured in database.php
		'db'=>require(dirname(__FILE__).'/database.php'),

		'errorHandler'=>array(
			// use 'site/error' action to display errors
			'errorAction'=>YII_DEBUG ? null : 'site/error',
		),

		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
				),
				// uncomment the following to show log messages on web pages
				/*
				array(
					'class'=>'CWebLogRoute',
				),
				*/
			),
		),

	),

	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
	'params'=>array(
		// this is used in contact page
		'adminEmail'=>'webmaster@example.com',
	),
);
