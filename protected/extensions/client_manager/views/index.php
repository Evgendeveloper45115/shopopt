<?php if ($info != null) {
    $user_id = Yii::app()->user->id;
    ?>
    <div id="cleint_manager_info">
        <div class="img_fon inline_style" style="background-image:url(<?= $info->user_avatar ?>)"></div>
        <div class="inline_style fio_date" style="width: 10vw">
            <p>Ваш менеджер</p>

            <p><?= $info->user_name ?></p>

            <p><?= $info->user_surname ?></p>
        </div>
        <div class="inline_style manager_dop_info">
            <p><?= $info->user_phone ?></p>

            <p>e-mail: <?= $info->user_email ?></p>
            <a href="/client/magazineReview/create" class="color_th_hover css_trans border_hover"><span>Оставить отзыв о менеджере</span>
                <span class="color_th">→</span></a>
            <?php
            if($info->site_connect_id == 36){
                ?>
                <div style="margin-top: 0.3vw; text-align: center">
                    <a href="#" id="manager_mail" class="color_th_hover css_trans border_hover" style="padding: 0.5vw;border-radius: 5px"><span>Связаться с менеджером</span></a>
                </div>
                <?php
            }
            ?>
        </div>
        <div class="inline_style manager_dop_info">
         <a href="/client/discount/admin" style="margin-top: 0"><p>На бонусном счете: <?= false === ($res = MagazineBonus::getUserBonus()) ? 0 : $res ?> р.</p></a>
            <?php
            if ($info->min_order != null) {
                ?>
                <p>Минимальная сумма заказа: <?= $info->min_order ?> р.</p>
                <?php
            }
            ?>
        </div>
    </div>
<?php } ?>