<?php

class ClientManagerWidget extends CWidget
{
    /**
     * @var string имя пользователя
     */
    public $username = '';

    /**
 * Запуск виджета
 */
    public function run()
    {
            $manager_id = MagazineUsers::model()->find(['condition' => 'id = "'.Yii::app()->user->id.'" and manager_id is not null']);

        $managerInfo = null;
        if($manager_id) {
            $managerInfo = MagazineUsers::model()->find(['condition' => 'id = ' . $manager_id->manager_id]);
        }

        $this->render('index', array(
            'info' => $managerInfo,
        ));
    }
}