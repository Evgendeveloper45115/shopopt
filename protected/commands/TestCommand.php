<?php
class TestCommand extends CConsoleCommand {
    public function run($args) {
        $orders = MagazineOrder::model()->findAll(['condition' =>'manager_id = 0 and DATE_ADD(NOW(), INTERVAL 3 HOUR) > time_order ']);
        foreach($orders as $order) {
            $emailAdmin = SiteConnect::model()->find(['condition' => 'id = ' . $order->site_connect_id]);

            mail($emailAdmin->email_admin, 'Заказ не обработан менеджерами', 'Заказ № ' .$order->id. ' никто не обработал');
        }
    }
}